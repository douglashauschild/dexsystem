package br.com.dh.dexSystem.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;

import com.itextpdf.text.Element;

import br.com.dh.dexSystem.controller.PageWrapper;
import br.com.dh.dexSystem.manager.CargoManager;
import br.com.dh.dexSystem.model.Cargo;
import br.com.dh.dexSystem.model.ServiceInterface;
import br.com.dh.dexSystem.model.vo.RelatorioColunaVO;
import br.com.dh.dexSystem.model.vo.RelatorioConteudoVO;
import br.com.dh.dexSystem.model.vo.RelatorioRegistroVO;
import br.com.dh.dexSystem.model.vo.RelatorioVO;
import br.com.dh.dexSystem.repository.CargoRepository;
import br.com.dh.dexSystem.repository.filter.CargoFilter;
import br.com.dh.dexSystem.util.FormatacaoUtils;
import br.com.dh.dexSystem.validation.ValidacaoCargo;

@Service
public class CargoService extends SincronizacaoServiceAbstract implements ServiceInterface<Cargo, CargoFilter> {

	@Autowired
	private CargoManager cargoManager;
	@Autowired
	private CargoRepository cargoRepository;
	@Autowired
	private ValidacaoCargo validacaoCargo;

	
	public PageWrapper<Cargo> pesquisar(CargoFilter filtro, Pageable pageable, HttpServletRequest httpServletRequest) {
		PageWrapper<Cargo> pageWrapper = null;
		try {
			pageWrapper = new PageWrapper<Cargo>(cargoManager.filtrar(filtro, pageable), httpServletRequest);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pageWrapper;
	}
	
	public String validarGravacao(Cargo cargo, Model model, Errors errors) throws Exception {
		String retorno = null;
		cargo = ajustarGravacao(cargo);
		String msgValidacao = validacaoCargo.validar(cargo);
		if (msgValidacao != null && !msgValidacao.isEmpty()) {
			retorno = verificaRetorno(msgValidacao, errors);
		} else {
			if (!cargo.isFinalizado() && cargo.getDataFim() != null) {
				retorno = verificaRetorno(validacaoCargo.validarExclusao(cargo, false), errors);
			} else if (cargo.isFinalizado() && cargo.getDataFim() == null) {
				retorno = verificaRetorno(validacaoCargo.validarAtivacao(cargo), errors);
			}
		}
		return retorno;
	}
	
	private String verificaRetorno(String msgValidacao, Errors errors) {
		String retorno = null;
		if (msgValidacao != null && !msgValidacao.isEmpty()) {
			errors.rejectValue(null, null, msgValidacao);
			retorno = "cadastros/cargo/cargo-cadastro";
		}
		return retorno;
	}
	
	public Cargo ajustarGravacao(Cargo cargo) throws Exception {
		cargo.setDataInicio(FormatacaoUtils.getData(cargo.getDataInicioString()));
		cargo.setDataFim(cargo.getDataFimString() != null && !cargo.getDataFimString().isEmpty() ? FormatacaoUtils.getData(cargo.getDataFimString()) : null);
		return cargo;
	}
	
	public void salvar(Cargo cargo) throws Exception {
		cargoRepository.saveAndFlush((Cargo) gerarChecksum(cargo.getId() == null, cargo));
	}
	
	public String validarExclusao(Cargo cargo) {
		return validacaoCargo.validarExclusao(cargo, true);
	}
	
	public void excluir(Cargo cargo) throws Exception {
		cargo.setExcluido('1');
		salvar(cargo);
	}
	
	public Cargo buscarPorId(Long id) {
		Cargo cargo = cargoManager.buscarPorId(id);
		cargo.setDataInicioString(cargo.getDataInicio() != null ? FormatacaoUtils.getDataString(cargo.getDataInicio()) : null);
		cargo.setDataFimString(cargo.getDataFim() != null ? FormatacaoUtils.getDataString(cargo.getDataFim()) : null);
		cargo.setIdEmpresa(cargo.getSetor().getIdEmpresa());
		cargo.setFinalizado(cargo.getDataFim() != null);
		return cargo;
	}
	
	public List<Cargo> buscarTodos(Long idSetor, Long idCargoSelecionado, boolean inclusiveInativos) {
		List<Cargo> cargos = cargoManager.buscarTodosPorSetor(idSetor, inclusiveInativos);
		if (idCargoSelecionado != null) {
			Cargo cargoSelecionado = buscarPorId(idCargoSelecionado);
			if (!cargos.contains(cargoSelecionado)) {
				cargos.add(cargoSelecionado);
			}
		}
		return cargos;
	}
	
	public RelatorioVO getRelatorioPdfVO(CargoFilter filtro, HttpServletRequest httpServletRequest) {
		PageWrapper<Cargo> paginaWrapper = pesquisar(filtro, null, httpServletRequest);
		List<Cargo> cargos = paginaWrapper.getConteudo();
		
		RelatorioVO relatorioVO = new RelatorioVO();

		//titulo do documento
		relatorioVO.setTitulo("CARGOS");
		
		//defini tamanho das colunas
		float[] tamanhoColunas = {3, 3, 1, 1, (float) 0.8, (float) 0.5};
		relatorioVO.setTamanhoColunas(tamanhoColunas);
		
		//obtem nome usuario
		relatorioVO.setUsuario(getUsuario().getNome());
		
		//monta colunas
		relatorioVO.setColunas(new ArrayList<RelatorioColunaVO>());
		relatorioVO.getColunas().add(new RelatorioColunaVO("SETOR", Element.ALIGN_LEFT));
		relatorioVO.getColunas().add(new RelatorioColunaVO("CARGO", Element.ALIGN_LEFT));
		relatorioVO.getColunas().add(new RelatorioColunaVO("DATA INÍCIO", Element.ALIGN_CENTER));
		relatorioVO.getColunas().add(new RelatorioColunaVO("DATA FIM", Element.ALIGN_CENTER));
		relatorioVO.getColunas().add(new RelatorioColunaVO("STATUS", Element.ALIGN_CENTER));
		relatorioVO.getColunas().add(new RelatorioColunaVO("ID", Element.ALIGN_CENTER));
			
		//monta dados
		relatorioVO.setConteudos(new ArrayList<>());
		for (Cargo cargo : cargos) {
			RelatorioConteudoVO conteudoVO = new RelatorioConteudoVO();
			conteudoVO.setConteudo(new ArrayList<RelatorioRegistroVO>());
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(cargo.getSetor().getNome(), Element.ALIGN_LEFT));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(cargo.getNome(), Element.ALIGN_LEFT));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(cargo.getDataInicioFormatada(), Element.ALIGN_CENTER));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(cargo.getDataFimFormatada(), Element.ALIGN_CENTER));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(cargo.getStatusDescricao(), Element.ALIGN_CENTER));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(cargo.getId().toString(), Element.ALIGN_CENTER));
			
			relatorioVO.getConteudos().add(conteudoVO);
		}
		return relatorioVO;
	}
}
