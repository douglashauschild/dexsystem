package br.com.dh.dexSystem.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.dh.dexSystem.manager.EmpregadoManager;
import br.com.dh.dexSystem.manager.EmpresaManager;
import br.com.dh.dexSystem.manager.EpiManager;
import br.com.dh.dexSystem.manager.MotivoManager;
import br.com.dh.dexSystem.manager.PessoaFisicaManager;
import br.com.dh.dexSystem.model.Cargo;
import br.com.dh.dexSystem.model.Cidade;
import br.com.dh.dexSystem.model.Empregado;
import br.com.dh.dexSystem.model.Empresa;
import br.com.dh.dexSystem.model.Epi;
import br.com.dh.dexSystem.model.Estado;
import br.com.dh.dexSystem.model.Grade;
import br.com.dh.dexSystem.model.GradeItem;
import br.com.dh.dexSystem.model.Motivo;
import br.com.dh.dexSystem.model.NivelAcesso;
import br.com.dh.dexSystem.model.PessoaFisica;
import br.com.dh.dexSystem.model.Setor;
import br.com.dh.dexSystem.model.UnidadeMedida;
import br.com.dh.dexSystem.model.constant.SexoEnum;
import br.com.dh.dexSystem.model.constant.SimNaoEnum;
import br.com.dh.dexSystem.model.constant.StatusCaEnum;
import br.com.dh.dexSystem.model.constant.StatusEmpresaEnum;
import br.com.dh.dexSystem.model.constant.StatusSetorCargoEnum;
import br.com.dh.dexSystem.model.constant.TipoConfirmacaoEnum;
import br.com.dh.dexSystem.model.constant.TipoContatoEnum;
import br.com.dh.dexSystem.model.constant.TipoEmpresaEnum;
import br.com.dh.dexSystem.model.constant.TipoSelecaoEmpregadoEnum;
import br.com.dh.dexSystem.model.constant.TipoSelecaoEmpresaEnum;
import br.com.dh.dexSystem.model.constant.TipoSelecaoEpiEnum;
import br.com.dh.dexSystem.model.constant.TipoSelecaoPessoaFisicaEnum;
import br.com.dh.dexSystem.model.constant.VidaUtilEnum;
import br.com.dh.dexSystem.model.vo.SelectCharVO;
import br.com.dh.dexSystem.model.vo.SelectVO;
import br.com.dh.dexSystem.repository.CidadeRepository;
import br.com.dh.dexSystem.repository.EstadoRepository;
import br.com.dh.dexSystem.repository.GradeItemRepository;
import br.com.dh.dexSystem.repository.GradeRepository;
import br.com.dh.dexSystem.repository.NivelAcessoRepository;
import br.com.dh.dexSystem.repository.UnidadeMedidaRepository;

@Service
public class CombosService extends ServiceExtend {
	
	@Autowired
	private ParametroSistemaService parametroSistemaService;
	@Autowired
	private UnidadeMedidaRepository unidadeMedidaRepository;
	@Autowired
	private GradeRepository gradeRepository;
	@Autowired
	private EmpresaManager empresaManager;
	@Autowired
	private EmpresaService empresaService;
	@Autowired
	private SetorService setorService;
	@Autowired
	private CargoService cargoService;
	@Autowired
	private EpiManager epiManager;
	@Autowired
	private EmpregadoManager empregadoManager;
	@Autowired
	private PessoaFisicaManager pessoaFisicaManager;
	@Autowired
	private MotivoManager motivoManager;
	@Autowired
	private EstadoRepository estadoRepository;
	@Autowired
	private CidadeRepository cidadeRepository;
	@Autowired
	private NivelAcessoRepository nivelAcessoRepository;
	@Autowired
	private GradeItemRepository gradeItemRepository;

	
	public VidaUtilEnum[] getVidaUtilUnidadeCombo() {
		return VidaUtilEnum.values();
	}
	
	public List<SelectVO> getNiveisAcessoCombo() {
		List<SelectVO> niveisAcessoVO = new ArrayList<SelectVO>();
		niveisAcessoVO.add(new SelectVO(0L, "-- Selecione --"));
		List<NivelAcesso> niveisAcesso = nivelAcessoRepository.findAll();
		for (NivelAcesso nivelAcesso : niveisAcesso) {
			niveisAcessoVO.add(new SelectVO(nivelAcesso.getId(), nivelAcesso.getNivel() + " - " + nivelAcesso.getNome()));
		}
		return niveisAcessoVO;
	}
	
	public List<SelectCharVO> getSexoCombo(boolean filtro) {
		List<SelectCharVO> sexoVO = new ArrayList<SelectCharVO>();
		sexoVO.add(filtro ? new SelectCharVO(null, "-- Todos --") : new SelectCharVO('0', "-- Selecione --"));
		for (SexoEnum sexo : SexoEnum.values()) {
			sexoVO.add(new SelectCharVO(sexo.getKey(), sexo.getLabel()));
		}
		return sexoVO;
	}
	
	public List<SelectCharVO> getTipoEmpresaCombo(boolean filtro) {
		List<SelectCharVO> tipoEmpresaVO = new ArrayList<SelectCharVO>();
		if (filtro) {
			tipoEmpresaVO.add(new SelectCharVO(null, "-- Todas --"));
		}
		for (TipoEmpresaEnum tipo : TipoEmpresaEnum.values()) {
			tipoEmpresaVO.add(new SelectCharVO(tipo.getKey(), tipo.getDescricao()));
		}
		return tipoEmpresaVO;
	}
	
	public List<SelectVO> getStatusCombo() {
		List<SelectVO> statusVO = new ArrayList<SelectVO>();
		statusVO.add(new SelectVO(null, "-- Todas --"));
		for (StatusEmpresaEnum status : StatusEmpresaEnum.values()) {
			statusVO.add(new SelectVO(status.getKey(), status.getDescricao()));
		}
		return statusVO;
	}
	
	public List<SelectVO> getStatusSetorCargoCombo() {
		List<SelectVO> statusVO = new ArrayList<SelectVO>();
		statusVO.add(new SelectVO(null, "-- Todos --"));
		for (StatusSetorCargoEnum status : StatusSetorCargoEnum.values()) {
			statusVO.add(new SelectVO(status.getKey(), status.getDescricao()));
		}
		return statusVO;
	}
	
	public List<SelectVO> getStatusCaCombo() {
		List<SelectVO> statusCaVO = new ArrayList<SelectVO>();
		statusCaVO.add(new SelectVO(null, "-- Todos --"));
		for (StatusCaEnum status : StatusCaEnum.values()) {
			statusCaVO.add(new SelectVO(status.getKey(), status.getDescricao()));
		}
		return statusCaVO;
	}
	
	public List<SelectVO> getGradesCombo() {
		List<SelectVO> gradesVO = new ArrayList<SelectVO>();
		gradesVO.add(new SelectVO(0L, "-- Selecione --"));
		List<Grade> grades = gradeRepository.findAll();
		for (Grade grade : grades) {
			gradesVO.add(new SelectVO(grade.getId(), grade.getNome()));
		}
		return gradesVO;
	}
	
	
	public List<SelectVO> getUnidadesMedidaCombo() {
		List<SelectVO> unidadesVO = new ArrayList<SelectVO>();
		unidadesVO.add(new SelectVO(0L, "-- Selecione --"));
		List<UnidadeMedida> unidades = unidadeMedidaRepository.findAll();
		for (UnidadeMedida unidade : unidades) {
			unidadesVO.add(new SelectVO(unidade.getId(), unidade.getDescricao()));
		}
		return unidadesVO;
	}
	
	public List<SelectVO> getEstadosCombo() {
		List<SelectVO> estadosVO = new ArrayList<SelectVO>();
		estadosVO.add(new SelectVO(0L, "-- Selecione --"));
		List<Estado> estados = estadoRepository.findAll();
		for (Estado estado : estados) {
			estadosVO.add(new SelectVO(estado.getId(), estado.getNome()));
		}
		return estadosVO;
	}
	
	public List<SelectVO> getCidadesCombo(Long idEstado) {
		List<SelectVO> cidadesVO = new ArrayList<SelectVO>();
		List<Cidade> cidades = cidadeRepository.findByIdEstado(idEstado);
		for (Cidade cidade : cidades) {
			cidadesVO.add(new SelectVO(cidade.getId(), cidade.getNome()));
		}
		return cidadesVO;
	}
	
	public List<SelectVO> getTiposContatoCombo() {
		List<SelectVO> tiposVO = new ArrayList<SelectVO>();
		for (TipoContatoEnum tipo : TipoContatoEnum.values()) {
			tiposVO.add(new SelectVO(tipo.getKey(), tipo.getDescricao()));
		}
		return tiposVO;
	}
	
	public List<SelectCharVO> getTipoSelecaoEpiCombo() {
		List<SelectCharVO> tipoSelecaoEpiVO = new ArrayList<SelectCharVO>();
		for (TipoSelecaoEpiEnum tipo : TipoSelecaoEpiEnum.values()) {
			tipoSelecaoEpiVO.add(new SelectCharVO(tipo.getKey(), tipo.getDescricao()));
		}
		return tipoSelecaoEpiVO;
	}
	
	public List<SelectCharVO> getTipoSelecaoEmpresaCombo() {
		List<SelectCharVO> tipoSelecaoEmpresaVO = new ArrayList<SelectCharVO>();
		for (TipoSelecaoEmpresaEnum tipo : TipoSelecaoEmpresaEnum.values()) {
			tipoSelecaoEmpresaVO.add(new SelectCharVO(tipo.getKey(), tipo.getDescricao()));
		}
		return tipoSelecaoEmpresaVO;
	}
	
	public List<SelectCharVO> getTipoSelecaoPessoaFisicaCombo() {
		List<SelectCharVO> tipoSelecaoPessoaFisicaVO = new ArrayList<SelectCharVO>();
		for (TipoSelecaoPessoaFisicaEnum tipo : TipoSelecaoPessoaFisicaEnum.values()) {
			tipoSelecaoPessoaFisicaVO.add(new SelectCharVO(tipo.getKey(), tipo.getDescricao()));
		}
		return tipoSelecaoPessoaFisicaVO;
	}
	
	public List<SelectCharVO> getTipoSelecaoEmpregadoCombo() {
		List<SelectCharVO> tipoSelecaoEmpregadoVO = new ArrayList<SelectCharVO>();
		for (TipoSelecaoEmpregadoEnum tipo : TipoSelecaoEmpregadoEnum.values()) {
			tipoSelecaoEmpregadoVO.add(new SelectCharVO(tipo.getKey(), tipo.getDescricao()));
		}
		return tipoSelecaoEmpregadoVO;
	}
	
	public List<SelectVO> getEmpresasCombo(boolean filtro, Long idEmpresaSelecionada) {
		List<SelectVO> empresasVO = new ArrayList<SelectVO>();
		List<Empresa> empresas = empresaService.buscarTodos(idEmpresaSelecionada, filtro);
		for (Empresa empresa : empresas) {
			empresasVO.add(new SelectVO(empresa.getIdPessoa(), parametroSistemaService.getSelecaoEmpresa(empresa)));
		}
		return empresasVO;
	}
	
	public List<SelectVO> getEmpresasMatrizesCombo(Long idPessoa) {
		List<SelectVO> matrizesVO = new ArrayList<SelectVO>();
		List<Empresa> matrizes = empresaManager.buscarMatrizes(idPessoa);
		for (Empresa matriz : matrizes) {
			matrizesVO.add(new SelectVO(matriz.getIdPessoa(), parametroSistemaService.getSelecaoEmpresa(matriz)));
		}
		return matrizesVO;
	}
	
	public List<SelectVO> getPessoasFisicasCombo(boolean filtro) {
		List<SelectVO> pessoasFisicasVO = new ArrayList<SelectVO>();
		List<PessoaFisica> pessoasFisicas = pessoaFisicaManager.buscarTodas();
		for (PessoaFisica pessoaFisica : pessoasFisicas) {
			pessoasFisicasVO.add(new SelectVO(pessoaFisica.getIdPessoa(), parametroSistemaService.getSelecaoPessoaFisica(pessoaFisica)));
		}
		return pessoasFisicasVO;
	}
	
	public List<SelectVO> getEmpregadosCombo(Long idEmpresa) {
		List<SelectVO> empregadosVO = new ArrayList<SelectVO>();
		List<Empregado> empregados = empregadoManager.buscarAtivosPorEmpresa(idEmpresa);
		for (Empregado empregado : empregados) {
			empregadosVO.add(new SelectVO(empregado.getId(), parametroSistemaService.getSelecaoEmpregado(empregado)));
		}
		return empregadosVO;
	}
	
	public List<SelectVO> getEpisCombo(boolean filtro) {
		List<SelectVO> episVO = new ArrayList<SelectVO>();
		episVO.add(filtro ? (new SelectVO(null, "-- Todos --")) : (new SelectVO(0L, "-- Selecione --")));
		List<Epi> epis = epiManager.buscarTodos();
		for (Epi epi : epis) {
			episVO.add(new SelectVO(epi.getId(), parametroSistemaService.getSelecaoEpi(epi)));
		}
		return episVO;
	}
	
	public List<SelectVO> getSetoresCombo(boolean filtro, Long idEmpresa, Long idSetor) {
		List<SelectVO> setoresVO = new ArrayList<SelectVO>();
		List<Setor> setores = setorService.buscarTodos(idEmpresa, idSetor, filtro);
		for (Setor setor : setores) {
			setoresVO.add(new SelectVO(setor.getId(), setor.getNome()));
		}
		return setoresVO;
	}
	
	
	public List<SelectVO> getCargosCombo(boolean filtro, Long idSetor, Long idCargo) {
		List<SelectVO> cargosVO = new ArrayList<SelectVO>();
		List<Cargo> cargos = cargoService.buscarTodos(idSetor, idCargo, filtro);
		for (Cargo cargo : cargos) {
			cargosVO.add(new SelectVO(cargo.getId(), cargo.getNome()));
		}
		return cargosVO;
	}
	
	public List<SelectVO> getMotivosCombo() {
		List<SelectVO> motivosVO = new ArrayList<SelectVO>();
		motivosVO.add(new SelectVO(0L, "-- Selecione --"));
		List<Motivo> motivos = motivoManager.buscarTodosAtivos();
		for (Motivo motivo : motivos) {
			motivosVO.add(new SelectVO(motivo.getId(), motivo.getDescricao()));
		}
		return motivosVO;
	}
	
	public List<SelectVO> getGradeItensCombo(Long idGrade) {
		List<SelectVO> motivosVO = new ArrayList<SelectVO>();
		motivosVO.add(new SelectVO(0L, "-- Selecione --"));
		List<GradeItem> gradeItens = gradeItemRepository.buscarPorIdGrade(idGrade);
		for (GradeItem gradeItem : gradeItens) {
			motivosVO.add(new SelectVO(gradeItem.getId(), gradeItem.getTamanho()));
		}
		return motivosVO;
	}

	public List<SelectCharVO> getTipoConfirmacaoCombo() {
		List<SelectCharVO> tiposConfirmacaoVO = new ArrayList<SelectCharVO>();
		tiposConfirmacaoVO.add(new SelectCharVO(null, "-- Todos --"));
		for (TipoConfirmacaoEnum tipoConfirmacao : TipoConfirmacaoEnum.values()) {
			tiposConfirmacaoVO.add(new SelectCharVO(tipoConfirmacao.getKey(), tipoConfirmacao.getDescricao()));
		}
		return tiposConfirmacaoVO;
	}
	
	public List<SelectCharVO> getSimNaoCombo() {
		List<SelectCharVO> informarMotivoVO = new ArrayList<SelectCharVO>();
		for (SimNaoEnum simNao : SimNaoEnum.values()) {
			informarMotivoVO.add(new SelectCharVO(simNao.getKey(), simNao.getDescricao()));
		}
		return informarMotivoVO;
	}
}
