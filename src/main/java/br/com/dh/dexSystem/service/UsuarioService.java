package br.com.dh.dexSystem.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.itextpdf.text.Element;

import br.com.dh.dexSystem.controller.PageWrapper;
import br.com.dh.dexSystem.manager.UsuarioManager;
import br.com.dh.dexSystem.model.ServiceInterface;
import br.com.dh.dexSystem.model.Usuario;
import br.com.dh.dexSystem.model.vo.RelatorioColunaVO;
import br.com.dh.dexSystem.model.vo.RelatorioConteudoVO;
import br.com.dh.dexSystem.model.vo.RelatorioRegistroVO;
import br.com.dh.dexSystem.model.vo.RelatorioVO;
import br.com.dh.dexSystem.repository.UsuarioRepository;
import br.com.dh.dexSystem.repository.filter.UsuarioFilter;

@Service
public class UsuarioService extends SincronizacaoServiceAbstract implements ServiceInterface<Usuario, UsuarioFilter> {

	@Autowired
	private UsuarioManager usuarioManager;
	
	@Autowired
	private UsuarioRepository usuarioRepository;

	
	public PageWrapper<Usuario> pesquisar(UsuarioFilter filtro, Pageable pageable, HttpServletRequest httpServletRequest) {
		return new PageWrapper<>(usuarioManager.filtrar(filtro, pageable), httpServletRequest);
	}
	
	public boolean emailCadastrado(Usuario usuario) {
		Usuario usuarioAux = null;
		if (usuario.getId() != null) {
			usuarioAux = usuarioRepository.findByEmailAndIdNotInAndExcluidoIn(usuario.getEmail(), usuario.getId(), '0');
		} else {
			usuarioAux = usuarioRepository.findByEmailAndExcluidoIn(usuario.getEmail(), '0');
		}
		return usuarioAux != null;
	}
	
	public boolean editandoSenha(Usuario usuario) {
		String senhaSalva = buscarPorId(usuario.getId()).getSenha();
		return !usuario.getSenha().equals(senhaSalva);
	}
	
	public boolean senhasIguais(Usuario usuario) {
		return usuario.getSenha().equals(usuario.getConfirmaSenha());
	}
	
	public String getSenhaCrypt(String senha) {
		return new BCryptPasswordEncoder().encode(senha);
	}
	
	public void salvar(Usuario usuario) throws Exception {
		usuarioRepository.saveAndFlush((Usuario) gerarChecksum(usuario.getId() == null, usuario));
	}
	
	public void excluir(Usuario usuario) throws Exception {
		usuario.setExcluido('1');
		salvar(usuario);
	}
	
	public Usuario buscarPorId(Long id) {
		Usuario usuario = usuarioRepository.getOne(id);
		if (usuario != null) {
			usuario.setConfirmaSenha(usuario.getSenha());
		}
		return usuario;
	}
	
	public List<Usuario> buscarPorNivelAcesso(Long idNivelAcesso) {
		return usuarioRepository.findByIdNivelAcessoAndExcluidoIn(idNivelAcesso, '0');
	}

	
	public RelatorioVO getRelatorioPdfVO(UsuarioFilter filtro, HttpServletRequest httpServletRequest) {
		PageWrapper<Usuario> paginaWrapper = pesquisar(filtro, null, httpServletRequest);
		List<Usuario> usuarios = paginaWrapper.getConteudo();
		
		RelatorioVO relatorioVO = new RelatorioVO();

		//titulo do documento
		relatorioVO.setTitulo("USUÁRIOS");
		
		//defini tamanho das colunas
		float[] tamanhoColunas = {3, 4, 2, 1};
		relatorioVO.setTamanhoColunas(tamanhoColunas);
		
		//obtem nome usuario
		relatorioVO.setUsuario(getUsuario().getNome());
		
		//monta colunas
		relatorioVO.setColunas(new ArrayList<RelatorioColunaVO>());
		relatorioVO.getColunas().add(new RelatorioColunaVO("NOME", Element.ALIGN_LEFT));
		relatorioVO.getColunas().add(new RelatorioColunaVO("EMAIL", Element.ALIGN_LEFT));
		relatorioVO.getColunas().add(new RelatorioColunaVO("NÍVEL DE ACESSO", Element.ALIGN_LEFT));
		relatorioVO.getColunas().add(new RelatorioColunaVO("ID", Element.ALIGN_CENTER));
			
		//monta dados
		relatorioVO.setConteudos(new ArrayList<>());
		for (Usuario usuario : usuarios) {
			RelatorioConteudoVO conteudoVO = new RelatorioConteudoVO();
			conteudoVO.setConteudo(new ArrayList<RelatorioRegistroVO>());
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(usuario.getNome(), Element.ALIGN_LEFT));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(usuario.getEmail(), Element.ALIGN_LEFT));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(usuario.getNivelAcesso().getNomeCompleto(), Element.ALIGN_LEFT));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(usuario.getId().toString(), Element.ALIGN_CENTER));
			
			relatorioVO.getConteudos().add(conteudoVO);
		}
		return relatorioVO;
	}
}
