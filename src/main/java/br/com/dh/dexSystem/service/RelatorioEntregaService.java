package br.com.dh.dexSystem.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.dh.dexSystem.controller.PageWrapper;
import br.com.dh.dexSystem.manager.EmpregadoEpiManager;
import br.com.dh.dexSystem.model.EmpregadoEpi;
import br.com.dh.dexSystem.model.vo.RelatorioEntregaVO;
import br.com.dh.dexSystem.repository.filter.EmpregadoEpiFilter;

@Service
public class RelatorioEntregaService {

	@Autowired
	private EmpregadoEpiManager empregadoEpiManager;
	
	
	public PageWrapper<EmpregadoEpi> pesquisar(EmpregadoEpiFilter filtro, Pageable pageable, HttpServletRequest httpServletRequest) {
		PageWrapper<EmpregadoEpi> pageWrapper = null;
		try {
			pageWrapper = new PageWrapper<EmpregadoEpi>(empregadoEpiManager.filtrar(filtro, pageable), httpServletRequest);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pageWrapper;
	}
	
	
	public RelatorioEntregaVO getRelatorioVO(EmpregadoEpiFilter filtro, HttpServletRequest httpServletRequest) throws Exception {
		PageWrapper<EmpregadoEpi> paginaWrapper = pesquisar(filtro, null, httpServletRequest);
		List<EmpregadoEpi> empregadosEpis = paginaWrapper.getConteudo();
		return new RelatorioEntregaVO(empregadosEpis); 
	}

}
