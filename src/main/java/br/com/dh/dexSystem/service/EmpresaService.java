package br.com.dh.dexSystem.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;

import com.itextpdf.text.Element;

import br.com.dh.dexSystem.controller.PageWrapper;
import br.com.dh.dexSystem.manager.EmpresaManager;
import br.com.dh.dexSystem.model.Empresa;
import br.com.dh.dexSystem.model.Pessoa;
import br.com.dh.dexSystem.model.PessoaJuridica;
import br.com.dh.dexSystem.model.ServiceInterface;
import br.com.dh.dexSystem.model.vo.RelatorioColunaVO;
import br.com.dh.dexSystem.model.vo.RelatorioConteudoVO;
import br.com.dh.dexSystem.model.vo.RelatorioRegistroVO;
import br.com.dh.dexSystem.model.vo.RelatorioVO;
import br.com.dh.dexSystem.repository.EmpresaRepository;
import br.com.dh.dexSystem.repository.PessoaJuridicaRepository;
import br.com.dh.dexSystem.repository.PessoaRepository;
import br.com.dh.dexSystem.repository.filter.EmpresaFilter;
import br.com.dh.dexSystem.util.FormatacaoUtils;
import br.com.dh.dexSystem.validation.ValidacaoEmpresa;

@Service
public class EmpresaService extends SincronizacaoServiceAbstract implements ServiceInterface<Empresa, EmpresaFilter> {

	@Autowired
	private PessoaRepository pessoaRepository;
	@Autowired
	private PessoaJuridicaRepository pessoaJuridicaRepository;
	@Autowired
	private EmpresaRepository empresaRepository;
	@Autowired
	private EmpresaManager empresaManager;
	@Autowired
	private EnderecoService enderecoService;
	@Autowired
	private ContatoService contatoService;
	@Autowired
	private ValidacaoEmpresa validacaoEmpresa;
	
	
	public PageWrapper<Empresa> pesquisar(EmpresaFilter filtro, Pageable pageable, HttpServletRequest httpServletRequest) {
		PageWrapper<Empresa> pageWrapper = null;
		try {
			pageWrapper = new PageWrapper<Empresa>(empresaManager.filtrar(filtro, pageable), httpServletRequest);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pageWrapper;
	}
	
	public String validarGravacao(Empresa empresa, Model model, Errors errors) throws Exception {
		String retorno = null;
		empresa = ajustarGravacao(empresa);
		if (empresa.getPessoaJuridica().getDataFim() != null && (empresa.getPessoaJuridica().getDataFim().equals(empresa.getPessoaJuridica().getDataInicio()) || empresa.getPessoaJuridica().getDataFim().before(empresa.getPessoaJuridica().getDataInicio()))) {
			errors.rejectValue(null, null, "A data de fim não pode ser anterior a data de início da empresa!");
			retorno = "cadastros/empresa/empresa-cadastro";
		} else {
			if (!empresa.isFinalizada() && empresa.getPessoaJuridica().getDataFim() != null) {
				String msgValidacao = validacaoEmpresa.validarExclusao(empresa, false);
				if (msgValidacao != null && !msgValidacao.isEmpty()) {
					errors.rejectValue(null, null, msgValidacao);
					retorno = "cadastros/empresa/empresa-cadastro";
				}
			}
			Empresa empresaAux = empresaManager.isValidarExistencia(empresa);
			if (empresaAux != null) {
				errors.rejectValue(null, null, "Já existe uma empresa ativa cadastrada com este CNPJ!");
				retorno = "cadastros/empresa/empresa-cadastro";
			}
			String msgRetorno = validacaoEmpresa.validar(empresa);
			if (msgRetorno != null && !msgRetorno.isEmpty()) {
				errors.rejectValue(null, null, msgRetorno);
				retorno = "cadastros/empresa/empresa-cadastro";
			}
		}
		if (retorno != null) {
			model.addAttribute("contatosToString", empresa.getContatosToString());
		}
		return retorno;
	}
	
	public Empresa ajustarGravacao(Empresa empresa) throws Exception {
		empresa.getPessoaJuridica().setDataInicio(FormatacaoUtils.getData(empresa.getDataInicioString()));
		empresa.getPessoaJuridica().setDataFim(empresa.getDataFimString() != null && !empresa.getDataFimString().isEmpty() ? FormatacaoUtils.getData(empresa.getDataFimString()) : null);
		empresa.getPessoaJuridica().setCnpj(FormatacaoUtils.removerFormatacao(empresa.getPessoaJuridica().getCnpj()));
		return empresa;
	}
	
	public void salvar(Empresa empresa) throws Exception {
		Long idPessoa = empresa.getPessoaJuridica().getPessoa().getId();
		Pessoa pessoa = pessoaRepository.save((Pessoa) gerarChecksum(idPessoa == null, empresa.getPessoaJuridica().getPessoa()));
		
		empresa.getPessoaJuridica().setIdPessoa(pessoa.getId());
		pessoaJuridicaRepository.save((PessoaJuridica) gerarChecksum(idPessoa == null, empresa.getPessoaJuridica()));
		
		enderecoService.salvar(empresa.getEndereco(), pessoa.getId());
		contatoService.salvar(empresa.getContatosToString(), pessoa.getId());
		
		empresa.setIdPessoa(pessoa.getId());
		empresaRepository.saveAndFlush((Empresa) gerarChecksum(idPessoa == null, empresa));
	}
	
	public String validarExclusao(Empresa empresa) {
		return validacaoEmpresa.validarExclusao(empresa, true);
	}
	
	public void excluir(Empresa empresa) throws Exception {
		empresa.getPessoaJuridica().setExcluido('1');
		salvar(empresa);
	}
	
	public Empresa buscarPorId(Long id) {
		Empresa empresa = empresaManager.buscarPorId(id);
		if (empresa != null) {
			empresa.setDataInicioString(empresa.getPessoaJuridica().getDataInicio() != null ? FormatacaoUtils.getDataString(empresa.getPessoaJuridica().getDataInicio()) : null);
			empresa.setDataFimString(empresa.getPessoaJuridica().getDataFim() != null ? FormatacaoUtils.getDataString(empresa.getPessoaJuridica().getDataFim()) : null);
			empresa.setEndereco(enderecoService.buscarPorIdPessoa(empresa.getIdPessoa()));
			empresa.setContatosToString(contatoService.getContatosToString(empresa.getIdPessoa()));
			empresa.setFinalizada(empresa.getPessoaJuridica().getDataFim() != null);
		}
		return empresa;
	}
	
	public List<Empresa> buscarTodos(Long idEmpresaSelecionada, boolean inclusiveInativos) {
		List<Empresa> empresasAtivas = empresaManager.buscarTodos(inclusiveInativos);
		if (idEmpresaSelecionada != null) {
			Empresa empresaSelecionada = buscarPorId(idEmpresaSelecionada);
			if (!empresasAtivas.contains(empresaSelecionada)) {
				empresasAtivas.add(empresaSelecionada);
			}
		}
		return empresasAtivas;
	}
	
	public RelatorioVO getRelatorioPdfVO(EmpresaFilter filtro, HttpServletRequest httpServletRequest) {
		PageWrapper<Empresa> paginaWrapper = pesquisar(filtro, null, httpServletRequest);
		List<Empresa> empresas = paginaWrapper.getConteudo();
		
		RelatorioVO relatorioVO = new RelatorioVO();

		//titulo do documento
		relatorioVO.setTitulo("EMPRESAS");
		
		//defini tamanho das colunas
		float[] tamanhoColunas = {3, (float) 1.7, (float) 0.7, (float) 1.2, (float) 1.2, 1, (float) 0.8};
		relatorioVO.setTamanhoColunas(tamanhoColunas);
		
		//obtem nome usuario
		relatorioVO.setUsuario(getUsuario().getNome());
		
		//monta colunas
		relatorioVO.setColunas(new ArrayList<RelatorioColunaVO>());
		relatorioVO.getColunas().add(new RelatorioColunaVO("RAZÃO SOCIAL", Element.ALIGN_LEFT));
		relatorioVO.getColunas().add(new RelatorioColunaVO("CNPJ", Element.ALIGN_CENTER));
		relatorioVO.getColunas().add(new RelatorioColunaVO("TIPO", Element.ALIGN_CENTER));
		relatorioVO.getColunas().add(new RelatorioColunaVO("DATA INÍCIO", Element.ALIGN_CENTER));
		relatorioVO.getColunas().add(new RelatorioColunaVO("DATA FIM", Element.ALIGN_CENTER));
		relatorioVO.getColunas().add(new RelatorioColunaVO("STATUS", Element.ALIGN_CENTER));
		relatorioVO.getColunas().add(new RelatorioColunaVO("ID", Element.ALIGN_CENTER));
			
		//monta dados
		relatorioVO.setConteudos(new ArrayList<>());
		for (Empresa empresa : empresas) {
			RelatorioConteudoVO conteudoVO = new RelatorioConteudoVO();
			conteudoVO.setConteudo(new ArrayList<RelatorioRegistroVO>());
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(empresa.getPessoaJuridica().getPessoa().getNome(), Element.ALIGN_LEFT));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(empresa.getPessoaJuridica().getCnpjFormatado(), Element.ALIGN_CENTER));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(empresa.getTipoString(), Element.ALIGN_CENTER));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(empresa.getPessoaJuridica().getDataInicioFormatada(), Element.ALIGN_CENTER));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(empresa.getPessoaJuridica().getDataFimFormatada(), Element.ALIGN_CENTER));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(empresa.getStatusDescricao(), Element.ALIGN_CENTER));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(empresa.getIdPessoa().toString(), Element.ALIGN_CENTER));
			
			relatorioVO.getConteudos().add(conteudoVO);
		}
		return relatorioVO;
	}
}
