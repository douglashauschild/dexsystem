package br.com.dh.dexSystem.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.dh.dexSystem.model.Dispositivo;
import br.com.dh.dexSystem.repository.DispositivoRepository;

@Service
public class DispositivoService extends ServiceExtend {

	@Autowired
	private DispositivoRepository dispositivoRepository;

	
	public void salvar(Dispositivo dispositivo) throws Exception {
		dispositivoRepository.saveAndFlush(dispositivo);
	}
	
	public Dispositivo buscarPorId(Long id) {
		return dispositivoRepository.getOne(id);
	}
}
