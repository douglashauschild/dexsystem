package br.com.dh.dexSystem.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.itextpdf.text.Element;

import br.com.dh.dexSystem.controller.PageWrapper;
import br.com.dh.dexSystem.manager.UnidadeMedidaManager;
import br.com.dh.dexSystem.model.ServiceInterface;
import br.com.dh.dexSystem.model.UnidadeMedida;
import br.com.dh.dexSystem.model.vo.RelatorioColunaVO;
import br.com.dh.dexSystem.model.vo.RelatorioConteudoVO;
import br.com.dh.dexSystem.model.vo.RelatorioRegistroVO;
import br.com.dh.dexSystem.model.vo.RelatorioVO;
import br.com.dh.dexSystem.repository.UnidadeMedidaRepository;
import br.com.dh.dexSystem.repository.filter.UnidadeMedidaFilter;

@Service
public class UnidadeMedidaService extends SincronizacaoServiceAbstract implements ServiceInterface<UnidadeMedida, UnidadeMedidaFilter> {

	@Autowired
	private UnidadeMedidaManager unidadeMedidaManager;
	
	@Autowired
	private UnidadeMedidaRepository unidadeMedidaRepository;

	
	public PageWrapper<UnidadeMedida> pesquisar(UnidadeMedidaFilter filtro, Pageable pageable, HttpServletRequest httpServletRequest) {
		return new PageWrapper<>(unidadeMedidaManager.filtrar(filtro, pageable), httpServletRequest);
	}
	
	public boolean descricaoCadastrada(UnidadeMedida unidadeMedida) {
		List<UnidadeMedida> unidadesAux = null;
		if (unidadeMedida.getId() != null) {
			unidadesAux = unidadeMedidaRepository.findByDescricaoAndIdNotInAndExcluidoIn(unidadeMedida.getDescricao(), unidadeMedida.getId(), '0');
		} else {
			unidadesAux = unidadeMedidaRepository.findByDescricaoAndExcluidoIn(unidadeMedida.getDescricao(), '0');
		}
		return unidadesAux != null && !unidadesAux.isEmpty() ;
	}
	
	public void salvar(UnidadeMedida unidadeMedida) throws Exception {
		unidadeMedidaRepository.saveAndFlush((UnidadeMedida) gerarChecksum(unidadeMedida.getId() == null, unidadeMedida));
	}
	
	public void excluir(UnidadeMedida unidadeMedida) throws Exception {
		unidadeMedida.setExcluido('1');
		salvar(unidadeMedida);
	}
	
	public UnidadeMedida buscarPorId(Long id) {
		return unidadeMedidaRepository.getOne(id);
	}
	
	public RelatorioVO getRelatorioPdfVO(UnidadeMedidaFilter filtro, HttpServletRequest httpServletRequest) {
		PageWrapper<UnidadeMedida> paginaWrapper = pesquisar(filtro, null, httpServletRequest);
		List<UnidadeMedida> unidades = paginaWrapper.getConteudo();
		
		RelatorioVO relatorioVO = new RelatorioVO();

		//titulo do documento
		relatorioVO.setTitulo("UNIDADES DE MEDIDA");
		
		//defini tamanho das colunas
		float[] tamanhoColunas = {9, 1};
		relatorioVO.setTamanhoColunas(tamanhoColunas);
		
		//obtem nome usuario
		relatorioVO.setUsuario(getUsuario().getNome());
		
		//monta colunas
		relatorioVO.setColunas(new ArrayList<RelatorioColunaVO>());
		relatorioVO.getColunas().add(new RelatorioColunaVO("DESCRIÇÃO", Element.ALIGN_LEFT));
		relatorioVO.getColunas().add(new RelatorioColunaVO("ID", Element.ALIGN_CENTER));
			
		//monta dados
		relatorioVO.setConteudos(new ArrayList<>());
		for (UnidadeMedida unidade : unidades) {
			RelatorioConteudoVO conteudoVO = new RelatorioConteudoVO();
			conteudoVO.setConteudo(new ArrayList<RelatorioRegistroVO>());
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(unidade.getDescricao(), Element.ALIGN_LEFT));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(unidade.getId().toString(), Element.ALIGN_CENTER));
			
			relatorioVO.getConteudos().add(conteudoVO);
		}
		return relatorioVO;
	}
}
