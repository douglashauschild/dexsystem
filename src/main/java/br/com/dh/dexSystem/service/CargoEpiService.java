package br.com.dh.dexSystem.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;

import com.itextpdf.text.Element;

import br.com.dh.dexSystem.controller.PageWrapper;
import br.com.dh.dexSystem.manager.CargoEpiManager;
import br.com.dh.dexSystem.model.CargoEpi;
import br.com.dh.dexSystem.model.ServiceInterface;
import br.com.dh.dexSystem.model.vo.RelatorioColunaVO;
import br.com.dh.dexSystem.model.vo.RelatorioConteudoVO;
import br.com.dh.dexSystem.model.vo.RelatorioRegistroVO;
import br.com.dh.dexSystem.model.vo.RelatorioVO;
import br.com.dh.dexSystem.repository.CargoEpiRepository;
import br.com.dh.dexSystem.repository.filter.CargoEpiFilter;
import br.com.dh.dexSystem.util.FormatacaoUtils;
import br.com.dh.dexSystem.validation.ValidacaoCargoEpi;

@Service
public class CargoEpiService extends SincronizacaoServiceAbstract implements ServiceInterface<CargoEpi, CargoEpiFilter> {

	@Autowired
	private CargoEpiManager cargoEpiManager;
	@Autowired
	private CargoEpiRepository cargoEpiRepository;
	@Autowired
	private ValidacaoCargoEpi validacaoCargoEpi;
	@Autowired
	private AtualizacaoEpiService atualizacaoEpiService;


	public PageWrapper<CargoEpi> pesquisar(CargoEpiFilter filtro, Pageable pageable, HttpServletRequest httpServletRequest) {
		PageWrapper<CargoEpi> pageWrapper = null;
		try {
			pageWrapper = new PageWrapper<CargoEpi>(cargoEpiManager.filtrar(filtro, pageable), httpServletRequest);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pageWrapper;
	}
	
	public String validarGravacao(CargoEpi cargoEpi, Model model, Errors errors) throws Exception {
		String retorno = null;
		cargoEpi = ajustarGravacao(cargoEpi);
		String msgRetorno = validacaoCargoEpi.validar(cargoEpi);
		if (msgRetorno != null && !msgRetorno.isEmpty()) {
			retorno = verificaRetorno(msgRetorno, errors);
		} else {
			if (!cargoEpi.isFinalizado() && cargoEpi.getDataFim() != null) {
				model.addAttribute("msgConfirmacao", "Finalizando esta recomendação de EPI você não poderá mais editar este cadastro!<br>Você tem certeza que deseja continuar?");
				model.addAttribute("modalConfirmacao", true);
				retorno = "cadastros/cargoEpi/cargoEpi-cadastro";
			}
		}
		return retorno;
	}
	
	private String verificaRetorno(String msgValidacao, Errors errors) {
		String retorno = null;
		if (msgValidacao != null && !msgValidacao.isEmpty()) {
			errors.rejectValue(null, null, msgValidacao);
			retorno = "cadastros/cargoEpi/cargoEpi-cadastro";
		}
		return retorno;
	}
	
	public CargoEpi ajustarGravacao(CargoEpi cargoEpi) throws Exception {
		cargoEpi.setDataInicio(FormatacaoUtils.getData(cargoEpi.getDataInicioString()));
		cargoEpi.setDataFim(cargoEpi.getDataFimString() != null && !cargoEpi.getDataFimString().isEmpty() ? FormatacaoUtils.getData(cargoEpi.getDataFimString()) : null);
		return cargoEpi;
	}
	
	public void salvar(CargoEpi cargoEpi) throws Exception {
		cargoEpiRepository.saveAndFlush((CargoEpi) gerarChecksum(cargoEpi.getId() == null, cargoEpi));
		atualizacaoEpiService.atualizarEpi(cargoEpi);
	}
	
	public void excluir(CargoEpi cargoEpi) throws Exception {
		cargoEpi.setExcluido('1');
		salvar(cargoEpi);
	}
	
	public CargoEpi buscarPorId(Long id) {
		CargoEpi cargoEpi = cargoEpiManager.buscarPorId(id);
		cargoEpi.setDataInicioString(cargoEpi.getDataInicio() != null ? FormatacaoUtils.getDataString(cargoEpi.getDataInicio()) : null);
		cargoEpi.setDataFimString(cargoEpi.getDataFim() != null ? FormatacaoUtils.getDataString(cargoEpi.getDataFim()) : null);
		cargoEpi.setIdEmpresa(cargoEpi.getCargo().getSetor().getIdEmpresa());
		cargoEpi.setIdSetor(cargoEpi.getCargo().getIdSetor());
		cargoEpi.setFinalizado(cargoEpi.getDataFim() != null);
		return cargoEpi;
	}
	
	public RelatorioVO getRelatorioPdfVO(CargoEpiFilter filtro, HttpServletRequest httpServletRequest) {
		PageWrapper<CargoEpi> paginaWrapper = pesquisar(filtro, null, httpServletRequest);
		List<CargoEpi> cargosEpi = paginaWrapper.getConteudo();
		
		RelatorioVO relatorioVO = new RelatorioVO();

		//titulo do documento
		relatorioVO.setTitulo("EPI'S PREVISTOS PARA O CARGO");
		
		//defini tamanho das colunas
		float[] tamanhoColunas = { 2, 4, (float) 0.8, (float) 0.6, 1, 1, (float) 0.5};
		relatorioVO.setTamanhoColunas(tamanhoColunas);
		
		//obtem nome usuario
		relatorioVO.setUsuario(getUsuario().getNome());
		
		//monta colunas
		relatorioVO.setColunas(new ArrayList<RelatorioColunaVO>());
		relatorioVO.getColunas().add(new RelatorioColunaVO("CARGO", Element.ALIGN_LEFT));
		relatorioVO.getColunas().add(new RelatorioColunaVO("EPI", Element.ALIGN_LEFT));
		relatorioVO.getColunas().add(new RelatorioColunaVO("CA", Element.ALIGN_CENTER));
		relatorioVO.getColunas().add(new RelatorioColunaVO("QTD", Element.ALIGN_RIGHT));
		relatorioVO.getColunas().add(new RelatorioColunaVO("DATA INÍCIO", Element.ALIGN_CENTER));
		relatorioVO.getColunas().add(new RelatorioColunaVO("DATA FIM", Element.ALIGN_CENTER));
		relatorioVO.getColunas().add(new RelatorioColunaVO("ID", Element.ALIGN_CENTER));
			
		//monta dados
		relatorioVO.setConteudos(new ArrayList<>());
		for (CargoEpi cargoEpi : cargosEpi) {
			RelatorioConteudoVO conteudoVO = new RelatorioConteudoVO();
			conteudoVO.setConteudo(new ArrayList<RelatorioRegistroVO>());
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(cargoEpi.getCargo().getNome(), Element.ALIGN_LEFT));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(cargoEpi.getEpi().getNome(), Element.ALIGN_LEFT));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(cargoEpi.getEpi().getCa(), Element.ALIGN_CENTER));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(cargoEpi.getQuantidade().toString(), Element.ALIGN_RIGHT));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(cargoEpi.getDataInicioFormatada(), Element.ALIGN_CENTER));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(cargoEpi.getDataFimFormatada(), Element.ALIGN_CENTER));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(cargoEpi.getId().toString(), Element.ALIGN_CENTER));
			
			relatorioVO.getConteudos().add(conteudoVO);
		}
		return relatorioVO;
	}
	
}
