package br.com.dh.dexSystem.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.dh.dexSystem.model.Menu;
import br.com.dh.dexSystem.repository.MenuRepository;

@Service
public class MenuService extends ServiceExtend {

	@Autowired
	private MenuRepository menuRepository;
	
	
	public List<Menu> buscarTodos(Long idNivelAcesso) {
		List<Menu> menus = menuRepository.buscarTodosMenus(idNivelAcesso);
		for (Menu menu : menus) {
			if (menu.getUri() == null) {
				menu.setSubMenus(buscarSubMenu(idNivelAcesso, menu.getId()));
			}
		}
		return menus;
	}
	
	private List<Menu> buscarSubMenu(Long idNivelAcesso, Long idMenu) {
		List<Menu> subMenus = menuRepository.buscarTodosSubMenus(idNivelAcesso, idMenu);
		for (Menu subMenu : subMenus) {
			subMenu.setSubMenus(buscarSubMenu(idNivelAcesso, subMenu.getId()));
		}
		return subMenus;
	}
}
