package br.com.dh.dexSystem.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.dh.dexSystem.model.EmpregadoEpi;
import br.com.dh.dexSystem.model.Epi;
import br.com.dh.dexSystem.model.constant.DiaSemanaEnum;
import br.com.dh.dexSystem.model.constant.StatusEmpregadoEpiEnum;
import br.com.dh.dexSystem.model.vo.EpiUtilizadoVO;
import br.com.dh.dexSystem.model.vo.GraficoVO;
import br.com.dh.dexSystem.model.vo.TotalVO;
import br.com.dh.dexSystem.util.FormatacaoUtils;

@Service
public class DashboardService extends ServiceExtend {
	
	@Autowired
	private EmpregadoService empregadoService;
	@Autowired
	private EpiService epiService;
	@Autowired
	private EmpregadoEpiService empregadoEpiService;

	
	public String situacaoEpis() throws Exception {
		int emDia = 0;
		int caVencido = 0;
		int semCa = 0;
		List<Epi> epis = epiService.buscarTodos();
		for (Epi epi : epis) {
			if (epi.getCa() == null || epi.getCa().isEmpty()) {
				semCa++;
			} else if (epi.isCaVencido()) {
				caVencido++;
			} else {
				emDia++;
			}
		}
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("emDia", emDia);
		jsonObj.put("caVencido", caVencido);
		jsonObj.put("semCa", semCa);
		
		return jsonObj.toString();
	}
	
	
	public String episMaisUtilizados() throws Exception {
		JSONArray jsonArray = new JSONArray();
		List<EpiUtilizadoVO> episUtilizados = empregadoEpiService.buscarEpisMaisUtilizados();
		for (EpiUtilizadoVO epiUtilizadoVO : episUtilizados) {
			jsonArray.put(montarJsonEpisUtilizados(epiUtilizadoVO));
		}
		return jsonArray.toString();
	}
	
	private JSONObject montarJsonEpisUtilizados(EpiUtilizadoVO epiUtilizadoVO) {
		JSONObject jsonEpis = new JSONObject();
		jsonEpis.put("Nome", epiUtilizadoVO.getNome());
		jsonEpis.put("Quantidade", epiUtilizadoVO.getQtd());
		return jsonEpis;
	}
	
	
	public String statusEntregas(Long idEmpresa) throws Exception {
		idEmpresa = ajustarIdEmpresa(idEmpresa);
		int total = 0;
		int substituido = 0;
		int devolvido = 0;
		int vencido = 0;
		int emDia = 0;
		int aguardando = 0;
		int entregaVencida = 0;
		List<EmpregadoEpi> empregadoEpis = empregadoEpiService.buscarTudoPorEmpresa(idEmpresa);
		if (empregadoEpis != null && !empregadoEpis.isEmpty()) {
			total = empregadoEpis.size();
			
			for (EmpregadoEpi empregadoEpi : empregadoEpis) {
				if (empregadoEpi.getStatusDescricao().equals(StatusEmpregadoEpiEnum.SUBSTITUIDO.getDescricao())) {
					substituido++;
				} else if (empregadoEpi.getStatusDescricao().equals(StatusEmpregadoEpiEnum.DEVOLVIDO.getDescricao())) {
					devolvido++;
				} else if (empregadoEpi.getStatusDescricao().equals(StatusEmpregadoEpiEnum.VENCIDO.getDescricao())) {
					vencido++;
				} else if (empregadoEpi.getStatusDescricao().equals(StatusEmpregadoEpiEnum.EM_DIA.getDescricao())) {
					emDia++;
				} else if (empregadoEpi.getStatusDescricao().equals(StatusEmpregadoEpiEnum.AGUARDANDO.getDescricao())) {
					aguardando++;
				} else if (empregadoEpi.getStatusDescricao().equals(StatusEmpregadoEpiEnum.ENTREGA_VENCIDA.getDescricao())) {
					entregaVencida++;
				}
			}
		}
		JSONArray jsonArray = new JSONArray();
		jsonArray.put(montarJsonStatus("Entregas atrasadas", total, entregaVencida, "danger"));
		jsonArray.put(montarJsonStatus("EPIs vencidos", total, vencido, "danger"));
		jsonArray.put(montarJsonStatus("Aguardando entrega", total, aguardando, "primary"));
		jsonArray.put(montarJsonStatus("EPIs em dia", total, emDia, "success"));
		jsonArray.put(montarJsonStatus("EPIs substituídos", total, substituido, "info"));
		jsonArray.put(montarJsonStatus("EPIs devolvidos", total, devolvido, "warning"));
		
		JSONObject json = new JSONObject();
		json.put("total", total);
		json.put("status", jsonArray);
		
		return json.toString();
	}
	
	private JSONObject montarJsonStatus(String nome, int total, int totalStatus, String cor) {
		JSONObject jsonStatus = new JSONObject();
		jsonStatus.put("nome", nome);
		jsonStatus.put("total", totalStatus);
		jsonStatus.put("porcentagem", getPorcentagem(total, totalStatus));
		jsonStatus.put("cor", cor);
		return jsonStatus;
	}
	
	public TotalVO totais(Long idEmpresa) throws Exception {
		idEmpresa = ajustarIdEmpresa(idEmpresa);
		int empregados = ajustarRetorno(empregadoService.buscarAtivosPorEmpresa(idEmpresa).size());
		int epis = ajustarRetorno(epiService.buscarTodos().size());
		int totalEntregas = ajustarRetorno(empregadoEpiService.buscarTotalEntregasRealizadas(idEmpresa));
		int totalEpisEntregues = ajustarRetorno(empregadoEpiService.buscarTotalEpisEntregues(idEmpresa));
		return new TotalVO(empregados, epis, totalEntregas, totalEpisEntregues);
	}
	
	public Integer totalEpisEntregasMes(Long idEmpresa) throws Exception {
		idEmpresa = ajustarIdEmpresa(idEmpresa);
		Date dataInicial = FormatacaoUtils.getData(FormatacaoUtils.getPrimeiroDiaDoMes(FormatacaoUtils.getMesAtual()));
		Date dataFinal = FormatacaoUtils.getData(FormatacaoUtils.getUltimoDiaDoMes(FormatacaoUtils.getMesAtual()));
		Integer total = empregadoEpiService.buscarTotalEntregasRealizadas(idEmpresa, dataInicial, dataFinal);
		return ajustarRetorno(total);
	}
	
	public GraficoVO totalEntregasAno(Long idEmpresa) throws Exception {
		idEmpresa = ajustarIdEmpresa(idEmpresa);

		GraficoVO graficoVO = new GraficoVO();
		List<String> meses = new ArrayList<>();
		List<Integer> totaisEntrega = new ArrayList<>();
		List<Integer> totaisEpis = new ArrayList<>();

		for (int i = 0; i < 12; i++) {
			meses.add(getMes(i+1));
			
			Date dataInicial = FormatacaoUtils.getData(FormatacaoUtils.getPrimeiroDiaDoMes(i));
			Date dataFinal = FormatacaoUtils.getData(FormatacaoUtils.getUltimoDiaDoMes(i));
		
			Integer totalEntregas = empregadoEpiService.buscarTotalEntregasRealizadas(idEmpresa, dataInicial, dataFinal);
			Integer totalEpis = empregadoEpiService.buscarTotalEntregasRealizadas(idEmpresa, dataInicial, dataFinal, true);
			
			totaisEntrega.add(ajustarRetorno(totalEntregas));
			totaisEpis.add(ajustarRetorno(totalEpis));
		}
		
		JSONObject json = new JSONObject();
		json.put("Total de entregas realizadas", totaisEntrega);
		json.put("Total de EPIs entregues", totaisEpis);

		graficoVO.setAno(FormatacaoUtils.getAnoAtual().toString());
		graficoVO.setMeses(meses);
		graficoVO.setJson(json.toString());
		return graficoVO;
	}
	
	private String getMes(int i) {
		return DiaSemanaEnum.getByKey(i).getDescricao();
	}
	
	private Integer ajustarRetorno(Integer total) {
		return total == null ? 0 : total;
	}
	
	private Long ajustarIdEmpresa(Long idEmpresa) {
		return idEmpresa != null && idEmpresa == 0L ? null : idEmpresa;
	}
	
	private Double getPorcentagem(int total, int totalStatus) {
		if (total == 0 || totalStatus == 0) {
			return (double) 0;
		}
		return (double) ((totalStatus * 100) / total);
	}
}
