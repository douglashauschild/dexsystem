package br.com.dh.dexSystem.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.dh.dexSystem.controller.PageWrapper;
import br.com.dh.dexSystem.manager.GradeItemManager;
import br.com.dh.dexSystem.model.Grade;
import br.com.dh.dexSystem.model.GradeItem;
import br.com.dh.dexSystem.model.ServiceInterface;
import br.com.dh.dexSystem.model.constant.AcaoCadastroEnum;
import br.com.dh.dexSystem.model.constant.DelimitadorEnum;
import br.com.dh.dexSystem.repository.GradeItemRepository;
import br.com.dh.dexSystem.repository.filter.GradeItemFilter;

@Service
public class GradeItemService extends SincronizacaoServiceAbstract implements ServiceInterface<GradeItem, GradeItemFilter> {

	@Autowired
	private GradeItemManager gradeItemManager;
	@Autowired
	private GradeItemRepository gradeItemRepository;
	
	
	public PageWrapper<GradeItem> pesquisar(GradeItemFilter filtro, Pageable pageable, HttpServletRequest httpServletRequest) {
		return new PageWrapper<>(gradeItemManager.filtrar(filtro, pageable), httpServletRequest);
	}
	
	
	public List<GradeItem> getItens(Long idGrade) {
		return gradeItemRepository.buscarPorIdGrade(idGrade);
	}
	
	
	public void salvarGradeItens(Grade grade) throws Exception {
		String[] itens = grade.getItensToString().split("\\"+DelimitadorEnum.ITENS.getKey());
		for (String item : itens) {
			Character acao = item.charAt(item.length()-1);
			
			String gradeItem[] = item.split("\\"+DelimitadorEnum.INFORMACAO.getKey());
			String tamanho = gradeItem[0];
			Integer ordem = Integer.parseInt(gradeItem[1].split("\\"+DelimitadorEnum.ACAO.getKey())[0]);
			
			GradeItem gradeItemAct = null;
			if (acao == AcaoCadastroEnum.EXCLUIR.getKey()) {
				gradeItemAct = gradeItemRepository.buscarPorIdGradeTamanhoOrdem(grade.getId(), tamanho, ordem);
				if (gradeItemAct != null) {
					gradeItemAct.setExcluido('1');
				}
			} else if (acao == AcaoCadastroEnum.ALTERAR.getKey()) {
				String[] registros = item.split("\\"+DelimitadorEnum.ITENS_ALTERACAO.getKey());
				String[] itemNovo = registros[0].split("\\"+DelimitadorEnum.INFORMACAO.getKey());
				
				//pega o ultimo, pois se houver registros no meio, significa que nao estao salvos
				String[] itemAlterar = registros[registros.length-1].split("\\"+DelimitadorEnum.INFORMACAO.getKey());
				
				String tamanhoNovo = itemNovo[0];
				Integer ordemNovo = Integer.parseInt(itemNovo[1].split("\\"+DelimitadorEnum.ACAO.getKey())[0]);
				String tamanhoAlterar = itemAlterar[0];
				Integer ordemAlterar = Integer.parseInt(itemAlterar[1].split("\\"+DelimitadorEnum.ACAO.getKey())[0]);
				gradeItemAct = gradeItemRepository.buscarPorIdGradeTamanhoOrdem(grade.getId(), tamanhoAlterar, ordemAlterar);
				if (gradeItemAct != null) {
					gradeItemAct.setTamanho(tamanhoNovo);
					gradeItemAct.setOrdem(ordemNovo);
				} else {
					gradeItemAct = new GradeItem(grade.getId(), tamanhoNovo, ordemNovo);
				}
			} else if (acao == AcaoCadastroEnum.NOVO.getKey()) {
				gradeItemAct = new GradeItem(grade.getId(), tamanho, ordem);
			}
			if (gradeItemAct != null) {
				salvar(gradeItemAct);
			}
		}
	}
	

	public void salvar(GradeItem gradeItem) throws Exception {
		gradeItemRepository.saveAndFlush((GradeItem) gerarChecksum(gradeItem.getId() == null, gradeItem));
		
	}

	public void excluirPorGrade(Grade grade) throws Exception {
		List<GradeItem> gradeItens = gradeItemRepository.buscarPorIdGrade(grade.getId());
		for (GradeItem gradeItem : gradeItens) {
			excluir(gradeItem);
		}
	}
	
	public void excluir(GradeItem gradeItem) throws Exception {
		gradeItem.setExcluido('1');
		salvar(gradeItem);
	}

	public GradeItem buscarPorId(Long id) {
		return gradeItemRepository.getOne(id);
	}
}
