package br.com.dh.dexSystem.service;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import br.com.dh.dexSystem.model.Usuario;
import br.com.dh.dexSystem.security.UsuarioSistema;

public class ServiceExtend {

	public Usuario getUsuario() {
		return ((UsuarioSistema) ((SecurityContext) SecurityContextHolder.getContext()).getAuthentication().getPrincipal()).getUsuario();
	}
	
	public boolean checkRole(String role) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		return authentication.getAuthorities().stream().anyMatch(r -> r.getAuthority().equals(role));
	}
}
