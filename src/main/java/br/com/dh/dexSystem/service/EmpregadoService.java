package br.com.dh.dexSystem.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;

import com.itextpdf.text.Element;

import br.com.dh.dexSystem.controller.PageWrapper;
import br.com.dh.dexSystem.manager.AtividadeManager;
import br.com.dh.dexSystem.manager.EmpregadoManager;
import br.com.dh.dexSystem.model.Atividade;
import br.com.dh.dexSystem.model.Empregado;
import br.com.dh.dexSystem.model.Empresa;
import br.com.dh.dexSystem.model.ServiceInterface;
import br.com.dh.dexSystem.model.vo.EmpregadoVO;
import br.com.dh.dexSystem.model.vo.RelatorioColunaVO;
import br.com.dh.dexSystem.model.vo.RelatorioConteudoVO;
import br.com.dh.dexSystem.model.vo.RelatorioRegistroVO;
import br.com.dh.dexSystem.model.vo.RelatorioVO;
import br.com.dh.dexSystem.repository.EmpregadoRepository;
import br.com.dh.dexSystem.repository.filter.EmpregadoFilter;
import br.com.dh.dexSystem.util.FormatacaoUtils;
import br.com.dh.dexSystem.validation.ValidacaoEmpregado;

@Service
public class EmpregadoService extends SincronizacaoServiceAbstract implements ServiceInterface<Empregado, EmpregadoFilter> {

	@Autowired
	private EmpregadoRepository empregadoRepository;
	@Autowired
	private EmpregadoManager empregadoManager;
	@Autowired
	private AtividadeManager atividadeManager;
	@Autowired
	private ValidacaoEmpregado validacaoEmpregado;
	@Autowired
	private EmpresaService empresaService;
	@Autowired
	private EmpregadoEpiService empregadoEpiService;
	@Autowired
	private AtividadeService atividadeService;

	
	public PageWrapper<Empregado> pesquisar(EmpregadoFilter filtro, Pageable pageable, HttpServletRequest httpServletRequest) {
		PageWrapper<Empregado> pageWrapper = null;
		try {
			pageWrapper = new PageWrapper<Empregado>(empregadoManager.filtrar(filtro, pageable), httpServletRequest);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pageWrapper;
	}
	
	public String validarGravacao(Empregado empregado, Model model, Errors errors) throws Exception {
		String retorno = null;
		empregado = ajustarGravacao(empregado);
		String msgRetorno = validacaoEmpregado.validar(empregado);
		if (msgRetorno != null && !msgRetorno.isEmpty()) {
			errors.rejectValue(null, null, msgRetorno);
			retorno = "cadastros/empregado/empregado-cadastro";
		} else {
			if (((empregado.getSenha() != null && !empregado.getSenha().isEmpty()) && (empregado.getConfirmaSenha() == null || empregado.getConfirmaSenha().isEmpty())) || ((empregado.getSenha() == null || empregado.getSenha().isEmpty()) && (empregado.getConfirmaSenha() != null && !empregado.getConfirmaSenha().isEmpty()))) {
				errors.rejectValue("confirmaSenha", null, "Para cadastrar uma senha, a senha e a confimação da senha devem ser informadas!");
				retorno = "cadastros/empregado/empregado-cadastro";
			} else if (empregado.getSenha() != null && !empregado.getSenha().isEmpty() && empregado.getConfirmaSenha() != null && !empregado.getConfirmaSenha().isEmpty() && ((empregado.getId() == null && !senhasIguais(empregado)) || (empregado.getId() != null && editandoSenha(empregado) && !senhasIguais(empregado)))) {
				errors.rejectValue("confirmaSenha", null, "A senha e a confimação da senha não são iguais!");
				retorno = "cadastros/empregado/empregado-cadastro";
			} else if (empregado.getId() != null && empregado.getId() != 0L && empregado.getDataDemissao() != null) { //verifica demissao
				String msg = "Demitindo o empregado você estará finalizando a sua atividade atual e devolvendo todos EPIs entregues.<br><br>Tem certeza que deseja continuar com a demissão?"; 
				model.addAttribute("msgConfirmacao", msg);
				model.addAttribute("modalConfirmacao", true);
				retorno = "cadastros/empregado/empregado-cadastro";
			} else if ((empregado.getId() == null || empregado.getId() == 0L) && empregado.getDataDemissao() == null) {
				List<Empregado> empregadoEmpresas = empregadoManager.buscarEmpregadoEmOutrasEmpresas(empregado);
				if (empregadoEmpresas != null && !empregadoEmpresas.isEmpty()) {
					String msg = "Esta pessoa já está admitida nas seguintes empresas:<br><br>"; 
					
					msg += "<center><div class='table-empresasVinculadas'><table style='width: 400px;'><tr><th style='width: 150px;'>CNPJ</th><th style='text-align:left'>Razão Social</th></tr>";
					for (Empregado empregadoAux : empregadoEmpresas) {
						msg += "<tr><td style='width: 150px;'>"+empregadoAux.getEmpresa().getPessoaJuridica().getCnpjFormatado()+"</td><td>"+empregadoAux.getEmpresa().getRazaoSocial()+"</td></tr>";
					}
					msg += "</div></table></center><br>";
					msg += "Você tem certeza que deseja admiti-lá em mais uma empresa?";
				
					model.addAttribute("msgConfirmacao", msg);
					model.addAttribute("modalConfirmacao", true);
					retorno = "cadastros/empregado/empregado-cadastro";
				}
			}
		}
		return retorno;
	}
	
	public Empregado ajustarGravacao(Empregado empregado) throws Exception {
		empregado.setDataAdmissao(FormatacaoUtils.getData(empregado.getDataAdmissaoString()));
		empregado.setDataDemissao(empregado.getDataDemissaoString() != null && !empregado.getDataDemissaoString().isEmpty() ? FormatacaoUtils.getData(empregado.getDataDemissaoString()) : null);
		return empregado;
	}
	
	public Empregado finalizarRegistros(Empregado empregado) throws Exception {
		if (!empregado.isDemitido() && empregado.getDataDemissao() != null) {
			empregadoEpiService.finalizarEpis(empregado);
			atividadeService.finalizar(empregado);
		}
		return empregado;
	}
	
	public void salvar(Empregado empregado) throws Exception {
		if (empregado.getId() == null ||  editandoSenha(empregado)) {
			empregado.setSenha(new BCryptPasswordEncoder().encode(empregado.getSenha()));
		}
		empregadoRepository.saveAndFlush((Empregado) gerarChecksum(empregado.getId() == null, empregado));
	}
	
	public String validarExclusao(Empregado empregado) {
		return validacaoEmpregado.validarExclusao(empregado);
	}
	
	public void excluir(Empregado empregado) throws Exception {
		empregado.setExcluido('1');
		salvar(empregado);
	}
	
	public Empregado buscarPorId(Long id) {
		Empregado empregado = empregadoManager.buscarPorId(id);
		if (empregado != null) {
			empregado.setDataAdmissaoString(empregado.getDataAdmissao() != null ? FormatacaoUtils.getDataString(empregado.getDataAdmissao()) : null);
			empregado.setDataDemissaoString(empregado.getDataDemissao() != null ? FormatacaoUtils.getDataString(empregado.getDataDemissao()) : null);
			empregado.setDemitido(empregado.getDataDemissao() != null);
			empregado.setConfirmaSenha(empregado.getSenha() != null && !empregado.getSenha().isEmpty() ? empregado.getSenha() : null);
		}
		return empregado;
	}
	
	public EmpregadoVO getInformacoes(Long idEmpregado) {
		EmpregadoVO empregadoVO = null;
		Empregado empregado = buscarPorId(idEmpregado);
		if (empregado != null) {
			Empresa empresa = empresaService.buscarPorId(empregado.getIdEmpresa());
			empregadoVO = new EmpregadoVO(empresa != null ? empresa.getRazaoSocial() : null, empregado.getPessoaFisica().getPessoa().getNome(), empregado.getPessoaFisica().getIdade(), empregado.getMatricula(), empregado.getDataAdmissaoFormatada(), empregado.getDataDemissaoFormatada(), empregado.getPessoaFisica().getCpfFormatado(), empregado.getPessoaFisica().getRg());

			Atividade atividade = atividadeManager.buscarAtivaPorEmpregado(empregado);
			if (atividade != null) {
				empregadoVO.setSetor(atividade.getCargo().getSetor().getNome());
				empregadoVO.setCargo(atividade.getCargo().getNome());
			}
		}
		return empregadoVO;
	}
	
	public List<Empregado> buscarTodosAtivos() {
		return empregadoManager.buscarTodosAtivos();
	}
	
	public List<Empregado> buscarAtivosPorEmpresa(Long idEmpresa) {
		return empregadoManager.buscarAtivosPorEmpresa(idEmpresa);
	}
	
	public RelatorioVO getRelatorioPdfVO(EmpregadoFilter filtro, HttpServletRequest httpServletRequest) {
		PageWrapper<Empregado> paginaWrapper = pesquisar(filtro, null, httpServletRequest);
		List<Empregado> empregados = paginaWrapper.getConteudo();
		
		RelatorioVO relatorioVO = new RelatorioVO();

		//titulo do documento
		relatorioVO.setTitulo("EMPREGADOS");
		
		//defini tamanho das colunas
		float[] tamanhoColunas = {4, 3, 1, 1, (float) 0.8};
		relatorioVO.setTamanhoColunas(tamanhoColunas);
		
		//obtem nome usuario
		relatorioVO.setUsuario(getUsuario().getNome());
		
		//monta colunas
		relatorioVO.setColunas(new ArrayList<RelatorioColunaVO>());
		relatorioVO.getColunas().add(new RelatorioColunaVO("EMPRESA", Element.ALIGN_LEFT));
		relatorioVO.getColunas().add(new RelatorioColunaVO("EMPREGADO", Element.ALIGN_LEFT));
		relatorioVO.getColunas().add(new RelatorioColunaVO("DATA ADMISSÃO", Element.ALIGN_CENTER));
		relatorioVO.getColunas().add(new RelatorioColunaVO("DATA DEMISSÃO", Element.ALIGN_CENTER));
		relatorioVO.getColunas().add(new RelatorioColunaVO("ID", Element.ALIGN_CENTER));
			
		//monta dados
		relatorioVO.setConteudos(new ArrayList<>());
		for (Empregado empregado : empregados) {
			RelatorioConteudoVO conteudoVO = new RelatorioConteudoVO();
			conteudoVO.setConteudo(new ArrayList<RelatorioRegistroVO>());
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(empregado.getEmpresa().getPessoaJuridica().getPessoa().getNome(), Element.ALIGN_LEFT));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(empregado.getPessoaFisica().getPessoa().getNome(), Element.ALIGN_LEFT));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(empregado.getDataAdmissaoFormatada(), Element.ALIGN_CENTER));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(empregado.getDataDemissaoFormatada(), Element.ALIGN_CENTER));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(empregado.getId().toString(), Element.ALIGN_CENTER));
			
			relatorioVO.getConteudos().add(conteudoVO);
		}
		return relatorioVO;
	}
	
	private boolean editandoSenha(Empregado empregado) {
		boolean editando = false;
		if (empregado.getId() != null && empregado.getId() != 0L && empregado.getSenha() != null && !empregado.getSenha().isEmpty() && empregado.getConfirmaSenha() != null && !empregado.getConfirmaSenha().isEmpty()) {
			Empregado empregadoAux = buscarPorId(empregado.getId());
			if (empregadoAux != null) {
				String senhaSalva = empregadoAux.getSenha();
				editando = (!empregado.getSenha().equals(senhaSalva) || !empregado.getConfirmaSenha().equals(senhaSalva));
			}
		}
		return editando;
	}
	
	public boolean senhasIguais(Empregado empregado) {
		if (empregado.getSenha() != null && !empregado.getSenha().isEmpty() && empregado.getConfirmaSenha() != null && !empregado.getConfirmaSenha().isEmpty()) {
			return empregado.getSenha().equals(empregado.getConfirmaSenha());
		}
		return false;
	}
}
