package br.com.dh.dexSystem.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import com.itextpdf.text.Element;

import br.com.dh.dexSystem.controller.PageWrapper;
import br.com.dh.dexSystem.manager.GradeManager;
import br.com.dh.dexSystem.model.Grade;
import br.com.dh.dexSystem.model.GradeItem;
import br.com.dh.dexSystem.model.ServiceInterface;
import br.com.dh.dexSystem.model.constant.AcaoCadastroEnum;
import br.com.dh.dexSystem.model.constant.DelimitadorEnum;
import br.com.dh.dexSystem.model.vo.GradeItemVO;
import br.com.dh.dexSystem.model.vo.RelatorioColunaVO;
import br.com.dh.dexSystem.model.vo.RelatorioConteudoVO;
import br.com.dh.dexSystem.model.vo.RelatorioRegistroVO;
import br.com.dh.dexSystem.model.vo.RelatorioVO;
import br.com.dh.dexSystem.repository.GradeRepository;
import br.com.dh.dexSystem.repository.filter.GradeFilter;

@Service
public class GradeService extends SincronizacaoServiceAbstract implements ServiceInterface<Grade, GradeFilter> {

	@Autowired
	private GradeManager gradeManager;
	@Autowired
	private GradeRepository gradeRepository;
	@Autowired
	private GradeItemService gradeItemService;

	
	public PageWrapper<Grade> pesquisar(GradeFilter filtro, Pageable pageable, HttpServletRequest httpServletRequest) {
		return new PageWrapper<>(gradeManager.filtrar(filtro, pageable), httpServletRequest);
	}
	
	
	public Grade getGradeItens(Grade grade) {
		String itensToString = "";
		List<GradeItem> itens = gradeItemService.getItens(grade.getId());
		for (GradeItem gradeItem : itens) {
			itensToString += new GradeItemVO(gradeItem.getTamanho(), gradeItem.getOrdem()).toString(AcaoCadastroEnum.CADASTRADO.getKey());
		}
		grade.setItensToString(itensToString);
		return grade;
	}
	
	
	public String validarGravacao(Grade grade, Model model) {
		String retorno = null;
		if (nomeCadastrado(grade)) {
			model.addAttribute("grade", grade);
			model.addAttribute("modalConfirmacao", true);
			retorno = "cadastros/grade/grade-cadastro";
		}
		if (grade.getItensToString() == null || grade.getItensToString().isEmpty()) {
			model.addAttribute("grade", grade);
			model.addAttribute("modalAlerta", true);
			retorno = "cadastros/grade/grade-cadastro";
		}
		return retorno;
	}
	
	
	public void salvar(Grade grade) throws Exception {
		gradeRepository.saveAndFlush((Grade) gerarChecksum(grade.getId() == null, grade));
	}
	
	
	public void salvarGradeItens(Grade grade) throws Exception {
		if (grade != null && grade.getItensToString() != null && !grade.getItensToString().isEmpty()) {
			gradeItemService.salvarGradeItens(grade);
		}
	}
	
	
	public void excluir(Grade grade) throws Exception {
		grade.setExcluido('1');
		salvar(grade);
		gradeItemService.excluirPorGrade(grade);
	}
	
	
	public Grade buscarPorId(Long id) {
		return gradeRepository.getOne(id);
	}
	
	
	public String adicionarItem(GradeItemVO gradeItemVO, Model model) {
		String itensInseridos = gradeItemVO.getGrade().getItensToString();
		String itens[] = itensInseridos.split("\\"+DelimitadorEnum.ITENS.getKey());

		String validacao = validarItens(gradeItemVO, Arrays.asList(itens), model);
		if (validacao != null && !validacao.isEmpty()) {
			return validacao;	
		}
		GradeItemVO itemVO = new GradeItemVO(gradeItemVO.getTamanho(), gradeItemVO.getOrdem());
		gradeItemVO.getGrade().setItensToString(ordenarLista(gradeItemVO.getGrade().getItensToString() + itemVO.toString(AcaoCadastroEnum.NOVO.getKey())));
		model.addAttribute("grade", gradeItemVO.getGrade());
		return "cadastros/grade/grade-cadastro :: #gradeItens";
	}
	
	
    public String alterarItem(GradeItemVO gradeItemVO, Model model) {
		String itensToString = gradeItemVO.getGrade().getItensToString();
		GradeItemVO itemVO = new GradeItemVO(gradeItemVO.getTamanho(), gradeItemVO.getOrdem());
		String itemOriginal = gradeItemVO.getItemExclusao();
		
		List<String> itensValidacao = new ArrayList<String>();
		String itens[] = itensToString.split("\\"+DelimitadorEnum.ITENS.getKey());
		for (String item : itens) {
			if (!item.equals(itemOriginal)) {
				itensValidacao.add(item);
			}
		}
		String validacao = validarItens(gradeItemVO, itensValidacao, model);
		if (validacao != null && !validacao.isEmpty()) {
			return validacao;	
		}
		String itemAlterar = itemOriginal.replace(itemOriginal.charAt(itemOriginal.length()-1), AcaoCadastroEnum.ALTERAR.getKey());
		String novoItem = itemVO.toString(AcaoCadastroEnum.NOVO.getKey()).replace(DelimitadorEnum.ITENS.getKey(), DelimitadorEnum.ITENS_ALTERACAO.getKey()) + itemAlterar;
		gradeItemVO.getGrade().setItensToString(ordenarLista(gradeItemVO.getGrade().getItensToString().replace(itemOriginal, novoItem)));
		model.addAttribute("grade", gradeItemVO.getGrade());
		return "cadastros/grade/grade-cadastro :: #gradeItens";
    }
    
    
    public String removerItem(GradeItemVO gradeItemVO, Model model) {
		String itemExclusao = gradeItemVO.getItemExclusao();
		itemExclusao = itemExclusao.replace(itemExclusao.charAt(itemExclusao.length()-1), AcaoCadastroEnum.EXCLUIR.getKey());
		gradeItemVO.getGrade().setItensToString(ordenarLista(gradeItemVO.getGrade().getItensToString().replace(gradeItemVO.getItemExclusao(), itemExclusao)));
		model.addAttribute("grade", gradeItemVO.getGrade());
		return "cadastros/grade/grade-cadastro :: #gradeItens";
    }
    
	public RelatorioVO getRelatorioPdfVO(GradeFilter filtro, HttpServletRequest httpServletRequest) {
		PageWrapper<Grade> paginaWrapper = pesquisar(filtro, null, httpServletRequest);
		List<Grade> grades = paginaWrapper.getConteudo();
		
		RelatorioVO relatorioVO = new RelatorioVO();

		//titulo do documento
		relatorioVO.setTitulo("GRADES");
		
		//defini tamanho das colunas
		float[] tamanhoColunas = {9, 1};
		relatorioVO.setTamanhoColunas(tamanhoColunas);
		
		//obtem nome usuario
		relatorioVO.setUsuario(getUsuario().getNome());
		
		//monta colunas
		relatorioVO.setColunas(new ArrayList<RelatorioColunaVO>());
		relatorioVO.getColunas().add(new RelatorioColunaVO("NOME", Element.ALIGN_LEFT));
		relatorioVO.getColunas().add(new RelatorioColunaVO("ID", Element.ALIGN_CENTER));
			
		//monta dados
		relatorioVO.setConteudos(new ArrayList<>());
		for (Grade usuario : grades) {
			RelatorioConteudoVO conteudoVO = new RelatorioConteudoVO();
			conteudoVO.setConteudo(new ArrayList<RelatorioRegistroVO>());
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(usuario.getNome(), Element.ALIGN_LEFT));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(usuario.getId().toString(), Element.ALIGN_CENTER));
			
			relatorioVO.getConteudos().add(conteudoVO);
		}
		return relatorioVO;
	}
	
	
	private boolean nomeCadastrado(Grade grade) {
		List<Grade> gradesAux = null;
		if (grade.getId() != null) {
			gradesAux = gradeRepository.findByNomeAndIdNotInAndExcluidoIn(grade.getNome(), grade.getId(), '0');
		} else {
			gradesAux = gradeRepository.findByNomeAndExcluidoIn(grade.getNome(), '0');
		}
		return gradesAux != null && !gradesAux.isEmpty() ;
	}
	
	
	private String validarItens(GradeItemVO gradeItemVO, List<String> itens, Model model) {
		String retorno = null;
		List<String> tamanhos = new ArrayList<String>();
		List<String> ordens = new ArrayList<String>();
		for (String item : itens) {
			if (!item.isEmpty() && item.charAt(item.length()-1) != AcaoCadastroEnum.EXCLUIR.getKey()) {
				tamanhos.add(item.split("\\"+DelimitadorEnum.INFORMACAO.getKey())[0]);
				ordens.add(item.split("\\"+DelimitadorEnum.INFORMACAO.getKey())[1].split("\\"+DelimitadorEnum.ACAO.getKey())[0]);
			}
		}
		if (tamanhos.contains(gradeItemVO.getTamanho())) {
			model.addAttribute("grade", gradeItemVO.getGrade());
			model.addAttribute("msgErro", "Tamanho já inserido!");
			retorno = "cadastros/grade/item/gradeItem-cadastroModal :: #msgErro";
		} 
		if (ordens.contains(gradeItemVO.getOrdem().toString())) {
			model.addAttribute("grade", gradeItemVO.getGrade());
			model.addAttribute("msgErro", "Ordem já inserida!");
			retorno = "cadastros/grade/item/gradeItem-cadastroModal :: #msgErro";
		}
		return retorno;
	}
	
	
	private String ordenarLista(String itens) {  
	    List<String> listaOrdenada = Arrays.asList(itens.split("\\"+DelimitadorEnum.ITENS.getKey()));  
	    Collections.sort(listaOrdenada, new Comparator<String>() {  
	        @Override  
	        public int compare(String s1, String s2) {
	        	Integer n1 = Integer.valueOf(s1.substring(s1.indexOf(DelimitadorEnum.INFORMACAO.getKey())+1, s1.indexOf(DelimitadorEnum.ACAO.getKey())));
	        	Integer n2 = Integer.valueOf(s2.substring(s2.indexOf(DelimitadorEnum.INFORMACAO.getKey())+1, s2.indexOf(DelimitadorEnum.ACAO.getKey())));
	        	return n1.compareTo(n2);
	        }
	    });  
	    String lista = "";
	    for (String item : listaOrdenada) {
	    	lista += item + DelimitadorEnum.ITENS.getKey();
	    }
	    return lista;
	}  
}
