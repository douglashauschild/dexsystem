package br.com.dh.dexSystem.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;

import com.itextpdf.text.Element;

import br.com.dh.dexSystem.controller.PageWrapper;
import br.com.dh.dexSystem.manager.SetorManager;
import br.com.dh.dexSystem.model.ServiceInterface;
import br.com.dh.dexSystem.model.Setor;
import br.com.dh.dexSystem.model.vo.RelatorioColunaVO;
import br.com.dh.dexSystem.model.vo.RelatorioConteudoVO;
import br.com.dh.dexSystem.model.vo.RelatorioRegistroVO;
import br.com.dh.dexSystem.model.vo.RelatorioVO;
import br.com.dh.dexSystem.repository.SetorRepository;
import br.com.dh.dexSystem.repository.filter.SetorFilter;
import br.com.dh.dexSystem.util.FormatacaoUtils;
import br.com.dh.dexSystem.validation.ValidacaoSetor;

@Service
public class SetorService extends SincronizacaoServiceAbstract implements ServiceInterface<Setor, SetorFilter> {

	@Autowired
	private SetorManager setorManager;
	@Autowired
	private SetorRepository setorRepository;
	@Autowired
	private ValidacaoSetor validacaoSetor;

	
	public PageWrapper<Setor> pesquisar(SetorFilter filtro, Pageable pageable, HttpServletRequest httpServletRequest) {
		PageWrapper<Setor> pageWrapper = null;
		try {
			pageWrapper = new PageWrapper<Setor>(setorManager.filtrar(filtro, pageable), httpServletRequest);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pageWrapper;
	}
	
	public String validarGravacao(Setor setor, Model model, Errors errors) throws Exception {
		String retorno = null;
		setor = ajustarGravacao(setor);
		String msgRetorno = validacaoSetor.validar(setor);
		if (msgRetorno != null && !msgRetorno.isEmpty()) {
			retorno = verificaRetorno(msgRetorno, errors);
		} else {
			if (!setor.isFinalizado() && setor.getDataFim() != null) {
				retorno = verificaRetorno(validacaoSetor.validarExclusao(setor, false), errors);
			} else if (setor.isFinalizado() && setor.getDataFim() == null) {
				retorno = verificaRetorno(validacaoSetor.validarAtivacao(setor), errors);
			}
		}
		return retorno;
	}
	
	private String verificaRetorno(String msgValidacao, Errors errors) {
		String retorno = null;
		if (msgValidacao != null && !msgValidacao.isEmpty()) {
			errors.rejectValue(null, null, msgValidacao);
			retorno = "cadastros/setor/setor-cadastro";
		}
		return retorno;
	}
	
	
	public Setor ajustarGravacao(Setor setor) throws Exception {
		setor.setDataInicio(FormatacaoUtils.getData(setor.getDataInicioString()));
		setor.setDataFim(setor.getDataFimString() != null && !setor.getDataFimString().isEmpty() ? FormatacaoUtils.getData(setor.getDataFimString()) : null);
		return setor;
	}
	
	public void salvar(Setor setor) throws Exception {
		setorRepository.saveAndFlush((Setor) gerarChecksum(setor.getId() == null, setor));
	}
	
	public String validarExclusao(Setor setor) {
		return validacaoSetor.validarExclusao(setor, true);
	}
	
	public void excluir(Setor setor) throws Exception {
		setor.setExcluido('1');
		salvar(setor);
	}
	
	public Setor buscarPorId(Long id) {
		Setor setor = setorManager.buscarPorId(id);
		setor.setDataInicioString(setor.getDataInicio() != null ? FormatacaoUtils.getDataString(setor.getDataInicio()) : null);
		setor.setDataFimString(setor.getDataFim() != null ? FormatacaoUtils.getDataString(setor.getDataFim()) : null);
		setor.setFinalizado(setor.getDataFim() != null);
		return setor;
	}
	
	public List<Setor> buscarTodos(Long idEmpresa, Long idSetorSelecionado, boolean inclusiveInativos) {
		List<Setor> setores = setorManager.buscarTodosPorEmpresa(idEmpresa, inclusiveInativos);
		if (idSetorSelecionado != null) {
			Setor setorSelecionado = buscarPorId(idSetorSelecionado);
			if (!setores.contains(setorSelecionado)) {
				setores.add(setorSelecionado);
			}
		}
		return setores;
	}
	
	public RelatorioVO getRelatorioPdfVO(SetorFilter filtro, HttpServletRequest httpServletRequest) {
		PageWrapper<Setor> paginaWrapper = pesquisar(filtro, null, httpServletRequest);
		List<Setor> setors = paginaWrapper.getConteudo();
		
		RelatorioVO relatorioVO = new RelatorioVO();

		//titulo do documento
		relatorioVO.setTitulo("SETORES");
		
		//defini tamanho das colunas
		float[] tamanhoColunas = {3, (float) 1.3, (float) 1.3, 1, (float) 0.8};
		relatorioVO.setTamanhoColunas(tamanhoColunas);
		
		//obtem nome usuario
		relatorioVO.setUsuario(getUsuario().getNome());
		
		//monta colunas
		relatorioVO.setColunas(new ArrayList<RelatorioColunaVO>());
		relatorioVO.getColunas().add(new RelatorioColunaVO("SETOR", Element.ALIGN_LEFT));
		relatorioVO.getColunas().add(new RelatorioColunaVO("DATA INÍCIO", Element.ALIGN_CENTER));
		relatorioVO.getColunas().add(new RelatorioColunaVO("DATA FIM", Element.ALIGN_CENTER));
		relatorioVO.getColunas().add(new RelatorioColunaVO("STATUS", Element.ALIGN_CENTER));
		relatorioVO.getColunas().add(new RelatorioColunaVO("ID", Element.ALIGN_CENTER));
			
		//monta dados
		relatorioVO.setConteudos(new ArrayList<>());
		for (Setor setor : setors) {
			RelatorioConteudoVO conteudoVO = new RelatorioConteudoVO();
			conteudoVO.setConteudo(new ArrayList<RelatorioRegistroVO>());
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(setor.getNome(), Element.ALIGN_LEFT));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(setor.getDataInicioFormatada(), Element.ALIGN_CENTER));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(setor.getDataFimFormatada(), Element.ALIGN_CENTER));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(setor.getStatusDescricao(), Element.ALIGN_CENTER));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(setor.getId().toString(), Element.ALIGN_CENTER));
			
			relatorioVO.getConteudos().add(conteudoVO);
		}
		return relatorioVO;
	}
}
