package br.com.dh.dexSystem.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.itextpdf.text.Element;

import br.com.dh.dexSystem.controller.PageWrapper;
import br.com.dh.dexSystem.manager.NivelAcessoManager;
import br.com.dh.dexSystem.model.NivelAcesso;
import br.com.dh.dexSystem.model.Usuario;
import br.com.dh.dexSystem.model.VwMenuAcesso;
import br.com.dh.dexSystem.model.VwMenuAcessoPermissao;
import br.com.dh.dexSystem.model.constant.PermissoesEnum;
import br.com.dh.dexSystem.model.vo.RelatorioColunaVO;
import br.com.dh.dexSystem.model.vo.RelatorioConteudoVO;
import br.com.dh.dexSystem.model.vo.RelatorioRegistroVO;
import br.com.dh.dexSystem.model.vo.RelatorioVO;
import br.com.dh.dexSystem.repository.NivelAcessoRepository;
import br.com.dh.dexSystem.repository.VwMenuAcessoPermissaoRepository;
import br.com.dh.dexSystem.repository.VwMenuAcessoRepository;
import br.com.dh.dexSystem.repository.filter.NivelAcessoFilter;

@Service
public class NivelAcessoService extends ServiceExtend {

	@Autowired
	private NivelAcessoManager nivelAcessoManager;
	
	@Autowired
	private NivelAcessoRepository nivelAcessoRepository;
	
	@Autowired
	private NivelAcessoPermissaoService nivelAcessoPermissaoService;
	
	@Autowired
	private VwMenuAcessoRepository vwMenuAcessoRepository;
	
	@Autowired
	private VwMenuAcessoPermissaoRepository vwMenuAcessoPermissaoRepository;
	
	@Autowired
	private UsuarioService usuarioService;

	
	public PageWrapper<NivelAcesso> pesquisar(NivelAcessoFilter filtro, Pageable pageable, HttpServletRequest httpServletRequest) {
		return new PageWrapper<>(nivelAcessoManager.filtrar(filtro, pageable), httpServletRequest);
	}
	
	public List<VwMenuAcessoPermissao> getMenuAcessoPermissao() {
		return getMenuAcessoPermissao(null);
	}
	
	public List<VwMenuAcessoPermissao> getMenuAcessoPermissao(Long idNivelAcesso) {
		List<VwMenuAcessoPermissao> menusAcesso = new ArrayList<VwMenuAcessoPermissao>(); 
		if (idNivelAcesso != null) {
			menusAcesso = vwMenuAcessoPermissaoRepository.findByIdNivelAcesso(idNivelAcesso);
		} else {
			List<VwMenuAcesso> menus = vwMenuAcessoRepository.findAll();
			for (VwMenuAcesso menuAcesso : menus) {
				if (menuAcesso.getId().equals(1L) || menuAcesso.getUri() == null) {
					menusAcesso.add(new VwMenuAcessoPermissao(menuAcesso.getId(), menuAcesso.getMenu(), menuAcesso.getIdMenuPai(), menuAcesso.getUri(), '1', '0', '0', '0'));
				} else {
					menusAcesso.add(new VwMenuAcessoPermissao(menuAcesso.getId(), menuAcesso.getMenu(), menuAcesso.getIdMenuPai(), menuAcesso.getUri(), '1', '1', '1', '1'));
				}
			}
		}
		return menusAcesso;
	}
	
	public Map<String,Map<String,Boolean>> getPermissoes(Long idNivelAcesso) {
		Map<String,Map<String,Boolean>> permissoes = new HashMap<String, Map<String,Boolean>>();
		List<VwMenuAcessoPermissao> listaPermissoes = getMenuAcessoPermissao(idNivelAcesso);
		for (VwMenuAcessoPermissao acessoPermissao: listaPermissoes) {
			Map<String,Boolean> auxPermissoes = new HashMap<String,Boolean>();
			boolean acessar = (acessoPermissao.getAcessar().equals('1'));
			auxPermissoes.put(PermissoesEnum.ACESSAR.getKey(), acessar);
			boolean inserir = (acessoPermissao.getInserir().equals('1'));
			auxPermissoes.put(PermissoesEnum.INSERIR.getKey(), inserir);
			boolean alterar = (acessoPermissao.getAlterar().equals('1'));
			auxPermissoes.put(PermissoesEnum.ALTERAR.getKey(), alterar);
			boolean excluir = (acessoPermissao.getExcluir().equals('1'));
			auxPermissoes.put(PermissoesEnum.EXCLUIR.getKey(), excluir);
			permissoes.put(acessoPermissao.getUri(), auxPermissoes);
		}
		return permissoes;
	}
	
	public Long getProximoNivel() {
		return nivelAcessoRepository.getMaxNivel()+1;
	}
	
	public void salvar(NivelAcesso nivelAcesso) {
		nivelAcessoRepository.saveAndFlush(nivelAcesso);
		nivelAcessoPermissaoService.salvarAcessos(nivelAcesso);
	}
	
	public void excluir(NivelAcesso nivelAcesso) {
		nivelAcesso.setExcluido('1');
		salvar(nivelAcesso);
	}
	
	public NivelAcesso buscarPorId(Long id) {
		return nivelAcessoRepository.getOne(id);
	}
	
	public NivelAcesso buscarPorNivel(Long nivel) {
		return nivelAcessoRepository.buscarPorNivel(nivel);
	}
	
	public String isValidaGravacao(NivelAcesso nivelAcesso) {
		String retorno = null;
		NivelAcesso nivelAcessoAux = null;
		if (nivelAcesso.getId() != null) {
			nivelAcessoAux = nivelAcessoRepository.buscarPorNivelDiferenteId(nivelAcesso.getId(), nivelAcesso.getNivel());
		} else {
			nivelAcessoAux = nivelAcessoRepository.buscarPorNivel(nivelAcesso.getNivel());
		}
		if (nivelAcessoAux != null) {
			retorno = "Já existe um nível de acesso cadastrado com este nível!";
		} 
		return retorno;
	}
	
	public String isValidaExclusao(NivelAcesso nivelAcesso) {
		String retorno = null;
		if (nivelAcesso.getNivel() != null && nivelAcesso.getNivel().equals(1L)) {
			retorno = "Não é possível excluir o Nível de Acesso 1!";
		} else {
			List<Usuario> usuarios = usuarioService.buscarPorNivelAcesso(nivelAcesso.getId());
			if (usuarios != null && !usuarios.isEmpty()) {
				retorno = "Este nível de acesso não pode ser excluído pois está sendo utilizado no cadastro de usuários!";
			}
		}
		return retorno;
	}
	
	public RelatorioVO getRelatorioPdfVO(NivelAcessoFilter filtro, HttpServletRequest httpServletRequest) {
		PageWrapper<NivelAcesso> paginaWrapper = pesquisar(filtro, null, httpServletRequest);
		List<NivelAcesso> niveisAcesso = paginaWrapper.getConteudo();
		
		RelatorioVO relatorioVO = new RelatorioVO();

		//titulo do documento
		relatorioVO.setTitulo("NÍVEIS DE ACESSO");
		
		//defini tamanho das colunas
		float[] tamanhoColunas = {1, 8, 1};
		relatorioVO.setTamanhoColunas(tamanhoColunas);
		
		//obtem nome usuario
		relatorioVO.setUsuario(getUsuario().getNome());
		
		//monta colunas
		relatorioVO.setColunas(new ArrayList<RelatorioColunaVO>());
		relatorioVO.getColunas().add(new RelatorioColunaVO("NÍVEL", Element.ALIGN_CENTER));
		relatorioVO.getColunas().add(new RelatorioColunaVO("NOME", Element.ALIGN_LEFT));
		relatorioVO.getColunas().add(new RelatorioColunaVO("ID", Element.ALIGN_CENTER));
			
		//monta dados
		relatorioVO.setConteudos(new ArrayList<>());
		for (NivelAcesso nivelAcesso : niveisAcesso) {
			RelatorioConteudoVO conteudoVO = new RelatorioConteudoVO();
			conteudoVO.setConteudo(new ArrayList<RelatorioRegistroVO>());
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(nivelAcesso.getNivel().toString(), Element.ALIGN_CENTER));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(nivelAcesso.getNome(), Element.ALIGN_LEFT));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(nivelAcesso.getId().toString(), Element.ALIGN_CENTER));
			
			relatorioVO.getConteudos().add(conteudoVO);
		}
		return relatorioVO;
	}
}