package br.com.dh.dexSystem.service;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

import br.com.dh.dexSystem.model.ControleSincronizacao;
import br.com.dh.dexSystem.model.SincronizacaoAbstract;
import br.com.dh.dexSystem.model.vo.SincronizacaoVO;
import br.com.dh.dexSystem.repository.ControleSincronizacaoRepository;

@Service
public class ControleSincronizacaoService implements Serializable {

	private static final long serialVersionUID = -2321093814968516173L;
	
	@Autowired
	private ControleSincronizacaoRepository controleSincronizacaoRepository;

	
	public boolean salvarSincronizacao(SincronizacaoVO sincronizacaoVO) {
		boolean retorno = true;
		try {
			Long idDispositivo = sincronizacaoVO.getIdDispositivo();
			String tabela = sincronizacaoVO.getTabela();
			
			ObjectMapper objectMapper = new ObjectMapper();
			ArrayNode arrayNode = (ArrayNode) objectMapper.readTree(sincronizacaoVO.getJson());
			for (JsonNode jsonNode : arrayNode) {
				String checksum = jsonNode.get("checksum").asText();
				String checksumAlteracao = jsonNode.get("checksumAlteracao").asText();
				gravar(idDispositivo, tabela, checksum, checksumAlteracao);
			}
		} catch(Exception e) {
			e.printStackTrace();
			retorno = false;
		}
		return retorno;
	}
	
	
	public boolean salvarRecebimentoSincronizacao(SincronizacaoVO sincronizacaoVO, SincronizacaoAbstract sincronizacaoAbstract) {
		boolean retorno = true;
		try {
			Long idDispositivo = sincronizacaoVO.getIdDispositivo();
			String tabela = sincronizacaoVO.getTabela();
			gravar(idDispositivo, tabela, sincronizacaoAbstract.getChecksum(), sincronizacaoAbstract.getChecksumAlteracao());
			
		} catch(Exception e) {
			e.printStackTrace();
			retorno = false;
		}
		return retorno;
	}
	
	
	private void gravar(Long idDispositivo, String tabela, String checksum, String checksumAlteracao) {
		ControleSincronizacao controleSincronizacao = controleSincronizacaoRepository.findByIdDispositivoAndChecksum(idDispositivo, checksum);
		if (controleSincronizacao != null) {
			controleSincronizacao.setChecksumAlteracao(checksumAlteracao);
		} else {
			controleSincronizacao = new ControleSincronizacao(idDispositivo, tabela, checksum, checksumAlteracao);
		}
		controleSincronizacaoRepository.save(controleSincronizacao);
	}
}
