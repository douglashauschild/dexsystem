package br.com.dh.dexSystem.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;

import com.itextpdf.text.Element;

import br.com.dh.dexSystem.controller.PageWrapper;
import br.com.dh.dexSystem.manager.AtividadeManager;
import br.com.dh.dexSystem.manager.EmpregadoManager;
import br.com.dh.dexSystem.model.Atividade;
import br.com.dh.dexSystem.model.Empregado;
import br.com.dh.dexSystem.model.ServiceInterface;
import br.com.dh.dexSystem.model.vo.RelatorioColunaVO;
import br.com.dh.dexSystem.model.vo.RelatorioConteudoVO;
import br.com.dh.dexSystem.model.vo.RelatorioRegistroVO;
import br.com.dh.dexSystem.model.vo.RelatorioVO;
import br.com.dh.dexSystem.repository.AtividadeRepository;
import br.com.dh.dexSystem.repository.filter.AtividadeFilter;
import br.com.dh.dexSystem.util.FormatacaoUtils;
import br.com.dh.dexSystem.validation.ValidacaoAtividade;

@Service
public class AtividadeService extends SincronizacaoServiceAbstract implements ServiceInterface<Atividade, AtividadeFilter> {

	@Autowired
	private AtividadeManager atividadeManager;
	@Autowired
	private AtividadeRepository atividadeRepository;
	@Autowired
	private EmpregadoManager empregadoManager;
	@Autowired
	private ValidacaoAtividade validacaoAtividade;
	@Autowired
	private AtualizacaoEpiService atualizacaoEpiService;

	
	public PageWrapper<Atividade> pesquisar(AtividadeFilter filtro, Pageable pageable, HttpServletRequest httpServletRequest) {
		PageWrapper<Atividade> pageWrapper = null;
		try {
			pageWrapper = new PageWrapper<Atividade>(atividadeManager.filtrar(filtro, pageable), httpServletRequest);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pageWrapper;
	}
	
	public String validarGravacao(Atividade atividade, Model model, Errors errors) throws Exception {
		String retorno = null;
		atividade = ajustarGravacao(atividade);
		String msgRetorno = validacaoAtividade.validar(atividade);
		if (msgRetorno != null && !msgRetorno.isEmpty()) {
			errors.rejectValue(null, null, msgRetorno);
			retorno = "cadastros/atividade/atividade-cadastro";
		} else {
			if (!atividade.isFinalizada() && atividade.getDataFim() != null) {
				model.addAttribute("msgConfirmacao", "Finalizando esta atividade você não poderá mais editar este cadastro!<br>Você tem certeza que deseja continuar?");
				model.addAttribute("modalConfirmacao", true);
				retorno = "cadastros/atividade/atividade-cadastro";
			}
		}
		return retorno;
	}
	
	public Atividade ajustarGravacao(Atividade atividade) throws Exception {
		atividade.setDataInicio(FormatacaoUtils.getData(atividade.getDataInicioString()));
		atividade.setDataFim(atividade.getDataFimString() != null && !atividade.getDataFimString().isEmpty() ? FormatacaoUtils.getData(atividade.getDataFimString()) : null);
		atividade.setEmpregado(empregadoManager.buscarPorId(atividade.getIdEmpregado()));
		return atividade;
	}
	
	public void salvar(Atividade atividade, boolean atualizarEpis) throws Exception {
		salvar(atividade);
		if (atualizarEpis) {
			atualizacaoEpiService.atualizarEpiPorAtividade(atividade);
		}
	}
	
	public void salvar(Atividade atividade) throws Exception {
		atividadeRepository.saveAndFlush((Atividade) gerarChecksum(atividade.getId() == null, atividade));
	}
	
	public void excluir(Atividade atividade) throws Exception {
		atividade.setExcluido('1');
		salvar(atividade);
	}
	
	public void finalizar(Empregado empregado) throws Exception {
		Atividade atividade = atividadeManager.buscarAtivaPorEmpregado(empregado);
		if (atividade != null) {
			atividade.setDataFim(empregado.getDataDemissao());
			salvar(atividade);
		}
	}
	
	public Atividade buscarPorId(Long id) {
		Atividade atividade = atividadeManager.buscarPorId(id);
		atividade.setDataInicioString(atividade.getDataInicio() != null ? FormatacaoUtils.getDataString(atividade.getDataInicio()) : null);
		atividade.setDataFimString(atividade.getDataFim() != null ? FormatacaoUtils.getDataString(atividade.getDataFim()) : null);
		atividade.setIdEmpresa(atividade.getCargo().getSetor().getIdEmpresa());
		atividade.setIdSetor(atividade.getCargo().getIdSetor());
		atividade.setFinalizada(atividade.getDataFim() != null);
		return atividade;
	}
	
	public RelatorioVO getRelatorioPdfVO(AtividadeFilter filtro, HttpServletRequest httpServletRequest) {
		PageWrapper<Atividade> paginaWrapper = pesquisar(filtro, null, httpServletRequest);
		List<Atividade> atividades = paginaWrapper.getConteudo();
		
		RelatorioVO relatorioVO = new RelatorioVO();

		//titulo do documento
		relatorioVO.setTitulo("ATIVIDADES");
		
		//defini tamanho das colunas
		float[] tamanhoColunas = { 3, 2, 2, 2, (float) 1.3, (float) 1.3, (float) 0.8};
		relatorioVO.setTamanhoColunas(tamanhoColunas);
		
		//obtem nome usuario
		relatorioVO.setUsuario(getUsuario().getNome());
		
		//monta colunas
		relatorioVO.setColunas(new ArrayList<RelatorioColunaVO>());
		relatorioVO.getColunas().add(new RelatorioColunaVO("EMPREGADO", Element.ALIGN_LEFT));
		relatorioVO.getColunas().add(new RelatorioColunaVO("SETOR", Element.ALIGN_LEFT));
		relatorioVO.getColunas().add(new RelatorioColunaVO("CARGO", Element.ALIGN_LEFT));
		relatorioVO.getColunas().add(new RelatorioColunaVO("FUNÇÃO", Element.ALIGN_LEFT));
		relatorioVO.getColunas().add(new RelatorioColunaVO("DATA INÍCIO", Element.ALIGN_CENTER));
		relatorioVO.getColunas().add(new RelatorioColunaVO("DATA FIM", Element.ALIGN_CENTER));
		relatorioVO.getColunas().add(new RelatorioColunaVO("ID", Element.ALIGN_CENTER));
			
		//monta dados
		relatorioVO.setConteudos(new ArrayList<>());
		for (Atividade atividade : atividades) {
			RelatorioConteudoVO conteudoVO = new RelatorioConteudoVO();
			conteudoVO.setConteudo(new ArrayList<RelatorioRegistroVO>());
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(atividade.getEmpregado().getPessoaFisica().getPessoa().getNome(), Element.ALIGN_LEFT));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(atividade.getCargo().getSetor().getNome(), Element.ALIGN_LEFT));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(atividade.getCargo().getNome(), Element.ALIGN_LEFT));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(atividade.getFuncao(), Element.ALIGN_LEFT));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(atividade.getDataInicioFormatada(), Element.ALIGN_CENTER));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(atividade.getDataFimFormatada(), Element.ALIGN_CENTER));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(atividade.getId().toString(), Element.ALIGN_CENTER));
			
			relatorioVO.getConteudos().add(conteudoVO);
		}
		return relatorioVO;
	}
}
