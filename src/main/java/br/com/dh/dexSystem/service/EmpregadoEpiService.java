package br.com.dh.dexSystem.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;

import com.itextpdf.text.Element;

import br.com.dh.dexSystem.controller.PageWrapper;
import br.com.dh.dexSystem.manager.EmpregadoEpiManager;
import br.com.dh.dexSystem.model.Empregado;
import br.com.dh.dexSystem.model.EmpregadoEpi;
import br.com.dh.dexSystem.model.Epi;
import br.com.dh.dexSystem.model.ServiceInterface;
import br.com.dh.dexSystem.model.constant.StatusEmpregadoEpiEnum;
import br.com.dh.dexSystem.model.constant.TipoConfirmacaoEnum;
import br.com.dh.dexSystem.model.constant.VidaUtilEnum;
import br.com.dh.dexSystem.model.vo.DetalhesEntregaVO;
import br.com.dh.dexSystem.model.vo.EntregaVO;
import br.com.dh.dexSystem.model.vo.EpiUtilizadoVO;
import br.com.dh.dexSystem.model.vo.EstatisticasEntregaVO;
import br.com.dh.dexSystem.model.vo.FichaEpiVO;
import br.com.dh.dexSystem.model.vo.RelatorioColunaVO;
import br.com.dh.dexSystem.model.vo.RelatorioConteudoVO;
import br.com.dh.dexSystem.model.vo.RelatorioRegistroVO;
import br.com.dh.dexSystem.model.vo.RelatorioVO;
import br.com.dh.dexSystem.repository.EmpregadoEpiRepository;
import br.com.dh.dexSystem.repository.filter.EmpregadoEpiFilter;
import br.com.dh.dexSystem.util.FormatacaoUtils;

@Service
public class EmpregadoEpiService extends SincronizacaoServiceAbstract implements ServiceInterface<EmpregadoEpi, EmpregadoEpiFilter> {

	@Autowired
	private EmpregadoEpiManager empregadoEpiManager;
	@Autowired
	private EmpregadoEpiRepository empregadoEpiRepository;
	@Autowired
	private CombosService combosService;
	@Autowired
	private FichaEpiService fichaEpiService;


	public PageWrapper<EmpregadoEpi> pesquisar(EmpregadoEpiFilter filtro, Pageable pageable, HttpServletRequest httpServletRequest) {
		PageWrapper<EmpregadoEpi> pageWrapper = null;
		try {
			Page<EmpregadoEpi> lista = new PageImpl<>(new ArrayList<EmpregadoEpi>(), pageable, 0);
			if (filtro.getIdEmpresa() != null && filtro.getIdEmpresa() != 0L && filtro.getIdEmpregado() != null && filtro.getIdEmpregado() != 0L) {
				lista = empregadoEpiManager.filtrar(filtro, pageable);
			}
			pageWrapper = new PageWrapper<EmpregadoEpi>(lista, httpServletRequest);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pageWrapper;
	}
	
	public String validarGravacao(EmpregadoEpi empregadoEpi, Model model, Errors errors) throws Exception {
		String retorno = null;
		empregadoEpi = ajustarGravacao(empregadoEpi);
//		String msgRetorno = validacaoEmpregadoEpi.validar(empregadoEpi);
//		if (msgRetorno != null && !msgRetorno.isEmpty()) {
//			retorno = verificaRetorno(msgRetorno, errors);
//		} else {
//			if (!empregadoEpi.isFinalizado() && empregadoEpi.getDataFim() != null) {
//				model.addAttribute("msgConfirmacao", "Finalizando esta recomendação de EPI você não poderá mais editar este cadastro!<br>Você tem certeza que deseja continuar?");
//				model.addAttribute("modalConfirmacao", true);
//				retorno = "cadastros/empregadoEpi/empregadoEpi-cadastro";
//			}
//		}
		return retorno;
	}
	
	public EmpregadoEpi ajustarGravacao(EmpregadoEpi empregadoEpi) throws Exception {
		empregadoEpi.setDataEntregaString(null);
		empregadoEpi.setDataPrevistaEntrega(FormatacaoUtils.getData(empregadoEpi.getDataPrevistaEntregaString()));
		empregadoEpi.setIdUsuario(getUsuario().getId());
		empregadoEpi.setInclusaoManual('1');
		return empregadoEpi;
	}
	
	public void salvarSincronizacao(EmpregadoEpi empregadoEpi) {
		EmpregadoEpi empregadoEpiAux = empregadoEpiRepository.findByChecksum(empregadoEpi.getChecksum());
		if (empregadoEpiAux != null) {
			empregadoEpi.setId(empregadoEpiAux.getId());
		} else {
			empregadoEpi.setId(0L);
		}
		empregadoEpiRepository.saveAndFlush(empregadoEpi);
	}
	
	public void salvar(EmpregadoEpi empregadoEpi) throws Exception {
		empregadoEpiRepository.saveAndFlush((EmpregadoEpi) gerarChecksum(empregadoEpi.getId() == null, empregadoEpi));
	}
	
	public void excluir(EmpregadoEpi empregadoEpi) throws Exception {
		empregadoEpi.setExcluido('1');
		salvar(empregadoEpi);
	}
	
	public EmpregadoEpi buscarPorId(Long id) {
		return empregadoEpiManager.buscarPorId(id);
	}
	
	public List<EmpregadoEpi> buscarEpiPrevisto(Long idEmpregado, Long idEpi) {
		return empregadoEpiManager.buscarEpiPrevisto(idEmpregado, idEpi);
	}
	
	
	public EntregaVO getEntregaVO(Long idEmpresa, Long idEmpregado, String[] idsEpis) {
		List<Long> idsEpisLista = new ArrayList<Long>();
		for (int i = 0; i < idsEpis.length; i++) {
			idsEpisLista.add(Long.valueOf(idsEpis[i]));
		}
		List<EmpregadoEpi> episEntrega = empregadoEpiManager.buscarPorIds(idsEpisLista);
		for (EmpregadoEpi empregadoEpi : episEntrega) {
			empregadoEpi.setDataEntrega(new Date());
			if (empregadoEpi.getDataConfirmacao() != null) {
				empregadoEpi.setDevolver(true);
				empregadoEpi.setSubstituir(true);
				empregadoEpi.setIdMotivo(null);
			}
			if (empregadoEpi.getEpi().getGrade() != null) {
				empregadoEpi.setGradeItens(combosService.getGradeItensCombo(empregadoEpi.getEpi().getGrade().getId()));
			}
		}
		return new EntregaVO(idEmpresa, idEmpregado, episEntrega);
	}
	
	
	public void confirmarEntrega(EntregaVO entregaVO) throws Exception {
		Date dataAtual = new Date();
		for (EmpregadoEpi empregadoEpiLista : entregaVO.getEpisEntrega()) {
			EmpregadoEpi empregadoEpi = empregadoEpiManager.buscarPorId(empregadoEpiLista.getId());
			if (empregadoEpi.getDataConfirmacao() == null) {
				confirmar(empregadoEpi, empregadoEpiLista, dataAtual);
			} else if (empregadoEpiLista.isSubstituir()) {
				substituir(empregadoEpi, empregadoEpiLista, dataAtual);
			} else if (empregadoEpiLista.isDevolver() && !empregadoEpiLista.isSubstituir()) {
				devolver(empregadoEpi, dataAtual);
			}
		}
	}
	
	private void confirmar(EmpregadoEpi empregadoEpi, EmpregadoEpi empregadoEpiLista, Date dataAtual) throws Exception {
		empregadoEpi.setDataConfirmacao(dataAtual);
		empregadoEpi.setDataEntrega(dataAtual);
		empregadoEpi.setTipoConfirmacao(TipoConfirmacaoEnum.SENHA.getKey());
		empregadoEpi.setDataPrevistaTroca(getDataPrevistaTroca(empregadoEpi));
		if (empregadoEpiLista.getIdGradeItem() != null) {
			empregadoEpi.setIdGradeItem(empregadoEpiLista.getIdGradeItem());
		}
		if (empregadoEpiLista.getIdMotivo() != null && empregadoEpiLista.getIdMotivo() != 0L) {
			empregadoEpi.setIdMotivo(empregadoEpiLista.getIdMotivo());
		}
		salvar(empregadoEpi);
	}
	
	private void substituir(EmpregadoEpi empregadoEpi, EmpregadoEpi empregadoEpiLista, Date dataAtual) throws Exception {
		EmpregadoEpi novoEmpregadoEpi = empregadoEpi.clone();
		novoEmpregadoEpi.setId(null);
		novoEmpregadoEpi.setDataPrevistaEntrega(dataAtual);
		novoEmpregadoEpi.setDataEntrega(null);
		novoEmpregadoEpi.setDataPrevistaTroca(null);
		novoEmpregadoEpi.setInclusaoManual(null);
		
		empregadoEpi.setDataSubstituicao(dataAtual);
		if (empregadoEpiLista.isDevolver()) {
			empregadoEpi.setDataDevolucao(dataAtual);
		}
		salvar(empregadoEpi);
		confirmar(novoEmpregadoEpi, empregadoEpiLista, dataAtual);
	}
	
	public void devolver(EmpregadoEpi empregadoEpi, Date dataAtual) throws Exception {
		empregadoEpi.setDataDevolucao(dataAtual);
		salvar(empregadoEpi);
	}
	
	
	
	public void finalizarEpis(Empregado empregado) throws Exception {
		List<EmpregadoEpi> empregadoEpis = empregadoEpiManager.buscarTodosNaoFinalizados(empregado.getId());
		for (EmpregadoEpi empregadoEpi : empregadoEpis) {
			if (empregadoEpi.isConfirmado()) {
				devolver(empregadoEpi, empregado.getDataDemissao());
			} else {
				excluir(empregadoEpi);
			}
		}
	}
	
	
	public Date getDataPrevistaTroca(EmpregadoEpi empregadoEpi) {
		return getDataPrevistaTroca(empregadoEpi.getEpi(), empregadoEpi.getQuantidade(), empregadoEpi.getDataEntrega());
	}
	public Date getDataPrevistaTroca(Epi epi, Integer qtd, Date dataBase) {
		Integer vidaUtil = epi.getVidaUtil();
		Character vidaUtilUn = epi.getVidaUtilUnidade();
		if (vidaUtilUn != null && vidaUtil != null) {
			vidaUtil = vidaUtil * qtd;
			if (vidaUtilUn.equals(VidaUtilEnum.DIA.getKey())) {
				return FormatacaoUtils.incluirDia(dataBase, vidaUtil);
			} else if (vidaUtilUn.equals(VidaUtilEnum.MES.getKey())) {
				return FormatacaoUtils.incluirMes(dataBase, vidaUtil);
			} else if (vidaUtilUn.equals(VidaUtilEnum.ANO.getKey())) {
				return FormatacaoUtils.incluirAno(dataBase, vidaUtil);
			}
		}
		return null;
	}
	
	
	public EstatisticasEntregaVO buscarEstatisticas(Long idEmpregado, HttpServletRequest httpServletRequest) {
		int aguardando = 0;
		int emDia = 0;
		int vencido = 0;
		int entregaVencida = 0;
		
		try {
			List<EmpregadoEpi> lista = empregadoEpiManager.buscarParaEstatisticas(idEmpregado);
			for (EmpregadoEpi empregadoEpi : lista) {
				if (empregadoEpi.getStatusDescricao().equals(StatusEmpregadoEpiEnum.AGUARDANDO.getDescricao())) {
					aguardando++;
				} else if (empregadoEpi.getStatusDescricao().equals(StatusEmpregadoEpiEnum.EM_DIA.getDescricao())) {
					emDia++;
				} else if (empregadoEpi.getStatusDescricao().equals(StatusEmpregadoEpiEnum.VENCIDO.getDescricao())) {
					vencido++;
				} else if (empregadoEpi.getStatusDescricao().equals(StatusEmpregadoEpiEnum.ENTREGA_VENCIDA.getDescricao())) {
					entregaVencida++;
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return new EstatisticasEntregaVO(aguardando, emDia, vencido, entregaVencida);
	}
	
	public DetalhesEntregaVO buscarDetalhes(Long idEmpregadoEpi, HttpServletRequest httpServletRequest) {
		DetalhesEntregaVO detalhesEntregaVO = null;
		EmpregadoEpi empregadoEpi = buscarPorId(idEmpregadoEpi);
		if (empregadoEpi != null) {
			detalhesEntregaVO = new DetalhesEntregaVO();
			detalhesEntregaVO.setEpi(empregadoEpi.getEpi().getNome());
			detalhesEntregaVO.setCa(empregadoEpi.getEpi().getCa());
			detalhesEntregaVO.setGrade(empregadoEpi.getEpi().getGrade() != null ? empregadoEpi.getEpi().getGrade().getNome() : "");
			detalhesEntregaVO.setTamanho(empregadoEpi.getGradeItem() != null ? empregadoEpi.getGradeItem().getTamanho() : "");
			detalhesEntregaVO.setQuantidade(empregadoEpi.getQuantidade().toString());
			detalhesEntregaVO.setPrevisaoEntrega(empregadoEpi.getDataPrevistaEntregaFormatada());
			detalhesEntregaVO.setEntrega(empregadoEpi.getDataEntregaFormatada());
			detalhesEntregaVO.setDataConfirmacao(empregadoEpi.getDataConfirmacaoFormatada());
			detalhesEntregaVO.setTipoConfirmacao(empregadoEpi.getTipoConfirmacaoString());
			detalhesEntregaVO.setPrevisaoTroca(empregadoEpi.getDataPrevistaTrocaFormatada());
			detalhesEntregaVO.setDataSubstituicao(empregadoEpi.getDataSubstituicaoFormatada());
			detalhesEntregaVO.setDataDevolucao(empregadoEpi.getDataDevolucaoFormatada());
		}
		return detalhesEntregaVO;
	}
	
	
	public RelatorioVO getRelatorioPdfVO(EmpregadoEpiFilter filtro, HttpServletRequest httpServletRequest) throws Exception {
		PageWrapper<EmpregadoEpi> paginaWrapper = pesquisar(filtro, null, httpServletRequest);
		List<EmpregadoEpi> empregadosEpi = paginaWrapper.getConteudo();
		
		RelatorioVO relatorioVO = new RelatorioVO();

		//titulo do documento
		relatorioVO.setTitulo("EPI'S DO EMPREGADO");
		
		//defini tamanho das colunas
		float[] tamanhoColunas = { 2, (float) 0.6, (float) 0.4, (float) 0.8, (float) 0.8, (float) 0.8, 1};
		relatorioVO.setTamanhoColunas(tamanhoColunas);
		
		//obtem nome usuario
		relatorioVO.setUsuario(getUsuario().getNome());
		
		//monta colunas
		relatorioVO.setColunas(new ArrayList<RelatorioColunaVO>());
		relatorioVO.getColunas().add(new RelatorioColunaVO("EPI", Element.ALIGN_LEFT));
		relatorioVO.getColunas().add(new RelatorioColunaVO("CA", Element.ALIGN_CENTER));
		relatorioVO.getColunas().add(new RelatorioColunaVO("QTD", Element.ALIGN_RIGHT));
		relatorioVO.getColunas().add(new RelatorioColunaVO("PREV. ENTREGA", Element.ALIGN_CENTER));
		relatorioVO.getColunas().add(new RelatorioColunaVO("ENTREGA", Element.ALIGN_CENTER));
		relatorioVO.getColunas().add(new RelatorioColunaVO("PREV. TROCA", Element.ALIGN_CENTER));
		relatorioVO.getColunas().add(new RelatorioColunaVO("STATUS", Element.ALIGN_CENTER));
			
		//monta dados
		relatorioVO.setConteudos(new ArrayList<>());
		for (EmpregadoEpi empregadoEpi : empregadosEpi) {
			RelatorioConteudoVO conteudoVO = new RelatorioConteudoVO();
			conteudoVO.setConteudo(new ArrayList<RelatorioRegistroVO>());
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(empregadoEpi.getEpi().getNome(), Element.ALIGN_LEFT));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(empregadoEpi.getEpi().getCa(), Element.ALIGN_CENTER));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(empregadoEpi.getQuantidade().toString(), Element.ALIGN_RIGHT));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(empregadoEpi.getDataPrevistaEntregaFormatada(), Element.ALIGN_CENTER));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(empregadoEpi.getDataEntregaFormatada(), Element.ALIGN_CENTER));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(empregadoEpi.getDataPrevistaTrocaFormatada(), Element.ALIGN_CENTER));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(empregadoEpi.getStatusDescricao(), Element.ALIGN_CENTER));
			
			relatorioVO.getConteudos().add(conteudoVO);
		}
		return relatorioVO;
	}
	
	public FichaEpiVO getFichaEpiPdfVO(EmpregadoEpiFilter filtro, HttpServletRequest httpServletRequest) {
		return fichaEpiService.getFichaEpiVO(filtro.getIdEmpregado());
	}
	
	public Integer buscarTotalEpisEntregues(Long idEmpresa) {
		return buscarTotalEntregasRealizadas(idEmpresa, null, null, true);
	}
	
	public Integer buscarTotalEntregasRealizadas(Long idEmpresa) {
		return buscarTotalEntregasRealizadas(idEmpresa, null, null);
	}
	
	public Integer buscarTotalEntregasRealizadas(Long idEmpresa, Date dataInicial, Date dataFinal) {
		return buscarTotalEntregasRealizadas(idEmpresa, dataInicial, dataFinal, false);
	}
	
	public Integer buscarTotalEntregasRealizadas(Long idEmpresa, Date dataInicial, Date dataFinal, boolean considerarQuantidade) {
		return empregadoEpiManager.buscarTotalEntregasRealizadas(idEmpresa, dataInicial, dataFinal, considerarQuantidade);
	}
	
	public List<EmpregadoEpi> buscarTudoPorEmpresa(Long idEmpresa) {
		return empregadoEpiManager.buscarTudoPorEmpresa(idEmpresa);
	}
	
	public List<EpiUtilizadoVO> buscarEpisMaisUtilizados() {
		return empregadoEpiManager.buscarEpisMaisUtilizados();
	}
	
}