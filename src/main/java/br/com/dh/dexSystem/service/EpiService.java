package br.com.dh.dexSystem.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;

import com.itextpdf.text.Element;

import br.com.dh.dexSystem.controller.PageWrapper;
import br.com.dh.dexSystem.manager.EpiManager;
import br.com.dh.dexSystem.model.Epi;
import br.com.dh.dexSystem.model.ServiceInterface;
import br.com.dh.dexSystem.model.vo.RelatorioColunaVO;
import br.com.dh.dexSystem.model.vo.RelatorioConteudoVO;
import br.com.dh.dexSystem.model.vo.RelatorioRegistroVO;
import br.com.dh.dexSystem.model.vo.RelatorioVO;
import br.com.dh.dexSystem.repository.EpiRepository;
import br.com.dh.dexSystem.repository.filter.EpiFilter;
import br.com.dh.dexSystem.util.FormatacaoUtils;

@Service
public class EpiService extends SincronizacaoServiceAbstract implements ServiceInterface<Epi, EpiFilter> {

	@Autowired
	private EpiManager epiManager;
	@Autowired
	private EpiRepository epiRepository;

	
	public PageWrapper<Epi> pesquisar(EpiFilter filtro, Pageable pageable, HttpServletRequest httpServletRequest) {
		PageWrapper<Epi> pageWrapper = null;
		try {
			pageWrapper = new PageWrapper<>(epiManager.filtrar(filtro, pageable), httpServletRequest);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pageWrapper;
	}
	
	public String validarGravacao(Epi epi, Model model, Errors errors) {
		String retorno = null;
		if (epi.getCa() != null && !epi.getCa().isEmpty() && (epi.getValidadeCa() == null || epi.getValidadeCa().isEmpty())) {
			errors.rejectValue("validadeCa", null, "Preencha a validade do CA ou deixe o CA em branco!");
			retorno = "cadastros/epi/epi-cadastro";
		} else if ((epi.getCa() == null || epi.getCa().isEmpty()) && epi.getValidadeCa() != null && !epi.getValidadeCa().isEmpty()) {
			errors.rejectValue("ca", null, "Preencha o CA ou deixe a validade do CA em branco!");
			retorno = "cadastros/epi/epi-cadastro";
		} else {
			if (caCadastrado(epi)) {
				model.addAttribute("epi", epi);
				model.addAttribute("modalConfirmacao", true);
				retorno = "cadastros/epi/epi-cadastro";
			}
		}
		return retorno;
	}
	
	public Epi ajustarGravacao(Epi epi) throws Exception {
		epi.setCa(epi.getCa() != null && !epi.getCa().isEmpty() ? epi.getCa().trim() : null);
		epi.setDataValidadeCa(epi.getValidadeCa() != null && !epi.getValidadeCa().isEmpty() ? FormatacaoUtils.getData(epi.getValidadeCa()) : null);		
		epi.setIdGrade(epi.getIdGrade() != null && epi.getIdGrade() != 0L ? epi.getIdGrade() : null);
		epi.setIdUnidade(epi.getIdUnidade() != null && epi.getIdUnidade() != 0L ? epi.getIdUnidade() : null);
		epi.setDescartavel(epi.isDescartavelB() ? '1' : '0');
		return epi;
	}
	
	public void salvar(Epi epi) throws Exception {
		epiRepository.saveAndFlush((Epi) gerarChecksum(epi.getId() == null, epi));
	}
	
	public void excluir(Epi epi) throws Exception {
		epi.setExcluido('1');
		salvar(epi);
	}
	
	public Epi buscarPorId(Long id) {
		Epi epi = epiManager.buscarPorId(id);
		epi.setValidadeCa(epi.getDataValidadeCa() != null ? FormatacaoUtils.getDataString(epi.getDataValidadeCa()) : null);
		epi.setDescartavelB(epi.isDescartavel());
		return epi;
	}
	
	public List<Epi> buscarTodos() {
		return epiManager.buscarTodos();
	}
	
	public RelatorioVO getRelatorioPdfVO(EpiFilter filtro, HttpServletRequest httpServletRequest) {
		PageWrapper<Epi> paginaWrapper = pesquisar(filtro, null, httpServletRequest);
		List<Epi> epis = paginaWrapper.getConteudo();
		
		RelatorioVO relatorioVO = new RelatorioVO();

		//titulo do documento
		relatorioVO.setTitulo("EPIS");
		
		//defini tamanho das colunas
		float[] tamanhoColunas = {(float) 0.8, (float) 5.7, (float) 1.3, 1, (float) 0.8};
		relatorioVO.setTamanhoColunas(tamanhoColunas);
		
		//obtem nome usuario
		relatorioVO.setUsuario(getUsuario().getNome());
		
		//monta colunas
		relatorioVO.setColunas(new ArrayList<RelatorioColunaVO>());
		relatorioVO.getColunas().add(new RelatorioColunaVO("CA", Element.ALIGN_CENTER));
		relatorioVO.getColunas().add(new RelatorioColunaVO("NOME", Element.ALIGN_LEFT));
		relatorioVO.getColunas().add(new RelatorioColunaVO("VALIDADE CA", Element.ALIGN_CENTER));
		relatorioVO.getColunas().add(new RelatorioColunaVO("STATUS CA", Element.ALIGN_CENTER));
		relatorioVO.getColunas().add(new RelatorioColunaVO("ID", Element.ALIGN_CENTER));
			
		//monta dados
		relatorioVO.setConteudos(new ArrayList<>());
		for (Epi epi : epis) {
			RelatorioConteudoVO conteudoVO = new RelatorioConteudoVO();
			conteudoVO.setConteudo(new ArrayList<RelatorioRegistroVO>());
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(epi.getCa(), Element.ALIGN_CENTER));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(epi.getNome(), Element.ALIGN_LEFT));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(epi.getDataValidadeCaFormatada(), Element.ALIGN_CENTER));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(epi.getStatusCaDescricao(), Element.ALIGN_CENTER));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(epi.getId().toString(), Element.ALIGN_CENTER));
			
			relatorioVO.getConteudos().add(conteudoVO);
		}
		return relatorioVO;
	}
	
	private boolean caCadastrado(Epi epi) {
		List<Epi> epiAux = null;
		if (epi.getId() != null) {
			epiAux = epiRepository.findByCaAndIdNotInAndExcluidoIn(epi.getCa(), epi.getId(), '0');
		} else {
			epiAux = epiRepository.findByCaAndExcluidoIn(epi.getCa(), '0');
		}
		return epiAux != null && !epiAux.isEmpty() ;
	}
	
	public Epi consultarCa(String ca) throws Exception {
		Document doc = Jsoup.connect("https://consultaca.com/"+ca).get();
		String nome = doc.select("#titulo-equipamento h1").text();
		String validadeCa = doc.select("span.validade_ca").text();
		
		Epi epi = new Epi();
		epi.setValidadeCa(validadeCa);
		epi.setNome(nome);
		return epi;
	}
}
