package br.com.dh.dexSystem.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;

import com.itextpdf.text.Element;

import br.com.dh.dexSystem.controller.PageWrapper;
import br.com.dh.dexSystem.manager.PessoaFisicaManager;
import br.com.dh.dexSystem.model.Pessoa;
import br.com.dh.dexSystem.model.PessoaFisica;
import br.com.dh.dexSystem.model.ServiceInterface;
import br.com.dh.dexSystem.model.vo.RelatorioColunaVO;
import br.com.dh.dexSystem.model.vo.RelatorioConteudoVO;
import br.com.dh.dexSystem.model.vo.RelatorioRegistroVO;
import br.com.dh.dexSystem.model.vo.RelatorioVO;
import br.com.dh.dexSystem.repository.PessoaFisicaRepository;
import br.com.dh.dexSystem.repository.PessoaRepository;
import br.com.dh.dexSystem.repository.filter.PessoaFisicaFilter;
import br.com.dh.dexSystem.util.FormatacaoUtils;

@Service
public class PessoaFisicaService extends SincronizacaoServiceAbstract implements ServiceInterface<PessoaFisica, PessoaFisicaFilter> {

	@Autowired
	private PessoaRepository pessoaRepository;
	@Autowired
	private PessoaFisicaRepository pessoaFisicaRepository;
	@Autowired
	private PessoaFisicaManager pessoaFisicaManager;
	@Autowired
	private EnderecoService enderecoService;
	@Autowired
	private ContatoService contatoService;

	
	public PageWrapper<PessoaFisica> pesquisar(PessoaFisicaFilter filtro, Pageable pageable, HttpServletRequest httpServletRequest) {
		PageWrapper<PessoaFisica> pageWrapper = null;
		try {
			pageWrapper = new PageWrapper<PessoaFisica>(pessoaFisicaManager.filtrar(filtro, pageable), httpServletRequest);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pageWrapper;
	}
	
	public String validarGravacao(PessoaFisica pessoaFisica, Model model, Errors errors) throws Exception {
		String retorno = null;
		pessoaFisica = ajustarGravacao(pessoaFisica);
		PessoaFisica pessoaFisicaAux = pessoaFisicaManager.isValidarExistencia(pessoaFisica);
		if (pessoaFisicaAux != null) {
			errors.rejectValue(null, null, "Já existe uma pessoa física cadastrada com este CPF!");
			retorno = "cadastros/pessoaFisica/pessoaFisica-cadastro";
			model.addAttribute("contatosToString", pessoaFisica.getContatosToString());
		}
		return retorno;
	}
	
	public PessoaFisica ajustarGravacao(PessoaFisica pessoaFisica) throws Exception {
		if (pessoaFisica.getCpf() != null && !pessoaFisica.getCpf().isEmpty()) {
			pessoaFisica.setCpf(FormatacaoUtils.removerFormatacao(pessoaFisica.getCpf()));
		}
		if (pessoaFisica.getRg() != null && !pessoaFisica.getRg().isEmpty()) {
			pessoaFisica.setRg(FormatacaoUtils.removerFormatacao(pessoaFisica.getRg()));
		}
		if (pessoaFisica.getDataNascimentoString() != null && !pessoaFisica.getDataNascimentoString().isEmpty()) {
			pessoaFisica.setDataNascimento(FormatacaoUtils.getData(pessoaFisica.getDataNascimentoString()));
		}
		if (pessoaFisica.getSexo() != null && pessoaFisica.getSexo().equals('0')) {
			pessoaFisica.setSexo(null);
		}
		return pessoaFisica;
	}
	
	public void salvar(PessoaFisica pessoaFisica) throws Exception {
		Long idPessoa = pessoaFisica.getPessoa().getId();
		Pessoa pessoa = pessoaRepository.save((Pessoa) gerarChecksum(idPessoa == null, pessoaFisica.getPessoa()));
		
		enderecoService.salvar(pessoaFisica.getEndereco(), pessoa.getId());
		contatoService.salvar(pessoaFisica.getContatosToString(), pessoa.getId());
		
		pessoaFisica.setIdPessoa(pessoa.getId());
		pessoaFisicaRepository.saveAndFlush((PessoaFisica) gerarChecksum(idPessoa == null, pessoaFisica));
	}
	
	public void excluir(PessoaFisica pessoaFisica) throws Exception {
		pessoaFisica.setExcluido('1');
		salvar(pessoaFisica);
	}
	
	public PessoaFisica buscarPorId(Long id) {
		PessoaFisica pessoaFisica = pessoaFisicaManager.buscarPorId(id);
		pessoaFisica.setDataNascimentoString(pessoaFisica.getDataNascimento() != null ? FormatacaoUtils.getDataString(pessoaFisica.getDataNascimento()) : null);
		pessoaFisica.setEndereco(enderecoService.buscarPorIdPessoa(pessoaFisica.getIdPessoa()));
		pessoaFisica.setContatosToString(contatoService.getContatosToString(pessoaFisica.getIdPessoa()));
		return pessoaFisica;
	}
	
	public RelatorioVO getRelatorioPdfVO(PessoaFisicaFilter filtro, HttpServletRequest httpServletRequest) {
		PageWrapper<PessoaFisica> paginaWrapper = pesquisar(filtro, null, httpServletRequest);
		List<PessoaFisica> pessoasFisicas = paginaWrapper.getConteudo();
		
		RelatorioVO relatorioVO = new RelatorioVO();

		//titulo do documento
		relatorioVO.setTitulo("PESSOAS FÍSICAS");
		
		//defini tamanho das colunas
		float[] tamanhoColunas = {4, (float) 1.3, (float) 1.3, (float) 1.3, 1, (float) 0.8};
		relatorioVO.setTamanhoColunas(tamanhoColunas);
		
		//obtem nome usuario
		relatorioVO.setUsuario(getUsuario().getNome());
		
		//monta colunas
		relatorioVO.setColunas(new ArrayList<RelatorioColunaVO>());
		relatorioVO.getColunas().add(new RelatorioColunaVO("NOME", Element.ALIGN_LEFT));
		relatorioVO.getColunas().add(new RelatorioColunaVO("CPF", Element.ALIGN_CENTER));
		relatorioVO.getColunas().add(new RelatorioColunaVO("RG", Element.ALIGN_CENTER));
		relatorioVO.getColunas().add(new RelatorioColunaVO("DATA NASCIMENTO", Element.ALIGN_CENTER));
		relatorioVO.getColunas().add(new RelatorioColunaVO("SEXO", Element.ALIGN_CENTER));
		relatorioVO.getColunas().add(new RelatorioColunaVO("ID", Element.ALIGN_CENTER));
			
		//monta dados
		relatorioVO.setConteudos(new ArrayList<>());
		for (PessoaFisica pessoa : pessoasFisicas) {
			RelatorioConteudoVO conteudoVO = new RelatorioConteudoVO();
			conteudoVO.setConteudo(new ArrayList<RelatorioRegistroVO>());
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(pessoa.getPessoa().getNome(), Element.ALIGN_LEFT));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(pessoa.getCpfFormatado(), Element.ALIGN_CENTER));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(pessoa.getRg().toString(), Element.ALIGN_CENTER));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(pessoa.getDataNascimentoFormatada(), Element.ALIGN_CENTER));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(pessoa.getSexoFormatado(), Element.ALIGN_CENTER));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(pessoa.getIdPessoa().toString(), Element.ALIGN_CENTER));
			relatorioVO.getConteudos().add(conteudoVO);
		}
		return relatorioVO;
	}
}
