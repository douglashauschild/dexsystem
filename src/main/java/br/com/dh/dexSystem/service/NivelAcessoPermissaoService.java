package br.com.dh.dexSystem.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.dh.dexSystem.model.NivelAcesso;
import br.com.dh.dexSystem.model.NivelAcessoPermissao;
import br.com.dh.dexSystem.model.NivelAcessoPermissaoPK;
import br.com.dh.dexSystem.model.VwMenuAcessoPermissao;
import br.com.dh.dexSystem.repository.NivelAcessoPermissaoRepository;

@Service
public class NivelAcessoPermissaoService extends ServiceExtend {

	@Autowired
	private NivelAcessoPermissaoRepository nivelAcessoPermissaoRepository;
	
	public void salvarAcessos(NivelAcesso nivelAcesso) {
		List<VwMenuAcessoPermissao> menusAcesso = nivelAcesso.getMenuAcessos();
		for (VwMenuAcessoPermissao menuAcessoPermissao : menusAcesso) {
			NivelAcessoPermissao nivelAcessoPermissao = new NivelAcessoPermissao();
			nivelAcessoPermissao.setId(new NivelAcessoPermissaoPK(nivelAcesso.getId(), menuAcessoPermissao.getIdMenu()));
			nivelAcessoPermissao.setAcessar(menuAcessoPermissao.getAcessar());
			nivelAcessoPermissao.setInserir(menuAcessoPermissao.getInserir());
			nivelAcessoPermissao.setAlterar(menuAcessoPermissao.getAlterar());
			nivelAcessoPermissao.setExcluir(menuAcessoPermissao.getExcluir());
			
			nivelAcessoPermissaoRepository.saveAndFlush(nivelAcessoPermissao);
		}
	}
	
}
