package br.com.dh.dexSystem.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.itextpdf.text.Element;

import br.com.dh.dexSystem.controller.PageWrapper;
import br.com.dh.dexSystem.manager.MotivoManager;
import br.com.dh.dexSystem.model.Motivo;
import br.com.dh.dexSystem.model.ServiceInterface;
import br.com.dh.dexSystem.model.vo.RelatorioColunaVO;
import br.com.dh.dexSystem.model.vo.RelatorioConteudoVO;
import br.com.dh.dexSystem.model.vo.RelatorioRegistroVO;
import br.com.dh.dexSystem.model.vo.RelatorioVO;
import br.com.dh.dexSystem.repository.MotivoRepository;
import br.com.dh.dexSystem.repository.filter.MotivoFilter;

@Service
public class MotivoService extends SincronizacaoServiceAbstract implements ServiceInterface<Motivo, MotivoFilter> {

	@Autowired
	private MotivoManager motivoManager;
	
	@Autowired
	private MotivoRepository motivoRepository;

	
	public PageWrapper<Motivo> pesquisar(MotivoFilter filtro, Pageable pageable, HttpServletRequest httpServletRequest) {
		return new PageWrapper<>(motivoManager.filtrar(filtro, pageable), httpServletRequest);
	}
	
	public boolean descricaoCadastrada(Motivo motivo) {
		List<Motivo> motivosAux = null;
		if (motivo.getId() != null) {
			motivosAux = motivoRepository.findByDescricaoAndIdNotInAndExcluidoIn(motivo.getDescricao(), motivo.getId(), '0');
		} else {
			motivosAux = motivoRepository.findByDescricaoAndExcluidoIn(motivo.getDescricao(), '0');
		}
		return motivosAux != null && !motivosAux.isEmpty() ;
	}
	
	public void salvar(Motivo motivo) throws Exception {
		motivoRepository.saveAndFlush((Motivo) gerarChecksum(motivo.getId() == null, motivo));
	}
	
	public void excluir(Motivo motivo) throws Exception {
		motivo.setExcluido('1');
		salvar(motivo);
	}
	
	public Motivo buscarPorId(Long id) {
		return motivoRepository.getOne(id);
	}
	
	
	public RelatorioVO getRelatorioPdfVO(MotivoFilter filtro, HttpServletRequest httpServletRequest) {
		PageWrapper<Motivo> paginaWrapper = pesquisar(filtro, null, httpServletRequest);
		List<Motivo> motivos = paginaWrapper.getConteudo();
		
		RelatorioVO relatorioVO = new RelatorioVO();

		//titulo do documento
		relatorioVO.setTitulo("MOTIVOS");
		
		//defini tamanho das colunas
		float[] tamanhoColunas = {9, 1};
		relatorioVO.setTamanhoColunas(tamanhoColunas);
		
		//obtem nome usuario
		relatorioVO.setUsuario(getUsuario().getNome());
		
		//monta colunas
		relatorioVO.setColunas(new ArrayList<RelatorioColunaVO>());
		relatorioVO.getColunas().add(new RelatorioColunaVO("DESCRIÇÃO", Element.ALIGN_LEFT));
		relatorioVO.getColunas().add(new RelatorioColunaVO("ID", Element.ALIGN_CENTER));
			
		//monta dados
		relatorioVO.setConteudos(new ArrayList<>());
		for (Motivo motivo : motivos) {
			RelatorioConteudoVO conteudoVO = new RelatorioConteudoVO();
			conteudoVO.setConteudo(new ArrayList<RelatorioRegistroVO>());
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(motivo.getDescricao(), Element.ALIGN_LEFT));
			conteudoVO.getConteudo().add(new RelatorioRegistroVO(motivo.getId().toString(), Element.ALIGN_CENTER));
			
			relatorioVO.getConteudos().add(conteudoVO);
		}
		return relatorioVO;
	}
}
