package br.com.dh.dexSystem.service;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.dh.dexSystem.model.PessoaDigital;
import br.com.dh.dexSystem.repository.PessoaDigitalRepository;

@Service
public class PessoaDigitalService implements Serializable {

	private static final long serialVersionUID = 8032739249490564981L;
	@Autowired
	private PessoaDigitalRepository pessoaDigitalRepository;

	
	public void salvarSincronizacao(PessoaDigital pessoaDigital) {
		PessoaDigital pessoaDigitalAux = pessoaDigitalRepository.findByChecksum(pessoaDigital.getChecksum());
		if (pessoaDigitalAux != null) {
			pessoaDigital.setId(pessoaDigitalAux.getId());
		} else {
			pessoaDigital.setId(0L);
		}
		pessoaDigitalRepository.saveAndFlush(pessoaDigital);
	}
}
