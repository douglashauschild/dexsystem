package br.com.dh.dexSystem.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import br.com.dh.dexSystem.model.Contato;
import br.com.dh.dexSystem.model.constant.AcaoCadastroEnum;
import br.com.dh.dexSystem.model.constant.DelimitadorEnum;
import br.com.dh.dexSystem.model.constant.TipoContatoEnum;
import br.com.dh.dexSystem.model.vo.ContatoVO;
import br.com.dh.dexSystem.repository.ContatoRepository;

@Service
public class ContatoService extends ServiceExtend {

	@Autowired
	private ContatoRepository contatoRepository;
	
	
	public void salvar(String contatosToString, Long idPessoa) {
		if (contatosToString != null && !contatosToString.isEmpty()) {
			String[] contatos = contatosToString.split("\\"+DelimitadorEnum.ITENS.getKey());
			for (String registroContato : contatos) {
				Character acao = registroContato.charAt(registroContato.length()-1);
				
				String contato[] = registroContato.split("\\"+DelimitadorEnum.INFORMACAO.getKey());
				Long tipo = TipoContatoEnum.getByDescricao(contato[0]).getKey();
				String tipoOutro = !contato[1].equals("NULL") ? contato[1] : null;
				String descricao = contato[2].split("\\"+DelimitadorEnum.ACAO.getKey())[0];
				
				Contato contatoAct = null;
				if (acao == AcaoCadastroEnum.EXCLUIR.getKey()) {
					Contato contatoExclusao = contatoRepository.findByIdPessoaAndTipoAndTipoOutroAndDescricao(idPessoa, tipo, tipoOutro, descricao);
					if (contatoExclusao != null) {
						contatoRepository.delete(contatoExclusao);
					}
				} else if (acao == AcaoCadastroEnum.ALTERAR.getKey()) {
					String[] registros = registroContato.split("\\"+DelimitadorEnum.ITENS_ALTERACAO.getKey());
					String[] contatoNovo = registros[0].split("\\"+DelimitadorEnum.INFORMACAO.getKey());
					
					//pega o ultimo, pois se houver registros no meio, significa que nao estao salvos
					String[] contatoAlterar = registros[registros.length-1].split("\\"+DelimitadorEnum.INFORMACAO.getKey());
					
					Long tipoNovo = TipoContatoEnum.getByDescricao(contatoNovo[0]).getKey();
					String tipoOutroNovo = !contatoNovo[1].equals("NULL") ? contatoNovo[1] : null;
					String descricaoNova = contatoNovo[2].split("\\"+DelimitadorEnum.ACAO.getKey())[0];
					
					Long tipoAlterar = TipoContatoEnum.getByDescricao(contatoAlterar[0]).getKey();
					String tipoOutroAlterar = !contatoAlterar[1].equals("NULL") ? contatoAlterar[1] : null;
					String descricaoAlterar = contatoAlterar[2].split("\\"+DelimitadorEnum.ACAO.getKey())[0];
					
					contatoAct = contatoRepository.findByIdPessoaAndTipoAndTipoOutroAndDescricao(idPessoa, tipoAlterar, tipoOutroAlterar, descricaoAlterar);
					if (contatoAct != null) {
						contatoAct.setTipo(tipoNovo);
						contatoAct.setTipoOutro(tipoOutroNovo);
						contatoAct.setDescricao(descricaoNova);
					} else {
						contatoAct = new Contato(idPessoa, tipoNovo, tipoOutroNovo, descricaoNova);
					}
				} else if (acao == AcaoCadastroEnum.NOVO.getKey()) {
					contatoAct = new Contato(idPessoa, tipo, tipoOutro, descricao);
				}
				if (contatoAct != null) {
					contatoRepository.saveAndFlush(contatoAct);
				}
			}
		}
	}
	
	public String getContatosToString(Long idPessoa) {
		String contatosToString = "";
		List<Contato> contatos = contatoRepository.findByIdPessoa(idPessoa);
		for (Contato contato : contatos) {
			contatosToString += new ContatoVO(contato.getTipo(), contato.getTipoOutro(), contato.getDescricao()).toString(AcaoCadastroEnum.CADASTRADO.getKey());   
		}
		return contatosToString;
	}
	
	public String adicionar(ContatoVO contatoVO, Model model) {
		String contatosInseridos = contatoVO.getContatosToString();
		String contatos[] = contatosInseridos.split("\\"+DelimitadorEnum.ITENS.getKey());

		String validacao = validarItens(contatoVO, Arrays.asList(contatos), model);
		if (validacao != null && !validacao.isEmpty()) {
			return validacao;	
		}
		ContatoVO newContatoVO = new ContatoVO(contatoVO.getTipo(), contatoVO.getTipoOutro(), contatoVO.getContato());
		contatoVO.setContatosToString(contatoVO.getContatosToString() + newContatoVO.toString(AcaoCadastroEnum.NOVO.getKey()));
		model.addAttribute("contatosToString", ordenarLista(contatoVO.getContatosToString()));
		return "cadastros/contato/contato-lista :: #contatos";
	}
	
	
    public String alterar(ContatoVO contatoVO, Model model) {
		String contatosToString = contatoVO.getContatosToString();
		String contatoOriginal = contatoVO.getContatoExclusao();
		
		List<String> contatoValidacao = new ArrayList<String>();
		String contatos[] = contatosToString.split("\\"+DelimitadorEnum.ITENS.getKey());
		for (String contato : contatos) {
			if (!contato.equals(contatoOriginal)) {
				contatoValidacao.add(contato);
			}
		}
		String validacao = validarItens(contatoVO, contatoValidacao, model);
		if (validacao != null && !validacao.isEmpty()) {
			return validacao;	
		}
		StringBuilder contatoAlterar = new StringBuilder(contatoOriginal);
		contatoAlterar.setCharAt(contatoOriginal.length()-1, AcaoCadastroEnum.ALTERAR.getKey());
		String novoItem = contatoVO.toString(AcaoCadastroEnum.NOVO.getKey()).replace(DelimitadorEnum.ITENS.getKey(), DelimitadorEnum.ITENS_ALTERACAO.getKey()) + contatoAlterar.toString();
		contatoVO.setContatosToString(contatoVO.getContatosToString().replace(contatoOriginal, novoItem));
		model.addAttribute("contatosToString", ordenarLista(contatoVO.getContatosToString()));
		return "cadastros/contato/contato-lista :: #contatos";
    }
    
    
    public String remover(ContatoVO contatoVO, Model model) {
		String contatoExclusao = contatoVO.getContatoExclusao();
		StringBuilder contatoReplace = new StringBuilder(contatoExclusao);
		contatoReplace.setCharAt(contatoExclusao.length()-1, AcaoCadastroEnum.EXCLUIR.getKey());
		contatoVO.setContatosToString(contatoVO.getContatosToString().replace(contatoVO.getContatoExclusao(), contatoReplace.toString()));
		model.addAttribute("contatosToString", ordenarLista(contatoVO.getContatosToString()));
		return "cadastros/contato/contato-lista :: #contatos";
    }
    
	
	private String validarItens(ContatoVO contatoVO, List<String> contatos, Model model) {
		String retorno = null;
		List<String> descricao = new ArrayList<String>();
		for (String contato : contatos) {
			if (!contato.isEmpty() && contato.charAt(contato.length()-1) != AcaoCadastroEnum.EXCLUIR.getKey()) {
				descricao.add(contato.split("\\"+DelimitadorEnum.INFORMACAO.getKey())[2].split("\\"+DelimitadorEnum.ACAO.getKey())[0]);
			}
		}
		if (descricao.contains(contatoVO.getContato())) {
			model.addAttribute("msgErro", "Contato já cadastrado!");
			retorno = "cadastros/contato/contato-cadastroModal :: #msgErro";
		} 
		return retorno;
	}
	
	
	private String ordenarLista(String itens) {  
	    List<String> listaOrdenada = Arrays.asList(itens.split("\\"+DelimitadorEnum.ITENS.getKey()));  
	    Collections.sort(listaOrdenada, new Comparator<String>() {  
	        @Override  
	        public int compare(String s1, String s2) {
	        	Long tipo1 = TipoContatoEnum.getByDescricao(s1.split(DelimitadorEnum.INFORMACAO.getKey().toString())[0]).getKey();
	        	Long tipo2 = TipoContatoEnum.getByDescricao(s2.split(DelimitadorEnum.INFORMACAO.getKey().toString())[0]).getKey();
	        	return tipo1.compareTo(tipo2);
	        }
	    });  
	    String lista = "";
	    for (String item : listaOrdenada) {
	    	lista += item + DelimitadorEnum.ITENS.getKey();
	    }
	    return lista;
	}  
}
