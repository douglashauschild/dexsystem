package br.com.dh.dexSystem.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.dh.dexSystem.model.Empregado;
import br.com.dh.dexSystem.model.Empresa;
import br.com.dh.dexSystem.model.Epi;
import br.com.dh.dexSystem.model.ParametroSistema;
import br.com.dh.dexSystem.model.PessoaFisica;
import br.com.dh.dexSystem.repository.ParametroSistemaRepository;

@Service
public class ParametroSistemaService extends SincronizacaoServiceAbstract {

	@Autowired
	private ParametroSistemaRepository parametroSistemaRepository;


	public ParametroSistema buscar() {
		return parametroSistemaRepository.findOne(1L);
	}
	
	public void salvar(ParametroSistema parametroSistema) throws Exception {
		parametroSistema = ajustarGravacao(parametroSistema);
		parametroSistemaRepository.saveAndFlush((ParametroSistema) gerarChecksum(parametroSistema.getId() == null, parametroSistema));
	}
	
	public ParametroSistema ajustarGravacao(ParametroSistema parametroSistema) throws Exception {
		return parametroSistema;
	}
	
	public String getSelecaoEmpresa(Empresa empresa) {
		ParametroSistema parametroSistema = buscar();
		String nome = empresa.getPessoaJuridica().getPessoa().getNome();
		if (!parametroSistema.isSelecaoEmpresaSimples()) {
			nome = "CNPJ: "+ empresa.getPessoaJuridica().getCnpjFormatado() + " | " + nome;
		}
		return nome;
	}
	
	public String getSelecaoEmpregado(Empregado empregado) {
		ParametroSistema parametroSistema = buscar();
		String nome = empregado.getPessoaFisica().getPessoa().getNome();
		if (!parametroSistema.isSelecaoEmpregadoSimples()) {
			nome = "Matr.: "+ empregado.getMatricula() + " | " + nome;
		}
		return nome;
	}
	
	public String getSelecaoPessoaFisica(PessoaFisica pessoaFisica) {
		ParametroSistema parametroSistema = buscar();
		String nome = pessoaFisica.getPessoa().getNome();
		if (!parametroSistema.isSelecaoPessoaFisicaSimples()) {
			nome = "CPF: "+ pessoaFisica.getCpfFormatado() + " | " + nome;
		}
		return nome;
	}
	
	public String getSelecaoEpi(Epi epi) {
		ParametroSistema parametroSistema = buscar();
		String nome = epi.getNome();
		String ca = epi.getCa() != null && !epi.getCa().isEmpty() ? epi.getCa() : " ";
		if (!parametroSistema.isSelecaoEpiSimples()) {
			nome = "CA: "+ ca + " | " + nome;
		}
		return nome;
	}
}
