package br.com.dh.dexSystem.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.dh.dexSystem.manager.AtividadeManager;
import br.com.dh.dexSystem.manager.CargoEpiManager;
import br.com.dh.dexSystem.manager.CargoManager;
import br.com.dh.dexSystem.model.Atividade;
import br.com.dh.dexSystem.model.Cargo;
import br.com.dh.dexSystem.model.CargoEpi;
import br.com.dh.dexSystem.model.Empregado;
import br.com.dh.dexSystem.model.EmpregadoEpi;

@Service
public class AtualizacaoEpiService extends ServiceExtend {

	@Autowired
	private CargoManager cargoManager;
	@Autowired
	private CargoEpiManager cargoEpiManager;
	@Autowired
	private AtividadeManager atividadeManager;
	@Autowired
	private EmpregadoEpiService empregadoEpiService;

	
	public void atualizarEpi(CargoEpi cargoEpi) throws Exception {
		Cargo cargo = cargoManager.buscarPorId(cargoEpi.getIdCargo());
		if (cargo != null) {
			List<Atividade> atividades = atividadeManager.buscarAtivasPorCargo(cargo);
			for (Atividade atividade : atividades) {
				Date dataInicio = atividade.getDataInicio().after(cargoEpi.getDataInicio()) ? atividade.getDataInicio() : cargoEpi.getDataInicio();
				atualizar(atividade.getIdEmpregado(), cargoEpi.getIdEpi(), dataInicio, cargoEpi.getQuantidade());
			}
		}
	}	
	
	public void atualizarEpiPorEmpregado(Empregado empregado) throws Exception {
		Atividade atividade = atividadeManager.buscarAtivaPorEmpregado(empregado);
		if (atividade != null && atividade.isAtiva()) {
			atualizarEpiPorAtividade(atividade);
		}
	}
	
	public void atualizarEpiPorAtividade(Atividade atividade) throws Exception {
		if (atividade != null) {
			Cargo cargo = cargoManager.buscarPorId(atividade.getIdCargo());
			List<CargoEpi> cargoEpis = cargoEpiManager.buscarAtivosPorCargo(cargo);
			for (CargoEpi cargoEpi : cargoEpis) {
				atualizar(atividade.getIdEmpregado(), cargoEpi.getIdEpi(), atividade.getDataInicio(), cargoEpi.getQuantidade());
			}
		}
	}	
	
	private void atualizar(Long idEmpregado, Long idEpi, Date dataInicio, Integer quantidade) throws Exception {
		List<EmpregadoEpi> empregadoEpis = empregadoEpiService.buscarEpiPrevisto(idEmpregado, idEpi);
		if (empregadoEpis == null || empregadoEpis.isEmpty()) {
			empregadoEpiService.salvar(getNovoEmpregadoEpi(idEmpregado, dataInicio, idEpi, quantidade));
		} else {
			for (EmpregadoEpi empregadoEpi : empregadoEpis) {
				if (empregadoEpi.getDataConfirmacao() == null) {
					empregadoEpi.setDataPrevistaEntrega(dataInicio);
					empregadoEpi.setQuantidade(quantidade);	
					empregadoEpiService.salvar(empregadoEpi);
				}
			}
		} 
	}
	
	private EmpregadoEpi getNovoEmpregadoEpi(Long idEmpregado, Date dataInicio, Long idEpi, Integer quantidade) {
		EmpregadoEpi empregadoEpi = new EmpregadoEpi();
		empregadoEpi.setDataPrevistaEntrega(dataInicio);
		empregadoEpi.setIdEmpregado(idEmpregado);
		empregadoEpi.setIdEpi(idEpi);
		empregadoEpi.setQuantidade(quantidade);
		empregadoEpi.setIdUsuario(getUsuario().getId());
		return empregadoEpi;
	}
}
