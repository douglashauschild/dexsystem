package br.com.dh.dexSystem.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.dh.dexSystem.model.Estado;
import br.com.dh.dexSystem.repository.EstadoRepository;

@Service
public class EstadoService extends ServiceExtend {

	@Autowired
	private EstadoRepository estadoRepository;
	
	
	public Estado buscarPorId(Long id) {
		return estadoRepository.getOne(id);
	}

	public List<Estado> buscarTodos() {
		return estadoRepository.findAll();
	}
}
