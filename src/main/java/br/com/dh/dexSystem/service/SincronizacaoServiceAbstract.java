package br.com.dh.dexSystem.service;

import br.com.dh.dexSystem.model.SincronizacaoAbstract;

public abstract class SincronizacaoServiceAbstract extends ServiceExtend {
	
	public SincronizacaoAbstract gerarChecksum(boolean inclusao, SincronizacaoAbstract sincronizacaoAbstract) throws Exception {
		if (inclusao) {
			sincronizacaoAbstract.setChecksum(sincronizacaoAbstract.getHash(true));
		}
		sincronizacaoAbstract.setChecksumAlteracao(sincronizacaoAbstract.getHash(false));
		return sincronizacaoAbstract;
	}
	
}