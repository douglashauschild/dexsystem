package br.com.dh.dexSystem.service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import br.com.dh.dexSystem.manager.EmpregadoEpiManager;
import br.com.dh.dexSystem.model.Cargo;
import br.com.dh.dexSystem.model.Empregado;
import br.com.dh.dexSystem.model.Empresa;
import br.com.dh.dexSystem.model.Setor;
import br.com.dh.dexSystem.model.vo.EmpregadoVO;
import br.com.dh.dexSystem.model.vo.FichaEpiVO;
import br.com.dh.dexSystem.repository.EmpregadoRepository;
import br.com.dh.dexSystem.util.FileUtils;
import br.com.dh.dexSystem.view.FichaEpiColetivaView;

@Service
public class FichaEpiService extends SincronizacaoServiceAbstract {
	
	@Value("${api.path}")
	protected String caminho;

	@Autowired
	private EmpresaService empresaService;
	@Autowired
	private EmpregadoService empregadoService;
	@Autowired
	private SetorService setorService;
	@Autowired
	private CargoService cargoService;
	@Autowired
	private FichaEpiColetivaView fichaEpiColetivaView;
	@Autowired 
	private EmpregadoEpiManager empregadoEpiManager;
	@Autowired
	private EmpregadoRepository empregadoRepository;
	@Autowired
	private EmpregadoEpiService empregadoEpiService;

	
	public String getObservacao(Long idEmpresa, Long idEmpregado, Long idSetor, Long idCargo) {
		String nomeEmpresa = null;
		String nomeEmpregado = null;
		String nomeSetor = null;
		String nomeCargo = null;
		
		if (idEmpresa != null && idEmpresa != 0L) {
			Empresa empresa = empresaService.buscarPorId(idEmpresa);
			if (empresa != null) {
				nomeEmpresa = empresa.getRazaoSocial();
			}
		}
		if (idEmpregado != null && idEmpregado != 0L) {
			Empregado empregado = empregadoService.buscarPorId(idEmpregado);
			if (empregado != null) {
				nomeEmpregado = empregado.getPessoaFisica().getPessoa().getNome();
			}
		}
		if (idSetor != null && idSetor != 0L) {
			Setor setor = setorService.buscarPorId(idSetor);
			if (setor != null) {
				nomeSetor = setor.getNome();
			}
		}
		if (idCargo != null && idCargo != 0L) {
			Cargo cargo = cargoService.buscarPorId(idCargo);
			if (cargo != null) {
				nomeCargo = cargo.getNome();
			}
		}
		
		String msg = null;
		if (nomeEmpregado != null) {
			msg = "O sistema irá gerar a Ficha de EPI do empregado " + nomeEmpregado;
		} else if (nomeEmpresa != null || nomeSetor != null || nomeCargo != null) {
			msg = "O sistema irá gerar a Ficha de EPI de ";
			if (nomeEmpresa != null) {
				msg += "todos os empregados ativos da empresa " + nomeEmpresa;
			}
			if (nomeSetor != null) {
				if (nomeCargo != null) {
					msg += ", setor " + nomeSetor + " e cargo "+ nomeCargo + ".";
				} else {
					msg += " e setor " + nomeSetor + ".";
				}
			}
		} else {
			msg = "O sistema irá gerar a Ficha de EPI de todos os empregados ativos.";
		}
		return "2) "+msg;
	}
	

	public boolean isPossuiEntregas() {
		Integer total = empregadoEpiService.buscarTotalEntregasRealizadas(null);
		return total != null && total > 0;
	}
	
	public String gerarFicha(Long idEmpresa, Long idSetor, Long idCargo, Long idEmpregado) throws Exception {
		List<Empregado> empregados = new ArrayList<Empregado>();
		if (idEmpregado != null && idEmpregado != 0L) {
			empregados.add(empregadoService.buscarPorId(idEmpregado));
		} else {
			if (idCargo != null && idCargo != 0L) {
				empregados = empregadoRepository.buscarAtivosPorIdCargo(idCargo);
			} else if (idSetor != null && idSetor != 0L) {
				empregados = empregadoRepository.buscarAtivosPorIdSetor(idSetor);
			} else if (idEmpresa != null && idEmpresa != 0L) {
				empregados = empregadoService.buscarAtivosPorEmpresa(idEmpresa);
			} else {
				empregados = empregadoService.buscarTodosAtivos();
			}
		}
		return gerarArquivos(empregados);
	}
	
	private String gerarArquivos(List<Empregado> empregados) throws Exception {
		List<File> arquivos = new ArrayList<File>();
		for (Empregado empregado : empregados) {
			FichaEpiVO fichaEpiVO = getFichaEpiVO(empregado.getId());
			arquivos.add(new File(fichaEpiColetivaView.gerarFichaEpi(fichaEpiVO, FileUtils.criarPastaTemp(caminho))));
		}
		String nome = UUID.randomUUID().toString() + ".zip";
		File zipFile = new File(caminho + File.separator + nome);
		FileUtils.zip(arquivos, zipFile);
		FileUtils.excluirArquivos(arquivos);
		return nome;
	}
	
	public FichaEpiVO getFichaEpiVO(Long idEmpregado) {
		FichaEpiVO fichaEpiVO = new FichaEpiVO();
		EmpregadoVO empregadoVO = empregadoService.getInformacoes(idEmpregado);
		if (empregadoVO != null) {
			fichaEpiVO = new FichaEpiVO();
			fichaEpiVO.setUsuario(getUsuario().getNome());
			fichaEpiVO.setEmpresa(empregadoVO.getEmpresa());
			fichaEpiVO.setEmpregado(empregadoVO.getNome());
			fichaEpiVO.setMatricula(empregadoVO.getMatricula());
			fichaEpiVO.setCpf(empregadoVO.getCpf());
			fichaEpiVO.setRg(empregadoVO.getRg());
			fichaEpiVO.setDataAdmissao(empregadoVO.getDataAdmissao());
			fichaEpiVO.setDataDemissao(empregadoVO.getDataDemissao());
			fichaEpiVO.setSetor(empregadoVO.getSetor());
			fichaEpiVO.setCargo(empregadoVO.getCargo());
			fichaEpiVO.setEpis(empregadoEpiManager.buscarParaFichaEpi(idEmpregado));
		}
		return fichaEpiVO;
	}
}
