package br.com.dh.dexSystem.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.dh.dexSystem.model.Endereco;
import br.com.dh.dexSystem.repository.EnderecoRepository;
import br.com.dh.dexSystem.util.FormatacaoUtils;

@Service
public class EnderecoService extends ServiceExtend {

	@Autowired
	private EnderecoRepository enderecoRepository;

	
	public Endereco buscarPorIdPessoa(Long idPessoa) {
		Endereco endereco = enderecoRepository.findByIdPessoa(idPessoa);
		if (endereco != null) {
			endereco.setIdEstado(endereco.getCidade().getIdEstado());
		}
		return endereco;
	}
	
	public void salvar(Endereco endereco, Long idPessoa) {
		if (endereco != null && !endereco.isVazio()) {
			if (endereco.getIdEstado() != null && endereco.getIdEstado() == 0L) {
				endereco.setIdEstado(null);
			}
			if (endereco.getIdCidade() != null && endereco.getIdCidade() == 0L) {
				endereco.setIdCidade(null);
			}
			enderecoRepository.saveAndFlush(ajustarGravacao(endereco, idPessoa));
		}
	}
	
	private Endereco ajustarGravacao(Endereco endereco, Long idPessoa) {
		endereco.setCep(FormatacaoUtils.removerFormatacao(endereco.getCep()));
		endereco.setIdPessoa(idPessoa);
		return endereco;
	}
}