package br.com.dh.dexSystem.util;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class TokenUtils {
	
	private final static String SEPARADOR = "#";

	public static String montarToken(String apiHash, String email) {
		String token = apiHash + SEPARADOR + email;
		return new String(Base64.getEncoder().encode(CriptografiaUtils.encrypt(token, CriptografiaUtils.CHAVE).getBytes(StandardCharsets.UTF_8)));
	}
	
	public static boolean validarToken(String token) {
		boolean valido = false;
		try {
			if (token != null && !token.isEmpty() && token.contains("Bearer")) {
				valido = getTokenExtraido(token).length == 2;
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return valido;
	}
	
	public static String[] getTokenExtraido(String token) {
		token = token.replace("Bearer", "").trim();
		token = CriptografiaUtils.decrypt(new String(Base64.getDecoder().decode(token.getBytes()), StandardCharsets.UTF_8), CriptografiaUtils.CHAVE);
		return token.split(SEPARADOR);
	}
}
