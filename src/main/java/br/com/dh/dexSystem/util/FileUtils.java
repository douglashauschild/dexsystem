package br.com.dh.dexSystem.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Stack;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class FileUtils {

	public static String criarPastaTemp(String caminho) {
		if (caminho != null || !fileExists(caminho)) {
			String caminhoTmp = caminho + File.separator + "tmp";
			if (!fileExists(caminhoTmp)) {
				(new File(caminhoTmp)).mkdir();
			}
			if (fileExists(caminhoTmp)) {
				return new File(caminhoTmp).getPath();
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
	
	public static boolean fileExists(String caminho) {
		if (caminho != null) {
			File file = new File(caminho);
			return file.exists();
		}
		return false;
	}
	
	public static void zip(List<File> files, File outputFile) throws IOException {
		if (files != null && files.size() > 0) {
			File filesZip[] = new File[files.size()];
			for (int i=0; i<files.size(); i++) {
				filesZip[i] = files.get(i);
			}
			zip(filesZip, outputFile);
		}
	}
	
	public static void zip(File[] files, File outputFile) throws IOException {
		if (files != null && files.length > 0) {
			ZipOutputStream out = new ZipOutputStream(new FileOutputStream(outputFile));
			Stack<File> parentDirs = new Stack<File>();
			zipFiles(parentDirs, files, out);
			out.close();
		}
	}
	
	private static void zipFiles(Stack<File> parentDirs, File[] files, ZipOutputStream out) throws IOException {
		byte[] buf = new byte[1024];
		for (int i = 0; i < files.length; i++) {
			if (files[i] == null) {
				break;
			}
			if (files[i].isDirectory()) {
				parentDirs.push(files[i]);
				zipFiles(parentDirs, files[i].listFiles(), out);
				parentDirs.pop();
			} else {
				FileInputStream in = new FileInputStream(files[i]);
				String path = "";
				for(File parentDir : parentDirs) {
					path += parentDir.getName() + "/";
				}
				out.putNextEntry(new ZipEntry(path + files[i].getName()));
				int len;
				while ((len = in.read(buf)) > 0) {
					out.write(buf, 0, len);
				}
				out.closeEntry();
				in.close();
			}
		}
	}
	
	public static void excluirArquivos(List<File> files) {
		for (File file : files) {
			file.delete();
		}
	}
}
