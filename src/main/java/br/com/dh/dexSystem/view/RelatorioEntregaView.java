package br.com.dh.dexSystem.view;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import br.com.dh.dexSystem.model.EmpregadoEpi;
import br.com.dh.dexSystem.model.vo.RelatorioEntregaVO;
import br.com.dh.dexSystem.security.UsuarioSistema;

@Component("relatorioEntregaView")
public class RelatorioEntregaView extends AbstractPdfView {
	
	@Override
    protected void buildPdfDocument(Map<String, Object> model, Document document, PdfWriter writer, HttpServletRequest request, HttpServletResponse response) throws Exception {
		RelatorioEntregaVO relatorioEntregaVO = (RelatorioEntregaVO) model.get("relatorioEntrega");
		
		document.addTitle("Relatório de Entregas");
		document.addCreationDate();
		
		Paragraph titulo = new Paragraph("RELATÓRIO DE ENTREGAS");
		titulo.setAlignment(Element.ALIGN_CENTER);
        document.add(titulo);
		
        PdfPCell cell = new PdfPCell();
        cell.setPadding(5);
		cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
		
		Font font = FontFactory.getFont(FontFactory.HELVETICA);
		
		PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(100.0f);
    	table.setWidths(new float[] { 8, 2 });
        table.setSpacingBefore(10);		
		
		cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell.setPhrase(new Phrase("ENTREGA", font));
		table.addCell(cell);
		
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setPhrase(new Phrase("STATUS", font));
		table.addCell(cell);

		table.setHeaderRows(1);

		cell.setBackgroundColor(BaseColor.WHITE);
		for (EmpregadoEpi empregadoEpi : relatorioEntregaVO.getEmpregadoEpis()) {
    		cell.setHorizontalAlignment(Element.ALIGN_LEFT);
    		cell.addElement(new Phrase("EMPREGADO: " + empregadoEpi.getEmpregado().getPessoaFisica().getPessoa().getNome(), font));
    		
    		Font fontAux = FontFactory.getFont(FontFactory.HELVETICA);
    		fontAux.setSize(10);
    		
    		Paragraph paragraph = new Paragraph("EMPRESA: " + empregadoEpi.getEmpregado().getEmpresa().getRazaoSocial(), fontAux);
    		paragraph.setSpacingAfter(5f);
    		cell.addElement(paragraph);
    		cell.addElement(new Phrase("EPI: " + empregadoEpi.getEpi().getNome() + (empregadoEpi.getDataConfirmacao() != null ? (" - Entrega confirmada via " + empregadoEpi.getTipoConfirmacaoString()) : ""), fontAux));
    		
    		PdfPTable tableCell = new PdfPTable(3);
    		tableCell.setWidthPercentage(100);
    		tableCell.getDefaultCell().setBorder(0);
    		tableCell.getDefaultCell().setLeft(0);
    		tableCell.getDefaultCell().setPaddingLeft(0);
    		tableCell.getDefaultCell().setBorderWidthLeft(0);
    	
    		tableCell.addCell(new Phrase("CA: " + empregadoEpi.getCa(), fontAux));
    		tableCell.addCell(new Phrase("Data prevista de entrega: " + empregadoEpi.getDataPrevistaEntregaFormatada(), fontAux));
    		tableCell.addCell(new Phrase("Data de confirmação: " + empregadoEpi.getDataConfirmacaoFormatada(), fontAux));
    		
    		tableCell.addCell(new Phrase("Grade: " + empregadoEpi.getGrade(), fontAux));
    		tableCell.addCell(new Phrase("Data de entrega: " + empregadoEpi.getDataEntregaFormatada(), fontAux));
    		tableCell.addCell(new Phrase("Data de substituição: " + empregadoEpi.getDataSubstituicaoFormatada(), fontAux));
    		
    		tableCell.addCell(new Phrase("Quantidade: " + empregadoEpi.getQuantidade().toString(), fontAux));
    		tableCell.addCell(new Phrase("Data prevista de troca: " + empregadoEpi.getDataPrevistaTrocaFormatada(), fontAux));
    		tableCell.addCell(new Phrase("Data de devolução: " + empregadoEpi.getDataDevolucaoFormatada(), fontAux));
    		cell.addElement(tableCell);
    		table.addCell(cell);
    		
    		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
    		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
    		cell.setPhrase(new Phrase(empregadoEpi.getStatusDescricao(), font));
    		table.addCell(cell);
		}
		cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
		cell.setColspan(2);
		cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell.setPhrase(new Phrase("TOTAL: " + relatorioEntregaVO.getEmpregadoEpis().size(), font));
		table.addCell(cell);
        document.add(table);
    }

	
	@Override
	protected String getUsuario() {
		return ((UsuarioSistema) ((SecurityContext) SecurityContextHolder.getContext()).getAuthentication().getPrincipal()).getUsuario().getNome();
	}
}
