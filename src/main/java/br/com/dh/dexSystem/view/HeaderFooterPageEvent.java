package br.com.dh.dexSystem.view;
	
import java.util.Date;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

import br.com.dh.dexSystem.util.FormatacaoUtils;

public class HeaderFooterPageEvent extends PdfPageEventHelper {
	
	private String usuario;
	
	public HeaderFooterPageEvent(String usuario) {
		super();
		this.usuario = usuario;
	}

	@Override
    public void onEndPage(PdfWriter writer, Document document) {
        try {
            Font font = FontFactory.getFont(FontFactory.HELVETICA);
            
    	    PdfContentByte content = writer.getDirectContent();
    	    Phrase dataImpressao = new Phrase("Emitido por " + usuario + " em: " + FormatacaoUtils.getDataHoraString(new Date()));
    	    Phrase numeroPagina = new Phrase(String.format("Página %d", writer.getCurrentPageNumber()), font);
    	    
    	    ColumnText.showTextAligned(content, Element.ALIGN_LEFT, dataImpressao, document.leftMargin(), document.bottom() - 10, 0);
    	    ColumnText.showTextAligned(content, Element.ALIGN_RIGHT, numeroPagina, 800, document.bottom() - 10, 0);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
}