package br.com.dh.dexSystem.view;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import br.com.dh.dexSystem.model.vo.RelatorioColunaVO;
import br.com.dh.dexSystem.model.vo.RelatorioConteudoVO;
import br.com.dh.dexSystem.model.vo.RelatorioRegistroVO;
import br.com.dh.dexSystem.model.vo.RelatorioVO;
import br.com.dh.dexSystem.security.UsuarioSistema;

@Component("pdfView")
public class PdfView extends AbstractPdfView {

	
	@Override
    protected void buildPdfDocument(Map<String, Object> model, Document document, PdfWriter writer, HttpServletRequest request, HttpServletResponse response) throws Exception {
		RelatorioVO relatorioVO = (RelatorioVO) model.get("relatorio");

		document.addTitle(relatorioVO.getTitulo());
		
		Paragraph titulo = new Paragraph(relatorioVO.getTitulo());
		titulo.setAlignment(Element.ALIGN_CENTER);
        document.add(titulo);

        PdfPTable table = new PdfPTable(relatorioVO.getTamanhoColunas());
        table.setWidthPercentage(100.0f);
        table.setSpacingBefore(10);

        // define fonte da linha
        Font font = FontFactory.getFont(FontFactory.HELVETICA);

        // define celula do cabecalho
        PdfPCell cell = new PdfPCell();
        cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
        cell.setPadding(5);

        // imprimi colunas do cabecalho
        for (RelatorioColunaVO colunaVO : relatorioVO.getColunas()) {
	        cell.setPhrase(new Phrase(colunaVO.getNome(), font));
	        cell.setHorizontalAlignment(colunaVO.getAlinhamento());
	        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	        table.addCell(cell);
        }
        
        // imprimi dados do relatorio
        for (RelatorioConteudoVO conteudosVO : relatorioVO.getConteudos()) {
        	for (RelatorioRegistroVO registroVO : conteudosVO.getConteudo()) {
        		cell = new PdfPCell();
        		cell.setBackgroundColor(BaseColor.WHITE);
        		cell.setHorizontalAlignment(registroVO.getAlinhamento());
        		cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        		cell.setPadding(5);
        		cell.setPhrase(new Phrase(registroVO.getRegistro()));
        		table.addCell(cell);
        	}
        }
        
        //add rodape tabela
        cell = new PdfPCell();
        cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
        cell.setPadding(5);
        cell.setColspan(relatorioVO.getColunas().size());
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setPhrase(new Phrase("TOTAL: " + relatorioVO.getConteudos().size(), font));
        table.addCell(cell);
        
        //add ao documento       
        document.add(table);
    }

	@Override
	protected String getUsuario() {
		return ((UsuarioSistema) ((SecurityContext) SecurityContextHolder.getContext()).getAuthentication().getPrincipal()).getUsuario().getNome();
	}
}
