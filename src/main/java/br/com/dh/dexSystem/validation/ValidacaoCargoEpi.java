package br.com.dh.dexSystem.validation;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.dh.dexSystem.manager.CargoEpiManager;
import br.com.dh.dexSystem.manager.CargoManager;
import br.com.dh.dexSystem.model.Cargo;
import br.com.dh.dexSystem.model.CargoEpi;
import br.com.dh.dexSystem.util.FormatacaoUtils;

@Service
public class ValidacaoCargoEpi implements Serializable {	
	
	private static final long serialVersionUID = -5064647309552014402L;
	@Autowired
	private CargoManager cargoManager;
	@Autowired
	private CargoEpiManager cargoEpiManager;
	

	public String validar(CargoEpi cargoEpi) {
		Cargo cargo = cargoManager.buscarPorId(cargoEpi.getIdCargo());
		if ((cargoEpi.getDataFim() != null) && (cargoEpi.getDataFim().before(cargoEpi.getDataInicio()))) {
			return "A data de fim não pode ser anterior a data de início!";
		}
		if (cargo.getDataInicio().after(cargoEpi.getDataInicio())) {
			return "A data de início da recomendação deste EPI não pode ser anterior a data de início ("+FormatacaoUtils.getDataString(cargo.getDataInicio())+") do cargo!";
		}
		if ((cargoEpi.getDataFim() != null) && (cargo.getDataFim() != null) && (cargo.getDataFim().before(cargoEpi.getDataFim()))) {
			return "A data de fim da recomendação deste EPI não pode ser posterior a data de fim ("+FormatacaoUtils.getDataString(cargo.getDataFim())+") do cargo!";
		}
		List<CargoEpi> cargosEpi = cargoEpiManager.buscarPorCargoEpi(cargoEpi);
    	for (CargoEpi cargoEpiAux : cargosEpi) {
    		if (cargoEpiAux.getId().equals(cargoEpi.getId())) {
    			continue;
    		}
        	if ((cargoEpiAux.getDataFim() == null) && (cargoEpi.getDataFim() == null)) {
        		return "Já existe uma recomendação sem data fim deste EPI!";
        	} 
        	if (cargoEpi.getDataFim() == null) {
        		if ((cargoEpi.getDataInicio().before(cargoEpiAux.getDataFim())) || (cargoEpi.getDataInicio().equals(cargoEpiAux.getDataFim()))) {
        			return "A data de início não pode ser anterior ou igual a data de fim ("+FormatacaoUtils.getDataString(cargoEpiAux.getDataFim())+") da última recomendação deste EPI!";
        		}
        	} else if (cargoEpiAux.getDataFim() == null) {
	        	if ((cargoEpi.getDataInicio().after(cargoEpiAux.getDataInicio())) || (cargoEpi.getDataFim().equals(cargoEpiAux.getDataInicio())) || 
	        		(cargoEpi.getDataInicio().equals(cargoEpiAux.getDataInicio())) || (cargoEpi.getDataFim().after(cargoEpiAux.getDataInicio()))) {
	        		return "Já existe uma recomendação deste EPI entre estas datas!";
	        	}    	        		
        	} else {    		
	        	if (((cargoEpi.getDataInicio().after(cargoEpiAux.getDataInicio()) && cargoEpi.getDataInicio().before(cargoEpiAux.getDataFim())) ||
	        	   (cargoEpi.getDataFim().after(cargoEpiAux.getDataInicio()) && cargoEpi.getDataFim().before(cargoEpiAux.getDataFim()))) ||
	        	   ((cargoEpi.getDataInicio().equals(cargoEpiAux.getDataInicio())) || (cargoEpi.getDataInicio().equals(cargoEpiAux.getDataFim())) ||
	        	   (cargoEpi.getDataFim().equals(cargoEpiAux.getDataInicio())) || (cargoEpi.getDataFim().equals(cargoEpiAux.getDataFim()))) ||
	        	   ((cargoEpi.getDataInicio().before(cargoEpiAux.getDataInicio())) && (cargoEpi.getDataFim().after(cargoEpiAux.getDataFim())))) {
	        		return "Já existe uma recomendação deste EPI entre estas datas!";
	        	}    		
        	}
    	}
		return null;
	}
}