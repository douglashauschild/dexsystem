package br.com.dh.dexSystem.validation;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.dh.dexSystem.manager.AtividadeManager;
import br.com.dh.dexSystem.manager.EmpregadoManager;
import br.com.dh.dexSystem.manager.EmpresaManager;
import br.com.dh.dexSystem.model.Atividade;
import br.com.dh.dexSystem.model.Empregado;
import br.com.dh.dexSystem.model.Empresa;
import br.com.dh.dexSystem.util.FormatacaoUtils;

@Service
public class ValidacaoEmpregado implements Serializable {	

	private static final long serialVersionUID = -5855369587941857700L;
	
	@Autowired
	private EmpregadoManager empregadoManager;
	@Autowired
	private EmpresaManager empresaManager;
	@Autowired
	private AtividadeManager atividadeManager;
	

	public String validar(Empregado empregado) {
		Empresa empresa = empresaManager.buscarPorId(empregado.getIdEmpresa());
		if ((empregado.getDataDemissao() != null) && (empregado.getDataDemissao().before(empregado.getDataAdmissao()))) {
			return "A data de demissão não pode ser anterior a data de admissão!";
		}
		if (empresa.getPessoaJuridica().getDataInicio().after(empregado.getDataAdmissao())) {
			return "A data de admissão do empregado não pode ser anterior a data de início ("+FormatacaoUtils.getDataString(empresa.getPessoaJuridica().getDataInicio())+") da empresa!";
		}
		if ((empregado.getDataDemissao() != null) && (empresa.getPessoaJuridica().getDataFim() != null) && (empresa.getPessoaJuridica().getDataFim().before(empregado.getDataDemissao()))) {
			return "A data de demissão do empregado não pode ser posterior a data de fim ("+FormatacaoUtils.getDataString(empresa.getPessoaJuridica().getDataFim())+") da empresa!";
		}
		Atividade atividade = atividadeManager.buscarAtivaPorEmpregado(empregado);
		if (atividade != null && atividade.getDataInicio().before(empregado.getDataAdmissao())) {
			return "A data de admissão do empregado não pode ser posterior a data de início ("+FormatacaoUtils.getDataString(atividade.getDataInicio())+") da sua atividade atual!";
		}
		List<Empregado> listaEmpregados = empregadoManager.buscarParaValidacao(empregado);
		for (Empregado empregadoAux : listaEmpregados) {
			if (empregado.getDataDemissao() == null && empregadoAux.getDataDemissao() == null) {
				return "Este empregado já está admitido nesta empresa desde "+FormatacaoUtils.getDataString(empregadoAux.getDataAdmissao())+"!";
			} else if (empregado.getDataDemissao() == null) {
				if ((empregado.getDataAdmissao().before(empregadoAux.getDataDemissao())) || (empregado.getDataAdmissao().equals(empregadoAux.getDataDemissao()))) {
					return "A data de admissão deste empregado não pode ser anterior ou igual a data de demissão ("+FormatacaoUtils.getDataString(empregadoAux.getDataDemissao())+") dos últimos contratos de trabalho!";
				}
			} else if (empregadoAux.getDataDemissao() == null) {
				if ((empregado.getDataAdmissao().after(empregadoAux.getDataAdmissao())) || (empregado.getDataDemissao().equals(empregadoAux.getDataAdmissao())) || 
					(empregado.getDataAdmissao().equals(empregadoAux.getDataAdmissao())) || (empregado.getDataDemissao().after(empregadoAux.getDataAdmissao()))) {
					return "Este empregado já possui um contrato de trabalho entre essas datas!";
				}
			} else {
				if (((empregado.getDataAdmissao().after(empregadoAux.getDataAdmissao()) && empregado.getDataAdmissao().before(empregadoAux.getDataDemissao())) ||
					(empregado.getDataDemissao().after(empregadoAux.getDataAdmissao()) && empregado.getDataDemissao().before(empregadoAux.getDataDemissao()))) ||
					((empregado.getDataAdmissao().equals(empregadoAux.getDataAdmissao())) || (empregado.getDataAdmissao().equals(empregadoAux.getDataDemissao())) ||
					(empregado.getDataDemissao().equals(empregadoAux.getDataAdmissao())) || (empregado.getDataDemissao().equals(empregadoAux.getDataDemissao()))) ||
					((empregado.getDataAdmissao().before(empregadoAux.getDataAdmissao())) && (empregado.getDataDemissao().after(empregadoAux.getDataDemissao())))) {
					return "Este empregado já possui um contrato de trabalho entre essas datas!";
				}
			}
		}
		return null;
	}
	
	
	public String validarExclusao(Empregado empregado) {
		Atividade atividade = atividadeManager.buscarAtivaPorEmpregado(empregado);
		if (atividade != null) {
			return "Não será permitido excluir este empregado enquanto houver atividades ativas vinculadas a ele!";
		}
		return null;
	}
}