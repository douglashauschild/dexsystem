package br.com.dh.dexSystem.validation;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.dh.dexSystem.manager.EmpregadoManager;
import br.com.dh.dexSystem.manager.SetorManager;
import br.com.dh.dexSystem.model.Empregado;
import br.com.dh.dexSystem.model.Empresa;
import br.com.dh.dexSystem.model.Setor;

@Service
public class ValidacaoEmpresa implements Serializable {	

	private static final long serialVersionUID = 8871460489750672512L;
	
	@Autowired
	private SetorManager setorManager;
	@Autowired
	private EmpregadoManager empregadoManager;
	

	public String validar(Empresa empresa) {
		List<Setor> setores = setorManager.buscarAtivosPorEmpresaComDataInicioMenor(empresa.getPessoaJuridica().getIdPessoa(), empresa.getPessoaJuridica().getDataInicio());
		if (setores != null && !setores.isEmpty()) {
			return "A data de início da empresa não pode ser posterior a data de início dos seus setores cadastrados!";
		}
		return null;
	}
	
	public String validarExclusao(Empresa empresa, boolean excluindo) {
		List<Setor> setores = setorManager.buscarTodosPorEmpresa(empresa.getIdPessoa(), false);
		List<Empregado> empregados = empregadoManager.buscarAtivosPorEmpresa(empresa.getIdPessoa());
		if ((setores != null && !setores.isEmpty()) || (empregados != null && !empregados.isEmpty())) {
			String retorno = "Não será permitido ";
			if (excluindo) {
				retorno += "excluir ";
			} else {
				retorno += "finalizar ";
			}
			retorno += "esta empresa enquanto houver setores e empregados ativos vinculados a ela!";
			return retorno;
		}
		return null;
	}
}