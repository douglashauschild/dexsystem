package br.com.dh.dexSystem.validation;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.dh.dexSystem.manager.AtividadeManager;
import br.com.dh.dexSystem.manager.EmpresaManager;
import br.com.dh.dexSystem.model.Atividade;
import br.com.dh.dexSystem.model.Empresa;
import br.com.dh.dexSystem.util.FormatacaoUtils;

@Service
public class ValidacaoAtividade implements Serializable {	

	private static final long serialVersionUID = -2230483725029541656L;
	
	@Autowired
	private AtividadeManager atividadeManager;
	@Autowired
	private EmpresaManager empresaManager;
	

	public String validar(Atividade atividade) {
		Empresa empresa = empresaManager.buscarPorId(atividade.getEmpregado().getIdEmpresa());
		if ((atividade.getDataFim() != null) && (atividade.getDataFim().before(atividade.getDataInicio()))) {
			return "A data de fim não pode ser anterior a data de início!";
		}
		if (empresa.getPessoaJuridica().getDataInicio().after(atividade.getDataInicio())) {
			return "A data de início da atividade não pode ser anterior a data de início ("+FormatacaoUtils.getDataString(empresa.getPessoaJuridica().getDataInicio())+") da empresa!";
		}
		if (atividade.getEmpregado().getDataAdmissao().after(atividade.getDataInicio())) {
			return "A data de início da atividade não pode ser anterior a data de admissão ("+FormatacaoUtils.getDataString(atividade.getEmpregado().getDataAdmissao())+") do empregado!";
		}
		if ((atividade.getDataFim() != null) && (empresa.getPessoaJuridica().getDataFim() != null) && (empresa.getPessoaJuridica().getDataFim().before(atividade.getDataFim()))) {
			return "A data de fim da atividade não pode ser posterior a data de fim ("+FormatacaoUtils.getDataString(empresa.getPessoaJuridica().getDataFim())+") da empresa!";
		}
		
		if ((atividade.getDataFim() != null) && (atividade.getEmpregado().getDataDemissao() != null) && (atividade.getEmpregado().getDataDemissao().before(atividade.getDataFim()))) {
			return "A data de fim da atividade não pode ser posterior a data de demissão ("+FormatacaoUtils.getDataString(atividade.getEmpregado().getDataDemissao())+") do empregado!";
		}
		List<Atividade> listaAtividades = atividadeManager.buscarParaValidacao(atividade);
		for(Atividade atividadeAux : listaAtividades) {
			if (atividade.getDataFim() == null && atividadeAux.getDataFim() == null) {
				return "Este empregado já possui uma atividade ativa nesta empresa desde "+FormatacaoUtils.getDataString(atividadeAux.getDataInicio())+"!";
			} else if (atividade.getDataFim() == null) {
				if ((atividade.getDataInicio().before(atividadeAux.getDataFim())) || (atividade.getDataInicio().equals(atividadeAux.getDataFim()))) {
					return "A data de início desta atividade não pode ser anterior ou igual a data de fim ("+FormatacaoUtils.getDataString(atividadeAux.getDataFim())+") das últimas atividades!";
				}
			} else if (atividadeAux.getDataFim() == null) {
				if ((atividade.getDataInicio().after(atividadeAux.getDataInicio())) || (atividade.getDataFim().equals(atividadeAux.getDataInicio())) || 
					(atividade.getDataInicio().equals(atividadeAux.getDataInicio())) || (atividade.getDataFim().after(atividadeAux.getDataInicio()))) {
					return "Este empregado já possui uma atividade nesta empresa entre essas datas!";
				}
			} else {
				if (((atividade.getDataInicio().after(atividadeAux.getDataInicio()) && atividade.getDataInicio().before(atividadeAux.getDataFim())) ||
					(atividade.getDataFim().after(atividadeAux.getDataInicio()) && atividade.getDataFim().before(atividadeAux.getDataFim()))) ||
					((atividade.getDataInicio().equals(atividadeAux.getDataInicio())) || (atividade.getDataInicio().equals(atividadeAux.getDataFim())) ||
					(atividade.getDataFim().equals(atividadeAux.getDataInicio())) || (atividade.getDataFim().equals(atividadeAux.getDataFim()))) ||
					((atividade.getDataInicio().before(atividadeAux.getDataInicio())) && (atividade.getDataFim().after(atividadeAux.getDataFim())))) {
					return "Este empregado já possui uma atividade nesta empresa entre essas datas!";
				}
			}
		}
		return null;
	}
}