package br.com.dh.dexSystem.validation;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.dh.dexSystem.manager.CargoManager;
import br.com.dh.dexSystem.manager.EmpresaManager;
import br.com.dh.dexSystem.manager.SetorManager;
import br.com.dh.dexSystem.model.Cargo;
import br.com.dh.dexSystem.model.Empresa;
import br.com.dh.dexSystem.model.Setor;
import br.com.dh.dexSystem.util.FormatacaoUtils;

@Service
public class ValidacaoSetor implements Serializable {	

	private static final long serialVersionUID = 7044435756278653209L;
	@Autowired
	private EmpresaManager empresaManager;
	@Autowired
	private SetorManager setorManager;
	@Autowired
	private CargoManager cargoManager;
	

	public String validar(Setor setor) {
		Empresa empresa = empresaManager.buscarPorId(setor.getIdEmpresa());
		if ((setor.getDataFim() != null) && (setor.getDataFim().before(setor.getDataInicio()))) {
			return "A data de fim não pode ser anterior a data de início!";
		}
		if (empresa.getPessoaJuridica().getDataInicio().after(setor.getDataInicio())) {
			return "A data de início do setor não pode ser anterior a data de início ("+FormatacaoUtils.getDataString(empresa.getPessoaJuridica().getDataInicio())+") da empresa!";
		}
		if ((setor.getDataFim() != null) && (empresa.getPessoaJuridica().getDataFim() != null) && (empresa.getPessoaJuridica().getDataFim().before(setor.getDataFim()))) {
			return "A data de fim do setor não pode ser posterior a data de fim ("+FormatacaoUtils.getDataString(empresa.getPessoaJuridica().getDataFim())+") da empresa!";
		}
		List<Cargo> cargos = cargoManager.buscarAtivosPorSetorComDataInicioMenor(setor.getId(), setor.getDataInicio());
		if (cargos != null && !cargos.isEmpty()) {
			return "A data de início do setor não pode ser posterior a data de início dos seus cargos cadastrados!";
		}
		List<Setor> setores = setorManager.buscarPorSetor(setor);
    	for (Setor setorAux : setores) {
    		if (setorAux.getId().equals(setor.getId())) {
    			continue;
    		}
        	if ((setorAux.getDataFim() == null) && (setor.getDataFim() == null)) {
        		return "Já existe um setor ativo com este nome vinculado a esta empresa!";
        	} 
        	if (setor.getDataFim() == null) {
        		if ((setor.getDataInicio().before(setorAux.getDataFim())) || (setor.getDataInicio().equals(setorAux.getDataFim()))) {
        			return "A data de início não pode ser anterior ou igual a data de fim ("+FormatacaoUtils.getDataString(setorAux.getDataFim())+") do último setor lançado com este nome!";
        		}
        	} else if (setorAux.getDataFim() == null) {
	        	if ((setor.getDataInicio().after(setorAux.getDataInicio())) || (setor.getDataFim().equals(setorAux.getDataInicio())) || 
	        		(setor.getDataInicio().equals(setorAux.getDataInicio())) || (setor.getDataFim().after(setorAux.getDataInicio()))) {
	        		return "Já existe um setor cadastrado com este nome entre estas datas!";
	        	}    	        		
        	} else {    		
	        	if (((setor.getDataInicio().after(setorAux.getDataInicio()) && setor.getDataInicio().before(setorAux.getDataFim())) ||
	        	   (setor.getDataFim().after(setorAux.getDataInicio()) && setor.getDataFim().before(setorAux.getDataFim()))) ||
	        	   ((setor.getDataInicio().equals(setorAux.getDataInicio())) || (setor.getDataInicio().equals(setorAux.getDataFim())) ||
	        	   (setor.getDataFim().equals(setorAux.getDataInicio())) || (setor.getDataFim().equals(setorAux.getDataFim()))) ||
	        	   ((setor.getDataInicio().before(setorAux.getDataInicio())) && (setor.getDataFim().after(setorAux.getDataFim())))) {
	        		return "Já existe um setor cadastrado com este nome entre estas datas!";
	        	}    		
        	}
    	}
		return null;
	}
	
	
	public String validarExclusao(Setor setor, boolean excluindo) {
		List<Cargo> cargos = cargoManager.buscarTodosPorSetor(setor.getId(), false);
		if (cargos != null && !cargos.isEmpty()) {
			String retorno = "Não será permitido ";
			if (excluindo) {
				retorno += "excluir ";
			} else {
				retorno += "finalizar ";
			}
			retorno += "este setor enquanto houver cargos ativos vinculados a ele!";
			return retorno;
		}
		return null;
	}
	
	
	public String validarAtivacao(Setor setor) {
		String retorno = null;
		Empresa empresa = empresaManager.buscarPorId(setor.getIdEmpresa());
		if (empresa == null) {
			retorno = "Não será possível reativar este setor pois a empresa vinculada a ele foi excluída!";
		} else if (empresa != null && !empresa.isAtiva()) {
			retorno = "Não será permitido reativar este setor enquanto a empresa "+empresa.getRazaoSocial()+" estiver inativa!";
		}
		return retorno;
	}
}