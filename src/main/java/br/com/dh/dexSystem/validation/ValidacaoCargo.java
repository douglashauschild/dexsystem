package br.com.dh.dexSystem.validation;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.dh.dexSystem.manager.AtividadeManager;
import br.com.dh.dexSystem.manager.CargoEpiManager;
import br.com.dh.dexSystem.manager.CargoManager;
import br.com.dh.dexSystem.manager.SetorManager;
import br.com.dh.dexSystem.model.Atividade;
import br.com.dh.dexSystem.model.Cargo;
import br.com.dh.dexSystem.model.CargoEpi;
import br.com.dh.dexSystem.model.Setor;
import br.com.dh.dexSystem.util.FormatacaoUtils;

@Service
public class ValidacaoCargo implements Serializable {	

	private static final long serialVersionUID = -8408028747728040028L;
	
	@Autowired
	private SetorManager setorManager;
	@Autowired
	private CargoManager cargoManager;
	@Autowired
	private CargoEpiManager cargoEpiManager;
	@Autowired
	private AtividadeManager atividadeManager;
	

	public String validar(Cargo cargo) {
		Setor setor = setorManager.buscarPorId(cargo.getIdSetor());
		if ((cargo.getDataFim() != null) && (cargo.getDataFim().before(cargo.getDataInicio()))) {
			return "A data de fim não pode ser anterior a data de início!";
		}
		if (setor.getDataInicio().after(cargo.getDataInicio())) {
			return "A data de início do cargo não pode ser anterior a data de início ("+FormatacaoUtils.getDataString(setor.getDataInicio())+") do setor!";
		}
		if ((cargo.getDataFim() != null) && (setor.getDataFim() != null) && (setor.getDataFim().before(cargo.getDataFim()))) {
			return "A data de fim do cargo não pode ser posterior a data de fim ("+FormatacaoUtils.getDataString(setor.getDataFim())+") do setor!";
		}
		List<CargoEpi> cargoEpi = cargoEpiManager.buscarAtivosPorCargoComDataInicioMenor(cargo, cargo.getDataInicio());
		if (cargoEpi != null && !cargoEpi.isEmpty()) {
			return "A data de início do cargo não pode ser posterior a data de início das recomendações de EPIs cadastradas!";
		}
		List<Cargo> cargos = cargoManager.buscarPorCargo(cargo);
    	for (Cargo cargoAux : cargos) {
    		if (cargoAux.getId().equals(cargo.getId())) {
    			continue;
    		}
        	if ((cargoAux.getDataFim() == null) && (cargo.getDataFim() == null)) {
        		return "Já existe um cargo ativo com este nome vinculado a este setor!";
        	} 
        	if (cargo.getDataFim() == null) {
        		if ((cargo.getDataInicio().before(cargoAux.getDataFim())) || (cargo.getDataInicio().equals(cargoAux.getDataFim()))) {
        			return "A data de início não pode ser anterior ou igual a data de fim ("+FormatacaoUtils.getDataString(cargoAux.getDataFim())+") do último cargo lançado com este nome!";
        		}
        	} else if (cargoAux.getDataFim() == null) {
	        	if ((cargo.getDataInicio().after(cargoAux.getDataInicio())) || (cargo.getDataFim().equals(cargoAux.getDataInicio())) || 
	        		(cargo.getDataInicio().equals(cargoAux.getDataInicio())) || (cargo.getDataFim().after(cargoAux.getDataInicio()))) {
	        		return "Já existe um cargo cadastrado com este nome entre estas datas!";
	        	}    	        		
        	} else {    		
	        	if (((cargo.getDataInicio().after(cargoAux.getDataInicio()) && cargo.getDataInicio().before(cargoAux.getDataFim())) ||
	        	   (cargo.getDataFim().after(cargoAux.getDataInicio()) && cargo.getDataFim().before(cargoAux.getDataFim()))) ||
	        	   ((cargo.getDataInicio().equals(cargoAux.getDataInicio())) || (cargo.getDataInicio().equals(cargoAux.getDataFim())) ||
	        	   (cargo.getDataFim().equals(cargoAux.getDataInicio())) || (cargo.getDataFim().equals(cargoAux.getDataFim()))) ||
	        	   ((cargo.getDataInicio().before(cargoAux.getDataInicio())) && (cargo.getDataFim().after(cargoAux.getDataFim())))) {
	        		return "Já existe um cargo cadastrado com este nome entre estas datas!";
	        	}    		
        	}
    	}
		return null;
	}
	
	
	public String validarExclusao(Cargo cargo, boolean excluindo) {
		List<CargoEpi> cargosEpis = cargoEpiManager.buscarAtivosPorCargo(cargo);
		List<Atividade> atividades = atividadeManager.buscarAtivasPorCargo(cargo);
		if ((cargosEpis != null && !cargosEpis.isEmpty()) || (atividades != null && !atividades.isEmpty())) {
			String retorno = "Não será permitido ";
			if (excluindo) {
				retorno += "excluir ";
			} else {
				retorno += "finalizar ";
			}
			retorno += "este cargo enquanto houver EPIs recomendados ou atividades ativas vinculadas a ele!";
			return retorno;
		}
		return null;
	}
	
	
	public String validarAtivacao(Cargo cargo) {
		String retorno = null;
		Setor setor = setorManager.buscarPorId(cargo.getIdSetor());
		if (setor == null) {
			retorno = "Não será possível reativar este cargo pois o setor vinculado a ele foi excluído!";
		} else if (setor != null && !setor.isAtivo()) {
			retorno = "Não será permitido reativar este cargo enquanto o setor "+setor.getNome()+" estiver inativo!";
		}
		return retorno;
	}
}