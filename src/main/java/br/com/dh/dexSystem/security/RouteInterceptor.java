package br.com.dh.dexSystem.security;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import br.com.dh.dexSystem.model.constant.PermissoesEnum;
import br.com.dh.dexSystem.util.TokenUtils;

public class RouteInterceptor extends HandlerInterceptorAdapter {
	
	@Value("${api.hash}")
	private String apiHash;
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		boolean acessar = true;
		String url403 = request.getContextPath()+"/403";
		String url404 = request.getContextPath()+"/404";
		
		String path = request.getRequestURI().substring(request.getContextPath().length());
		if (path != null && path.contains("/api")) {
			if (path.contains("/api/autenticar")) {
				return true;
			} else {
				boolean tokenValido = TokenUtils.validarToken(request.getHeader("Authorization"));
				if (!tokenValido) {
					response.setStatus(401);
				}
				return tokenValido;
			}
		}
		if (path != null && !path.isEmpty() && !path.equals("/") && getRotasLiberadas(path)) {
			if (path.equals("/error")) {
				int status = (Integer) request.getAttribute("javax.servlet.error.status_code");
				if (status == 404) {
					response.sendRedirect(url404);
				} else {
					response.sendRedirect(url403);
				}
				acessar = false;
			} else {
				Map<String, Map<String, Boolean>> permissoes = ((UsuarioSistema) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsuario().getPermissoes();
				
				String[] paths = path.split("/");
				Map<String, Boolean> permissao = permissoes.get("/"+paths[1]);
				if (paths.length > 2) {
					String acaoPath = "/"+paths[2];
					if (acaoPath.equals("/novo") || acaoPath.equals("/editar") || acaoPath.equals("/excluir")) {
						if (acaoPath.equals("/novo")) {
							if (!permissao.get(PermissoesEnum.INSERIR.getKey())) {
								response.sendRedirect(url403);
								acessar = false;
							}
						} else if (acaoPath.equals("/editar")) {
							if (!permissao.get(PermissoesEnum.ALTERAR.getKey())) {
								response.sendRedirect(url403);
								acessar = false;
							}
						} else if (acaoPath.equals("/excluir")) {
							if (!permissao.get(PermissoesEnum.EXCLUIR.getKey())) {
								response.sendRedirect(url403);
								acessar = false;
							}
						}
					} else {
						//significa que ja passou pela primeira autenticacao e pode acessar
						acessar = true;
					}
				} else {
					if (!permissao.get(PermissoesEnum.ACESSAR.getKey())) {
						response.sendRedirect(request.getContextPath()+"/403");
						acessar = false;
					}
				}
			}
		}
		return acessar;
	}
	
	private boolean getRotasLiberadas(String path) {
		return !path.equals("/login") && !path.equals("/dashboard") && !path.equals("/403") && !path.equals("/404") && path.contains("/download");
	}
}