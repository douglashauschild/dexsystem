package br.com.dh.dexSystem.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Service;

import br.com.dh.dexSystem.model.Usuario;
import br.com.dh.dexSystem.repository.UsuarioRepository;

@Service("authenticationSuccessHandlerImpl")
public class AuthenticationSuccessHandlerImpl implements AuthenticationSuccessHandler {
	 
	@Value("${server.contextPath}")
	private String contextPath;
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
 
    	HttpSession session = httpServletRequest.getSession();
        User authUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Usuario usuario = usuarioRepository.findByEmailAndExcluidoIn(authUser.getUsername(), '0');
        session.setAttribute("ID", usuario.getId());
        
        httpServletResponse.sendRedirect(contextPath+"/dashboard");
    }
}
