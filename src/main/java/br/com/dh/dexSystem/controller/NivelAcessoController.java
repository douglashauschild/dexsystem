package br.com.dh.dexSystem.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.dh.dexSystem.model.NivelAcesso;
import br.com.dh.dexSystem.repository.filter.NivelAcessoFilter;
import br.com.dh.dexSystem.service.NivelAcessoService;

@Controller	
@RequestMapping("/nivelAcesso")	
public class NivelAcessoController extends ControllerExtend {
	
	@Autowired
	private NivelAcessoService nivelAcessoService;
	
	private NivelAcessoFilter nivelAcessoFilter;
	
	
	@RequestMapping
	public ModelAndView pesquisar(NivelAcessoFilter filtro, BindingResult result, Pageable pageable, HttpServletRequest httpServletRequest) {
		this.nivelAcessoFilter = filtro;
		
		ModelAndView mv = newModelAndView("cadastros/nivelAcesso/nivelAcesso-lista", httpServletRequest);

		PageWrapper<NivelAcesso> paginaWrapper = nivelAcessoService.pesquisar(filtro, pageable, httpServletRequest);
		mv.addObject("pagina", paginaWrapper);
		
		return mv;
	}
	
	
	@RequestMapping("/novo")
	public ModelAndView novo() {	
		ModelAndView mv = new ModelAndView("cadastros/nivelAcesso/nivelAcesso-cadastro");
		NivelAcesso nivelAcesso = new NivelAcesso();
		nivelAcesso.setNivel(nivelAcessoService.getProximoNivel());
		nivelAcesso.setMenuAcessos(nivelAcessoService.getMenuAcessoPermissao());
		
		mv.addObject("nivelAcesso", nivelAcesso);
		return mv;
	}
	
	
	@RequestMapping(value = "/gravar", method = RequestMethod.POST)
	public String gravar(NivelAcesso nivelAcesso, Errors errors, RedirectAttributes attributes) {
		String msgValidacao = nivelAcessoService.isValidaGravacao(nivelAcesso);
		if (msgValidacao != null && !msgValidacao.isEmpty()) {
			errors.rejectValue(null, null, msgValidacao);
			return "cadastros/nivelAcesso/nivelAcesso-cadastro";
		}
		nivelAcessoService.salvar(nivelAcesso);
		attributes.addFlashAttribute("mensagem", "Nível de Acesso salvo com sucesso!");
		return "redirect:/nivelAcesso";
	}
	
	
	@RequestMapping("/editar/{id}")
	public ModelAndView edicao(@PathVariable("id") Long id) {
		NivelAcesso nivelAcesso = nivelAcessoService.buscarPorId(id);
		nivelAcesso.setMenuAcessos(nivelAcessoService.getMenuAcessoPermissao(nivelAcesso.getId()));
		
		ModelAndView mv = new ModelAndView("cadastros/nivelAcesso/nivelAcesso-cadastro");
		mv.addObject("nivelAcesso", nivelAcesso);
		return mv;
	}
	
	
	@RequestMapping("/excluir/{id}")
	public String excluir(@PathVariable("id") Long id, RedirectAttributes attributes) {
		NivelAcesso nivelAcesso = nivelAcessoService.buscarPorId(id);
		String msgValidacao = nivelAcessoService.isValidaExclusao(nivelAcesso);
		if (msgValidacao != null && !msgValidacao.isEmpty()) {
			attributes.addFlashAttribute("mensagemErro", msgValidacao);
		} else {
			nivelAcessoService.excluir(nivelAcesso);
			attributes.addFlashAttribute("mensagem", "Nível de Acesso excluído com sucesso!");
		}
		return "redirect:/nivelAcesso";
	}
	
	
	@RequestMapping(value = "/downloadPdf")
	public String gerarPdf(Model model, HttpServletRequest httpServletRequest)  {
		model.addAttribute("relatorio", nivelAcessoService.getRelatorioPdfVO(nivelAcessoFilter, httpServletRequest));
		return "pdfView";
	}

}