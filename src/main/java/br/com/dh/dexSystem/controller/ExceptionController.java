package br.com.dh.dexSystem.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ExceptionController {
	
	@RequestMapping(value = "/403")
	public ModelAndView acessoNegado() {
		ModelAndView mv = new ModelAndView("erros/403");
		return mv;
	}
	
	@RequestMapping(value = "/404")
	public ModelAndView naoEncontrado() {
		ModelAndView mv = new ModelAndView("erros/404");
		return mv;
	}
}