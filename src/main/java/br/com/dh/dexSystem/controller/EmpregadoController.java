package br.com.dh.dexSystem.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.dh.dexSystem.model.Empregado;
import br.com.dh.dexSystem.model.vo.EmpregadoVO;
import br.com.dh.dexSystem.model.vo.SelectVO;
import br.com.dh.dexSystem.repository.filter.EmpregadoFilter;
import br.com.dh.dexSystem.service.CombosService;
import br.com.dh.dexSystem.service.EmpregadoService;

@Controller
@RequestMapping("/empregado")	
public class EmpregadoController extends ControllerExtend {
	
	@Autowired
	private EmpregadoService empregadoService;
	@Autowired
	private CombosService combosService;
	
	private EmpregadoFilter empregadoFilter;
	
	
	@RequestMapping
	public ModelAndView pesquisar(EmpregadoFilter filtro, BindingResult result, Pageable pageable, HttpServletRequest httpServletRequest) throws Exception {
		this.empregadoFilter = filtro;
		
		ModelAndView mv = newModelAndView("cadastros/empregado/empregado-lista", httpServletRequest);
		
		PageWrapper<Empregado> paginaWrapper = empregadoService.pesquisar(filtro, pageable, httpServletRequest);
		mv.addObject("pagina", paginaWrapper);

		return mv;
	}
	
	
	@RequestMapping("/novo")
	public ModelAndView novo() {	
		ModelAndView mv = new ModelAndView("cadastros/empregado/empregado-cadastro");
		mv.addObject("empregado", new Empregado());
		return mv;
	}
	
	
	@RequestMapping(value = "/gravar", method = RequestMethod.POST)
	public String gravar(Empregado empregado, Model model, Errors errors, RedirectAttributes attributes) throws Exception {
		String validarGravacao = empregadoService.validarGravacao(empregado, model, errors);
		if (validarGravacao != null && !validarGravacao.isEmpty()) {
			return validarGravacao;
		}
		return gravarEmpregado(empregado, attributes);
	}
	
	
	@RequestMapping(value = "/gravarModal", method = RequestMethod.POST)
	public String gravarModal(Empregado empregado, Errors errors, RedirectAttributes attributes) throws Exception {
		return gravarEmpregado(empregado, attributes);
	}
	
	
	private String gravarEmpregado(Empregado empregado, RedirectAttributes attributes) throws Exception {
		empregado = empregadoService.ajustarGravacao(empregado);
		empregado = empregadoService.finalizarRegistros(empregado);
		empregadoService.salvar(empregado);
		attributes.addFlashAttribute("mensagem", "Empregado salvo com sucesso!");
		return "redirect:/empregado";
	}
	
	
	@RequestMapping("/editar/{id}")
	public ModelAndView edicao(@PathVariable("id") Long id) {
		Empregado empregado = empregadoService.buscarPorId(id);
		
		ModelAndView mv = new ModelAndView("cadastros/empregado/empregado-cadastro");
		mv.addObject("empregado", empregado);
		return mv;
	}
	
	
	@RequestMapping("/excluir/{id}")
	public String excluir(@PathVariable("id") Long id, RedirectAttributes attributes) throws Exception {
		Empregado empregado = empregadoService.buscarPorId(id);
		String msgValidacao = empregadoService.validarExclusao(empregado);
		if (msgValidacao != null && !msgValidacao.isEmpty()) {
			attributes.addFlashAttribute("mensagemErro", msgValidacao);
		} else {
			empregadoService.excluir(empregado);
			attributes.addFlashAttribute("mensagem", "Empregado excluído com sucesso!");
		}
		return "redirect:/empregado";
	}
	
	
	@RequestMapping(value = "/cadastro/senha", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<String> entregar(@RequestParam("idEmpregado") Long idEmpregado, @RequestParam("senha") String senha, @RequestParam("confirmaSenha") String confirmaSenha)  {
		try {
			Empregado empregado = empregadoService.buscarPorId(idEmpregado);
			if (empregado != null) {
				empregado.setSenha(senha);
				empregado.setConfirmaSenha(confirmaSenha);
				
				if (empregadoService.senhasIguais(empregado)) {
					empregado.setSenha(new BCryptPasswordEncoder().encode(empregado.getSenha()));
					empregadoService.salvar(empregado);
					
					return new ResponseEntity<String>("Senha cadastrada com sucesso!", HttpStatus.OK);
				} else {
					return new ResponseEntity<String>("A senha e a confimação da senha não são iguais!", HttpStatus.FORBIDDEN);
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<String>("Ocorreu algum problema ao cadastrar a senha! Contate um administrador do sistema.", HttpStatus.INTERNAL_SERVER_ERROR);
	}

	
	@RequestMapping(value = "/downloadPdf")
	public String gerarPdf(Model model, HttpServletRequest httpServletRequest)  {
		model.addAttribute("relatorio", empregadoService.getRelatorioPdfVO(empregadoFilter, httpServletRequest));
		return "pdfView";
	}

	@ModelAttribute("pessoasFisicasCombo")
	public @ResponseBody List<SelectVO> getPessoasFisicasCombo()  {
		return combosService.getPessoasFisicasCombo(false);
	}
	
	@RequestMapping(value = "/buscar", method = RequestMethod.GET)
	public @ResponseBody List<SelectVO> getEmpregadosCombo(@RequestParam("idEmpresa") Long idEmpresa)  {
		return combosService.getEmpregadosCombo(idEmpresa);
	}
	
	@RequestMapping(value = "/buscar/informacoes", method = RequestMethod.POST)
	public @ResponseBody EmpregadoVO getEmpregadoInformacao(@RequestParam("idEmpregado") Long idEmpregado)  {
		return empregadoService.getInformacoes(idEmpregado);
	}
}