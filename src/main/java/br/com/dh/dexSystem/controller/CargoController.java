package br.com.dh.dexSystem.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.dh.dexSystem.model.Cargo;
import br.com.dh.dexSystem.model.vo.SelectVO;
import br.com.dh.dexSystem.repository.filter.CargoFilter;
import br.com.dh.dexSystem.service.CargoService;
import br.com.dh.dexSystem.service.CombosService;

@Controller
@RequestMapping("/cargo")	
public class CargoController extends ControllerExtend {
	
	@Autowired
	private CargoService cargoService;
	@Autowired
	private CombosService combosService;
	
	private CargoFilter cargoFilter;
	
	
	@RequestMapping
	public ModelAndView pesquisar(CargoFilter filtro, BindingResult result, Pageable pageable, HttpServletRequest httpServletRequest) throws Exception {
		this.cargoFilter = filtro;
		
		ModelAndView mv = newModelAndView("cadastros/cargo/cargo-lista", httpServletRequest);
		
		PageWrapper<Cargo> paginaWrapper = cargoService.pesquisar(filtro, pageable, httpServletRequest);
		mv.addObject("pagina", paginaWrapper);

		return mv;
	}
	
	
	@RequestMapping("/novo")
	public ModelAndView novo() {	
		ModelAndView mv = new ModelAndView("cadastros/cargo/cargo-cadastro");
		mv.addObject("cargo", new Cargo());
		return mv;
	}
	
	
	@RequestMapping(value = "/gravar", method = RequestMethod.POST)
	public String gravar(Cargo cargo, Model model, Errors errors, RedirectAttributes attributes) throws Exception {
		String validarGravacao = cargoService.validarGravacao(cargo, model, errors);
		if (validarGravacao != null && !validarGravacao.isEmpty()) {
			return validarGravacao;
		}
		cargoService.salvar(cargo);
		attributes.addFlashAttribute("mensagem", "Cargo salvo com sucesso!");
		return "redirect:/cargo";
	}

	
	@RequestMapping("/editar/{id}")
	public ModelAndView edicao(@PathVariable("id") Long id) {
		Cargo cargo = cargoService.buscarPorId(id);
		
		ModelAndView mv = new ModelAndView("cadastros/cargo/cargo-cadastro");
		mv.addObject("cargo", cargo);
		return mv;
	}
	
	
	@RequestMapping("/excluir/{id}")
	public String excluir(@PathVariable("id") Long id, RedirectAttributes attributes) throws Exception {
		Cargo cargo = cargoService.buscarPorId(id);
		String msgValidacao = cargoService.validarExclusao(cargo);
		if (msgValidacao != null && !msgValidacao.isEmpty()) {
			attributes.addFlashAttribute("mensagemErro", msgValidacao);
		} else {
			cargoService.excluir(cargo);
			attributes.addFlashAttribute("mensagem", "Cargo excluído com sucesso!");
		}
		return "redirect:/cargo";
	}
	
	
	@RequestMapping(value = "/downloadPdf")
	public String gerarPdf(Model model, HttpServletRequest httpServletRequest)  {
		model.addAttribute("relatorio", cargoService.getRelatorioPdfVO(cargoFilter, httpServletRequest));
		return "pdfView";
	}
	
	@ModelAttribute("statusCombo")
	public @ResponseBody List<SelectVO> getStatusCombo()  {
		return combosService.getStatusSetorCargoCombo();
	}
	
	@RequestMapping(value = "/buscar", method = RequestMethod.GET)
	public @ResponseBody List<SelectVO> getCargosCombo(@RequestParam("filtro") boolean filtro, @RequestParam("idSetor") Long idSetor, @RequestParam("idCargo") Long idCargo)  {
		return combosService.getCargosCombo(filtro, idSetor, idCargo);
	}

}