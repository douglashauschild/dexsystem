package br.com.dh.dexSystem.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.dh.dexSystem.model.Empregado;
import br.com.dh.dexSystem.model.EmpregadoEpi;
import br.com.dh.dexSystem.model.ParametroSistema;
import br.com.dh.dexSystem.model.vo.DetalhesEntregaVO;
import br.com.dh.dexSystem.model.vo.EntregaVO;
import br.com.dh.dexSystem.model.vo.EstatisticasEntregaVO;
import br.com.dh.dexSystem.model.vo.SelectVO;
import br.com.dh.dexSystem.repository.filter.EmpregadoEpiFilter;
import br.com.dh.dexSystem.service.AtualizacaoEpiService;
import br.com.dh.dexSystem.service.CombosService;
import br.com.dh.dexSystem.service.EmpregadoEpiService;
import br.com.dh.dexSystem.service.EmpregadoService;
import br.com.dh.dexSystem.service.ParametroSistemaService;
import br.com.dh.dexSystem.util.FormatacaoUtils;

@Controller
@RequestMapping("/controleEntrega")	
public class ControleEntregaController extends ControllerExtend {
	
	@Autowired
	private EmpregadoService empregadoService;
	@Autowired
	private EmpregadoEpiService empregadoEpiService;
	@Autowired
	private CombosService combosService;
	@Autowired
	private ParametroSistemaService parametroSistemaService;
	@Autowired
	private AtualizacaoEpiService atualizacaoEpiService;
	
	private EmpregadoEpiFilter empregadoEpiFilter;
	
	
	@RequestMapping
	public ModelAndView pesquisar(EmpregadoEpiFilter filtro, BindingResult result, Pageable pageable, HttpServletRequest httpServletRequest) throws Exception {
		this.empregadoEpiFilter = filtro;
		
		boolean possuiSenha = false;
		if (filtro.getIdEmpregado() != null) {
			Empregado empregado = empregadoService.buscarPorId(filtro.getIdEmpregado());
			if (empregado != null) {
				possuiSenha = empregado.isPossuiSenha();
				
				if (filtro.getIdEmpresa() == null) {
					filtro.setIdEmpresa(empregado.getIdEmpresa());
				}
			}
		}
		
		ModelAndView mv = newModelAndView("cadastros/controleEntrega/controleEntrega", httpServletRequest);
		
		PageWrapper<EmpregadoEpi> paginaWrapper = empregadoEpiService.pesquisar(filtro, pageable, httpServletRequest);
		mv.addObject("pagina", paginaWrapper);
		mv.addObject("entregaVO", new EntregaVO());
		mv.addObject("possuiSenha", possuiSenha);

		return mv;
	}
	
	@RequestMapping(value = "/entregar", method = RequestMethod.POST)
	public String entregar(@RequestParam("idEmpresa") Long idEmpresa, @RequestParam("idEmpregado") Long idEmpregado, @RequestParam("episSelecionados") String episSelecionados, Model model)  {
		String idsEpisSelecionados[] = episSelecionados.split(",");
		model.addAttribute("entregaVO", empregadoEpiService.getEntregaVO(idEmpresa, idEmpregado, idsEpisSelecionados));
		return "cadastros/controleEntrega/controleEntrega-entregaModal :: #entregaModalContent";
	}
	
	@RequestMapping(value = "/entregar/verificarSenha", method = RequestMethod.POST)
	public @ResponseBody boolean verificarSenhaEntrega(@RequestParam("idEmpregado") Long idEmpregado, @RequestParam("senha") String senha)  {
		boolean senhaCorreta = true;
		Empregado empregado = empregadoService.buscarPorId(idEmpregado);
		if (empregado != null) {
			senhaCorreta = new BCryptPasswordEncoder().matches(senha, empregado.getSenha());
		}
		return senhaCorreta;
	}
	
	@RequestMapping(value = "/entregar/confirmar", method = RequestMethod.POST)
	public String confirmarEntrega(EntregaVO entregaVO, RedirectAttributes attributes) throws Exception  {
		empregadoEpiService.confirmarEntrega(entregaVO);
		attributes.addFlashAttribute("mensagem", "Todos EPIs foram entregues com sucesso!");
		return "redirect:/controleEntrega?idEmpresa="+entregaVO.getIdEmpresa()+"&idEmpregado="+entregaVO.getIdEmpregado();
	}

	@RequestMapping(value = "/atualizar/{idEmpregado}", method = RequestMethod.GET)
	public ModelAndView atualizar(@PathVariable("idEmpregado") Long idEmpregado, EmpregadoEpiFilter filtro, BindingResult result, Pageable pageable, HttpServletRequest httpServletRequest) throws Exception  {
		boolean atualizacaoOk = false;
		Empregado empregado = empregadoService.buscarPorId(idEmpregado);
		if (empregado != null) {
			try {
				atualizacaoEpiService.atualizarEpiPorEmpregado(empregado);
				atualizacaoOk = true;
			} catch(Exception e) {
				e.printStackTrace();
				atualizacaoOk = false;
			}
		} 
		filtro.setIdEmpregado(empregado.getId());
		filtro.setIdEmpresa(empregado.getIdEmpresa());
		ModelAndView mv = pesquisar(filtro, result, pageable, httpServletRequest);
		mv.addObject(atualizacaoOk ? "mensagem" : "mensagemErro", atualizacaoOk ? "EPIs atualizados com sucesso!" : "Ocorreu algum problema ao atualizar os EPIs!");
		return mv;
	}
	
	
	@RequestMapping("/novo/{idEmpregado}")
	public ModelAndView novo(@PathVariable("idEmpregado") Long idEmpregado) {	
		EmpregadoEpi empregadoEpi = new EmpregadoEpi();
		ModelAndView mv = new ModelAndView("cadastros/controleEntrega/controleEntrega-cadastro");
		Empregado empregado = empregadoService.buscarPorId(idEmpregado);
		if (empregado != null) {
			empregadoEpi.setEmpregado(empregado);
			empregadoEpi.setIdEmpregado(idEmpregado);
		}
		empregadoEpi.setDataPrevistaEntregaString(FormatacaoUtils.getDataString(new Date()));
		empregadoEpi.setQuantidade(1);
		mv.addObject("empregadoEpi", empregadoEpi);
		return mv;
	}
	
	@RequestMapping(value = "/gravar", method = RequestMethod.POST)
	public String gravar(EmpregadoEpi empregadoEpi, Model model, Errors errors, RedirectAttributes attributes) throws Exception {
		String validarGravacao = empregadoEpiService.validarGravacao(empregadoEpi, model, errors);
		if (validarGravacao != null && !validarGravacao.isEmpty()) {
			return validarGravacao;
		}
		return gravarEmpregadoEpi(empregadoEpi, attributes);
	}
	
	@RequestMapping(value = "/gravarModal", method = RequestMethod.POST)
	public String gravarModal(EmpregadoEpi empregadoEpi, Errors errors, RedirectAttributes attributes) throws Exception {
		return gravarEmpregadoEpi(empregadoEpi, attributes);
	}
	
	private String gravarEmpregadoEpi(EmpregadoEpi empregadoEpi, RedirectAttributes attributes) throws Exception {
		Long idEmpresa = empregadoEpi.getEmpregado().getIdEmpresa();
		Long idEmpregado = empregadoEpi.getIdEmpregado();
		empregadoEpi = empregadoEpiService.ajustarGravacao(empregadoEpi);
		empregadoEpi.setEmpregado(null);
		empregadoEpiService.salvar(empregadoEpi);
		attributes.addFlashAttribute("mensagem", "Entrega salva com sucesso!");
		return "redirect:/controleEntrega?idEmpresa="+idEmpresa+"&idEmpregado="+idEmpregado;
	}
	
	@RequestMapping("/excluir/{id}")
	public String excluir(@PathVariable("id") Long id, RedirectAttributes attributes) throws Exception {
		EmpregadoEpi empregadoEpi = empregadoEpiService.buscarPorId(id);
		Long idEmpresa = empregadoEpi.getEmpregado().getIdEmpresa();
		Long idEmpregado = empregadoEpi.getIdEmpregado();
		
		empregadoEpiService.excluir(empregadoEpi);
		attributes.addFlashAttribute("mensagem", "Entrega excluída com sucesso!");
		return "redirect:/controleEntrega?idEmpresa="+idEmpresa+"&idEmpregado="+idEmpregado;
	}
	
	@RequestMapping(value = "/downloadPdf")
	public String gerarPdf(Model model, HttpServletRequest httpServletRequest) throws Exception  {
		model.addAttribute("relatorio", empregadoEpiService.getRelatorioPdfVO(empregadoEpiFilter, httpServletRequest));
		return "pdfView";
	}
	
	@RequestMapping(value = "/download/fichaEpi")
	public String gerarFichaEpi(Model model, HttpServletRequest httpServletRequest)  {
		model.addAttribute("fichaEpi", empregadoEpiService.getFichaEpiPdfVO(empregadoEpiFilter, httpServletRequest));
		return "fichaEpiView";
	}
	
	@ModelAttribute("motivosCombo")
	public @ResponseBody List<SelectVO> getMotivosCombo()  {
		return combosService.getMotivosCombo();
	}
	
	@ModelAttribute("informarMotivo")
	public @ResponseBody boolean isInformarMotivo()  {
		ParametroSistema parametroSistema = parametroSistemaService.buscar();
		return parametroSistema != null && parametroSistema.isInformarMotivoEntrega();
	}
	
	@RequestMapping(value = "/buscar/estatisticas", method = RequestMethod.POST)
	public @ResponseBody EstatisticasEntregaVO getEstatisticas(@RequestParam("idEmpregado") Long idEmpregado, HttpServletRequest httpServletRequest)  {
		return empregadoEpiService.buscarEstatisticas(idEmpregado, httpServletRequest);
	}	
	
	@RequestMapping(value = "/buscar/detalhes", method = RequestMethod.POST)
	public @ResponseBody DetalhesEntregaVO getDetalhes(@RequestParam("idEmpregadoEpi") Long idEmpregadoEpi, HttpServletRequest httpServletRequest)  {
		return empregadoEpiService.buscarDetalhes(idEmpregadoEpi, httpServletRequest);
	}	
	
	@ModelAttribute("episCombo")
	public @ResponseBody List<SelectVO> getEpisCombo()  {
		return combosService.getEpisCombo(false);
	}
	
}