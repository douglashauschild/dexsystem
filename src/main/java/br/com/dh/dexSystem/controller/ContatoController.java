package br.com.dh.dexSystem.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dh.dexSystem.model.constant.TipoContatoEnum;
import br.com.dh.dexSystem.model.vo.ContatoVO;
import br.com.dh.dexSystem.model.vo.SelectVO;
import br.com.dh.dexSystem.service.CombosService;
import br.com.dh.dexSystem.service.ContatoService;

@Controller
@RequestMapping("/contato")	
public class ContatoController extends ControllerExtend {
	
	@Autowired
	private ContatoService contatoService;
	@Autowired
	private CombosService combosService;
	
	
	@RequestMapping(value = "/incluir", method = RequestMethod.POST)
    public String adicionarItem(@RequestBody ContatoVO contatoVO, Model model) {
		return contatoService.adicionar(contatoVO, model);
    }
	
	
	@RequestMapping(value = "/alterar", method = RequestMethod.POST)
    public String alterarItem(@RequestBody ContatoVO contatoVO, Model model) {
		return contatoService.alterar(contatoVO, model);
    }
	
	
	@RequestMapping(value = "/remover", method = RequestMethod.POST)
    public String removerItem(@RequestBody ContatoVO contatoVO, Model model) {
		return contatoService.remover(contatoVO, model);
    }
	
	
	@RequestMapping(value = "/tipos", method = RequestMethod.GET)
	public @ResponseBody List<SelectVO> getTiposCombo()  {
		return combosService.getTiposContatoCombo();
	}
	
	
	@RequestMapping(value = "/tipo", method = RequestMethod.GET)
	public @ResponseBody Long getTiposCombo(@RequestParam("tipo") String tipo)  {
		return TipoContatoEnum.getByDescricao(tipo).getKey();
	}
}