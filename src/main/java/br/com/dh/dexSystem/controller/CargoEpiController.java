package br.com.dh.dexSystem.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.dh.dexSystem.model.CargoEpi;
import br.com.dh.dexSystem.model.vo.SelectVO;
import br.com.dh.dexSystem.repository.filter.CargoEpiFilter;
import br.com.dh.dexSystem.service.CargoEpiService;
import br.com.dh.dexSystem.service.CombosService;

@Controller
@RequestMapping("/cargoEpi")	
public class CargoEpiController extends ControllerExtend {
	
	@Autowired
	private CargoEpiService cargoEpiService;
	@Autowired
	private CombosService combosService;
	
	private CargoEpiFilter cargoEpiFilter;
	
	
	@RequestMapping
	public ModelAndView pesquisar(CargoEpiFilter filtro, BindingResult result, Pageable pageable, HttpServletRequest httpServletRequest) throws Exception {
		this.cargoEpiFilter = filtro;
		
		ModelAndView mv = newModelAndView("cadastros/cargoEpi/cargoEpi-lista", httpServletRequest);
		
		PageWrapper<CargoEpi> paginaWrapper = cargoEpiService.pesquisar(filtro, pageable, httpServletRequest);
		mv.addObject("pagina", paginaWrapper);

		return mv;
	}
	
	
	@RequestMapping("/novo")
	public ModelAndView novo() {	
		ModelAndView mv = new ModelAndView("cadastros/cargoEpi/cargoEpi-cadastro");
		mv.addObject("cargoEpi", new CargoEpi());
		return mv;
	}
	
	
	@RequestMapping(value = "/gravar", method = RequestMethod.POST)
	public String gravar(CargoEpi cargoEpi, Model model, Errors errors, RedirectAttributes attributes) throws Exception {
		String validarGravacao = cargoEpiService.validarGravacao(cargoEpi, model, errors);
		if (validarGravacao != null && !validarGravacao.isEmpty()) {
			return validarGravacao;
		}
		return gravarCargoEpi(cargoEpi, attributes);
	}
	
	@RequestMapping(value = "/gravarModal", method = RequestMethod.POST)
	public String gravarModal(CargoEpi cargoEpi, Errors errors, RedirectAttributes attributes) throws Exception {
		return gravarCargoEpi(cargoEpi, attributes);
	}
	
	private String gravarCargoEpi(CargoEpi cargoEpi, RedirectAttributes attributes) throws Exception {
		cargoEpi = cargoEpiService.ajustarGravacao(cargoEpi);
		cargoEpiService.salvar(cargoEpi);
		attributes.addFlashAttribute("mensagem", "Cargo EPI salvo com sucesso!");
		return "redirect:/cargoEpi";
	}
	
	
	@RequestMapping("/editar/{id}")
	public ModelAndView edicao(@PathVariable("id") Long id) {
		CargoEpi cargoEpi = cargoEpiService.buscarPorId(id);
		
		ModelAndView mv = new ModelAndView("cadastros/cargoEpi/cargoEpi-cadastro");
		mv.addObject("cargoEpi", cargoEpi);
		return mv;
	}
	
	
	@RequestMapping("/excluir/{id}")
	public String excluir(@PathVariable("id") Long id, RedirectAttributes attributes) throws Exception {
		CargoEpi cargoEpi = cargoEpiService.buscarPorId(id);
		cargoEpiService.excluir(cargoEpi);
		attributes.addFlashAttribute("mensagem", "Cargo EPI excluído com sucesso!");
		return "redirect:/cargoEpi";
	}
	
	
	@RequestMapping(value = "/downloadPdf")
	public String gerarPdf(Model model, HttpServletRequest httpServletRequest)  {
		model.addAttribute("relatorio", cargoEpiService.getRelatorioPdfVO(cargoEpiFilter, httpServletRequest));
		return "pdfView";
	}
	
	
	@ModelAttribute("episCombo")
	public @ResponseBody List<SelectVO> getEpisCombo()  {
		return combosService.getEpisCombo(false);
	}
}