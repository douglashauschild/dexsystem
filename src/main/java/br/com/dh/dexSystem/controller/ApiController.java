package br.com.dh.dexSystem.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dh.dexSystem.model.Usuario;
import br.com.dh.dexSystem.model.constant.TabelaSincronizacaoEnum;
import br.com.dh.dexSystem.model.vo.SincronizacaoVO;
import br.com.dh.dexSystem.model.vo.TokenVO;
import br.com.dh.dexSystem.repository.UsuarioRepository;
import br.com.dh.dexSystem.service.ApiService;
import br.com.dh.dexSystem.service.ControleSincronizacaoService;
import br.com.dh.dexSystem.util.CriptografiaUtils;
import br.com.dh.dexSystem.util.TokenUtils;

@Controller
@RequestMapping("/api")
public class ApiController extends ApiExtend<Object> {

	@Autowired
	private UsuarioRepository usuarioRepository;
	@Autowired
	private ApiService apiService;
	@Autowired
	private ControleSincronizacaoService controleSincronizacaoService;
	

	@RequestMapping(value = "/autenticar", method = RequestMethod.POST, produces = "application/json")
	public @ResponseBody ResponseEntity<TokenVO> autenticar(@RequestBody TokenVO tokenVO) throws Exception {
		String token = null;
		
		Usuario usuario = usuarioRepository.findByEmailAndExcluidoIn(tokenVO.getEmail(), '0');
		if (usuario == null) {
			return new ResponseEntity<TokenVO>(HttpStatus.NOT_FOUND);
		} else {
			if (!usuario.isPossuiAcessoDesktop()) {
				return new ResponseEntity<TokenVO>(HttpStatus.NOT_ACCEPTABLE);
			}
			String senha = CriptografiaUtils.decrypt(tokenVO.getSenha(), CriptografiaUtils.CHAVE);
			if (!new BCryptPasswordEncoder().matches(senha, usuario.getSenha())) {
				return new ResponseEntity<TokenVO>(HttpStatus.UNAUTHORIZED);
			}
		}
		token = TokenUtils.montarToken(apiHash, usuario.getEmail());
		tokenVO.setToken(token);
		tokenVO.setSenha(null);
		return new ResponseEntity<TokenVO>(tokenVO, HttpStatus.OK);
	}
	

	@RequestMapping(value = "/usuario", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public ResponseEntity<Usuario> buscarUsuarioAutenticado(HttpServletRequest request) {
		try {
			String token = request.getHeader("Authorization");
			if (!validarUsuarioToken(token)) {
				return new ResponseEntity<Usuario>(HttpStatus.UNAUTHORIZED);
			}
			
			Usuario usuario = getUsuario(token);
			return new ResponseEntity<Usuario>(usuario, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	@RequestMapping(value = "/sincronizar", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ResponseEntity<SincronizacaoVO> sincronizar(@RequestBody SincronizacaoVO sincronizacaoVO, HttpServletRequest request) {
		try {
			String token = request.getHeader("Authorization");
			if (!validarUsuarioToken(token)) {
				return new ResponseEntity<SincronizacaoVO>(HttpStatus.UNAUTHORIZED);
			}
			Long idDispositivo = sincronizacaoVO.getIdDispositivo();
			String tabela = sincronizacaoVO.getTabela();
			
			Class classe = TabelaSincronizacaoEnum.getByTabela(tabela).getClasse();
			List<Object> objects = apiService.buscarDados(idDispositivo, tabela, classe);
			sincronizacaoVO.setJson(getGson().toJson(objects));
			
			return new ResponseEntity<SincronizacaoVO>(sincronizacaoVO, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	@RequestMapping(value = "/sincronizar/confirmar", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ResponseEntity<HttpStatus> confirmarSincronizacao(@RequestBody SincronizacaoVO sincronizacaoVO, HttpServletRequest request) {
		try {
			String token = request.getHeader("Authorization");
			if (!validarUsuarioToken(token)) {
				return new ResponseEntity<HttpStatus>(HttpStatus.UNAUTHORIZED);
			}
			boolean ok = controleSincronizacaoService.salvarSincronizacao(sincronizacaoVO);
			if (!ok) {
				return new ResponseEntity<HttpStatus>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
			return new ResponseEntity<HttpStatus>(HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}