package br.com.dh.dexSystem.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.dh.dexSystem.model.PessoaFisica;
import br.com.dh.dexSystem.model.vo.SelectCharVO;
import br.com.dh.dexSystem.model.vo.SelectVO;
import br.com.dh.dexSystem.repository.filter.PessoaFisicaFilter;
import br.com.dh.dexSystem.service.CombosService;
import br.com.dh.dexSystem.service.PessoaFisicaService;

@Controller
@RequestMapping("/pessoaFisica")	
public class PessoaFisicaController extends ControllerExtend {
	
	@Autowired
	private PessoaFisicaService pessoaFisicaService;
	@Autowired
	private CombosService combosService;
	
	private PessoaFisicaFilter pessoaFisicaFilter;
	
	
	@RequestMapping
	public ModelAndView pesquisar(PessoaFisicaFilter filtro, BindingResult result, Pageable pageable, HttpServletRequest httpServletRequest) throws Exception {
		this.pessoaFisicaFilter = filtro;
		
		ModelAndView mv = newModelAndView("cadastros/pessoaFisica/pessoaFisica-lista", httpServletRequest);
		
		PageWrapper<PessoaFisica> paginaWrapper = pessoaFisicaService.pesquisar(filtro, pageable, httpServletRequest);
		mv.addObject("pagina", paginaWrapper);

		return mv;
	}
	
	
	@RequestMapping("/novo")
	public ModelAndView novo() {	
		ModelAndView mv = new ModelAndView("cadastros/pessoaFisica/pessoaFisica-cadastro");
		PessoaFisica pessoaFisica = new PessoaFisica();
		mv.addObject("pessoaFisica", new PessoaFisica());
		mv.addObject("contatosToString", pessoaFisica.getContatosToString());
		return mv;
	}
	
	
	@RequestMapping(value = "/gravar", method = RequestMethod.POST)
	public String gravar(PessoaFisica pessoaFisica, Model model, Errors errors, RedirectAttributes attributes) throws Exception {
		String validarGravacao = pessoaFisicaService.validarGravacao(pessoaFisica, model, errors);
		if (validarGravacao != null && !validarGravacao.isEmpty()) {
			return validarGravacao;
		}
		pessoaFisicaService.salvar(pessoaFisica);
		attributes.addFlashAttribute("mensagem", "Pessoa Física salva com sucesso!");
		return "redirect:/pessoaFisica";
	}
	
	
	@RequestMapping("/editar/{id}")
	public ModelAndView edicao(@PathVariable("id") Long id) {
		PessoaFisica pessoaFisica = pessoaFisicaService.buscarPorId(id);
		
		ModelAndView mv = new ModelAndView("cadastros/pessoaFisica/pessoaFisica-cadastro");
		mv.addObject("pessoaFisica", pessoaFisica);
		mv.addObject("contatosToString", pessoaFisica.getContatosToString());
		return mv;
	}
	
	
	@RequestMapping("/excluir/{id}")
	public String excluir(@PathVariable("id") Long id, RedirectAttributes attributes) throws Exception {
		PessoaFisica pessoaFisica = pessoaFisicaService.buscarPorId(id);
		pessoaFisicaService.excluir(pessoaFisica);
		attributes.addFlashAttribute("mensagem", "Pessoa Física excluída com sucesso!");
		return "redirect:/pessoaFisica";
	}
	
	
	@RequestMapping(value = "/buscar", method = RequestMethod.GET)
	public @ResponseBody List<SelectVO> getPessoasFisicasCombo(@RequestParam("filtro") boolean filtro)  {
		return combosService.getPessoasFisicasCombo(filtro);
	}
	
	@RequestMapping(value = "/downloadPdf")
	public String gerarPdf(Model model, HttpServletRequest httpServletRequest)  {
		model.addAttribute("relatorio", pessoaFisicaService.getRelatorioPdfVO(pessoaFisicaFilter, httpServletRequest));
		return "pdfView";
	}
	
	@ModelAttribute("sexoCombo")
	public @ResponseBody List<SelectCharVO> getSexoCombo()  {
		return combosService.getSexoCombo(false);
	}
	
	@ModelAttribute("sexoFiltroCombo")
	public @ResponseBody List<SelectCharVO> getSexoFiltroCombo()  {
		return combosService.getSexoCombo(true);
	}
}