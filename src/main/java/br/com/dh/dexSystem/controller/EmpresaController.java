package br.com.dh.dexSystem.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.dh.dexSystem.model.Empresa;
import br.com.dh.dexSystem.model.vo.SelectCharVO;
import br.com.dh.dexSystem.model.vo.SelectVO;
import br.com.dh.dexSystem.repository.filter.EmpresaFilter;
import br.com.dh.dexSystem.service.CombosService;
import br.com.dh.dexSystem.service.EmpresaService;

@Controller
@RequestMapping("/empresa")	
public class EmpresaController extends ControllerExtend {
	
	@Autowired
	private EmpresaService empresaService;
	@Autowired
	private CombosService combosService;
	
	private EmpresaFilter empresaFilter;
	
	
	@RequestMapping
	public ModelAndView pesquisar(EmpresaFilter filtro, BindingResult result, Pageable pageable, HttpServletRequest httpServletRequest) throws Exception {
		this.empresaFilter = filtro;
		
		ModelAndView mv = newModelAndView("cadastros/empresa/empresa-lista", httpServletRequest);
		
		PageWrapper<Empresa> paginaWrapper = empresaService.pesquisar(filtro, pageable, httpServletRequest);
		mv.addObject("pagina", paginaWrapper);

		return mv;
	}
	
	
	@RequestMapping("/novo")
	public ModelAndView novo() {	
		ModelAndView mv = new ModelAndView("cadastros/empresa/empresa-cadastro");
		Empresa empresa = new Empresa();
		mv.addObject("empresa", empresa);
		mv.addObject("contatosToString", empresa.getContatosToString());
		return mv;
	}
	
	
	@RequestMapping(value = "/gravar", method = RequestMethod.POST)
	public String gravar(Empresa empresa, Model model, Errors errors, RedirectAttributes attributes) throws Exception {
		String validarGravacao = empresaService.validarGravacao(empresa, model, errors);
		if (validarGravacao != null && !validarGravacao.isEmpty()) {
			return validarGravacao;
		}
		empresaService.salvar(empresa);
		attributes.addFlashAttribute("mensagem", "Empresa salva com sucesso!");
		return "redirect:/empresa";
	}
	
	
	@RequestMapping("/editar/{id}")
	public ModelAndView edicao(@PathVariable("id") Long id) {
		Empresa empresa = empresaService.buscarPorId(id);
		
		ModelAndView mv = new ModelAndView("cadastros/empresa/empresa-cadastro");
		mv.addObject("empresa", empresa);
		mv.addObject("contatosToString", empresa.getContatosToString());
		return mv;
	}
	
	
	@RequestMapping("/excluir/{id}")
	public String excluir(@PathVariable("id") Long id, RedirectAttributes attributes) throws Exception {
		Empresa empresa = empresaService.buscarPorId(id);
		String msgValidacao = empresaService.validarExclusao(empresa);
		if (msgValidacao != null && !msgValidacao.isEmpty()) {
			attributes.addFlashAttribute("mensagemErro", msgValidacao);
		} else {
			empresaService.excluir(empresa);
			attributes.addFlashAttribute("mensagem", "Empresa excluída com sucesso!");
		}
		return "redirect:/empresa";
	}
	
	
	@RequestMapping(value = "/downloadPdf")
	public String gerarPdf(Model model, HttpServletRequest httpServletRequest)  {
		model.addAttribute("relatorio", empresaService.getRelatorioPdfVO(empresaFilter, httpServletRequest));
		return "pdfView";
	}
	
	@ModelAttribute("tipoEmpresaCombo")
	public @ResponseBody List<SelectCharVO> getTipoEmpresaCombo()  {
		return combosService.getTipoEmpresaCombo(false);
	}
	
	@ModelAttribute("tipoEmpresaFiltroCombo")
	public @ResponseBody List<SelectCharVO> getTipoEmpresaFiltroCombo()  {
		return combosService.getTipoEmpresaCombo(true);
	}
	
	@ModelAttribute("statusCombo")
	public @ResponseBody List<SelectVO> getStatusCombo()  {
		return combosService.getStatusCombo();
	}
	
	@RequestMapping(value = "/buscar", method = RequestMethod.GET)
	public @ResponseBody List<SelectVO> getEmpresasCombo(@RequestParam("filtro") boolean filtro, @RequestParam("idEmpresa") Long idEmpresa)  {
		return combosService.getEmpresasCombo(filtro, idEmpresa);
	}
	
	@RequestMapping(value = "/matrizes", method = RequestMethod.GET)
	public @ResponseBody List<SelectVO> getEmpresasMatrizesCombo(@RequestParam("id") Long id)  {
		return combosService.getEmpresasMatrizesCombo(id);
	}
}