package br.com.dh.dexSystem.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.dh.dexSystem.model.ParametroSistema;
import br.com.dh.dexSystem.model.vo.SelectCharVO;
import br.com.dh.dexSystem.service.CombosService;
import br.com.dh.dexSystem.service.ParametroSistemaService;

@Controller
@RequestMapping("/parametroSistema")	
public class ParametroSistemaController extends ControllerExtend {
	
	@Autowired
	private ParametroSistemaService parametroSistemaService;
	@Autowired
	private CombosService combosService;
	
	
	@RequestMapping
	public ModelAndView pesquisar(HttpServletRequest httpServletRequest) {
		ModelAndView mv = newModelAndView("cadastros/parametroSistema/parametroSistema-cadastro", httpServletRequest);
		mv.addObject("parametroSistema", parametroSistemaService.buscar());
		return mv;
	}
	
	@RequestMapping(value = "/gravar", method = RequestMethod.POST)
	public String gravar(ParametroSistema parametroSistema, RedirectAttributes attributes) throws Exception {
		parametroSistemaService.salvar(parametroSistema);
		attributes.addFlashAttribute("mensagem", "Parâmetros do Sistema salvo com sucesso!");
		return "redirect:/dashboard";
	}
	
	@ModelAttribute("tipoSelecaoEmpresaCombo")
	public @ResponseBody List<SelectCharVO> getTipoSelecaoEmpresaCombo()  {
		return combosService.getTipoSelecaoEmpresaCombo();
	}
	
	@ModelAttribute("tipoSelecaoEpiCombo")
	public @ResponseBody List<SelectCharVO> getTipoSelecaoEpiCombo()  {
		return combosService.getTipoSelecaoEpiCombo();
	}
	
	@ModelAttribute("tipoSelecaoPessoaFisicaCombo")
	public @ResponseBody List<SelectCharVO> getTipoSelecaoPessoaFisicaCombo()  {
		return combosService.getTipoSelecaoPessoaFisicaCombo();
	}
	
	@ModelAttribute("tipoSelecaoEmpregadoCombo")
	public @ResponseBody List<SelectCharVO> getTipoSelecaoEmpregadoCombo()  {
		return combosService.getTipoSelecaoEmpregadoCombo();
	}
	
	@ModelAttribute("informarMotivoCombo")
	public @ResponseBody List<SelectCharVO> getInformarMotivoCombo()  {
		return combosService.getSimNaoCombo();
	}
}