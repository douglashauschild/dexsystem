package br.com.dh.dexSystem.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.dh.dexSystem.model.UnidadeMedida;
import br.com.dh.dexSystem.repository.filter.UnidadeMedidaFilter;
import br.com.dh.dexSystem.service.UnidadeMedidaService;

@Controller
@RequestMapping("/unidadeMedida")	
public class UnidadeMedidaController extends ControllerExtend {
	
	@Autowired
	private UnidadeMedidaService unidadeMedidaService;
	
	private UnidadeMedidaFilter unidadeMedidaFilter;
	
	
	@RequestMapping
	public ModelAndView pesquisar(UnidadeMedidaFilter filtro, BindingResult result, Pageable pageable, HttpServletRequest httpServletRequest) {
		this.unidadeMedidaFilter = filtro;
		
		ModelAndView mv = newModelAndView("cadastros/unidadeMedida/unidadeMedida-lista", httpServletRequest);
		
		PageWrapper<UnidadeMedida> paginaWrapper = unidadeMedidaService.pesquisar(filtro, pageable, httpServletRequest);
		mv.addObject("pagina", paginaWrapper);

		return mv;
	}
	
	
	@RequestMapping("/novo")
	public ModelAndView novo() {	
		ModelAndView mv = new ModelAndView("cadastros/unidadeMedida/unidadeMedida-cadastro");
		mv.addObject("unidadeMedida", new UnidadeMedida());
		return mv;
	}
	
	
	@RequestMapping(value = "/gravar", method = RequestMethod.POST)
	public String gravar(UnidadeMedida unidadeMedida, Model model, Errors errors, RedirectAttributes attributes) throws Exception {
		if (unidadeMedidaService.descricaoCadastrada(unidadeMedida)) {
			unidadeMedida.setAbrirModalConfirmacao(true);
			model.addAttribute("unidadeMedida", unidadeMedida);
			return "cadastros/unidadeMedida/unidadeMedida-cadastro";
		}
		return gravarUnidade(unidadeMedida, attributes);
	}
	
	@RequestMapping(value = "/gravarModal", method = RequestMethod.POST)
	public String gravarModal(UnidadeMedida unidadeMedida, Errors errors, RedirectAttributes attributes) throws Exception {
		return gravarUnidade(unidadeMedida, attributes);
	}
	
	private String gravarUnidade(UnidadeMedida unidadeMedida, RedirectAttributes attributes) throws Exception {
		unidadeMedidaService.salvar(unidadeMedida);
		attributes.addFlashAttribute("mensagem", "Unidade de Medida salva com sucesso!");
		return "redirect:/unidadeMedida";
	}
	
	@RequestMapping("/editar/{id}")
	public ModelAndView edicao(@PathVariable("id") Long id) {
		UnidadeMedida unidadeMedida = unidadeMedidaService.buscarPorId(id);
		
		ModelAndView mv = new ModelAndView("cadastros/unidadeMedida/unidadeMedida-cadastro");
		mv.addObject("unidadeMedida", unidadeMedida);
		return mv;
	}
	
	
	@RequestMapping("/excluir/{id}")
	public String excluir(@PathVariable("id") Long id, RedirectAttributes attributes) throws Exception {
		UnidadeMedida unidadeMedida = unidadeMedidaService.buscarPorId(id);
		unidadeMedidaService.excluir(unidadeMedida);
		attributes.addFlashAttribute("mensagem", "Unidade de Medida excluída com sucesso!");
		return "redirect:/unidadeMedida";
	}
	
	
	@RequestMapping(value = "/downloadPdf")
	public String gerarPdf(Model model, HttpServletRequest httpServletRequest)  {
		model.addAttribute("relatorio", unidadeMedidaService.getRelatorioPdfVO(unidadeMedidaFilter, httpServletRequest));
		return "pdfView";
	}

}