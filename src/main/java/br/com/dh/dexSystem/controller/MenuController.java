package br.com.dh.dexSystem.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dh.dexSystem.model.Menu;
import br.com.dh.dexSystem.security.UsuarioSistema;
import br.com.dh.dexSystem.service.MenuService;
import br.com.dh.dexSystem.service.NivelAcessoPermissaoService;

@Controller
@RequestMapping("/menu")
public class MenuController {
	
	@Autowired
	private MenuService menuService;
	
	@Autowired	
	NivelAcessoPermissaoService nivelAcessoPermissaoService;
	
	@RequestMapping(value = "/buscar", consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<Menu> buscar(@AuthenticationPrincipal User user)  {
		
		Long idNivelAcesso = ((UsuarioSistema) user).getUsuario().getIdNivelAcesso();
				
		return menuService.buscarTodos(idNivelAcesso);
	}
}
