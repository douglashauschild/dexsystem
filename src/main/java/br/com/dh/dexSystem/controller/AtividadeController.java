package br.com.dh.dexSystem.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.dh.dexSystem.model.Atividade;
import br.com.dh.dexSystem.repository.filter.AtividadeFilter;
import br.com.dh.dexSystem.service.AtividadeService;

@Controller
@RequestMapping("/atividade")	
public class AtividadeController extends ControllerExtend {
	
	@Autowired
	private AtividadeService atividadeService;
	
	private AtividadeFilter atividadeFilter;
	
	
	@RequestMapping
	public ModelAndView pesquisar(AtividadeFilter filtro, BindingResult result, Pageable pageable, HttpServletRequest httpServletRequest) throws Exception {
		this.atividadeFilter = filtro;
		
		ModelAndView mv = newModelAndView("cadastros/atividade/atividade-lista", httpServletRequest);
		
		PageWrapper<Atividade> paginaWrapper = atividadeService.pesquisar(filtro, pageable, httpServletRequest);
		mv.addObject("pagina", paginaWrapper);

		return mv;
	}
	
	
	@RequestMapping("/novo")
	public ModelAndView novo() {	
		ModelAndView mv = new ModelAndView("cadastros/atividade/atividade-cadastro");
		mv.addObject("atividade", new Atividade());
		return mv;
	}

	
	@RequestMapping(value = "/gravar", method = RequestMethod.POST)
	public String gravar(Atividade atividade, Model model, Errors errors, RedirectAttributes attributes) throws Exception {
		String validarGravacao = atividadeService.validarGravacao(atividade, model, errors);
		if (validarGravacao != null && !validarGravacao.isEmpty()) {
			return validarGravacao;
		}
		return gravarAtividade(atividade, attributes);
	}
	
	@RequestMapping(value = "/gravarModal", method = RequestMethod.POST)
	public String gravarModal(Atividade atividade, Errors errors, RedirectAttributes attributes) throws Exception {
		return gravarAtividade(atividade, attributes);
	}
	
	private String gravarAtividade(Atividade atividade, RedirectAttributes attributes) throws Exception {
		atividade = atividadeService.ajustarGravacao(atividade);
		atividadeService.salvar(atividade, true);
		attributes.addFlashAttribute("mensagem", "Atividade salva com sucesso!");
		return "redirect:/atividade";
	}
	

	
	@RequestMapping("/editar/{id}")
	public ModelAndView edicao(@PathVariable("id") Long id) {
		Atividade atividade = atividadeService.buscarPorId(id);
		
		ModelAndView mv = new ModelAndView("cadastros/atividade/atividade-cadastro");
		mv.addObject("atividade", atividade);
		return mv;
	}
	
	
	@RequestMapping("/excluir/{id}")
	public String excluir(@PathVariable("id") Long id, RedirectAttributes attributes) throws Exception {
		Atividade atividade = atividadeService.buscarPorId(id);
		atividadeService.excluir(atividade);
		attributes.addFlashAttribute("mensagem", "Atividade excluída com sucesso!");
		return "redirect:/atividade";
	}
	
	
	@RequestMapping(value = "/downloadPdf")
	public String gerarPdf(Model model, HttpServletRequest httpServletRequest)  {
		model.addAttribute("relatorio", atividadeService.getRelatorioPdfVO(atividadeFilter, httpServletRequest));
		return "pdfView";
	}
}