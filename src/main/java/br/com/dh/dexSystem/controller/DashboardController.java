package br.com.dh.dexSystem.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.dh.dexSystem.model.vo.GraficoVO;
import br.com.dh.dexSystem.model.vo.TotalVO;
import br.com.dh.dexSystem.repository.filter.DashboardFilter;
import br.com.dh.dexSystem.service.DashboardService;

@Controller
@RequestMapping("/dashboard")
public class DashboardController {

	@Autowired
	private DashboardService dashboardService;
	
	@RequestMapping
	public ModelAndView pesquisar(DashboardFilter filtro) {
		ModelAndView mv = new ModelAndView("dashboard");
		return mv;
	}

	@RequestMapping(value = "/buscarTotais", consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody TotalVO buscarTotais(@RequestParam("idEmpresa") Long idEmpresa) throws Exception  {
		return dashboardService.totais(idEmpresa);
	}
	
	@RequestMapping(value = "/situacaoEpis", consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody String buscarSituacaoEpis() throws Exception  {
		return dashboardService.situacaoEpis();
	}
	
	@RequestMapping(value = "/episUtilizados", consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody String buscarEpisMaisUtilizados() throws Exception  {
		return dashboardService.episMaisUtilizados();
	}
	
	@RequestMapping(value = "/statusEntregas", consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody String buscarStatusEntregas(@RequestParam("idEmpresa") Long idEmpresa) throws Exception  {
		return dashboardService.statusEntregas(idEmpresa);
	}
	
	@RequestMapping(value = "/buscarEntregas", consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody GraficoVO buscarEntregasAno(@RequestParam("idEmpresa") Long idEmpresa) throws Exception  {
		return dashboardService.totalEntregasAno(idEmpresa);
	}
}
