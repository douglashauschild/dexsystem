package br.com.dh.dexSystem.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dh.dexSystem.client.ViaCepRestClient;
import br.com.dh.dexSystem.manager.CidadeManager;
import br.com.dh.dexSystem.model.Cidade;
import br.com.dh.dexSystem.model.vo.SelectVO;
import br.com.dh.dexSystem.model.vo.ViaCepVO;
import br.com.dh.dexSystem.service.CombosService;
import br.com.dh.dexSystem.util.FormatacaoUtils;

@Controller
@RequestMapping("/endereco")	
public class EnderecoController extends ControllerExtend {
	
	@Autowired
	private CombosService combosService;
	@Autowired
	private CidadeManager cidadeManager;
	@Autowired
	private ViaCepRestClient viaCepRestClient;
	
	
	
	@RequestMapping(value = "/estados", method = RequestMethod.GET)
	public @ResponseBody List<SelectVO> getEstadosCombo()  {
		return combosService.getEstadosCombo();
	}
	
	@RequestMapping(value = "/cidades", method = RequestMethod.GET)
	public @ResponseBody List<SelectVO> getCidadesCombo(@RequestParam("idEstado") Long idEstado)  {
		return combosService.getCidadesCombo(idEstado);
	}
	
	@ResponseBody
	@RequestMapping(value = "/consultar/cep", method = RequestMethod.GET)
	public ViaCepVO getCep(@RequestParam("cep") String cep) throws Exception  {
		cep = FormatacaoUtils.removerFormatacao(cep);
		ViaCepVO viaCepVO = viaCepRestClient.consultar(cep);
		Cidade cidade = cidadeManager.buscarCidade(viaCepVO.getUf(), viaCepVO.getLocalidade());
		if (cidade != null) {
			viaCepVO.setIdCidade(cidade.getId());
			viaCepVO.setIdEstado(cidade.getIdEstado());
		}
		return viaCepVO;
	}
	

}