package br.com.dh.dexSystem.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.dh.dexSystem.model.Motivo;
import br.com.dh.dexSystem.repository.filter.MotivoFilter;
import br.com.dh.dexSystem.service.MotivoService;

@Controller
@RequestMapping("/motivo")	
public class MotivoController extends ControllerExtend {
	
	@Autowired
	private MotivoService motivoService;
	
	private MotivoFilter motivoFilter;
	
	
	@RequestMapping
	public ModelAndView pesquisar(MotivoFilter filtro, BindingResult result, Pageable pageable, HttpServletRequest httpServletRequest) {
		this.motivoFilter = filtro;
		
		ModelAndView mv = newModelAndView("cadastros/motivo/motivo-lista", httpServletRequest);
		
		PageWrapper<Motivo> paginaWrapper = motivoService.pesquisar(filtro, pageable, httpServletRequest);
		mv.addObject("pagina", paginaWrapper);

		return mv;
	}
	
	
	@RequestMapping("/novo")
	public ModelAndView novo() {	
		ModelAndView mv = new ModelAndView("cadastros/motivo/motivo-cadastro");
		mv.addObject("motivo", new Motivo());
		return mv;
	}
	
	
	@RequestMapping(value = "/gravar", method = RequestMethod.POST)
	public String gravar(Motivo motivo, Model model, Errors errors, RedirectAttributes attributes) throws Exception {
		if (motivoService.descricaoCadastrada(motivo)) {
			motivo.setAbrirModalConfirmacao(true);
			model.addAttribute("motivo", motivo);
			return "cadastros/motivo/motivo-cadastro";
		}
		return gravarMotivo(motivo, attributes);
	}
	
	@RequestMapping(value = "/gravarModal", method = RequestMethod.POST)
	public String gravarModal(Motivo motivo, Errors errors, RedirectAttributes attributes) throws Exception {
		return gravarMotivo(motivo, attributes);
	}
	
	private String gravarMotivo(Motivo motivo, RedirectAttributes attributes) throws Exception {
		motivoService.salvar(motivo);
		attributes.addFlashAttribute("mensagem", "Motivo salvo com sucesso!");
		return "redirect:/motivo";
	}
	
	@RequestMapping("/editar/{id}")
	public ModelAndView edicao(@PathVariable("id") Long id) {
		Motivo motivo = motivoService.buscarPorId(id);
		
		ModelAndView mv = new ModelAndView("cadastros/motivo/motivo-cadastro");
		mv.addObject("motivo", motivo);
		return mv;
	}
	
	
	@RequestMapping("/excluir/{id}")
	public String excluir(@PathVariable("id") Long id, RedirectAttributes attributes) throws Exception {
		Motivo motivo = motivoService.buscarPorId(id);
		motivoService.excluir(motivo);
		attributes.addFlashAttribute("mensagem", "Motivo excluído com sucesso!");
		return "redirect:/motivo";
	}
	
	
	@RequestMapping(value = "/downloadPdf")
	public String gerarPdf(Model model, HttpServletRequest httpServletRequest)  {
		model.addAttribute("relatorio", motivoService.getRelatorioPdfVO(motivoFilter, httpServletRequest));
		return "pdfView";
	}
}