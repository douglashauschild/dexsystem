package br.com.dh.dexSystem.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.dh.dexSystem.model.Epi;
import br.com.dh.dexSystem.model.constant.VidaUtilEnum;
import br.com.dh.dexSystem.model.vo.SelectVO;
import br.com.dh.dexSystem.repository.filter.EpiFilter;
import br.com.dh.dexSystem.service.CombosService;
import br.com.dh.dexSystem.service.EpiService;

@Controller
@RequestMapping("/epi")	
public class EpiController extends ControllerExtend {
	
	@Autowired
	private EpiService epiService;
	@Autowired
	private CombosService combosService;
	
	private EpiFilter epiFilter;
	
	
	@RequestMapping
	public ModelAndView pesquisar(EpiFilter filtro, BindingResult result, Pageable pageable, HttpServletRequest httpServletRequest) {
		this.epiFilter = filtro;
		
		ModelAndView mv = newModelAndView("cadastros/epi/epi-lista", httpServletRequest);
		
		PageWrapper<Epi> paginaWrapper = epiService.pesquisar(filtro, pageable, httpServletRequest);
		mv.addObject("pagina", paginaWrapper);
		
		return mv;
	}
	
	
	@RequestMapping("/novo")
	public ModelAndView novo() {	
		ModelAndView mv = new ModelAndView("cadastros/epi/epi-cadastro");
		mv.addObject("epi", new Epi());
		return mv;
	}
	

	@RequestMapping(value = "/gravar", method = RequestMethod.POST)
	public String gravar(Epi epi, Model model, Errors errors, RedirectAttributes attributes) throws Exception {	
		String validarGravacao = epiService.validarGravacao(epi, model, errors);
		if (validarGravacao != null && !validarGravacao.isEmpty()) {
			return validarGravacao;
		}
		return gravarEpi(epi, attributes);
	}
	
	
	@RequestMapping(value = "/gravarModal", method = RequestMethod.POST)
	public String gravarModal(Epi epi, Errors errors, RedirectAttributes attributes) throws Exception {
		return gravarEpi(epi, attributes);
	}
	
	
	private String gravarEpi(Epi epi, RedirectAttributes attributes) throws Exception {
		epi = epiService.ajustarGravacao(epi);
		epiService.salvar(epi);
		attributes.addFlashAttribute("mensagem", "Epi salvo com sucesso!");
		return "redirect:/epi";
	}
	

	@RequestMapping("/editar/{id}")
	public ModelAndView edicao(@PathVariable("id") Long id) {
		Epi epi = epiService.buscarPorId(id);
		
		ModelAndView mv = new ModelAndView("cadastros/epi/epi-cadastro");
		mv.addObject("epi", epi);
		return mv;
	}
	
	
	@RequestMapping("/excluir/{id}")
	public String excluir(@PathVariable("id") Long id, RedirectAttributes attributes) throws Exception {
		Epi epi = epiService.buscarPorId(id);
		epiService.excluir(epi);
		attributes.addFlashAttribute("mensagem", "Epi excluído com sucesso!");
		return "redirect:/epi";
	}
	
	@RequestMapping(value = "/downloadPdf")
	public String gerarPdf(Model model, HttpServletRequest httpServletRequest)  {
		model.addAttribute("relatorio", epiService.getRelatorioPdfVO(epiFilter, httpServletRequest));
		return "pdfView";
	}
	
	@ModelAttribute("vidaUtilUnidadeCombo")
	public @ResponseBody VidaUtilEnum[] getVidaUtilUnCombo()  {
		return combosService.getVidaUtilUnidadeCombo();
	}
	
	@ModelAttribute("unidadesMedidaCombo")
	public @ResponseBody List<SelectVO> getUnidadesMedidaCombo()  {
		return combosService.getUnidadesMedidaCombo();
	}
	
	@ModelAttribute("gradesCombo")
	public @ResponseBody List<SelectVO> getGradesCombo()  {
		return combosService.getGradesCombo();
	}
	
	@ModelAttribute("statusCaCombo")
	public @ResponseBody List<SelectVO> getStatusCaCombo()  {
		return combosService.getStatusCaCombo();
	}
	
	@RequestMapping(value = "/consultarCa")
	public @ResponseBody Epi getConsultarCa(@RequestParam("ca") String ca) {
		Epi epi = null;
		try {
			epi = epiService.consultarCa(ca);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return epi;
	}
	
	@RequestMapping(value = "/buscarPorId", method = RequestMethod.GET)
	public @ResponseBody Epi getEpi(@RequestParam("idEpi") Long idEpi)  {
		return epiService.buscarPorId(idEpi);
	}
}