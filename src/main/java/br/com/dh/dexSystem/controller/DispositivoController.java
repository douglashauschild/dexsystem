package br.com.dh.dexSystem.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dh.dexSystem.model.Dispositivo;
import br.com.dh.dexSystem.service.DispositivoService;

@Controller
@RequestMapping("/api/dispositivo")	
public class DispositivoController extends ApiExtend {
	
	@Autowired
	private DispositivoService dispositivoService;
	
	
	@RequestMapping(value = "/novo", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Dispositivo> novoDispositivo(@RequestParam("versao") String versao, HttpServletRequest request) throws Exception  {
		try {
			String token = request.getHeader("Authorization");
			if (!validarUsuarioToken(token)) {
				return new ResponseEntity<Dispositivo>(HttpStatus.UNAUTHORIZED);
			}
			Dispositivo dispositivo = new Dispositivo();
			dispositivo.setDataInicio(new Date());
			dispositivo.setVersao(versao);
			dispositivoService.salvar(dispositivo);
			return new ResponseEntity<Dispositivo>(dispositivo, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
