package br.com.dh.dexSystem.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/download")
public class DownloadController {
	
	@Value("${api.path}")
	protected String caminho;

	@ResponseBody
	@RequestMapping(value = "/zip/{nome}", method = RequestMethod.GET)
	public InputStreamResource downloadZip(@PathVariable("nome") String nome, HttpServletResponse response) throws IOException {
		nome += ".zip";
		File file = new File(caminho + File.separator + nome);
		response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment;filename="+nome);
		return new InputStreamResource(new FileInputStream(file));
	}
}




