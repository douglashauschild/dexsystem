package br.com.dh.dexSystem.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.dh.dexSystem.model.Setor;
import br.com.dh.dexSystem.model.vo.SelectVO;
import br.com.dh.dexSystem.repository.filter.SetorFilter;
import br.com.dh.dexSystem.service.CombosService;
import br.com.dh.dexSystem.service.SetorService;

@Controller
@RequestMapping("/setor")	
public class SetorController extends ControllerExtend {
	
	@Autowired
	private SetorService setorService;
	@Autowired
	private CombosService combosService;
	
	private SetorFilter setorFilter;
	
	
	@RequestMapping
	public ModelAndView pesquisar(SetorFilter filtro, BindingResult result, Pageable pageable, HttpServletRequest httpServletRequest) throws Exception {
		this.setorFilter = filtro;
		
		ModelAndView mv = newModelAndView("cadastros/setor/setor-lista", httpServletRequest);
		
		PageWrapper<Setor> paginaWrapper = setorService.pesquisar(filtro, pageable, httpServletRequest);
		mv.addObject("pagina", paginaWrapper);

		return mv;
	}
	
	
	@RequestMapping("/novo")
	public ModelAndView novo() {	
		ModelAndView mv = new ModelAndView("cadastros/setor/setor-cadastro");
		mv.addObject("setor", new Setor());
		return mv;
	}
	
	
	@RequestMapping(value = "/gravar", method = RequestMethod.POST)
	public String gravar(Setor setor, Model model, Errors errors, RedirectAttributes attributes) throws Exception {
		String validarGravacao = setorService.validarGravacao(setor, model, errors);
		if (validarGravacao != null && !validarGravacao.isEmpty()) {
			return validarGravacao;
		}
		setorService.salvar(setor);
		attributes.addFlashAttribute("mensagem", "Setor salvo com sucesso!");
		return "redirect:/setor";
	}

	
	@RequestMapping("/editar/{id}")
	public ModelAndView edicao(@PathVariable("id") Long id) {
		Setor setor = setorService.buscarPorId(id);
		
		ModelAndView mv = new ModelAndView("cadastros/setor/setor-cadastro");
		mv.addObject("setor", setor);
		return mv;
	}
	
	
	@RequestMapping("/excluir/{id}")
	public String excluir(@PathVariable("id") Long id, RedirectAttributes attributes) throws Exception {
		Setor setor = setorService.buscarPorId(id);
		
		String msgValidacao = setorService.validarExclusao(setor);
		if (msgValidacao != null && !msgValidacao.isEmpty()) {
			attributes.addFlashAttribute("mensagemErro", msgValidacao);
		} else {
			setorService.excluir(setor);
			attributes.addFlashAttribute("mensagem", "Setor excluído com sucesso!");
		}
		return "redirect:/setor";
	}
	
	
	@RequestMapping(value = "/downloadPdf")
	public String gerarPdf(Model model, HttpServletRequest httpServletRequest)  {
		model.addAttribute("relatorio", setorService.getRelatorioPdfVO(setorFilter, httpServletRequest));
		return "pdfView";
	}
	
	@ModelAttribute("statusCombo")
	public @ResponseBody List<SelectVO> getStatusCombo()  {
		return combosService.getStatusSetorCargoCombo();
	}
	
	@RequestMapping(value = "/buscar", method = RequestMethod.GET)
	public @ResponseBody List<SelectVO> getSetoresCombo(@RequestParam("filtro") boolean filtro, @RequestParam("idEmpresa") Long idEmpresa, @RequestParam("idSetor") Long idSetor)  {
		return combosService.getSetoresCombo(filtro, idEmpresa, idSetor);
	}

}