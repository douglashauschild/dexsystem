package br.com.dh.dexSystem.controller;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.client.HttpClientErrorException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import br.com.dh.dexSystem.model.SincronizacaoAbstract;
import br.com.dh.dexSystem.model.Usuario;
import br.com.dh.dexSystem.model.vo.SincronizacaoVO;
import br.com.dh.dexSystem.repository.UsuarioRepository;
import br.com.dh.dexSystem.service.ControleSincronizacaoService;
import br.com.dh.dexSystem.util.TokenUtils;

public class ApiExtend<T> {
	
	@Value("${api.hash}")
	protected String apiHash;
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	@Autowired
	private ControleSincronizacaoService controleSincronizacaoService;
	
	
	public boolean validarUsuarioToken(String token) {
		Usuario usuario = getUsuario(token);
		return usuario != null && getApiHashToken(token).equals(apiHash);
	}
	
	public Usuario getUsuario(String token) {
		String[] infoTokens = TokenUtils.getTokenExtraido(token);
		return usuarioRepository.findByEmailAndExcluidoIn(infoTokens[1], '0');
	}
	
	
	public List<T> receberSincronizacao(Class<T[]> classe, SincronizacaoVO sincronizacaoVO) throws HttpClientErrorException, Exception {
		T[] array = getGson().fromJson(sincronizacaoVO.getJson(), classe);
		return Arrays.asList(array);
	}
	
	
	protected void salvarControleSincronizacao(SincronizacaoVO sincronizacaoVO, SincronizacaoAbstract sincronizacaoAbstract) {
		controleSincronizacaoService.salvarRecebimentoSincronizacao(sincronizacaoVO, sincronizacaoAbstract);
	}
	
	
	private String getApiHashToken(String token) {
		String[] infoTokens = TokenUtils.getTokenExtraido(token);
		return infoTokens[0];
	}
	
	protected Gson getGson() {
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.setDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		gsonBuilder.registerTypeAdapter(java.util.Date.class, new JsonSerializer<Date>() {
			public JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext context) {
				return new JsonPrimitive(src.getTime());
			}
		});
		gsonBuilder.registerTypeAdapter(byte[].class, new JsonSerializer<byte[]>() {
		    public JsonElement serialize(byte[] src, Type typeOfSrc, JsonSerializationContext context) {
		        return new JsonPrimitive(Base64.encodeBase64String(src));
		    }
		});
		gsonBuilder.registerTypeAdapter(byte[].class, new JsonDeserializer<byte[]>() {
			@Override
			public byte[] deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
				return Base64.decodeBase64(json.getAsString());
			}
		});
		return gsonBuilder.create();
	}
}