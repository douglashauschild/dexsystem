package br.com.dh.dexSystem.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.dh.dexSystem.repository.filter.FichaEpiFilter;
import br.com.dh.dexSystem.service.FichaEpiService;

@Controller
@RequestMapping("/fichaEpi")	
public class FichaEpiController extends ControllerExtend {
	
	@Autowired
	private FichaEpiService fichaEpiService;
	
	
	@RequestMapping
	public ModelAndView pesquisar(FichaEpiFilter filtro, BindingResult result, Pageable pageable, HttpServletRequest httpServletRequest) throws Exception {
		ModelAndView mv = newModelAndView("relatorios/fichaEpi/fichaEpi", httpServletRequest);
		mv.addObject("possuiEntregas", fichaEpiService.isPossuiEntregas());
		return mv;
	}
	
	
	@RequestMapping(value = "/observacao", method = RequestMethod.GET)
	public @ResponseBody String getObservacao(@RequestParam("idEmpresa") Long idEmpresa, @RequestParam("idEmpregado") Long idEmpregado, @RequestParam("idSetor") Long idSetor, @RequestParam("idCargo") Long idCargo)  {
		return fichaEpiService.getObservacao(idEmpresa, idEmpregado, idSetor, idCargo);
	}
	
	
	@RequestMapping(value = "/gerar", method = RequestMethod.GET)
	public @ResponseBody String gerar(@RequestParam("idEmpresa") Long idEmpresa, @RequestParam("idEmpregado") Long idEmpregado, @RequestParam("idSetor") Long idSetor, @RequestParam("idCargo") Long idCargo) throws Exception  {
		return fichaEpiService.gerarFicha(idEmpresa, idSetor, idCargo, idEmpregado);
	}
}




