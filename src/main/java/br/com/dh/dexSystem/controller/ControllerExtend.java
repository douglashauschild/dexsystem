package br.com.dh.dexSystem.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;

import br.com.dh.dexSystem.model.constant.PermissoesEnum;
import br.com.dh.dexSystem.service.UsuarioService;

public class ControllerExtend {
	
	@Autowired
	private UsuarioService usuarioService;
	
	
	public ModelAndView newModelAndView(String pagina, HttpServletRequest httpServletRequest) {
		boolean bloquearInsercao = false;
		boolean bloquearAlteracao = false;
		boolean bloquearExclusao = false;
		
		ModelAndView mv = new ModelAndView(pagina);
		try {
			String path = httpServletRequest.getRequestURI().replace(httpServletRequest.getContextPath(), "");
	
			Map<String, Map<String, Boolean>> permissoes = usuarioService.getUsuario().getPermissoes();
			Map<String, Boolean> permissao = permissoes.get(path);
			if (permissao != null) {
				bloquearInsercao = !permissao.get(PermissoesEnum.INSERIR.getKey());
				bloquearAlteracao = !permissao.get(PermissoesEnum.ALTERAR.getKey());
				bloquearExclusao = !permissao.get(PermissoesEnum.EXCLUIR.getKey());
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		mv.addObject("bloquearInsercao", bloquearInsercao);
		mv.addObject("bloquearAlteracao", bloquearAlteracao);
		mv.addObject("bloquearExclusao", bloquearExclusao);
		return mv;
	}
}