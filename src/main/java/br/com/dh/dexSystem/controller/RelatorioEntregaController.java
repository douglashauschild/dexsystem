package br.com.dh.dexSystem.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.dh.dexSystem.model.EmpregadoEpi;
import br.com.dh.dexSystem.model.vo.SelectCharVO;
import br.com.dh.dexSystem.model.vo.SelectVO;
import br.com.dh.dexSystem.repository.filter.EmpregadoEpiFilter;
import br.com.dh.dexSystem.service.CombosService;
import br.com.dh.dexSystem.service.RelatorioEntregaService;

@Controller
@RequestMapping("/relatorioEntrega")	
public class RelatorioEntregaController extends ControllerExtend {
	
	@Autowired
	private RelatorioEntregaService relatorioEntregaService;
	@Autowired
	private CombosService combosService;
	
	private EmpregadoEpiFilter empregadoEpiFilter;
	
	
	@RequestMapping
	public ModelAndView pesquisar(EmpregadoEpiFilter filtro, BindingResult result, Pageable pageable, HttpServletRequest httpServletRequest) throws Exception {
		filtro.setRelatorio(true);
		this.empregadoEpiFilter = filtro;
		
		ModelAndView mv = newModelAndView("relatorios/entrega/entrega-lista", httpServletRequest);
		
		PageWrapper<EmpregadoEpi> paginaWrapper = relatorioEntregaService.pesquisar(filtro, pageable, httpServletRequest);
		mv.addObject("pagina", paginaWrapper);
		return mv;
	}
	
	@ModelAttribute("tipoConfirmacaoCombo")
	public @ResponseBody List<SelectCharVO> getTipoConfirmacaoCombo()  {
		return combosService.getTipoConfirmacaoCombo();
	}
	
	@ModelAttribute("episCombo")
	public @ResponseBody List<SelectVO> getEpisCombo() {
		return combosService.getEpisCombo(true);
	}
	
	@ModelAttribute("gradesCombo")
	public @ResponseBody List<SelectVO> getGradesCombo() {
		return combosService.getGradesCombo();
	}
	
	@RequestMapping(value = "/downloadPdf")
	public String gerarPdf(Model model, HttpServletRequest httpServletRequest) throws Exception  {
		model.addAttribute("relatorioEntrega", relatorioEntregaService.getRelatorioVO(empregadoEpiFilter, httpServletRequest));
		return "relatorioEntregaView";
	}
	
}