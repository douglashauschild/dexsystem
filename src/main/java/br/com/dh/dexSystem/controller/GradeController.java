package br.com.dh.dexSystem.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.dh.dexSystem.model.Grade;
import br.com.dh.dexSystem.model.vo.GradeItemVO;
import br.com.dh.dexSystem.model.vo.SelectVO;
import br.com.dh.dexSystem.repository.filter.GradeFilter;
import br.com.dh.dexSystem.service.CombosService;
import br.com.dh.dexSystem.service.GradeService;

@Controller
@RequestMapping("/grade")	
public class GradeController extends ControllerExtend {
	
	@Autowired
	private GradeService gradeService;
	@Autowired
	private CombosService combosService;
	
	private GradeFilter gradeFilter;
	
	
	@RequestMapping
	public ModelAndView pesquisar(GradeFilter filtro, BindingResult result, Pageable pageable, HttpServletRequest httpServletRequest) {
		this.gradeFilter = filtro;
		
		ModelAndView mv = newModelAndView("cadastros/grade/grade-lista", httpServletRequest);
		
		PageWrapper<Grade> paginaWrapper = gradeService.pesquisar(filtro, pageable, httpServletRequest);
		mv.addObject("pagina", paginaWrapper);

		return mv;
	}
	
	
	@RequestMapping("/novo")
	public ModelAndView novo() {	
		ModelAndView mv = new ModelAndView("cadastros/grade/grade-cadastro");
		Grade grade = new Grade();
		grade = gradeService.getGradeItens(grade);
		
		mv.addObject("grade", grade);
		return mv;
	}
	
	
	@RequestMapping(value = "/gravar", method = RequestMethod.POST)
	public String gravar(Grade grade, Model model, Errors errors, RedirectAttributes attributes) throws Exception {	
		String validarGravacao = gradeService.validarGravacao(grade, model);
		if (validarGravacao != null && !validarGravacao.isEmpty()) {
			return validarGravacao;
		}
		return gravarGrade(grade, attributes);
	}
	
	
	@RequestMapping(value = "/gravarModal", method = RequestMethod.POST)
	public String gravarModal(Grade grade, Errors errors, RedirectAttributes attributes) throws Exception {
		return gravarGrade(grade, attributes);
	}
	
	
	private String gravarGrade(Grade grade, RedirectAttributes attributes) throws Exception {
		gradeService.salvar(grade);
		gradeService.salvarGradeItens(grade);
		attributes.addFlashAttribute("mensagem", "Grade salva com sucesso!");
		return "redirect:/grade";
	}
	
	
	@RequestMapping("/editar/{id}")
	public ModelAndView edicao(@PathVariable("id") Long id) {
		Grade grade = gradeService.buscarPorId(id);
		grade = gradeService.getGradeItens(grade);
		
		ModelAndView mv = new ModelAndView("cadastros/grade/grade-cadastro");
		mv.addObject("grade", grade);
		
		return mv;
	}
	
	
	@RequestMapping("/excluir/{id}")
	public String excluir(@PathVariable("id") Long id, RedirectAttributes attributes) throws Exception {
		Grade grade = gradeService.buscarPorId(id);
		gradeService.excluir(grade);
		attributes.addFlashAttribute("mensagem", "Grade excluída com sucesso!");
		return "redirect:/grade";
	}
	
	
	@RequestMapping(value = "/item/incluir", method = RequestMethod.POST)
    public String adicionarItem(@RequestBody GradeItemVO gradeItemVO, Model model) {
		return gradeService.adicionarItem(gradeItemVO, model);
    }
	
	
	@RequestMapping(value = "/item/alterar", method = RequestMethod.POST)
    public String alterarItem(@RequestBody GradeItemVO gradeItemVO, Model model) {
		return gradeService.alterarItem(gradeItemVO, model);
    }
	
	
	@RequestMapping(value = "/item/remover", method = RequestMethod.POST)
    public String removerItem(@RequestBody GradeItemVO gradeItemVO, Model model) {
		return gradeService.removerItem(gradeItemVO, model);
    }
	
	
	@RequestMapping(value = "/downloadPdf")
	public String gerarPdf(Model model, HttpServletRequest httpServletRequest)  {
		model.addAttribute("relatorio", gradeService.getRelatorioPdfVO(gradeFilter, httpServletRequest));
		return "pdfView";
	}
	
	
	@RequestMapping(value = "/buscarTamanhos", method = RequestMethod.GET)
	public @ResponseBody  List<SelectVO> getTamanhosEpi(@RequestParam("idGrade") Long idGrade)  {
		return combosService.getGradeItensCombo(idGrade);
	}
}