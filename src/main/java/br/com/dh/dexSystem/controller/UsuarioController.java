package br.com.dh.dexSystem.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.dh.dexSystem.model.Usuario;
import br.com.dh.dexSystem.model.vo.SelectCharVO;
import br.com.dh.dexSystem.model.vo.SelectVO;
import br.com.dh.dexSystem.repository.filter.UsuarioFilter;
import br.com.dh.dexSystem.service.CombosService;
import br.com.dh.dexSystem.service.UsuarioService;

@Controller
@RequestMapping("/usuario")	
public class UsuarioController extends ControllerExtend {
	
	@Autowired
	private UsuarioService usuarioService;
	@Autowired
	private CombosService combosService;
	
	private UsuarioFilter usuarioFilter;
	
	
	@RequestMapping
	public ModelAndView pesquisar(UsuarioFilter filtro, BindingResult result, Pageable pageable, HttpServletRequest httpServletRequest) {
		this.usuarioFilter = filtro;
		
		ModelAndView mv = newModelAndView("cadastros/usuario/usuario-lista", httpServletRequest);
		PageWrapper<Usuario> paginaWrapper = usuarioService.pesquisar(filtro, pageable, httpServletRequest);
		mv.addObject("pagina", paginaWrapper);
		return mv;
	}
	
	@RequestMapping("/novo")
	public ModelAndView novo() {	
		ModelAndView mv = new ModelAndView("cadastros/usuario/usuario-cadastro");
		mv.addObject("usuario", new Usuario());
		return mv;
	}
	
	@RequestMapping(value = "/gravar", method = RequestMethod.POST)
	public String gravar(Usuario usuario, Errors errors, RedirectAttributes attributes, HttpServletRequest request) throws Exception {
		if (usuarioService.emailCadastrado(usuario)) {
			errors.rejectValue("email", null, "Este email já está cadastrado!");
			return "cadastros/usuario/usuario-cadastro";
		}
		if ((usuario.getId() == null && !usuarioService.senhasIguais(usuario)) || (usuario.getId() != null && usuarioService.editandoSenha(usuario) && !usuarioService.senhasIguais(usuario))) {
			errors.rejectValue("confirmaSenha", null, "A senha e a confimação da senha não são iguais!");
			return "cadastros/usuario/usuario-cadastro";
		} 
		if (usuario.getId() == null || usuarioService.editandoSenha(usuario)) {
			usuario.setSenha(usuarioService.getSenhaCrypt(usuario.getSenha()));
		}
		usuarioService.salvar(usuario);
		attributes.addFlashAttribute("mensagem", "Usuário salvo com sucesso!");
		return "redirect:/usuario";
	}
	
	@RequestMapping("/editar/{id}")
	public ModelAndView edicao(@PathVariable("id") Long id) {
		Usuario usuario = usuarioService.buscarPorId(id);
		
		ModelAndView mv = new ModelAndView("cadastros/usuario/usuario-cadastro");
		mv.addObject("usuario", usuario);
		return mv;
	}
	
	@RequestMapping("/excluir/{id}")
	public String excluir(@PathVariable("id") Long id, RedirectAttributes attributes) throws Exception {
		Usuario usuario = usuarioService.buscarPorId(id);
		usuarioService.excluir(usuario);
		attributes.addFlashAttribute("mensagem", "Usuário excluído com sucesso!");
		return "redirect:/usuario";
	}
	
	@RequestMapping(value = "/downloadPdf")
	public String gerarPdf(Model model, HttpServletRequest httpServletRequest)  {
		model.addAttribute("relatorio", usuarioService.getRelatorioPdfVO(usuarioFilter, httpServletRequest));
		return "pdfView";
	}
	
	@ModelAttribute("niveisAcessoCombo")
	public @ResponseBody List<SelectVO> getNiveisAcessoCombo()  {
		return combosService.getNiveisAcessoCombo();
	}
	
	@ModelAttribute("acessoDesktopCombo")
	public @ResponseBody List<SelectCharVO> getAcessoDesktopCombo()  {
		return combosService.getSimNaoCombo();
	}
}