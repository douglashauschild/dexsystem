package br.com.dh.dexSystem.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.dh.dexSystem.model.EmpregadoEpi;
import br.com.dh.dexSystem.model.vo.SincronizacaoVO;
import br.com.dh.dexSystem.service.EmpregadoEpiService;

@Controller
@RequestMapping("/api/empregadoEpi")	
public class EmpregadoEpiController extends ApiExtend<EmpregadoEpi> {
	
	@Autowired
	private EmpregadoEpiService empregadoEpiService;
	
	
	@RequestMapping(value = "/sincronizar/receber", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ResponseEntity<SincronizacaoVO> receberSincronizacao(@RequestBody SincronizacaoVO sincronizacaoVO, HttpServletRequest request) {
		try {
			String token = request.getHeader("Authorization");
			if (!validarUsuarioToken(token)) {
				return new ResponseEntity<SincronizacaoVO>(HttpStatus.UNAUTHORIZED);
			}
			List<EmpregadoEpi> empregadoEpis = super.receberSincronizacao(EmpregadoEpi[].class, sincronizacaoVO);
			if (empregadoEpis != null && !empregadoEpis.isEmpty()) {
				for (EmpregadoEpi empregadoEpi : empregadoEpis) {
					super.salvarControleSincronizacao(sincronizacaoVO, empregadoEpi);
					empregadoEpiService.salvarSincronizacao(empregadoEpi);
				}
				return new ResponseEntity<SincronizacaoVO>(sincronizacaoVO, HttpStatus.OK);
			}
			return new ResponseEntity<SincronizacaoVO>(HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
