package br.com.dh.dexSystem.client;

import java.io.Serializable;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import br.com.dh.dexSystem.model.vo.ViaCepVO;

@Component("viaCepRestClient")
public class ViaCepRestClient implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(ViaCepRestClient.class);
	
	@Value("${rest.viacep.url:https://viacep.com.br/ws/}")
	String url;

	public ViaCepVO consultar(String cep) throws Exception {
		ViaCepVO viaCepVO = null;
		try {
			RestTemplate rt = new RestTemplate();
			String uri = url + cep + "/json/";
			
			HttpEntity<ViaCepVO> entity = new HttpEntity<ViaCepVO>(getHeaders());
			ResponseEntity<ViaCepVO> ret = rt.exchange(uri, HttpMethod.GET, entity, ViaCepVO.class, cep);
			
			viaCepVO = ret.getBody();
		} catch (HttpClientErrorException e) {
			logger.error("http error:  " + e.getStatusCode() + " - " + e.getResponseBodyAsString());
			throw e;
		} catch (Exception e) {
			logger.error("error:  " + e.getMessage());
			throw e;
		}
		return viaCepVO;
	}

	
	protected HttpHeaders getHeaders() throws Exception {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		return headers;
	}
}
