package br.com.dh.dexSystem.repository.filter;

public class DashboardFilter extends Filter {

	private Long idEmpresa;

	public Long getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
}
