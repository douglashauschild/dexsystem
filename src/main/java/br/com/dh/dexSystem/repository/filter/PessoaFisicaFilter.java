package br.com.dh.dexSystem.repository.filter;

public class PessoaFisicaFilter extends Filter {

	private Character sexo;

	public Character getSexo() {
		return sexo;
	}

	public void setSexo(Character sexo) {
		this.sexo = sexo;
	}
}
