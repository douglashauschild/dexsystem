package br.com.dh.dexSystem.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.dh.dexSystem.model.Contato;

public interface ContatoRepository extends JpaRepository<Contato, Long> {

	public List<Contato> findByIdPessoa(Long idPessoa);
	
	public Contato findByIdPessoaAndTipoAndTipoOutroAndDescricao(Long idPessoa, Long tipo, String tipoOutro, String descricao);
}
