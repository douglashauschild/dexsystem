package br.com.dh.dexSystem.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.dh.dexSystem.model.ControleSincronizacao;

public interface ControleSincronizacaoRepository extends JpaRepository<ControleSincronizacao, Long> {

	public ControleSincronizacao findByIdDispositivoAndChecksum(Long idDispositivo, String checksum);
	
}
