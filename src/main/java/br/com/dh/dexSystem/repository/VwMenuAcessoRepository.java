package br.com.dh.dexSystem.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.dh.dexSystem.model.VwMenuAcesso;

public interface VwMenuAcessoRepository extends JpaRepository<VwMenuAcesso, Long> {

	
}
