package br.com.dh.dexSystem.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.dh.dexSystem.model.Atividade;

public interface AtividadeRepository extends JpaRepository<Atividade, Long> {
	
	
}
