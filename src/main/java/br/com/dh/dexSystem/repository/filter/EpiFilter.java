package br.com.dh.dexSystem.repository.filter;

public class EpiFilter extends Filter {

	private Long idGrade;
	private Long idUnidade;
	private Long statusCa;
	
	
	public Long getIdGrade() {
		return idGrade;
	}
	
	public void setIdGrade(Long idGrade) {
		this.idGrade = idGrade;
	}
	
	public Long getIdUnidade() {
		return idUnidade;
	}

	public void setIdUnidade(Long idUnidade) {
		this.idUnidade = idUnidade;
	}

	public Long getStatusCa() {
		return statusCa;
	}

	public void setStatusCa(Long statusCa) {
		this.statusCa = statusCa;
	}
}
