package br.com.dh.dexSystem.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.dh.dexSystem.model.Grade;

public interface GradeRepository extends JpaRepository<Grade, Long> {

	public List<Grade> findByNomeAndExcluidoIn(String nome, Character excluido);

	public List<Grade> findByNomeAndIdNotInAndExcluidoIn(String nome, Long id, Character excluido);
	
	@Query("select g from Grade g where g.excluido = '0'")
	public List<Grade> buscarTodas();
}
