package br.com.dh.dexSystem.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.dh.dexSystem.model.VwMenuAcessoPermissao;

public interface VwMenuAcessoPermissaoRepository extends JpaRepository<VwMenuAcessoPermissao, Long> {

	public List<VwMenuAcessoPermissao> findByIdNivelAcesso(Long idNivelAcesso);
	
}
