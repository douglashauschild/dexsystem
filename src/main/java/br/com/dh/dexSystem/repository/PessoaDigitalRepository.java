package br.com.dh.dexSystem.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.dh.dexSystem.model.PessoaDigital;

public interface PessoaDigitalRepository extends JpaRepository<PessoaDigital, Long> {
	
	public PessoaDigital findByChecksum(String checksum);
	
}
