package br.com.dh.dexSystem.repository.filter;

public class Filter {

	private Long id;
	private String pesquisa;
	private String dataInicio;
	private String dataFim;
	private boolean filtroAberto;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPesquisa() {
		return pesquisa;
	}

	public void setPesquisa(String pesquisa) {
		this.pesquisa = pesquisa;
	}

	public String getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(String dataInicio) {
		this.dataInicio = dataInicio;
	}

	public String getDataFim() {
		return dataFim;
	}

	public void setDataFim(String dataFim) {
		this.dataFim = dataFim;
	}

	public boolean isFiltroAberto() {
		return filtroAberto;
	}

	public void setFiltroAberto(boolean filtroAberto) {
		this.filtroAberto = filtroAberto;
	}
}
