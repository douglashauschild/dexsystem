package br.com.dh.dexSystem.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.dh.dexSystem.model.CargoEpi;

public interface CargoEpiRepository extends JpaRepository<CargoEpi, Long> {
	
	
}
