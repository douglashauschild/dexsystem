package br.com.dh.dexSystem.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.dh.dexSystem.model.Cidade;

public interface CidadeRepository extends JpaRepository<Cidade, Long> {

	public List<Cidade> findByIdEstado(Long idEstado);
}
