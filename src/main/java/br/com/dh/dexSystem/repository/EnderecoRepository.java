package br.com.dh.dexSystem.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.dh.dexSystem.model.Endereco;

public interface EnderecoRepository extends JpaRepository<Endereco, Long> {

	public Endereco findByIdPessoa(Long idPessoa);
	
}
