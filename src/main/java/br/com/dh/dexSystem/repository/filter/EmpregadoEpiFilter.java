package br.com.dh.dexSystem.repository.filter;

public class EmpregadoEpiFilter extends Filter {
	
	private Long idEmpresa;
	private Long idEmpregado;
	private Long idSetor;
	private Long idCargo;
	private Long idEpi;
	private Long idGrade;
	private Character tipoConfirmacao;
	
	private boolean substituido;
	private boolean devolvido;
	private boolean vencido;
	private boolean ativo;
	private boolean aguardando;
	private boolean entregaVencida;
	
	private boolean relatorio;
	
	
	public EmpregadoEpiFilter() {
		super();
		this.substituido = false;
		this.devolvido = false;
		this.vencido = true;
		this.ativo = true;
		this.aguardando = true;
		this.entregaVencida = true;
	}
	

	public Long getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public Long getIdEmpregado() {
		return idEmpregado;
	}

	public void setIdEmpregado(Long idEmpregado) {
		this.idEmpregado = idEmpregado;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public boolean isVencido() {
		return vencido;
	}

	public void setVencido(boolean vencido) {
		this.vencido = vencido;
	}

	public boolean isSubstituido() {
		return substituido;
	}

	public void setSubstituido(boolean substituido) {
		this.substituido = substituido;
	}

	public boolean isDevolvido() {
		return devolvido;
	}

	public void setDevolvido(boolean devolvido) {
		this.devolvido = devolvido;
	}

	public boolean isAguardando() {
		return aguardando;
	}

	public void setAguardando(boolean aguardando) {
		this.aguardando = aguardando;
	}

	public boolean isEntregaVencida() {
		return entregaVencida;
	}

	public void setEntregaVencida(boolean entregaVencida) {
		this.entregaVencida = entregaVencida;
	}

	public Long getIdSetor() {
		return idSetor;
	}

	public void setIdSetor(Long idSetor) {
		this.idSetor = idSetor;
	}

	public Long getIdCargo() {
		return idCargo;
	}

	public void setIdCargo(Long idCargo) {
		this.idCargo = idCargo;
	}

	public Character getTipoConfirmacao() {
		return tipoConfirmacao;
	}

	public void setTipoConfirmacao(Character tipoConfirmacao) {
		this.tipoConfirmacao = tipoConfirmacao;
	}

	public Long getIdEpi() {
		return idEpi;
	}

	public void setIdEpi(Long idEpi) {
		this.idEpi = idEpi;
	}

	public Long getIdGrade() {
		return idGrade;
	}

	public void setIdGrade(Long idGrade) {
		this.idGrade = idGrade;
	}

	public boolean isRelatorio() {
		return relatorio;
	}

	public void setRelatorio(boolean relatorio) {
		this.relatorio = relatorio;
	}
}