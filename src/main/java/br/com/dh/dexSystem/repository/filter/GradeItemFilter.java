package br.com.dh.dexSystem.repository.filter;

public class GradeItemFilter extends Filter {
	
	private Long idGrade;

	public Long getIdGrade() {
		return idGrade;
	}

	public void setIdGrade(Long idGrade) {
		this.idGrade = idGrade;
	}
}
