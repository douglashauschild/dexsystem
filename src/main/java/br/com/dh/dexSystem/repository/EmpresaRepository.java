package br.com.dh.dexSystem.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.dh.dexSystem.model.Empresa;

public interface EmpresaRepository extends JpaRepository<Empresa, Long> {
	
	
}
