package br.com.dh.dexSystem.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.dh.dexSystem.model.Empregado;

public interface EmpregadoRepository extends JpaRepository<Empregado, Long> {
	
	
	@Query(value = "select e.* from empregado e "
			+ "inner join atividade a on e.id = a.empregado_id "
			+ "inner join cargo c on c.id = a.cargo_id "
			+ "where (e.excluido is null or e.excluido = '0') and (a.excluido is null or a.excluido = '0') and "
			+ "(e.data_admissao <= now() and (e.data_demissao is null or e.data_demissao > now())) and "
			+ "(a.data_inicio <= now() and (a.data_fim is null or a.data_fim > now())) and "
			+ "c.setor_id = ?", nativeQuery = true)
	public List<Empregado> buscarAtivosPorIdSetor(Long idSetor);
	
	
	@Query(value = "select e.* from empregado e "
			+ "inner join atividade a on e.id = a.empregado_id "
			+ "where (e.excluido is null or e.excluido = '0') and (a.excluido is null or a.excluido = '0') and "
			+ "(e.data_admissao <= now() and (e.data_demissao is null or e.data_demissao > now())) and "
			+ "(a.data_inicio <= now() and (a.data_fim is null or a.data_fim > now())) and "
			+ "a.cargo_id =  ?1", nativeQuery = true)
	public List<Empregado> buscarAtivosPorIdCargo(Long idCargo);
	
}
