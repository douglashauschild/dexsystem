package br.com.dh.dexSystem.repository.filter;

public class EmpregadoFilter extends Filter {

	private Long idEmpresa;
	private Long idPessoa;
	private String dataInicioAdmissao;
	private String dataFimAdmissao;
	private String dataInicioDemissao;
	private String dataFimDemissao;

	public Long getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public Long getIdPessoa() {
		return idPessoa;
	}

	public void setIdPessoa(Long idPessoa) {
		this.idPessoa = idPessoa;
	}

	public String getDataInicioAdmissao() {
		return dataInicioAdmissao;
	}

	public void setDataInicioAdmissao(String dataInicioAdmissao) {
		this.dataInicioAdmissao = dataInicioAdmissao;
	}

	public String getDataFimAdmissao() {
		return dataFimAdmissao;
	}

	public void setDataFimAdmissao(String dataFimAdmissao) {
		this.dataFimAdmissao = dataFimAdmissao;
	}

	public String getDataInicioDemissao() {
		return dataInicioDemissao;
	}

	public void setDataInicioDemissao(String dataInicioDemissao) {
		this.dataInicioDemissao = dataInicioDemissao;
	}

	public String getDataFimDemissao() {
		return dataFimDemissao;
	}

	public void setDataFimDemissao(String dataFimDemissao) {
		this.dataFimDemissao = dataFimDemissao;
	}
}
