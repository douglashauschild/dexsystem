package br.com.dh.dexSystem.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.dh.dexSystem.model.ParametroSistema;

public interface ParametroSistemaRepository extends JpaRepository<ParametroSistema, Long> {
	
	
}
