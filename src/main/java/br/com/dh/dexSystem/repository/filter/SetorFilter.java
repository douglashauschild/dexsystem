package br.com.dh.dexSystem.repository.filter;

public class SetorFilter extends Filter {

	private Long idEmpresa;
	private Long status;
	
	public Long getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}
}
