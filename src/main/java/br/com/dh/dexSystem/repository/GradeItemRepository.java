package br.com.dh.dexSystem.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.dh.dexSystem.model.GradeItem;

@Transactional
public interface GradeItemRepository extends JpaRepository<GradeItem, Long> {
	
	@Query("select gi from GradeItem gi where gi.idGrade = ?1 and gi.excluido = '0' order by ordem")
	public List<GradeItem> buscarPorIdGrade(Long idGrade);
	
	
	@Query("select gi from GradeItem gi where gi.idGrade = ?1 and gi.tamanho = ?2 and gi.ordem = ?3 and gi.excluido = '0' order by ordem")
	public GradeItem buscarPorIdGradeTamanhoOrdem(Long idGrade, String Tamanho, Integer ordem);
}
