package br.com.dh.dexSystem.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.dh.dexSystem.model.Setor;

public interface SetorRepository extends JpaRepository<Setor, Long> {
	
	
}
