package br.com.dh.dexSystem.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.dh.dexSystem.model.EmpregadoEpi;

public interface EmpregadoEpiRepository extends JpaRepository<EmpregadoEpi, Long> {
	
	public EmpregadoEpi findByChecksum(String checksum);
}
