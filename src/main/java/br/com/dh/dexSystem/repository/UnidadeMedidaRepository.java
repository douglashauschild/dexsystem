package br.com.dh.dexSystem.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.dh.dexSystem.model.UnidadeMedida;

public interface UnidadeMedidaRepository extends JpaRepository<UnidadeMedida, Long> {
	
	public List<UnidadeMedida> findByDescricaoAndExcluidoIn(String descricao, Character excluido);
	
	public List<UnidadeMedida> findByDescricaoAndIdNotInAndExcluidoIn(String descricao, Long id, Character excluido);
	
	@Query("select u from UnidadeMedida u where u.excluido = '0'")
	public List<UnidadeMedida> buscarTodas();
	
}
