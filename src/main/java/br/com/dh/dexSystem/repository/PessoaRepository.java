package br.com.dh.dexSystem.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.dh.dexSystem.model.Pessoa;

public interface PessoaRepository extends JpaRepository<Pessoa, Long> {
	
	
}
