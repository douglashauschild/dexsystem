package br.com.dh.dexSystem.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.dh.dexSystem.model.NivelAcessoPermissao;
import br.com.dh.dexSystem.model.NivelAcessoPermissaoPK;

public interface NivelAcessoPermissaoRepository extends JpaRepository<NivelAcessoPermissao, NivelAcessoPermissaoPK> {

}
