package br.com.dh.dexSystem.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.dh.dexSystem.model.Cargo;

public interface CargoRepository extends JpaRepository<Cargo, Long> {
	
	
}
