package br.com.dh.dexSystem.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.dh.dexSystem.model.Motivo;

public interface MotivoRepository extends JpaRepository<Motivo, Long> {
	
	public List<Motivo> findByDescricaoAndExcluidoIn(String descricao, Character excluido);
	
	public List<Motivo> findByDescricaoAndIdNotInAndExcluidoIn(String descricao, Long id, Character excluido);
	
}
