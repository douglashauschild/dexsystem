package br.com.dh.dexSystem.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.dh.dexSystem.model.Estado;

public interface EstadoRepository extends JpaRepository<Estado, Long> {
	
	public List<Estado> findAll();
	
}
