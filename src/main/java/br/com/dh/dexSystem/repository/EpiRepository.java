package br.com.dh.dexSystem.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.dh.dexSystem.model.Epi;

public interface EpiRepository extends JpaRepository<Epi, Long> {
	
	public List<Epi> findByCaAndExcluidoIn(String ca, Character excluido);

	public List<Epi> findByCaAndIdNotInAndExcluidoIn(String ca, Long id, Character excluido);
	
}
