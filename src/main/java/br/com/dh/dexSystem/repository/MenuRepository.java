package br.com.dh.dexSystem.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.dh.dexSystem.model.Menu;

public interface MenuRepository extends JpaRepository<Menu, Long> {

	@Query(value = "select m from NivelAcessoPermissao nap, Menu m "+
			   	   "where m.id = nap.id.idMenu and nap.acessar = '1' and "+
			   	   "nap.id.idNivelAcesso = ?1 and m.idMenu is null "+
			   	   "order by m.ordem")
	public List<Menu> buscarTodosMenus(Long idNivelAcesso);
	
	@Query(value = "select m from NivelAcessoPermissao nap, Menu m "+
			   	   "where m.id = nap.id.idMenu and nap.acessar = '1' and "+
			   	   "nap.id.idNivelAcesso = ?1 and m.idMenu = ?2 "+
		   	   	   "order by m.ordem")
	public List<Menu> buscarTodosSubMenus(Long idNivelAcesso, Long idMenu);

}
