package br.com.dh.dexSystem.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.dh.dexSystem.model.Dispositivo;

public interface DispositivoRepository extends JpaRepository<Dispositivo, Long> {
	
	
}
