package br.com.dh.dexSystem.repository.filter;

public class EmpresaFilter extends Filter {

	private Character tipo;
	private Long status;
	
	public Character getTipo() {
		return tipo;
	}
	
	public void setTipo(Character tipo) {
		this.tipo = tipo;
	}
	
	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}
}
