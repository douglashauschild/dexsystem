package br.com.dh.dexSystem.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.dh.dexSystem.model.NivelAcesso;

public interface NivelAcessoRepository extends JpaRepository<NivelAcesso, Long> {

	@Query("select coalesce(max(na.nivel), 0) from NivelAcesso na where (na.excluido is null or na.excluido = '0')")
	public Long getMaxNivel();
	
	@Query("select na from NivelAcesso na where (na.excluido is null or na.excluido = '0') and nivel = ?1")
	public NivelAcesso buscarPorNivel(Long nivel);
	
	@Query("select na from NivelAcesso na where (na.excluido is null or na.excluido = '0') and nivel = ?2 and id != ?1")
	public NivelAcesso buscarPorNivelDiferenteId(Long id, Long nivel);
	
	@Query("select na from NivelAcesso na where (na.excluido is null or na.excluido = '0')")
	public List<NivelAcesso> buscarTodos();
}
