package br.com.dh.dexSystem.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.dh.dexSystem.model.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

	public Usuario findByEmailAndExcluidoIn(String email, Character excluido);
	
	public Usuario findByEmailAndIdNotInAndExcluidoIn(String email, Long id, Character excluido);
	
	public List<Usuario> findByIdNivelAcessoAndExcluidoIn(Long idNivelAcesso, Character excluido);
	
}
