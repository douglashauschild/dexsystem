package br.com.dh.dexSystem.repository.filter;

public class UsuarioFilter extends Filter {

	private Character tipo;

	public Character getTipo() {
		return tipo;
	}

	public void setTipo(Character tipo) {
		this.tipo = tipo;
	}
}
