package br.com.dh.dexSystem.manager;


import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.dh.dexSystem.model.Motivo;
import br.com.dh.dexSystem.repository.filter.MotivoFilter;

public interface MotivoManager {

	public Page<Motivo> filtrar(MotivoFilter filtro, Pageable pageable);
	public List<Motivo> buscarTodosAtivos();
	
}
