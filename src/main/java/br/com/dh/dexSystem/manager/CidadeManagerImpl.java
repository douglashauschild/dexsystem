package br.com.dh.dexSystem.manager;


import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dh.dexSystem.model.Cidade;

@Service("cidadeManager")
@Transactional
public class CidadeManagerImpl implements CidadeManager {

	@PersistenceContext
	private EntityManager manager;
	
	
	public Cidade buscarCidade(String uf, String nome) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Cidade.class);
		criteria.createAlias("estado", "estado", JoinType.INNER_JOIN);
		
		criteria.add(Restrictions.eq("estado.sigla", uf));
		criteria.add(Restrictions.eq("nome", nome));
		criteria.setMaxResults(1);
		return (Cidade) criteria.uniqueResult();
	}
	
}
