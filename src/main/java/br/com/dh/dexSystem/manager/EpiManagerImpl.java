package br.com.dh.dexSystem.manager;


import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import br.com.dh.dexSystem.model.Epi;
import br.com.dh.dexSystem.model.constant.StatusCaEnum;
import br.com.dh.dexSystem.repository.filter.EpiFilter;
import br.com.dh.dexSystem.repository.paginacao.PaginacaoUtil;
import br.com.dh.dexSystem.util.FormatacaoUtils;

@Service("epiManager")
@Transactional
public class EpiManagerImpl implements EpiManager {

	@PersistenceContext
	private EntityManager manager;
	
	@Autowired
	private PaginacaoUtil paginacaoUtil;
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public Page<Epi> filtrar(EpiFilter filtro, Pageable pageable) throws Exception {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Epi.class);
		
		paginacaoUtil.preparar(criteria, pageable);
		adicionarFiltro(filtro, criteria);
		
		return new PageImpl<>(criteria.list(), pageable, total(filtro));
	}
	
	
	private Long total(EpiFilter filtro) throws Exception {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Epi.class);
		adicionarFiltro(filtro, criteria);
		criteria.setProjection(Projections.rowCount());
		return (Long) criteria.uniqueResult();
	}

	
	private void adicionarFiltro(EpiFilter filtro, Criteria criteria) throws Exception {
		criteria.add(Restrictions.or(Restrictions.isNull("excluido"), Restrictions.eq("excluido", '0')));
		
		if (filtro != null) {
			Criterion critPesquisa = null;
			if (filtro.getStatusCa() != null) {
				Date dataAtual = new Date();
				if (filtro.getStatusCa().equals(StatusCaEnum.SEM_CA.getKey())) {
					criteria.add(Restrictions.isNull("dataValidadeCa"));
				} else if (filtro.getStatusCa().equals(StatusCaEnum.CA_EM_DIA.getKey())) {
					criteria.add(Restrictions.gt("dataValidadeCa", dataAtual));
				} else if (filtro.getStatusCa().equals(StatusCaEnum.CA_VENCIDO.getKey())) {
					criteria.add(Restrictions.le("dataValidadeCa", dataAtual));
				}
			}
			if ((filtro.getDataInicio() != null && !filtro.getDataInicio().isEmpty()) || (filtro.getDataFim() != null && !filtro.getDataFim().isEmpty())) {
				Date dataInicio = filtro.getDataInicio() != null && !filtro.getDataInicio().isEmpty() ? FormatacaoUtils.getData(filtro.getDataInicio()) : null;
				Date dataFim = filtro.getDataFim() != null && !filtro.getDataFim().isEmpty() ? FormatacaoUtils.getData(filtro.getDataFim()) : null;
				if (dataInicio != null && dataFim == null) {
					criteria.add(Restrictions.ge("dataValidadeCa", dataInicio));
				} else if (dataInicio == null && dataFim != null) {
					criteria.add(Restrictions.le("dataValidadeCa", dataFim));
				} else if (dataInicio != null && dataFim != null) {
					criteria.add(Restrictions.between("dataValidadeCa", dataInicio, dataFim));
				}
			}
			if (!StringUtils.isEmpty(filtro.getPesquisa())) {
				if (filtro.getPesquisa().matches("^[0-9]{1,45}$")) {
					critPesquisa = Restrictions.eq("id", Long.parseLong(filtro.getPesquisa()));
				}
				if (critPesquisa == null) {
					critPesquisa = Restrictions.ilike("nome", filtro.getPesquisa(), MatchMode.ANYWHERE);
                } else {
                    critPesquisa = Restrictions.or(critPesquisa, Restrictions.ilike("nome", filtro.getPesquisa(), MatchMode.ANYWHERE));
                }
				if (critPesquisa == null) {
					critPesquisa = Restrictions.ilike("ca", filtro.getPesquisa(), MatchMode.ANYWHERE);
                } else {
                    critPesquisa = Restrictions.or(critPesquisa, Restrictions.ilike("ca", filtro.getPesquisa(), MatchMode.ANYWHERE));
                }
			}
			if (critPesquisa != null) {
				criteria.add(critPesquisa);
            }
		}
	}
	
	
	public Epi buscarPorId(Long id) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Epi.class);
		criteria.createAlias("grade", "grade", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("unidadeMedida", "unidadeMedida", JoinType.LEFT_OUTER_JOIN);
		
		criteria.add(Restrictions.eq("id", id));
		criteria.add(Restrictions.or(Restrictions.isNull("excluido"), Restrictions.eq("excluido", '0')));
		criteria.setMaxResults(1);
		return (Epi) criteria.uniqueResult();
	}
	
	
	public List<Epi> buscarTodos() {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Epi.class);
		criteria.createAlias("grade", "grade", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("unidadeMedida", "unidadeMedida", JoinType.LEFT_OUTER_JOIN);
		
		criteria.add(Restrictions.or(Restrictions.isNull("excluido"), Restrictions.eq("excluido", '0')));
		return criteria.list();
	}
}
