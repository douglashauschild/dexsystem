package br.com.dh.dexSystem.manager;


import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import br.com.dh.dexSystem.model.Empregado;
import br.com.dh.dexSystem.repository.filter.EmpregadoFilter;
import br.com.dh.dexSystem.repository.paginacao.PaginacaoUtil;
import br.com.dh.dexSystem.util.FormatacaoUtils;

@Service("empregadoManager")
@Transactional
public class EmpregadoManagerImpl implements EmpregadoManager {

	@PersistenceContext
	private EntityManager manager;
	
	@Autowired
	private PaginacaoUtil paginacaoUtil;
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public Page<Empregado> filtrar(EmpregadoFilter filtro, Pageable pageable) throws Exception {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Empregado.class);
		
		paginacaoUtil.preparar(criteria, pageable);
		adicionarFiltro(filtro, criteria);
		
		return new PageImpl<>(criteria.list(), pageable, total(filtro));
	}
	
	
	private Long total(EmpregadoFilter filtro) throws Exception {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Empregado.class);
		adicionarFiltro(filtro, criteria);
		criteria.setProjection(Projections.rowCount());
		return (Long) criteria.uniqueResult();
	}

	
	private void adicionarFiltro(EmpregadoFilter filtro, Criteria criteria) throws Exception {
		criteria.createAlias("pessoaFisica", "pessoaFisica", JoinType.INNER_JOIN);
		criteria.createAlias("pessoaFisica.pessoa", "pessoa", JoinType.INNER_JOIN);
		criteria.createAlias("empresa", "empresa", JoinType.INNER_JOIN);
		criteria.createAlias("empresa.pessoaJuridica", "pessoaJuridica", JoinType.INNER_JOIN);
		criteria.createAlias("pessoaJuridica.pessoa", "pessoaEmp", JoinType.INNER_JOIN);
		
		criteria.add(Restrictions.or(Restrictions.isNull("excluido"), Restrictions.eq("excluido", '0')));
		
		if (filtro != null) {
			if (filtro.getIdEmpresa() != null && filtro.getIdEmpresa() != 0L) {
				criteria.add(Restrictions.eq("idEmpresa", filtro.getIdEmpresa()));
			}
			if (filtro.getIdPessoa() != null && filtro.getIdPessoa() != 0L) {
				criteria.add(Restrictions.eq("idPessoa", filtro.getIdPessoa()));
			}
			if ((filtro.getDataInicioAdmissao() != null && !filtro.getDataInicioAdmissao().isEmpty()) || (filtro.getDataFimAdmissao() != null && !filtro.getDataFimAdmissao().isEmpty())) {
				Date dataInicio = filtro.getDataInicioAdmissao() != null && !filtro.getDataInicioAdmissao().isEmpty() ? FormatacaoUtils.getData(filtro.getDataInicioAdmissao()) : null;
				Date dataFim = filtro.getDataFimAdmissao() != null && !filtro.getDataFimAdmissao().isEmpty() ? FormatacaoUtils.getData(filtro.getDataFimAdmissao()) : null;
				if (dataInicio != null && dataFim == null) {
					criteria.add(Restrictions.ge("dataAdmissao", dataInicio));
				} else if (dataInicio == null && dataFim != null) {
					criteria.add(Restrictions.le("dataAdmissao", dataFim));
				} else if (dataInicio != null && dataFim != null) {
					criteria.add(Restrictions.between("dataAdmissao", dataInicio, dataFim));
				}
			}
			if ((filtro.getDataInicioDemissao() != null && !filtro.getDataInicioDemissao().isEmpty()) || (filtro.getDataFimDemissao() != null && !filtro.getDataFimDemissao().isEmpty())) {
				Date dataInicio = filtro.getDataInicioDemissao() != null && !filtro.getDataInicioDemissao().isEmpty() ? FormatacaoUtils.getData(filtro.getDataInicioDemissao()) : null;
				Date dataFim = filtro.getDataFimDemissao() != null && !filtro.getDataFimDemissao().isEmpty() ? FormatacaoUtils.getData(filtro.getDataFimDemissao()) : null;
				if (dataInicio != null && dataFim == null) {
					criteria.add(Restrictions.ge("dataDemissao", dataInicio));
				} else if (dataInicio == null && dataFim != null) {
					criteria.add(Restrictions.le("dataDemissao", dataFim));
				} else if (dataInicio != null && dataFim != null) {
					criteria.add(Restrictions.between("dataDemissao", dataInicio, dataFim));
				}
			}
			Criterion critPesquisa = null;
			if (!StringUtils.isEmpty(filtro.getPesquisa())) {
				if (filtro.getPesquisa().matches("^[0-9]{1,45}$")) {
					critPesquisa = Restrictions.eq("idPessoa", Long.parseLong(filtro.getPesquisa()));
				}
				if (critPesquisa == null) {
					critPesquisa = Restrictions.ilike("pessoa.nome", filtro.getPesquisa(), MatchMode.ANYWHERE);
                } else {
                    critPesquisa = Restrictions.or(critPesquisa, Restrictions.ilike("pessoa.nome", filtro.getPesquisa(), MatchMode.ANYWHERE));
                }
			}
			if (critPesquisa != null) {
				criteria.add(critPesquisa);
            }
		}
	}
	
	
	public Empregado buscarPorId(Long id) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Empregado.class);
		criteria.createAlias("pessoaFisica", "pessoaFisica", JoinType.INNER_JOIN);
		criteria.createAlias("pessoaFisica.pessoa", "pessoa", JoinType.INNER_JOIN);
		
		criteria.add(Restrictions.eq("id", id));
		criteria.add(Restrictions.or(Restrictions.isNull("excluido"), Restrictions.eq("excluido", '0')));
		criteria.setMaxResults(1);
		return (Empregado) criteria.uniqueResult();
	}
	
	
	public List<Empregado> buscarTodosAtivos() {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Empregado.class);
		criteria.createAlias("pessoaFisica", "pessoaFisica", JoinType.INNER_JOIN);
		criteria.createAlias("pessoaFisica.pessoa", "pessoa", JoinType.INNER_JOIN);
		
		criteria.add(RestrictionEmpregadoAtivo());
		criteria.addOrder(Order.asc("pessoa.nome"));
		return criteria.list();
	}
	
	
	public List<Empregado> buscarAtivosPorEmpresa(Long idEmpresa) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Empregado.class);
		criteria.createAlias("pessoaFisica", "pessoaFisica", JoinType.INNER_JOIN);
		criteria.createAlias("pessoaFisica.pessoa", "pessoa", JoinType.INNER_JOIN);
		
		if (idEmpresa != null) {
			criteria.add(Restrictions.eq("idEmpresa", idEmpresa));
		}
		criteria.add(RestrictionEmpregadoAtivo());
		criteria.addOrder(Order.asc("pessoa.nome"));
		return criteria.list();
	}
	
	
	public List<Empregado> buscarParaValidacao(Empregado empregado) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Empregado.class);
		criteria.createAlias("pessoaFisica", "pessoaFisica", JoinType.INNER_JOIN);
		criteria.createAlias("pessoaFisica.pessoa", "pessoa", JoinType.INNER_JOIN);
		
		criteria.add(Restrictions.eq("idEmpresa", empregado.getIdEmpresa()));
		criteria.add(Restrictions.eq("idPessoa", empregado.getIdPessoa()));
		
		if (empregado.getId() != null) {
			criteria.add(Restrictions.ne("id", empregado.getId()));
		}
		criteria.add(Restrictions.or(Restrictions.isNull("excluido"), Restrictions.eq("excluido", '0')));
		return criteria.list();
	}
	
	
	public List<Empregado> buscarEmpregadoEmOutrasEmpresas(Empregado empregado) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Empregado.class);
		criteria.createAlias("pessoaFisica", "pessoaFisica", JoinType.INNER_JOIN);
		criteria.createAlias("pessoaFisica.pessoa", "pessoa", JoinType.INNER_JOIN);
		
		criteria.add(Restrictions.ne("idEmpresa", empregado.getIdEmpresa()));
		criteria.add(Restrictions.eq("idPessoa", empregado.getIdPessoa()));
		criteria.add(RestrictionEmpregadoAtivo());
		return criteria.list();
	}
	
	
	private Criterion RestrictionEmpregadoAtivo() {
		Date dataAtual = new Date();
		return Restrictions.and(
			Restrictions.and(
					Restrictions.le("dataAdmissao", dataAtual), 
					Restrictions.or(Restrictions.isNull("dataDemissao"), Restrictions.gt("dataDemissao", dataAtual))
			), 
			Restrictions.or(Restrictions.isNull("excluido"), Restrictions.eq("excluido", '0'))
		);
	}
}
