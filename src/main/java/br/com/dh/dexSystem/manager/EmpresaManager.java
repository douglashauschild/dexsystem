package br.com.dh.dexSystem.manager;


import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.dh.dexSystem.model.Empresa;
import br.com.dh.dexSystem.repository.filter.EmpresaFilter;

public interface EmpresaManager {

	public Page<Empresa> filtrar(EmpresaFilter filtro, Pageable pageable) throws Exception;
	public List<Empresa> buscarMatrizes(Long id);
	public List<Empresa> buscarTodos(boolean inclusiveInativos);
	public Empresa buscarPorId(Long id);
	public Empresa isValidarExistencia(Empresa empresa);
}
