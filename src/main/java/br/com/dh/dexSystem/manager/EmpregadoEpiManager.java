package br.com.dh.dexSystem.manager;


import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.dh.dexSystem.model.EmpregadoEpi;
import br.com.dh.dexSystem.model.vo.EpiUtilizadoVO;
import br.com.dh.dexSystem.repository.filter.EmpregadoEpiFilter;

public interface EmpregadoEpiManager {

	public Page<EmpregadoEpi> filtrar(EmpregadoEpiFilter filtro, Pageable pageable) throws Exception;
	public EmpregadoEpi buscarPorId(Long id);
	public List<EmpregadoEpi> buscarEpiPrevisto(Long idEmpregado, Long idEpi);
	public List<EmpregadoEpi> buscarPorIds(List<Long> ids);
	public List<EmpregadoEpi> buscarParaEstatisticas(Long idEmpregado);
	public List<EmpregadoEpi> buscarParaFichaEpi(Long idEmpregado);
	public Integer buscarTotalEntregasRealizadas(Long idEmpresa, Date dataInicial, Date dataFinal, boolean considerarQuantidade);
	public List<EmpregadoEpi> buscarTudoPorEmpresa(Long idEmpresa);
	public List<EmpregadoEpi> buscarTodosNaoFinalizados(Long idEmpregado);
	public List<EpiUtilizadoVO> buscarEpisMaisUtilizados();
}