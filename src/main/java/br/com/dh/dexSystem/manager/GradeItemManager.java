package br.com.dh.dexSystem.manager;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.dh.dexSystem.model.GradeItem;
import br.com.dh.dexSystem.repository.filter.GradeItemFilter;

public interface GradeItemManager {

	public Page<GradeItem> filtrar(GradeItemFilter filtro, Pageable pageable);
	
}
