package br.com.dh.dexSystem.manager;


import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import br.com.dh.dexSystem.model.Empresa;
import br.com.dh.dexSystem.model.constant.StatusEmpresaEnum;
import br.com.dh.dexSystem.model.constant.TipoEmpresaEnum;
import br.com.dh.dexSystem.repository.filter.EmpresaFilter;
import br.com.dh.dexSystem.repository.paginacao.PaginacaoUtil;
import br.com.dh.dexSystem.util.FormatacaoUtils;

@Service("empresaManager")
@Transactional
public class EmpresaManagerImpl implements EmpresaManager {

	@PersistenceContext
	private EntityManager manager;
	
	@Autowired
	private PaginacaoUtil paginacaoUtil;
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public Page<Empresa> filtrar(EmpresaFilter filtro, Pageable pageable) throws Exception {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Empresa.class);
		
		paginacaoUtil.preparar(criteria, pageable);
		adicionarFiltro(filtro, criteria);
		
		return new PageImpl<>(criteria.list(), pageable, total(filtro));
	}
	
	
	private Long total(EmpresaFilter filtro) throws Exception {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Empresa.class);
		adicionarFiltro(filtro, criteria);
		criteria.setProjection(Projections.rowCount());
		return (Long) criteria.uniqueResult();
	}

	
	private void adicionarFiltro(EmpresaFilter filtro, Criteria criteria) throws Exception {
		criteria.createAlias("pessoaJuridica", "pessoaJuridica", JoinType.INNER_JOIN);
		criteria.createAlias("pessoaJuridica.pessoa", "pessoa", JoinType.INNER_JOIN);
		
		criteria.add(Restrictions.or(Restrictions.isNull("pessoaJuridica.excluido"), Restrictions.eq("pessoaJuridica.excluido", '0')));
		
		if (filtro != null) {
			if (filtro.getStatus() != null) {
				if (filtro.getStatus().equals(StatusEmpresaEnum.ATIVA.getKey())) {
					criteria.add(RestrictionEmpresaAtiva());
				} else  {
					criteria.add(Restrictions.not(RestrictionEmpresaAtiva()));
				}
			}
			if (filtro.getTipo() != null) {
				criteria.add(Restrictions.eq("tipo", filtro.getTipo()));
			}
			Criterion critPesquisa = null;
			if (!StringUtils.isEmpty(filtro.getPesquisa())) {
				if (filtro.getPesquisa().matches("^[0-9]{1,45}$")) {
					critPesquisa = Restrictions.eq("idPessoa", Long.parseLong(filtro.getPesquisa()));
				}
				if (critPesquisa == null) {
					critPesquisa = Restrictions.ilike("pessoa.nome", filtro.getPesquisa(), MatchMode.ANYWHERE);
                } else {
                    critPesquisa = Restrictions.or(critPesquisa, Restrictions.ilike("pessoa.nome", filtro.getPesquisa(), MatchMode.ANYWHERE));
                }
				if (critPesquisa == null) {
					critPesquisa = Restrictions.ilike("pessoaJuridica.cnpj", FormatacaoUtils.removerFormatacao(filtro.getPesquisa()), MatchMode.ANYWHERE);
                } else {
                    critPesquisa = Restrictions.or(critPesquisa, Restrictions.ilike("pessoaJuridica.cnpj", FormatacaoUtils.removerFormatacao(filtro.getPesquisa()), MatchMode.ANYWHERE));
                }
			}
			if (critPesquisa != null) {
				criteria.add(critPesquisa);
            }
		}
	}
	
	
	public List<Empresa> buscarMatrizes(Long id) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Empresa.class);
		criteria.createAlias("pessoaJuridica", "pessoaJuridica", JoinType.INNER_JOIN);
		criteria.createAlias("pessoaJuridica.pessoa", "pessoa", JoinType.INNER_JOIN);
		
		if (id != null) {
			criteria.add(Restrictions.ne("idPessoa", id));
		}
		criteria.add(Restrictions.eq("tipo", TipoEmpresaEnum.MATRIZ.getKey()));
		criteria.add(RestrictionEmpresaAtiva());
		return criteria.list();
	}
	
	
	public List<Empresa> buscarTodos(boolean inclusiveInativos) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Empresa.class);
		criteria.createAlias("pessoaJuridica", "pessoaJuridica", JoinType.INNER_JOIN);
		criteria.createAlias("pessoaJuridica.pessoa", "pessoa", JoinType.INNER_JOIN);
		
		if (!inclusiveInativos) {
			criteria.add(RestrictionEmpresaAtiva());
		}
		return criteria.list();
	}
	
	
	public Empresa buscarPorId(Long id) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Empresa.class);
		criteria.createAlias("pessoaJuridica", "pessoaJuridica", JoinType.INNER_JOIN);
		criteria.createAlias("pessoaJuridica.pessoa", "pessoa", JoinType.INNER_JOIN);
		
		criteria.add(Restrictions.eq("idPessoa", id));
		criteria.add(Restrictions.or(Restrictions.isNull("pessoaJuridica.excluido"), Restrictions.eq("pessoaJuridica.excluido", '0')));
		criteria.setMaxResults(1);
		return (Empresa) criteria.uniqueResult();
	}
	
	
	public Empresa isValidarExistencia(Empresa empresa) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Empresa.class);
		criteria.createAlias("pessoaJuridica", "pessoaJuridica", JoinType.INNER_JOIN);
		criteria.createAlias("pessoaJuridica.pessoa", "pessoa", JoinType.INNER_JOIN);
		
		if (empresa.getIdPessoa() != null) {
			criteria.add(Restrictions.ne("idPessoa", empresa.getIdPessoa()));
		}
		criteria.add(Restrictions.eq("pessoaJuridica.cnpj", empresa.getPessoaJuridica().getCnpjLimpo()));
		criteria.add(RestrictionEmpresaAtiva());
		
		criteria.setMaxResults(1);
		return (Empresa) criteria.uniqueResult();
	}
	
	
	private Criterion RestrictionEmpresaAtiva() {
		Date dataAtual = new Date();
		return Restrictions.and(
			Restrictions.and(
					Restrictions.le("pessoaJuridica.dataInicio", dataAtual), 
					Restrictions.or(Restrictions.isNull("pessoaJuridica.dataFim"), Restrictions.gt("pessoaJuridica.dataFim", dataAtual))
			), 
			Restrictions.or(Restrictions.isNull("pessoaJuridica.excluido"), Restrictions.eq("pessoaJuridica.excluido", '0'))
		);
	}
}
