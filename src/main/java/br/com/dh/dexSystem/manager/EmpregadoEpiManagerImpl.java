package br.com.dh.dexSystem.manager;


import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import br.com.dh.dexSystem.model.EmpregadoEpi;
import br.com.dh.dexSystem.model.vo.EpiUtilizadoVO;
import br.com.dh.dexSystem.repository.filter.EmpregadoEpiFilter;
import br.com.dh.dexSystem.repository.paginacao.PaginacaoUtil;
import br.com.dh.dexSystem.util.FormatacaoUtils;

@Service("empregadoEpiManager")
@Transactional
public class EmpregadoEpiManagerImpl implements EmpregadoEpiManager {

	@PersistenceContext
	private EntityManager manager;
	
	@Autowired
	private PaginacaoUtil paginacaoUtil;
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public Page<EmpregadoEpi> filtrar(EmpregadoEpiFilter filtro, Pageable pageable) throws Exception {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(EmpregadoEpi.class);
		
		paginacaoUtil.preparar(criteria, pageable);
		adicionarFiltro(filtro, criteria);
		
		if (filtro.isRelatorio()) {
			criteria.addOrder(Order.desc("dataEntrega"));
		}
		
		return new PageImpl<>(criteria.list(), pageable, total(filtro));
	}
	
	
	private Long total(EmpregadoEpiFilter filtro) throws Exception {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(EmpregadoEpi.class);
		adicionarFiltro(filtro, criteria);
		criteria.setProjection(Projections.rowCount());
		return (Long) criteria.uniqueResult();
	}

	
	private void adicionarFiltro(EmpregadoEpiFilter filtro, Criteria criteria) throws Exception {
		criteria.createAlias("empregado", "empregado", JoinType.INNER_JOIN);
		criteria.createAlias("epi", "epi", JoinType.INNER_JOIN);
		criteria.createAlias("gradeItem", "gradeItem", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("motivo", "motivo", JoinType.LEFT_OUTER_JOIN);
		
		criteria.add(Restrictions.or(Restrictions.isNull("excluido"), Restrictions.eq("excluido", '0')));
		if (filtro.getIdEmpregado() != null && filtro.getIdEmpregado() != 0L) {
			criteria.add(Restrictions.eq("idEmpregado", filtro.getIdEmpregado()));
		}
		if (filtro.getIdEmpresa() != null && filtro.getIdEmpresa() != 0L) {
			criteria.add(Restrictions.eq("empregado.idEmpresa", filtro.getIdEmpresa()));
		}
		if (filtro != null) {
			if (!filtro.isSubstituido() || !filtro.isDevolvido() || !filtro.isVencido() || !filtro.isAtivo() || !filtro.isAguardando() || !filtro.isEntregaVencida()) {
				Criterion filtrosCheckbox = null;
				if (filtro.isSubstituido()) {
					filtrosCheckbox = Restrictions.isNotNull("dataSubstituicao");
				}
				if (filtro.isDevolvido()) {
					Criterion fAux = Restrictions.and(Restrictions.isNotNull("dataDevolucao"), Restrictions.isNull("dataSubstituicao"));
					if (filtrosCheckbox != null) {
						filtrosCheckbox = Restrictions.or(filtrosCheckbox, fAux);
					} else {
						filtrosCheckbox = fAux;
					}
				}
				if (filtro.isEntregaVencida()) {
					Criterion fAux = Restrictions.and(Restrictions.isNull("dataSubstituicao"), Restrictions.isNull("dataDevolucao"), Restrictions.isNull("dataConfirmacao"), Restrictions.isNotNull("dataPrevistaEntrega"), Restrictions.lt("dataPrevistaEntrega", new Date()));
					if (filtrosCheckbox != null) {
						filtrosCheckbox = Restrictions.or(filtrosCheckbox, fAux);
					} else {
						filtrosCheckbox = fAux;
					}
				}
				if (filtro.isVencido()) {
					Criterion fAux = Restrictions.and(Restrictions.isNull("dataSubstituicao"), Restrictions.isNull("dataDevolucao"), Restrictions.isNotNull("dataConfirmacao"), Restrictions.isNotNull("dataPrevistaTroca"), Restrictions.lt("dataPrevistaTroca", new Date()));
					if (filtrosCheckbox != null) {
						filtrosCheckbox = Restrictions.or(filtrosCheckbox, fAux);
					} else {
						filtrosCheckbox = fAux;
					}
				}
				if (filtro.isAtivo()) {
					Criterion fAux = Restrictions.and(Restrictions.isNotNull("dataConfirmacao"), Restrictions.isNull("dataSubstituicao"), Restrictions.isNull("dataDevolucao"));
					if (filtrosCheckbox != null) {
						filtrosCheckbox = Restrictions.or(filtrosCheckbox, fAux);
					} else {
						filtrosCheckbox = fAux;
					}
				}
				if (filtro.isAguardando()) {
					Criterion fAux = Restrictions.and(Restrictions.isNull("dataConfirmacao"), Restrictions.isNotNull("dataPrevistaEntrega"), Restrictions.ge("dataPrevistaEntrega", new Date()));
					if (filtrosCheckbox != null) {
						filtrosCheckbox = Restrictions.or(filtrosCheckbox, fAux);
					} else {
						filtrosCheckbox = fAux;
					}
				}
				if (filtrosCheckbox != null) {
					criteria.add(filtrosCheckbox);
				}
			}
			if (filtro.getIdEpi() != null && filtro.getIdEpi() != 0L) {
				criteria.add(Restrictions.eq("idEpi", filtro.getIdEpi()));
			}
			if (filtro.getIdGrade() != null && filtro.getIdGrade() != 0L) {
				criteria.add(Restrictions.eq("gradeItem.idGrade", filtro.getIdGrade()));
			}
			if (filtro.getTipoConfirmacao() != null) {
				criteria.add(Restrictions.eq("tipoConfirmacao", filtro.getTipoConfirmacao()));
			}
			if ((filtro.getDataInicio() != null && !filtro.getDataInicio().isEmpty()) || (filtro.getDataFim() != null && !filtro.getDataFim().isEmpty())) {
				Date dataInicio = filtro.getDataInicio() != null && !filtro.getDataInicio().isEmpty() ? FormatacaoUtils.getData(filtro.getDataInicio()) : null;
				Date dataFim = filtro.getDataFim() != null && !filtro.getDataFim().isEmpty() ? FormatacaoUtils.getData(filtro.getDataFim()) : null;
				if (dataInicio != null && dataFim == null) {
					criteria.add(Restrictions.ge("dataEntrega", dataInicio));
				} else if (dataInicio == null && dataFim != null) {
					criteria.add(Restrictions.le("dataEntrega", dataFim));
				} else if (dataInicio != null && dataFim != null) {
					criteria.add(Restrictions.between("dataEntrega", dataInicio, dataFim));
				}
			}
			Criterion critPesquisa = null;
			if (!StringUtils.isEmpty(filtro.getPesquisa())) {
				if (filtro.getPesquisa().matches("^[0-9]{1,45}$")) {
					critPesquisa = Restrictions.eq("id", Long.parseLong(filtro.getPesquisa()));
				}
				if (critPesquisa == null) {
					critPesquisa = Restrictions.ilike("epi.nome", filtro.getPesquisa(), MatchMode.ANYWHERE);
                } else {
                    critPesquisa = Restrictions.or(critPesquisa, Restrictions.ilike("epi.nome", filtro.getPesquisa(), MatchMode.ANYWHERE));
                }
				if (critPesquisa == null) {
					critPesquisa = Restrictions.ilike("epi.ca", filtro.getPesquisa(), MatchMode.ANYWHERE);
                } else {
                    critPesquisa = Restrictions.or(critPesquisa, Restrictions.ilike("epi.ca", filtro.getPesquisa(), MatchMode.ANYWHERE));
                }
			}
			if (critPesquisa != null) {
				criteria.add(critPesquisa);
            }
		}
	}
	
	
	public EmpregadoEpi buscarPorId(Long id) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(EmpregadoEpi.class);
		criteria.createAlias("empregado", "empregado", JoinType.INNER_JOIN);
		criteria.createAlias("epi", "epi", JoinType.INNER_JOIN);
		criteria.createAlias("gradeItem", "gradeItem", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("motivo", "motivo", JoinType.LEFT_OUTER_JOIN);
		
		criteria.add(Restrictions.eq("id", id));
		criteria.add(Restrictions.or(Restrictions.isNull("excluido"), Restrictions.eq("excluido", '0')));
		criteria.setMaxResults(1);
		return (EmpregadoEpi) criteria.uniqueResult();
	}
	
	
	public List<EmpregadoEpi> buscarEpiPrevisto(Long idEmpregado, Long idEpi) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(EmpregadoEpi.class);
		criteria.createAlias("empregado", "empregado", JoinType.INNER_JOIN);
		criteria.createAlias("epi", "epi", JoinType.INNER_JOIN);
		criteria.createAlias("gradeItem", "gradeItem", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("motivo", "motivo", JoinType.LEFT_OUTER_JOIN);
		
		criteria.add(Restrictions.eq("idEmpregado", idEmpregado));
		criteria.add(Restrictions.eq("idEpi", idEpi));
		criteria.add(Restrictions.isNull("dataSubstituicao"));
		criteria.add(Restrictions.isNull("dataDevolucao"));
		
		criteria.add(Restrictions.or(Restrictions.isNull("inclusaoManual"), Restrictions.eq("inclusaoManual", '0')));
		criteria.add(Restrictions.or(Restrictions.isNull("excluido"), Restrictions.eq("excluido", '0')));
		criteria.setMaxResults(1);
		return criteria.list();
	}
	
	
	public List<EmpregadoEpi> buscarPorIds(List<Long> ids) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(EmpregadoEpi.class);
		criteria.createAlias("empregado", "empregado", JoinType.INNER_JOIN);
		criteria.createAlias("epi", "epi", JoinType.INNER_JOIN);
		criteria.createAlias("epi.grade", "grade", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("gradeItem", "gradeItem", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("motivo", "motivo", JoinType.LEFT_OUTER_JOIN);
		
		criteria.add(Restrictions.in("id", ids));
		criteria.add(Restrictions.or(Restrictions.isNull("excluido"), Restrictions.eq("excluido", '0')));
		return criteria.list();
	}
	
	public List<EmpregadoEpi> buscarParaEstatisticas(Long idEmpregado) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(EmpregadoEpi.class);
		
		criteria.add(Restrictions.or(Restrictions.isNull("excluido"), Restrictions.eq("excluido", '0')));
		criteria.add(Restrictions.eq("idEmpregado", idEmpregado));
		
		Criterion vencidos = Restrictions.or(
										Restrictions.and(Restrictions.isNull("dataConfirmacao"), Restrictions.isNotNull("dataPrevistaEntrega"), Restrictions.lt("dataPrevistaEntrega", new Date())), 
										Restrictions.and(Restrictions.isNotNull("dataConfirmacao"), Restrictions.isNotNull("dataPrevistaTroca"), Restrictions.lt("dataPrevistaTroca", new Date()))
									);
		Criterion ativos = Restrictions.and(Restrictions.isNotNull("dataConfirmacao"), Restrictions.isNull("dataSubstituicao"), Restrictions.isNull("dataDevolucao"));
		Criterion aguardando = Restrictions.and(Restrictions.isNull("dataConfirmacao"), Restrictions.isNotNull("dataPrevistaEntrega"), Restrictions.ge("dataPrevistaEntrega", new Date()));
		criteria.add(Restrictions.or(vencidos, ativos, aguardando));
		return criteria.list();
	}
	
	public List<EmpregadoEpi> buscarParaFichaEpi(Long idEmpregado) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(EmpregadoEpi.class);
		criteria.createAlias("epi", "epi", JoinType.INNER_JOIN);
		criteria.createAlias("motivo", "motivo", JoinType.LEFT_OUTER_JOIN);
		
		criteria.add(Restrictions.or(Restrictions.isNull("excluido"), Restrictions.eq("excluido", '0')));
		criteria.add(Restrictions.eq("idEmpregado", idEmpregado));
		criteria.add(Restrictions.isNotNull("dataConfirmacao"));
		criteria.addOrder(Order.asc("dataEntrega"));
		return criteria.list();
	}
	
	public Integer buscarTotalEntregasRealizadas(Long idEmpresa, Date dataInicial, Date dataFinal, boolean considerarQuantidade) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(EmpregadoEpi.class);
		criteria.createAlias("empregado", "empregado", JoinType.INNER_JOIN);
		
		criteria.add(Restrictions.or(Restrictions.isNull("excluido"), Restrictions.eq("excluido", '0')));
		criteria.add(Restrictions.isNotNull("dataConfirmacao"));
		
		if (idEmpresa != null) {
			criteria.add(Restrictions.eq("empregado.idEmpresa", idEmpresa));
		}
		if (dataInicial != null && dataFinal != null) {
			criteria.add(Restrictions.between("dataEntrega", dataInicial, dataFinal));
		}
		if (considerarQuantidade) {
			criteria.setProjection(Projections.sum("quantidade"));
			Object obj = criteria.uniqueResult();
			return obj != null ? Integer.valueOf(obj.toString()) : null;
		} else {
			return criteria.list().size();
		}
	}
	
	
	public List<EmpregadoEpi> buscarTudoPorEmpresa(Long idEmpresa) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(EmpregadoEpi.class);
		criteria.createAlias("empregado", "empregado", JoinType.INNER_JOIN);
		
		if (idEmpresa != null) {
			criteria.add(Restrictions.eq("empregado.idEmpresa", idEmpresa));
		}
		criteria.add(Restrictions.or(Restrictions.isNull("excluido"), Restrictions.eq("excluido", '0')));
		return criteria.list();
	}
	
	
	public List<EmpregadoEpi> buscarTodosNaoFinalizados(Long idEmpregado) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(EmpregadoEpi.class);
		criteria.createAlias("empregado", "empregado", JoinType.INNER_JOIN);
		criteria.createAlias("epi", "epi", JoinType.INNER_JOIN);
		criteria.createAlias("gradeItem", "gradeItem", JoinType.LEFT_OUTER_JOIN);
		criteria.createAlias("motivo", "motivo", JoinType.LEFT_OUTER_JOIN);
		
		criteria.add(Restrictions.eq("idEmpregado", idEmpregado));
		criteria.add(Restrictions.isNull("dataSubstituicao"));
		criteria.add(Restrictions.isNull("dataDevolucao"));
		
		criteria.add(Restrictions.or(Restrictions.isNull("excluido"), Restrictions.eq("excluido", '0')));
		return criteria.list();
	}
	
	
	public List<EpiUtilizadoVO> buscarEpisMaisUtilizados() {
		SQLQuery sqlQuery = manager.unwrap(Session.class).createSQLQuery("select e.nome, count(e.id) as qtd from empregado_epi ee inner join epi e on ee.epi_id = e.id where ee.data_confirmacao is not null and (ee.excluido is null or ee.excluido = '0') and (e.excluido is null or e.excluido = '0') group by e.nome order by qtd desc limit 5");
		sqlQuery.addScalar("nome", StringType.INSTANCE);
		sqlQuery.addScalar("qtd", StringType.INSTANCE);
		sqlQuery.setResultTransformer(Transformers.aliasToBean(EpiUtilizadoVO.class));
		return sqlQuery.list();
	}
}
