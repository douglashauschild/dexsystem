package br.com.dh.dexSystem.manager;


import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.dh.dexSystem.model.PessoaFisica;
import br.com.dh.dexSystem.repository.filter.PessoaFisicaFilter;

public interface PessoaFisicaManager {

	public Page<PessoaFisica> filtrar(PessoaFisicaFilter filtro, Pageable pageable) throws Exception;
	public PessoaFisica buscarPorId(Long id);
	public List<PessoaFisica> buscarTodas();
	public PessoaFisica isValidarExistencia(PessoaFisica pessoaFisica);
}
