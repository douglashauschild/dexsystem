package br.com.dh.dexSystem.manager;


import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import br.com.dh.dexSystem.model.PessoaFisica;
import br.com.dh.dexSystem.model.constant.SexoEnum;
import br.com.dh.dexSystem.repository.filter.PessoaFisicaFilter;
import br.com.dh.dexSystem.repository.paginacao.PaginacaoUtil;
import br.com.dh.dexSystem.util.FormatacaoUtils;

@Service("pessoaFisicaManager")
@Transactional
public class PessoaFisicaManagerImpl implements PessoaFisicaManager {

	@PersistenceContext
	private EntityManager manager;
	
	@Autowired
	private PaginacaoUtil paginacaoUtil;
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public Page<PessoaFisica> filtrar(PessoaFisicaFilter filtro, Pageable pageable) throws Exception {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(PessoaFisica.class);
		
		paginacaoUtil.preparar(criteria, pageable);
		adicionarFiltro(filtro, criteria);
		
		return new PageImpl<>(criteria.list(), pageable, total(filtro));
	}
	
	
	private Long total(PessoaFisicaFilter filtro) throws Exception {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(PessoaFisica.class);
		adicionarFiltro(filtro, criteria);
		criteria.setProjection(Projections.rowCount());
		return (Long) criteria.uniqueResult();
	}

	
	private void adicionarFiltro(PessoaFisicaFilter filtro, Criteria criteria) throws Exception {
		criteria.createAlias("pessoa", "pessoa", JoinType.INNER_JOIN);
		
		criteria.add(Restrictions.or(Restrictions.isNull("excluido"), Restrictions.eq("excluido", '0')));
		
		if (filtro != null) {
			Criterion critPesquisa = null;
			if (filtro.getSexo() != null) {
				criteria.add(Restrictions.eq("sexo", SexoEnum.getByKey(filtro.getSexo()).getKey()));
			}
			if ((filtro.getDataInicio() != null && !filtro.getDataInicio().isEmpty()) || (filtro.getDataFim() != null && !filtro.getDataFim().isEmpty())) {
				Date dataInicio = filtro.getDataInicio() != null && !filtro.getDataInicio().isEmpty() ? FormatacaoUtils.getData(filtro.getDataInicio()) : null;
				Date dataFim = filtro.getDataFim() != null && !filtro.getDataFim().isEmpty() ? FormatacaoUtils.getData(filtro.getDataFim()) : null;
				if (dataInicio != null && dataFim == null) {
					criteria.add(Restrictions.ge("dataNascimento", dataInicio));
				} else if (dataInicio == null && dataFim != null) {
					criteria.add(Restrictions.le("dataNascimento", dataFim));
				} else if (dataInicio != null && dataFim != null) {
					criteria.add(Restrictions.between("dataNascimento", dataInicio, dataFim));
				}
			}
			if (!StringUtils.isEmpty(filtro.getPesquisa())) {
				if (filtro.getPesquisa().matches("^[0-9]{1,45}$")) {
					critPesquisa = Restrictions.eq("idPessoa", Long.parseLong(filtro.getPesquisa()));
				}
				if (critPesquisa == null) {
					critPesquisa = Restrictions.ilike("pessoa.nome", filtro.getPesquisa(), MatchMode.ANYWHERE);
                } else {
                    critPesquisa = Restrictions.or(critPesquisa, Restrictions.ilike("pessoa.nome", filtro.getPesquisa(), MatchMode.ANYWHERE));
                }
				if (critPesquisa == null) {
					critPesquisa = Restrictions.ilike("cpf", FormatacaoUtils.removerFormatacao(filtro.getPesquisa()), MatchMode.ANYWHERE);
                } else {
                    critPesquisa = Restrictions.or(critPesquisa, Restrictions.ilike("cpf", FormatacaoUtils.removerFormatacao(filtro.getPesquisa()), MatchMode.ANYWHERE));
                }
				if (critPesquisa == null) {
					critPesquisa = Restrictions.ilike("rg", filtro.getPesquisa(), MatchMode.ANYWHERE);
                } else {
                    critPesquisa = Restrictions.or(critPesquisa, Restrictions.ilike("rg", filtro.getPesquisa(), MatchMode.ANYWHERE));
                }
			}
			if (critPesquisa != null) {
				criteria.add(critPesquisa);
            }
		}
	}
	
	
	public PessoaFisica buscarPorId(Long id) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(PessoaFisica.class);
		criteria.createAlias("pessoa", "pessoa", JoinType.INNER_JOIN);
		
		criteria.add(Restrictions.eq("idPessoa", id));
		criteria.add(Restrictions.or(Restrictions.isNull("excluido"), Restrictions.eq("excluido", '0')));
		criteria.setMaxResults(1);
		return (PessoaFisica) criteria.uniqueResult();
	}

	
	public List<PessoaFisica> buscarTodas() {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(PessoaFisica.class);
		criteria.createAlias("pessoa", "pessoa", JoinType.INNER_JOIN);
		
		criteria.add(Restrictions.or(Restrictions.isNull("excluido"), Restrictions.eq("excluido", '0')));
		return criteria.list();
	}
	
	
	public PessoaFisica isValidarExistencia(PessoaFisica pessoaFisica) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(PessoaFisica.class);
		criteria.createAlias("pessoa", "pessoa", JoinType.INNER_JOIN);
		
		if (pessoaFisica.getIdPessoa() != null) {
			criteria.add(Restrictions.ne("idPessoa", pessoaFisica.getIdPessoa()));
		}
		criteria.add(Restrictions.eq("cpf", pessoaFisica.getCpfLimpo()));
		
		criteria.setMaxResults(1);
		return (PessoaFisica) criteria.uniqueResult();
	}
}
