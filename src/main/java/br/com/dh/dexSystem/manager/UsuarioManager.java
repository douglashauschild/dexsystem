package br.com.dh.dexSystem.manager;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.dh.dexSystem.model.Usuario;
import br.com.dh.dexSystem.repository.filter.UsuarioFilter;

public interface UsuarioManager {

	public Page<Usuario> filtrar(UsuarioFilter filtro, Pageable pageable);
	
}
