package br.com.dh.dexSystem.manager;


import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import br.com.dh.dexSystem.model.UnidadeMedida;
import br.com.dh.dexSystem.repository.filter.UnidadeMedidaFilter;
import br.com.dh.dexSystem.repository.paginacao.PaginacaoUtil;

@Service("unidadeMedidaManager")
@Transactional
public class UnidadeMedidaManagerImpl implements UnidadeMedidaManager {

	@PersistenceContext
	private EntityManager manager;
	
	@Autowired
	private PaginacaoUtil paginacaoUtil;
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public Page<UnidadeMedida> filtrar(UnidadeMedidaFilter filtro, Pageable pageable) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(UnidadeMedida.class);
		
		paginacaoUtil.preparar(criteria, pageable);
		adicionarFiltro(filtro, criteria);
		
		return new PageImpl<>(criteria.list(), pageable, total(filtro));
	}
	
	
	private Long total(UnidadeMedidaFilter filtro) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(UnidadeMedida.class);
		adicionarFiltro(filtro, criteria);
		criteria.setProjection(Projections.rowCount());
		return (Long) criteria.uniqueResult();
	}

	
	private void adicionarFiltro(UnidadeMedidaFilter filtro, Criteria criteria) {
		criteria.add(Restrictions.or(Restrictions.isNull("excluido"), Restrictions.eq("excluido", '0')));
		
		if (filtro != null) {
			Criterion critPesquisa = null;
			if (!StringUtils.isEmpty(filtro.getPesquisa())) {
				if (filtro.getPesquisa().matches("^[0-9]{1,45}$")) {
					critPesquisa = Restrictions.eq("id", Long.parseLong(filtro.getPesquisa()));
				}
				if (critPesquisa == null) {
					critPesquisa = Restrictions.ilike("descricao", filtro.getPesquisa(), MatchMode.ANYWHERE);
                } else {
                    critPesquisa = Restrictions.or(critPesquisa, Restrictions.ilike("descricao", filtro.getPesquisa(), MatchMode.ANYWHERE));
                }
			}
			if (critPesquisa != null) {
				criteria.add(critPesquisa);
            }
		}
	}
}
