package br.com.dh.dexSystem.manager;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.dh.dexSystem.model.UnidadeMedida;
import br.com.dh.dexSystem.repository.filter.UnidadeMedidaFilter;

public interface UnidadeMedidaManager {

	public Page<UnidadeMedida> filtrar(UnidadeMedidaFilter filtro, Pageable pageable);
	
}
