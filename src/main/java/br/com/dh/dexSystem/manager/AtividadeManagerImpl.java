package br.com.dh.dexSystem.manager;


import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import br.com.dh.dexSystem.model.Atividade;
import br.com.dh.dexSystem.model.Cargo;
import br.com.dh.dexSystem.model.Empregado;
import br.com.dh.dexSystem.repository.filter.AtividadeFilter;
import br.com.dh.dexSystem.repository.paginacao.PaginacaoUtil;

@Service("atividadeManager")
@Transactional
public class AtividadeManagerImpl implements AtividadeManager {

	@PersistenceContext
	private EntityManager manager;
	
	@Autowired
	private PaginacaoUtil paginacaoUtil;
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public Page<Atividade> filtrar(AtividadeFilter filtro, Pageable pageable) throws Exception {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Atividade.class);
		
		paginacaoUtil.preparar(criteria, pageable);
		adicionarFiltro(filtro, criteria);
		
		return new PageImpl<>(criteria.list(), pageable, total(filtro));
	}
	
	
	private Long total(AtividadeFilter filtro) throws Exception {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Atividade.class);
		adicionarFiltro(filtro, criteria);
		criteria.setProjection(Projections.rowCount());
		return (Long) criteria.uniqueResult();
	}

	
	private void adicionarFiltro(AtividadeFilter filtro, Criteria criteria) throws Exception {
		criteria.createAlias("cargo", "cargo", JoinType.INNER_JOIN);
		criteria.createAlias("cargo.setor", "setor", JoinType.INNER_JOIN);
		criteria.createAlias("empregado", "empregado", JoinType.INNER_JOIN);
		criteria.createAlias("empregado.pessoaFisica", "pessoaFisica", JoinType.INNER_JOIN);
		criteria.createAlias("pessoaFisica.pessoa", "pessoa", JoinType.INNER_JOIN);
		
		criteria.add(Restrictions.or(Restrictions.isNull("excluido"), Restrictions.eq("excluido", '0')));
		
		if (filtro != null) {
			if (filtro.getIdEmpresa() != null && filtro.getIdEmpresa() != 0L) {
				criteria.add(Restrictions.eq("empregado.idEmpresa", filtro.getIdEmpresa()));
			}
			if (filtro.getIdEmpregado() != null && filtro.getIdEmpregado() != 0L) {
				criteria.add(Restrictions.eq("idEmpregado", filtro.getIdEmpregado()));
			}
			if (filtro.getIdSetor() != null && filtro.getIdSetor() != 0L) {
				criteria.add(Restrictions.eq("cargo.idSetor", filtro.getIdSetor()));
			}
			if (filtro.getIdCargo() != null && filtro.getIdCargo() != 0L) {
				criteria.add(Restrictions.eq("idCargo", filtro.getIdCargo()));
			}
			Criterion critPesquisa = null;
			if (!StringUtils.isEmpty(filtro.getPesquisa())) {
				if (filtro.getPesquisa().matches("^[0-9]{1,45}$")) {
					critPesquisa = Restrictions.eq("id", Long.parseLong(filtro.getPesquisa()));
				}
			}
			if (critPesquisa != null) {
				criteria.add(critPesquisa);
            }
		}
	}
	
	
	public Atividade buscarPorId(Long id) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Atividade.class);
		criteria.createAlias("cargo", "cargo", JoinType.INNER_JOIN);
		criteria.createAlias("empregado", "empregado", JoinType.INNER_JOIN);
		
		criteria.add(Restrictions.eq("id", id));
		criteria.add(Restrictions.or(Restrictions.isNull("excluido"), Restrictions.eq("excluido", '0')));
		criteria.setMaxResults(1);
		return (Atividade) criteria.uniqueResult();
	}
	
	
	public List<Atividade> buscarParaValidacao(Atividade atividade) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Atividade.class);
		criteria.createAlias("cargo", "cargo", JoinType.INNER_JOIN);
		criteria.createAlias("empregado", "empregado", JoinType.INNER_JOIN);
		
		criteria.add(Restrictions.eq("empregado.idEmpresa", atividade.getEmpregado().getIdEmpresa()));
		criteria.add(Restrictions.eq("empregado.id", atividade.getIdEmpregado()));
		if (atividade.getId() != null) {
			criteria.add(Restrictions.ne("id", atividade.getId()));
		}
		criteria.add(Restrictions.or(Restrictions.isNull("excluido"), Restrictions.eq("excluido", '0')));
		return criteria.list();
	}
	
	
	public List<Atividade> buscarAtivasPorCargo(Cargo cargo) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Atividade.class);
		criteria.createAlias("cargo", "cargo", JoinType.INNER_JOIN);
		
		criteria.add(Restrictions.eq("idCargo", cargo.getId()));
		criteria.add(RestrictionAtividadeAtiva());
		criteria.addOrder(Order.asc("dataInicio"));
		return criteria.list();
	}
	
	
	public Atividade buscarAtivaPorEmpregado(Empregado empregado) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Atividade.class);
		criteria.createAlias("cargo", "cargo", JoinType.INNER_JOIN);
		criteria.createAlias("cargo.setor", "setor", JoinType.INNER_JOIN);
		criteria.createAlias("empregado", "empregado", JoinType.INNER_JOIN);
		
		criteria.add(Restrictions.eq("idEmpregado", empregado.getId()));
		criteria.add(RestrictionAtividadeAtiva());
		criteria.addOrder(Order.asc("dataInicio"));
		criteria.setMaxResults(1);
		return (Atividade) criteria.uniqueResult();
	}

	private Criterion RestrictionAtividadeAtiva() {
		Date dataAtual = new Date();
		return Restrictions.and(
			Restrictions.and(
					Restrictions.le("dataInicio", dataAtual), 
					Restrictions.or(Restrictions.isNull("dataFim"), Restrictions.gt("dataFim", dataAtual))
			), 
			Restrictions.or(Restrictions.isNull("excluido"), Restrictions.eq("excluido", '0'))
		);
	}
}
