package br.com.dh.dexSystem.manager;


import java.util.List;

import br.com.dh.dexSystem.model.vo.ColunaVO;

public interface ApiManager {

	public List<Object> buscarDados(Long idDispositivo, String tabela, List<ColunaVO> colunas, Class classe);
}
