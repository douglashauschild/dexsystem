package br.com.dh.dexSystem.manager;


import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.dh.dexSystem.model.Epi;
import br.com.dh.dexSystem.repository.filter.EpiFilter;

public interface EpiManager {

	public Page<Epi> filtrar(EpiFilter filtro, Pageable pageable) throws Exception;
	public Epi buscarPorId(Long id);
	public List<Epi> buscarTodos();	
}
