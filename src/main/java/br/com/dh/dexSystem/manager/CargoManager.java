package br.com.dh.dexSystem.manager;


import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.dh.dexSystem.model.Cargo;
import br.com.dh.dexSystem.repository.filter.CargoFilter;

public interface CargoManager {

	public Page<Cargo> filtrar(CargoFilter filtro, Pageable pageable) throws Exception;
	public Cargo buscarPorId(Long id);
	public List<Cargo> buscarPorCargo(Cargo cargo);
	public List<Cargo> buscarTodosPorSetor(Long idSetor, boolean inclusiveInativos);
	public List<Cargo> buscarAtivosPorSetorComDataInicioMenor(Long idSetor, Date dataInicioSetor);
}
