package br.com.dh.dexSystem.manager;


import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import br.com.dh.dexSystem.model.Setor;
import br.com.dh.dexSystem.model.constant.StatusSetorCargoEnum;
import br.com.dh.dexSystem.repository.filter.SetorFilter;
import br.com.dh.dexSystem.repository.paginacao.PaginacaoUtil;

@Service("setorManager")
@Transactional
public class SetorManagerImpl implements SetorManager {

	@PersistenceContext
	private EntityManager manager;
	
	@Autowired
	private PaginacaoUtil paginacaoUtil;
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public Page<Setor> filtrar(SetorFilter filtro, Pageable pageable) throws Exception {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Setor.class);
		
		paginacaoUtil.preparar(criteria, pageable);
		adicionarFiltro(filtro, criteria);
		
		return new PageImpl<>(criteria.list(), pageable, total(filtro));
	}
	
	
	private Long total(SetorFilter filtro) throws Exception {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Setor.class);
		adicionarFiltro(filtro, criteria);
		criteria.setProjection(Projections.rowCount());
		return (Long) criteria.uniqueResult();
	}

	
	private void adicionarFiltro(SetorFilter filtro, Criteria criteria) throws Exception {
		criteria.createAlias("empresa", "empresa", JoinType.INNER_JOIN);
		
		criteria.add(Restrictions.or(Restrictions.isNull("excluido"), Restrictions.eq("excluido", '0')));
		
		if (filtro != null) {
			if (filtro.getStatus() != null) {
				if (filtro.getStatus().equals(StatusSetorCargoEnum.ATIVO.getKey())) {
					criteria.add(RestrictionSetorAtivo());
				} else  {
					criteria.add(Restrictions.not(RestrictionSetorAtivo()));
				}
			}
			if (filtro.getIdEmpresa() != null) {
				criteria.add(Restrictions.eq("idEmpresa", filtro.getIdEmpresa()));
			}
			Criterion critPesquisa = null;
			if (!StringUtils.isEmpty(filtro.getPesquisa())) {
				if (filtro.getPesquisa().matches("^[0-9]{1,45}$")) {
					critPesquisa = Restrictions.eq("id", Long.parseLong(filtro.getPesquisa()));
				}
				if (critPesquisa == null) {
					critPesquisa = Restrictions.ilike("nome", filtro.getPesquisa(), MatchMode.ANYWHERE);
                } else {
                    critPesquisa = Restrictions.or(critPesquisa, Restrictions.ilike("nome", filtro.getPesquisa(), MatchMode.ANYWHERE));
                }
			}
			if (critPesquisa != null) {
				criteria.add(critPesquisa);
            }
		}
	}
	
	
	public Setor buscarPorId(Long id) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Setor.class);
		criteria.createAlias("empresa", "empresa", JoinType.INNER_JOIN);
		
		criteria.add(Restrictions.eq("id", id));
		criteria.add(Restrictions.or(Restrictions.isNull("excluido"), Restrictions.eq("excluido", '0')));
		criteria.setMaxResults(1);
		return (Setor) criteria.uniqueResult();
	}
	
	
	public List<Setor> buscarPorSetor(Setor setor) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Setor.class);
		criteria.createAlias("empresa", "empresa", JoinType.INNER_JOIN);
		
		criteria.add(Restrictions.eq("idEmpresa", setor.getIdEmpresa()));
		criteria.add(Restrictions.eq("nome", setor.getNome()));
		criteria.add(Restrictions.or(Restrictions.isNull("excluido"), Restrictions.eq("excluido", '0')));
		criteria.addOrder(Order.asc("dataInicio"));
		return criteria.list();
	}
	
	
	public List<Setor> buscarTodosPorEmpresa(Long idEmpresa, boolean inclusiveInativos) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Setor.class);
		criteria.createAlias("empresa", "empresa", JoinType.INNER_JOIN);
		
		criteria.add(Restrictions.eq("idEmpresa", idEmpresa));
		if (!inclusiveInativos) {
			criteria.add(RestrictionSetorAtivo());
		}
		criteria.addOrder(Order.asc("nome"));
		return criteria.list();
	}
	
	
	public List<Setor> buscarAtivosPorEmpresaComDataInicioMenor(Long idEmpresa, Date dataInicioEmpresa) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Setor.class);
		criteria.createAlias("empresa", "empresa", JoinType.INNER_JOIN);
		
		criteria.add(Restrictions.eq("idEmpresa", idEmpresa));
		criteria.add(Restrictions.lt("dataInicio", dataInicioEmpresa));
		criteria.add(RestrictionSetorAtivo());
		criteria.addOrder(Order.asc("nome"));
		return criteria.list();
	}
	

	private Criterion RestrictionSetorAtivo() {
		Date dataAtual = new Date();
		return Restrictions.and(
			Restrictions.and(
					Restrictions.le("dataInicio", dataAtual), 
					Restrictions.or(Restrictions.isNull("dataFim"), Restrictions.gt("dataFim", dataAtual))
			), 
			Restrictions.or(Restrictions.isNull("excluido"), Restrictions.eq("excluido", '0'))
		);
	}
}
