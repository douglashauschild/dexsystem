package br.com.dh.dexSystem.manager;


import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.dh.dexSystem.model.Atividade;
import br.com.dh.dexSystem.model.Cargo;
import br.com.dh.dexSystem.model.Empregado;
import br.com.dh.dexSystem.repository.filter.AtividadeFilter;

public interface AtividadeManager {

	public Page<Atividade> filtrar(AtividadeFilter filtro, Pageable pageable) throws Exception;
	public Atividade buscarPorId(Long id);
	public List<Atividade> buscarParaValidacao(Atividade atividade);
	public List<Atividade> buscarAtivasPorCargo(Cargo cargo);
	public Atividade buscarAtivaPorEmpregado(Empregado empregado);
}
