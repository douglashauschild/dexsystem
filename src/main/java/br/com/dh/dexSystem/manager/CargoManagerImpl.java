package br.com.dh.dexSystem.manager;


import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import br.com.dh.dexSystem.model.Cargo;
import br.com.dh.dexSystem.model.constant.StatusSetorCargoEnum;
import br.com.dh.dexSystem.repository.filter.CargoFilter;
import br.com.dh.dexSystem.repository.paginacao.PaginacaoUtil;

@Service("cargoManager")
@Transactional
public class CargoManagerImpl implements CargoManager {

	@PersistenceContext
	private EntityManager manager;
	
	@Autowired
	private PaginacaoUtil paginacaoUtil;
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public Page<Cargo> filtrar(CargoFilter filtro, Pageable pageable) throws Exception {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Cargo.class);
		
		paginacaoUtil.preparar(criteria, pageable);
		adicionarFiltro(filtro, criteria);
		
		return new PageImpl<>(criteria.list(), pageable, total(filtro));
	}
	
	
	private Long total(CargoFilter filtro) throws Exception {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Cargo.class);
		adicionarFiltro(filtro, criteria);
		criteria.setProjection(Projections.rowCount());
		return (Long) criteria.uniqueResult();
	}

	
	private void adicionarFiltro(CargoFilter filtro, Criteria criteria) throws Exception {
		criteria.createAlias("setor", "setor", JoinType.INNER_JOIN);
		
		criteria.add(Restrictions.or(Restrictions.isNull("excluido"), Restrictions.eq("excluido", '0')));
		
		if (filtro != null) {
			if (filtro.getStatus() != null) {
				if (filtro.getStatus().equals(StatusSetorCargoEnum.ATIVO.getKey())) {
					criteria.add(RestrictionCargoAtivo());
				} else  {
					criteria.add(Restrictions.not(RestrictionCargoAtivo()));
				}
			}
			if (filtro.getIdEmpresa() != null && filtro.getIdEmpresa() != 0L) {
				criteria.add(Restrictions.eq("setor.idEmpresa", filtro.getIdEmpresa()));
			}
			if (filtro.getIdSetor() != null && filtro.getIdSetor() != 0L) {
				criteria.add(Restrictions.eq("idSetor", filtro.getIdSetor()));
			}
			Criterion critPesquisa = null;
			if (!StringUtils.isEmpty(filtro.getPesquisa())) {
				if (filtro.getPesquisa().matches("^[0-9]{1,45}$")) {
					critPesquisa = Restrictions.eq("id", Long.parseLong(filtro.getPesquisa()));
				}
				if (critPesquisa == null) {
					critPesquisa = Restrictions.ilike("nome", filtro.getPesquisa(), MatchMode.ANYWHERE);
                } else {
                    critPesquisa = Restrictions.or(critPesquisa, Restrictions.ilike("nome", filtro.getPesquisa(), MatchMode.ANYWHERE));
                }
			}
			if (critPesquisa != null) {
				criteria.add(critPesquisa);
            }
		}
	}
	
	
	public Cargo buscarPorId(Long id) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Cargo.class);
		criteria.createAlias("setor", "setor", JoinType.INNER_JOIN);
		
		criteria.add(Restrictions.eq("id", id));
		criteria.add(Restrictions.or(Restrictions.isNull("excluido"), Restrictions.eq("excluido", '0')));
		criteria.setMaxResults(1);
		return (Cargo) criteria.uniqueResult();
	}
	
	
	public List<Cargo> buscarPorCargo(Cargo cargo) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Cargo.class);
		criteria.createAlias("setor", "setor", JoinType.INNER_JOIN);
		
		criteria.add(Restrictions.eq("idSetor", cargo.getIdSetor()));
		criteria.add(Restrictions.eq("nome", cargo.getNome()));
		criteria.add(Restrictions.or(Restrictions.isNull("excluido"), Restrictions.eq("excluido", '0')));
		criteria.addOrder(Order.asc("dataInicio"));
		return criteria.list();
	}

	
	public List<Cargo> buscarTodosPorSetor(Long idSetor, boolean inclusiveInativos) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Cargo.class);
		criteria.createAlias("setor", "setor", JoinType.INNER_JOIN);
		criteria.createAlias("setor.empresa", "empresa", JoinType.INNER_JOIN);
		
		criteria.add(Restrictions.eq("idSetor", idSetor));
		if (!inclusiveInativos) {
			criteria.add(RestrictionCargoAtivo());
		}
		criteria.addOrder(Order.asc("nome"));
		return criteria.list();
	}
	
	public List<Cargo> buscarAtivosPorSetorComDataInicioMenor(Long idSetor, Date dataInicioSetor) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(Cargo.class);
		criteria.createAlias("setor", "setor", JoinType.INNER_JOIN);
		criteria.createAlias("setor.empresa", "empresa", JoinType.INNER_JOIN);
		
		criteria.add(Restrictions.eq("idSetor", idSetor));
		criteria.add(Restrictions.lt("dataInicio", dataInicioSetor));
		criteria.add(RestrictionCargoAtivo());
		criteria.addOrder(Order.asc("nome"));
		return criteria.list();
	}

	private Criterion RestrictionCargoAtivo() {
		Date dataAtual = new Date();
		return Restrictions.and(
			Restrictions.and(
					Restrictions.le("dataInicio", dataAtual), 
					Restrictions.or(Restrictions.isNull("dataFim"), Restrictions.gt("dataFim", dataAtual))
			), 
			Restrictions.or(Restrictions.isNull("excluido"), Restrictions.eq("excluido", '0'))
		);
	}
}
