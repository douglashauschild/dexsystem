package br.com.dh.dexSystem.manager;


import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dh.dexSystem.model.vo.ColunaVO;

@Service("apiManager")
@Transactional
public class ApiManagerImpl implements ApiManager {

	@PersistenceContext
	private EntityManager manager;

	
	public List<Object> buscarDados(Long idDispositivo, String tabela, List<ColunaVO> colunas, Class classe) {
		String colunasSql = "";
		for (ColunaVO colunaVO : colunas) {
			if (!colunasSql.isEmpty()) {
				colunasSql += ", ";
			}
			colunasSql += colunaVO.getColunas();
		}
		SQLQuery sqlQuery = manager.unwrap(Session.class).createSQLQuery("select "+colunasSql+" from "+tabela+" where (checksum not in (select checksum from controle_sincronizacao where tabela = '"+tabela+"' and dispositivo_id = "+idDispositivo+") or checksum in (select checksum from controle_sincronizacao where tabela = '"+tabela+"' and dispositivo_id = "+idDispositivo+" and checksum_alteracao != "+tabela+".checksum_alteracao))");
		
		for (ColunaVO colunaVO : colunas) {
			sqlQuery.addScalar(colunaVO.getColuna(), colunaVO.getTipo());
		}
		sqlQuery.setResultTransformer(Transformers.aliasToBean(classe));
		return sqlQuery.list();
	}
}
