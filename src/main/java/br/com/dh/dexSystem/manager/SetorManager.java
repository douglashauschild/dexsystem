package br.com.dh.dexSystem.manager;


import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.dh.dexSystem.model.Setor;
import br.com.dh.dexSystem.repository.filter.SetorFilter;

public interface SetorManager {

	public Page<Setor> filtrar(SetorFilter filtro, Pageable pageable) throws Exception;
	public Setor buscarPorId(Long id);
	public List<Setor> buscarPorSetor(Setor setor);
	public List<Setor> buscarTodosPorEmpresa(Long idEmpresa, boolean inclusiveInativos);
	public List<Setor> buscarAtivosPorEmpresaComDataInicioMenor(Long idEmpresa, Date dataInicioEmpresa);
}
