package br.com.dh.dexSystem.manager;


import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import br.com.dh.dexSystem.model.Cargo;
import br.com.dh.dexSystem.model.CargoEpi;
import br.com.dh.dexSystem.repository.filter.CargoEpiFilter;
import br.com.dh.dexSystem.repository.paginacao.PaginacaoUtil;

@Service("cargoEpiManager")
@Transactional
public class CargoEpiManagerImpl implements CargoEpiManager {

	@PersistenceContext
	private EntityManager manager;
	
	@Autowired
	private PaginacaoUtil paginacaoUtil;
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public Page<CargoEpi> filtrar(CargoEpiFilter filtro, Pageable pageable) throws Exception {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(CargoEpi.class);
		
		paginacaoUtil.preparar(criteria, pageable);
		adicionarFiltro(filtro, criteria);
		
		return new PageImpl<>(criteria.list(), pageable, total(filtro));
	}
	
	
	private Long total(CargoEpiFilter filtro) throws Exception {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(CargoEpi.class);
		adicionarFiltro(filtro, criteria);
		criteria.setProjection(Projections.rowCount());
		return (Long) criteria.uniqueResult();
	}

	
	private void adicionarFiltro(CargoEpiFilter filtro, Criteria criteria) throws Exception {
		criteria.createAlias("cargo", "cargo", JoinType.INNER_JOIN);
		criteria.createAlias("cargo.setor", "setor", JoinType.INNER_JOIN);
		criteria.createAlias("epi", "epi", JoinType.INNER_JOIN);
		
		criteria.add(Restrictions.or(Restrictions.isNull("excluido"), Restrictions.eq("excluido", '0')));
		
		if (filtro != null) {
			if (filtro.getIdEmpresa() != null && filtro.getIdEmpresa() != 0L) {
				criteria.add(Restrictions.eq("setor.idEmpresa", filtro.getIdEmpresa()));
			}
			if (filtro.getIdSetor() != null && filtro.getIdSetor() != 0L) {
				criteria.add(Restrictions.eq("cargo.idSetor", filtro.getIdSetor()));
			}
			if (filtro.getIdCargo() != null && filtro.getIdCargo() != 0L) {
				criteria.add(Restrictions.eq("idCargo", filtro.getIdCargo()));
			}
			Criterion critPesquisa = null;
			if (!StringUtils.isEmpty(filtro.getPesquisa())) {
				if (filtro.getPesquisa().matches("^[0-9]{1,45}$")) {
					critPesquisa = Restrictions.eq("id", Long.parseLong(filtro.getPesquisa()));
					
					if (critPesquisa == null) {
						critPesquisa = Restrictions.eq("quantidade", Integer.parseInt(filtro.getPesquisa()));
		            } else {
		                critPesquisa = Restrictions.or(critPesquisa, Restrictions.eq("quantidade", Integer.parseInt(filtro.getPesquisa())));
		            }
				}
				if (critPesquisa == null) {
					critPesquisa = Restrictions.ilike("cargo.nome", filtro.getPesquisa(), MatchMode.ANYWHERE);
	            } else {
	                critPesquisa = Restrictions.or(critPesquisa, Restrictions.ilike("cargo.nome", filtro.getPesquisa(), MatchMode.ANYWHERE));
	            }
				if (critPesquisa == null) {
					critPesquisa = Restrictions.ilike("epi.nome", filtro.getPesquisa(), MatchMode.ANYWHERE);
	            } else {
	                critPesquisa = Restrictions.or(critPesquisa, Restrictions.ilike("epi.nome", filtro.getPesquisa(), MatchMode.ANYWHERE));
	            }
				if (critPesquisa == null) {
					critPesquisa = Restrictions.ilike("epi.ca", filtro.getPesquisa(), MatchMode.ANYWHERE);
	            } else {
	                critPesquisa = Restrictions.or(critPesquisa, Restrictions.ilike("epi.ca", filtro.getPesquisa(), MatchMode.ANYWHERE));
	            }
			}
			if (critPesquisa != null) {
				criteria.add(critPesquisa);
            }
		}
	}
	
	
	public CargoEpi buscarPorId(Long id) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(CargoEpi.class);
		criteria.createAlias("cargo", "cargo", JoinType.INNER_JOIN);
		
		criteria.add(Restrictions.eq("id", id));
		criteria.add(Restrictions.or(Restrictions.isNull("excluido"), Restrictions.eq("excluido", '0')));
		criteria.setMaxResults(1);
		return (CargoEpi) criteria.uniqueResult();
	}
	
	
	public List<CargoEpi> buscarPorCargoEpi(CargoEpi cargoEpi) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(CargoEpi.class);
		criteria.createAlias("cargo", "cargo", JoinType.INNER_JOIN);
		
		criteria.add(Restrictions.eq("idCargo", cargoEpi.getIdCargo()));
		criteria.add(Restrictions.eq("idEpi", cargoEpi.getIdEpi()));
		criteria.add(Restrictions.or(Restrictions.isNull("excluido"), Restrictions.eq("excluido", '0')));
		criteria.addOrder(Order.asc("dataInicio"));
		return criteria.list();
	}
	
	
	public List<CargoEpi> buscarAtivosPorCargo(Cargo cargo) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(CargoEpi.class);
		criteria.createAlias("cargo", "cargo", JoinType.INNER_JOIN);
		
		criteria.add(Restrictions.eq("idCargo", cargo.getId()));
		criteria.add(RestrictionCargoEpiAtivo());
		criteria.addOrder(Order.asc("dataInicio"));
		return criteria.list();
	}


	public List<CargoEpi> buscarAtivosPorCargoComDataInicioMenor(Cargo cargo, Date dataInicioCargo) {
		Criteria criteria = manager.unwrap(Session.class).createCriteria(CargoEpi.class);
		criteria.createAlias("cargo", "cargo", JoinType.INNER_JOIN);
		
		criteria.add(Restrictions.eq("idCargo", cargo.getId()));
		criteria.add(Restrictions.lt("dataInicio", dataInicioCargo));
		criteria.add(RestrictionCargoEpiAtivo());
		criteria.addOrder(Order.asc("dataInicio"));
		return criteria.list();
	}
		
	
	private Criterion RestrictionCargoEpiAtivo() {
		Date dataAtual = new Date();
		return Restrictions.and(
			Restrictions.and(
					Restrictions.le("dataInicio", dataAtual), 
					Restrictions.or(Restrictions.isNull("dataFim"), Restrictions.gt("dataFim", dataAtual))
			), 
			Restrictions.or(Restrictions.isNull("excluido"), Restrictions.eq("excluido", '0'))
		);
	}
}
