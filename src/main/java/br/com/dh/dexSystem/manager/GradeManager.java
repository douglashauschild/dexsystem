package br.com.dh.dexSystem.manager;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.dh.dexSystem.model.Grade;
import br.com.dh.dexSystem.repository.filter.GradeFilter;

public interface GradeManager {

	public Page<Grade> filtrar(GradeFilter filtro, Pageable pageable);
	
}
