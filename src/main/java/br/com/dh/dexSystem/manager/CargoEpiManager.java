package br.com.dh.dexSystem.manager;


import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.dh.dexSystem.model.Cargo;
import br.com.dh.dexSystem.model.CargoEpi;
import br.com.dh.dexSystem.repository.filter.CargoEpiFilter;

public interface CargoEpiManager {

	public Page<CargoEpi> filtrar(CargoEpiFilter filtro, Pageable pageable) throws Exception;
	public CargoEpi buscarPorId(Long id);
	public List<CargoEpi> buscarPorCargoEpi(CargoEpi cargoEpi);
	public List<CargoEpi> buscarAtivosPorCargo(Cargo cargo);
	public List<CargoEpi> buscarAtivosPorCargoComDataInicioMenor(Cargo cargo, Date dataInicioCargo);
}
