package br.com.dh.dexSystem.manager;


import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.dh.dexSystem.model.Empregado;
import br.com.dh.dexSystem.repository.filter.EmpregadoFilter;

public interface EmpregadoManager {

	public Page<Empregado> filtrar(EmpregadoFilter filtro, Pageable pageable) throws Exception;
	public Empregado buscarPorId(Long id);
	public List<Empregado> buscarTodosAtivos();
	public List<Empregado> buscarAtivosPorEmpresa(Long idEmpresa);
	public List<Empregado> buscarParaValidacao(Empregado empregado);
	public List<Empregado> buscarEmpregadoEmOutrasEmpresas(Empregado empregado);
}
