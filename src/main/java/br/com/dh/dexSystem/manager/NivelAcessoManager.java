package br.com.dh.dexSystem.manager;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.dh.dexSystem.model.NivelAcesso;
import br.com.dh.dexSystem.repository.filter.NivelAcessoFilter;

public interface NivelAcessoManager {

	public Page<NivelAcesso> filtrar(NivelAcessoFilter filtro, Pageable pageable);
	
}
