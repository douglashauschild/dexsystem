package br.com.dh.dexSystem.manager;


import br.com.dh.dexSystem.model.Cidade;

public interface CidadeManager {

	public Cidade buscarCidade(String uf, String nome);
}
