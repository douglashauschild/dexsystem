package br.com.dh.dexSystem.model.vo;

import java.util.List;

public class RelatorioConteudoVO {

	private List<RelatorioRegistroVO> conteudo;

	
	public List<RelatorioRegistroVO> getConteudo() {
		return conteudo;
	}

	public void setConteudo(List<RelatorioRegistroVO> conteudo) {
		this.conteudo = conteudo;
	}	
}
