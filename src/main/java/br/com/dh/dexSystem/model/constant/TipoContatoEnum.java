package br.com.dh.dexSystem.model.constant;


public enum TipoContatoEnum {
	TELEFONE(1L, "Telefone"),
	EMAIL(2L, "E-mail"),
	OUTRO(3L, "Outro");

	private Long key;
	private String descricao;

	private TipoContatoEnum (Long key, String descricao) {
		this.key = key;
		this.descricao = descricao;
	}
	
	public Long getKey() {
		return key;
	}

	public void setKey(Long key) {
		this.key = key;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}


	public static TipoContatoEnum getByKey(Long key) throws EnumConstantNotPresentException {
		for (TipoContatoEnum permissao : TipoContatoEnum.values()) {
			if (permissao.getKey().equals(key)) {
				return permissao;
			}
		}
		throw new EnumConstantNotPresentException(TipoContatoEnum.class, "Chave " + key + " não encontrada");
	}
	
	public static TipoContatoEnum getByDescricao(String descricao) throws EnumConstantNotPresentException {
		for (TipoContatoEnum permissao : TipoContatoEnum.values()) {
			if (permissao.getDescricao().equals(descricao)) {
				return permissao;
			}
		}
		throw new EnumConstantNotPresentException(TipoContatoEnum.class, "Tipo de empresa " + descricao + " não encontrada");
	}
}