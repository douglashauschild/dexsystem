package br.com.dh.dexSystem.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "nivel_acesso_permissao")	
public class NivelAcessoPermissao implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private NivelAcessoPermissaoPK id;
	
	@Column(name = "acessar")
	private Character acessar;
	
	@Column(name = "inserir")
	private Character inserir;
	
	@Column(name = "alterar")
	private Character alterar;
	
	@Column(name = "excluir")
	private Character excluir;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "nivel_acesso_id", insertable = false, updatable = false)
	private NivelAcesso nivelAcesso;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "menu_id", insertable = false, updatable = false)
	private Menu menu;

	public boolean isAcessar() {
		return acessar != null && acessar.equals('1');
	}
	
	public boolean isInserir() {
		return inserir != null && inserir.equals('1');
	}
	
	public boolean isAlterar() {
		return alterar != null && alterar.equals('1');
	}
	
	public boolean isExcluir() {
		return excluir != null && excluir.equals('1');
	}

	public NivelAcessoPermissaoPK getId() {
		return id;
	}

	public void setId(NivelAcessoPermissaoPK id) {
		this.id = id;
	}

	public Character getAcessar() {
		return acessar;
	}

	public void setAcessar(Character acessar) {
		this.acessar = acessar;
	}

	public Character getInserir() {
		return inserir;
	}

	public void setInserir(Character inserir) {
		this.inserir = inserir;
	}

	public Character getAlterar() {
		return alterar;
	}

	public void setAlterar(Character alterar) {
		this.alterar = alterar;
	}

	public Character getExcluir() {
		return excluir;
	}

	public void setExcluir(Character excluir) {
		this.excluir = excluir;
	}

	public NivelAcesso getNivelAcesso() {
		return nivelAcesso;
	}

	public void setNivelAcesso(NivelAcesso nivelAcesso) {
		this.nivelAcesso = nivelAcesso;
	}

	public Menu getMenu() {
		return menu;
	}

	public void setMenu(Menu menu) {
		this.menu = menu;
	}
}