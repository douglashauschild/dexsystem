package br.com.dh.dexSystem.model.constant;


public enum PermissoesEnum {
	ACESSAR("ACESSAR", "Acessar"),
	INSERIR("INSERIR", "Inserir"),
	ALTERAR("ALTERAR", "Alterar"),
	EXCLUIR("EXCLUIR", "Excluir");
	
	private String key;
	private String descricao;

	private PermissoesEnum (String key, String descricao) {
		this.key = key;
		this.descricao = descricao;
	}
	
	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}


	public static PermissoesEnum getByKey(String key) throws EnumConstantNotPresentException {
		for (PermissoesEnum permissao : PermissoesEnum.values()) {
			if (permissao.getKey().equals(key)) {
				return permissao;
			}
		}
		throw new EnumConstantNotPresentException(PermissoesEnum.class, "Chave " + key + " não encontrada");
	}
	
	public static PermissoesEnum getByDescricao(String descricao) throws EnumConstantNotPresentException {
		for (PermissoesEnum permissao : PermissoesEnum.values()) {
			if (permissao.getDescricao().equals(descricao)) {
				return permissao;
			}
		}
		throw new EnumConstantNotPresentException(PermissoesEnum.class, "Permissão " + descricao + " não encontrada");
	}
}