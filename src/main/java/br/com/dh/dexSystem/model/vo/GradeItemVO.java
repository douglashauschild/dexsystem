package br.com.dh.dexSystem.model.vo;

import java.io.Serializable;

import br.com.dh.dexSystem.model.Grade;
import br.com.dh.dexSystem.model.constant.DelimitadorEnum;

public class GradeItemVO implements Serializable {

	private static final long serialVersionUID = 4381802856908718873L;

	private String tamanho;
	private Integer ordem;
	private Grade grade;
	private String itemExclusao;
	
	
	public GradeItemVO() {
		super();
	}

	public GradeItemVO(String tamanho, Integer ordem) {
		super();
		this.tamanho = tamanho;
		this.ordem = ordem;
	}

	public String getTamanho() {
		return tamanho;
	}
	
	public void setTamanho(String tamanho) {
		this.tamanho = tamanho;
	}
	
	public Integer getOrdem() {
		return ordem;
	}

	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}
	
	public Grade getGrade() {
		return grade;
	}

	public void setGrade(Grade grade) {
		this.grade = grade;
	}

	public String getItemExclusao() {
		return itemExclusao;
	}

	public void setItemExclusao(String itemExclusao) {
		this.itemExclusao = itemExclusao;
	}

	public String toString(Character act) {
		return tamanho + DelimitadorEnum.INFORMACAO.getKey() + ordem + DelimitadorEnum.ACAO.getKey() + act + DelimitadorEnum.ITENS.getKey();
	}
}