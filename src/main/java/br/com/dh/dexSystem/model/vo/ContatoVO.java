package br.com.dh.dexSystem.model.vo;

import java.io.Serializable;

import br.com.dh.dexSystem.model.constant.DelimitadorEnum;
import br.com.dh.dexSystem.model.constant.TipoContatoEnum;

public class ContatoVO implements Serializable {

	private static final long serialVersionUID = -3044350100880206358L;
	
	private String contatosToString;
	private Long tipo;
	private String tipoOutro;
	private String contato;
	private String contatoExclusao;
	
	public ContatoVO() {
		super();
		this.contatosToString = "";
	}
	
	public ContatoVO(Long tipo, String tipoOutro, String contato) {
		super();
		this.tipo = tipo;
		this.tipoOutro = tipoOutro;
		this.contato = contato;
	}

	public Long getTipo() {
		return tipo;
	}

	public void setTipo(Long tipo) {
		this.tipo = tipo;
	}

	public String getTipoOutro() {
		return tipoOutro;
	}

	public void setTipoOutro(String tipoOutro) {
		this.tipoOutro = tipoOutro;
	}

	public String getContato() {
		return contato;
	}

	public void setContato(String contato) {
		this.contato = contato;
	}

	public String getContatoExclusao() {
		return contatoExclusao;
	}

	public void setContatoExclusao(String contatoExclusao) {
		this.contatoExclusao = contatoExclusao;
	}

	public String getContatosToString() {	
		return contatosToString;
	}

	public void setContatosToString(String contatosToString) {
		this.contatosToString = contatosToString;
	}

	public String toString(Character act) {
		return TipoContatoEnum.getByKey(tipo).getDescricao() + DelimitadorEnum.INFORMACAO.getKey() + (tipoOutro != null && !tipoOutro.isEmpty() ? tipoOutro : "NULL") + DelimitadorEnum.INFORMACAO.getKey() + contato + DelimitadorEnum.ACAO.getKey() + act + DelimitadorEnum.ITENS.getKey();
	}
}