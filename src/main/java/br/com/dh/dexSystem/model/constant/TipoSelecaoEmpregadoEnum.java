package br.com.dh.dexSystem.model.constant;


public enum TipoSelecaoEmpregadoEnum {
	NOME('S', "Nome"),
	MATRICULA_NOME('C', "Matrícula | Nome");
	
	private Character key;
	private String descricao;

	private TipoSelecaoEmpregadoEnum (Character key, String descricao) {
		this.key = key;
		this.descricao = descricao;
	}
	
	public Character getKey() {
		return key;
	}

	public void setKey(Character key) {
		this.key = key;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}


	public static TipoSelecaoEmpregadoEnum getByKey(Character key) throws EnumConstantNotPresentException {
		for (TipoSelecaoEmpregadoEnum tipo : TipoSelecaoEmpregadoEnum.values()) {
			if (tipo.getKey().equals(key)) {
				return tipo;
			}
		}
		throw new EnumConstantNotPresentException(TipoSelecaoEmpregadoEnum.class, "Chave " + key + " não encontrada");
	}
	
	public static TipoSelecaoEmpregadoEnum getByDescricao(String descricao) throws EnumConstantNotPresentException {
		for (TipoSelecaoEmpregadoEnum tipo : TipoSelecaoEmpregadoEnum.values()) {
			if (tipo.getDescricao().equals(descricao)) {
				return tipo;
			}
		}
		throw new EnumConstantNotPresentException(TipoSelecaoEmpregadoEnum.class, "Tipo " + descricao + " não encontrado");
	}
}