package br.com.dh.dexSystem.model.vo;

import java.util.List;

public class RelatorioVO {

	private String titulo;
	private float[] tamanhoColunas;
	private List<RelatorioColunaVO> colunas;
	private List<RelatorioConteudoVO> conteudos;
	private String usuario;
	
	public List<RelatorioColunaVO> getColunas() {
		return colunas;
	}

	public void setColunas(List<RelatorioColunaVO> colunas) {
		this.colunas = colunas;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public List<RelatorioConteudoVO> getConteudos() {
		return conteudos;
	}

	public void setConteudos(List<RelatorioConteudoVO> conteudos) {
		this.conteudos = conteudos;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public float[] getTamanhoColunas() {
		return tamanhoColunas;
	}

	public void setTamanhoColunas(float[] tamanhoColunas) {
		this.tamanhoColunas = tamanhoColunas;
	}
}