package br.com.dh.dexSystem.model.constant;


public enum DelimitadorEnum {
	INFORMACAO('♦'),
	ACAO('►'),
	ITENS('↔'),
	ITENS_ALTERACAO('→');
	
	private Character key;

	private DelimitadorEnum (Character key) {
		this.key = key;
	}
	
	public Character getKey() {
		return key;
	}

	public void setKey(Character key) {
		this.key = key;
	}

	public static DelimitadorEnum getByKey(Character key) throws EnumConstantNotPresentException {
		for (DelimitadorEnum delimitador : DelimitadorEnum.values()) {
			if (delimitador.getKey().equals(key)) {
				return delimitador;
			}
		}
		throw new EnumConstantNotPresentException(DelimitadorEnum.class, "Chave " + key + " não encontrada");
	}
}