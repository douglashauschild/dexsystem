package br.com.dh.dexSystem.model.constant;


public enum StatusEmpresaEnum {
	INATIVA(0L, "Inativa", "uk-badge-danger"),
	ATIVA(1L, "Ativa", "uk-badge-success");

	
	private Long key;
	private String descricao;
	private String cor;

	private StatusEmpresaEnum (Long key, String descricao, String cor) {
		this.key = key;
		this.descricao = descricao;
		this.cor = cor;
	}
	
	public Long getKey() {
		return key;
	}

	public void setKey(Long key) {
		this.key = key;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

	public static StatusEmpresaEnum getByKey(Long key) throws EnumConstantNotPresentException {
		for (StatusEmpresaEnum permissao : StatusEmpresaEnum.values()) {
			if (permissao.getKey().equals(key)) {
				return permissao;
			}
		}
		throw new EnumConstantNotPresentException(StatusEmpresaEnum.class, "Chave " + key + " não encontrada");
	}
	
	public static StatusEmpresaEnum getByDescricao(String descricao) throws EnumConstantNotPresentException {
		for (StatusEmpresaEnum permissao : StatusEmpresaEnum.values()) {
			if (permissao.getDescricao().equals(descricao)) {
				return permissao;
			}
		}
		throw new EnumConstantNotPresentException(StatusEmpresaEnum.class, "Permissão " + descricao + " não encontrada");
	}
}