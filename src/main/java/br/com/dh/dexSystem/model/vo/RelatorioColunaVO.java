package br.com.dh.dexSystem.model.vo;

public class RelatorioColunaVO {

	private String nome;
	private int alinhamento;
	
	public RelatorioColunaVO(String nome) {
		super();
		this.nome = nome;
	}
	
	public RelatorioColunaVO(String nome, int alinhamento) {
		super();
		this.nome = nome;
		this.alinhamento = alinhamento;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getAlinhamento() {
		return alinhamento;
	}
	
	public void setAlinhamento(int alinhamento) {
		this.alinhamento = alinhamento;
	}
}
