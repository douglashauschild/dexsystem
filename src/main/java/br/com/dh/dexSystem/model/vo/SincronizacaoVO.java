package br.com.dh.dexSystem.model.vo;

import java.io.Serializable;

public class SincronizacaoVO implements Serializable {

	private static final long serialVersionUID = -7266855182903900628L;
	private Long idDispositivo;
	private String tabela;
	private String json;
	
	public SincronizacaoVO() {
		super();
	}
	
	public SincronizacaoVO(Long idDispositivo, String tabela) {
		super();
		this.idDispositivo = idDispositivo;
		this.tabela = tabela;
	}

	public SincronizacaoVO(Long idDispositivo, String tabela, String json) {
		super();
		this.idDispositivo = idDispositivo;
		this.tabela = tabela;
		this.json = json;
	}

	public Long getIdDispositivo() {
		return idDispositivo;
	}

	public void setIdDispositivo(Long idDispositivo) {
		this.idDispositivo = idDispositivo;
	}

	public String getTabela() {
		return tabela;
	}

	public void setTabela(String tabela) {
		this.tabela = tabela;
	}

	public String getJson() {
		return json;
	}

	public void setJson(String json) {
		this.json = json;
	}
}