package br.com.dh.dexSystem.model.vo;

public class RelatorioRegistroVO {

	private String registro;
	private int alinhamento;
	
	
	public RelatorioRegistroVO(String registro, int alinhamento) {
		super();
		this.registro = registro;
		this.alinhamento = alinhamento;
	}
	
	
	public String getRegistro() {
		return registro;
	}

	public void setRegistro(String registro) {
		this.registro = registro;
	}

	public int getAlinhamento() {
		return alinhamento;
	}
	
	public void setAlinhamento(int alinhamento) {
		this.alinhamento = alinhamento;
	}
}
