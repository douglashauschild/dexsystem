package br.com.dh.dexSystem.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable	
public class NivelAcessoPermissaoPK implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Column(name = "nivel_acesso_id")
	private Long idNivelAcesso;
	
	@Column(name = "menu_id")
	private Long idMenu;


	public NivelAcessoPermissaoPK() {
		super();
	}

	public NivelAcessoPermissaoPK(Long idNivelAcesso, Long idMenu) {
		super();
		this.idNivelAcesso = idNivelAcesso;
		this.idMenu = idMenu;
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idMenu == null) ? 0 : idMenu.hashCode());
		result = prime * result + ((idNivelAcesso == null) ? 0 : idNivelAcesso.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NivelAcessoPermissaoPK other = (NivelAcessoPermissaoPK) obj;
		if (idMenu == null) {
			if (other.idMenu != null)
				return false;
		} else if (!idMenu.equals(other.idMenu))
			return false;
		if (idNivelAcesso == null) {
			if (other.idNivelAcesso != null)
				return false;
		} else if (!idNivelAcesso.equals(other.idNivelAcesso))
			return false;
		return true;
	}

	public Long getIdNivelAcesso() {
		return idNivelAcesso;
	}

	public void setIdNivelAcesso(Long idNivelAcesso) {
		this.idNivelAcesso = idNivelAcesso;
	}

	public Long getIdMenu() {
		return idMenu;
	}

	public void setIdMenu(Long idMenu) {
		this.idMenu = idMenu;
	}
}