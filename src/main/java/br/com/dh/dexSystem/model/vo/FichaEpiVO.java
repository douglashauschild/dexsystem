package br.com.dh.dexSystem.model.vo;

import java.util.List;

import br.com.dh.dexSystem.model.EmpregadoEpi;

public class FichaEpiVO {

	private String usuario;
	private String empresa;
	private String empregado;
	private String cpf;
	private String rg;
	private String dataAdmissao;
	private String dataDemissao;
	private String matricula;
	private String setor;
	private String cargo;
	private List<EmpregadoEpi> epis;
	
	public String getEmpresa() {
		return empresa;
	}
	
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	
	public String getEmpregado() {
		return empregado;
	}
	
	public void setEmpregado(String empregado) {
		this.empregado = empregado;
	}
	
	public String getDataAdmissao() {
		return dataAdmissao;
	}

	public void setDataAdmissao(String dataAdmissao) {
		this.dataAdmissao = dataAdmissao;
	}

	public String getDataDemissao() {
		return dataDemissao;
	}

	public void setDataDemissao(String dataDemissao) {
		this.dataDemissao = dataDemissao;
	}

	public String getMatricula() {
		return matricula;
	}
	
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	
	public String getSetor() {
		return setor;
	}
	
	public void setSetor(String setor) {
		this.setor = setor;
	}
	
	public String getCargo() {
		return cargo;
	}
	
	public void setCargo(String cargo) {
		this.cargo = cargo;
	}
	
	public List<EmpregadoEpi> getEpis() {
		return epis;
	}

	public void setEpis(List<EmpregadoEpi> epis) {
		this.epis = epis;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}
}