package br.com.dh.dexSystem.model.constant;


public enum TipoSelecaoEmpresaEnum {
	RAZAO_SOCIAL('S', "Razão Social"),
	CNPJ_RAZAO_SOCIAL('C', "CNPJ | Razão Social");
	
	private Character key;
	private String descricao;

	private TipoSelecaoEmpresaEnum (Character key, String descricao) {
		this.key = key;
		this.descricao = descricao;
	}
	
	public Character getKey() {
		return key;
	}

	public void setKey(Character key) {
		this.key = key;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}


	public static TipoSelecaoEmpresaEnum getByKey(Character key) throws EnumConstantNotPresentException {
		for (TipoSelecaoEmpresaEnum tipo : TipoSelecaoEmpresaEnum.values()) {
			if (tipo.getKey().equals(key)) {
				return tipo;
			}
		}
		throw new EnumConstantNotPresentException(TipoSelecaoEmpresaEnum.class, "Chave " + key + " não encontrada");
	}
	
	public static TipoSelecaoEmpresaEnum getByDescricao(String descricao) throws EnumConstantNotPresentException {
		for (TipoSelecaoEmpresaEnum tipo : TipoSelecaoEmpresaEnum.values()) {
			if (tipo.getDescricao().equals(descricao)) {
				return tipo;
			}
		}
		throw new EnumConstantNotPresentException(TipoSelecaoEmpresaEnum.class, "Tipo " + descricao + " não encontrado");
	}
}