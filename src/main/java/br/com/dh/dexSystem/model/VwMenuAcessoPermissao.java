package br.com.dh.dexSystem.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "vw_menu_acesso_permissao")
public class VwMenuAcessoPermissao implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	private Long id;
	
	@Column(name = "nivel_acesso_id")
	private Long idNivelAcesso;
	
	@Column(name = "menu_id")
	private Long idMenu;
	
	@Column(name = "menu")
	private String menu;
	
	@Column(name = "menu_pai_id")
	private Long idMenuPai;
	
	@Column(name = "uri")
	private String uri;
	
	@Column(name = "acessar")
	private Character acessar;
	
	@Column(name = "inserir")
	private Character inserir;
	
	@Column(name = "alterar")
	private Character alterar;
	
	@Column(name = "excluir")
	private Character excluir;
	
	
	public VwMenuAcessoPermissao() {
		super();
	}

	public VwMenuAcessoPermissao(Long idMenu, String menu, Long idMenuPai, String uri, Character acessar, Character inserir, Character alterar, Character excluir) {
		super();
		this.idMenu = idMenu;
		this.menu = menu;
		this.idMenuPai = idMenuPai;
		this.uri = uri;
		this.acessar = acessar;
		this.inserir = inserir;
		this.alterar = alterar;
		this.excluir = excluir;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdNivelAcesso() {
		return idNivelAcesso;
	}

	public void setIdNivelAcesso(Long idNivelAcesso) {
		this.idNivelAcesso = idNivelAcesso;
	}

	public Long getIdMenu() {
		return idMenu;
	}

	public void setIdMenu(Long idMenu) {
		this.idMenu = idMenu;
	}

	public String getMenu() {
		return menu;
	}

	public void setMenu(String menu) {
		this.menu = menu;
	}

	public Character getAcessar() {
		return acessar;
	}

	public void setAcessar(Character acessar) {
		this.acessar = acessar;
	}

	public Character getInserir() {
		return inserir;
	}

	public void setInserir(Character inserir) {
		this.inserir = inserir;
	}

	public Character getAlterar() {
		return alterar;
	}

	public void setAlterar(Character alterar) {
		this.alterar = alterar;
	}

	public Character getExcluir() {
		return excluir;
	}

	public void setExcluir(Character excluir) {
		this.excluir = excluir;
	}

	public Long getIdMenuPai() {
		return idMenuPai;
	}

	public void setIdMenuPai(Long idMenuPai) {
		this.idMenuPai = idMenuPai;
	}

	public String getUri() {
		return uri != null && !uri.isEmpty() ? uri : null;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}
}