package br.com.dh.dexSystem.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "vw_menu_acesso")
public class VwMenuAcesso implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	private Long id;
	
	@Column(name = "menu")
	private String menu;
	
	@Column(name = "menu_id")
	private Long idMenuPai;
	
	@Column(name = "uri")
	private String uri;
	
			
	public VwMenuAcesso() {
		super();
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMenu() {
		return menu;
	}

	public void setMenu(String menu) {
		this.menu = menu;
	}

	public Long getIdMenuPai() {
		return idMenuPai;
	}

	public void setIdMenuPai(Long idMenuPai) {
		this.idMenuPai = idMenuPai;
	}


	public String getUri() {
		return uri;
	}


	public void setUri(String uri) {
		this.uri = uri;
	}
}