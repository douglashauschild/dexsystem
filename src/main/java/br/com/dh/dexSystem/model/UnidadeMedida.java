package br.com.dh.dexSystem.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "unidade_medida")	
public class UnidadeMedida extends SincronizacaoAbstract implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Sincronizar
	@Column(name = "id")
	private Long id;
	
	@Sincronizar
	@Column(name = "descricao")
	private String descricao;	
	
	@Sincronizar
	@Column(name = "excluido")
	private Character excluido;
	
	@Transient
	private boolean abrirModalConfirmacao;
	
	public UnidadeMedida() {
		super();
		this.excluido = '0';
	}

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public boolean isAbrirModalConfirmacao() {
		return abrirModalConfirmacao;
	}

	public void setAbrirModalConfirmacao(boolean abrirModalConfirmacao) {
		this.abrirModalConfirmacao = abrirModalConfirmacao;
	}

	public Character getExcluido() {
		return excluido;
	}

	public void setExcluido(Character excluido) {
		this.excluido = excluido;
	}
	
	@Override
	public String getHash(boolean inclusao) throws Exception {
		return gerarHash(inclusao, getChecksum(), getDescricao(), getExcluido());
	}
}