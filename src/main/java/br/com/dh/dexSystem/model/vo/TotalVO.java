package br.com.dh.dexSystem.model.vo;

public class TotalVO {

	private Integer empregados;
	private Integer epis;
	private Integer entregas;
	private Integer episEntregues;
	
	public TotalVO(Integer empregados, Integer epis, Integer entregas, Integer episEntregues) {
		super();
		this.empregados = empregados;
		this.epis = epis;
		this.entregas = entregas;
		this.episEntregues = episEntregues;
	}

	public Integer getEmpregados() {
		return empregados;
	}
	
	public void setEmpregados(Integer empregados) {
		this.empregados = empregados;
	}
	
	public Integer getEpis() {
		return epis;
	}
	
	public void setEpis(Integer epis) {
		this.epis = epis;
	}

	public Integer getEntregas() {
		return entregas;
	}

	public void setEntregas(Integer entregas) {
		this.entregas = entregas;
	}

	public Integer getEpisEntregues() {
		return episEntregues;
	}

	public void setEpisEntregues(Integer episEntregues) {
		this.episEntregues = episEntregues;
	}
}