package br.com.dh.dexSystem.model.vo;

public class EmpregadoVO {

	private String empresa;
	private String nome;
	private String idade;
	private String matricula;
	private String dataAdmissao;
	private String dataDemissao;
	private String setor;
	private String cargo;
	private String rg;
	private String cpf;
	
	public EmpregadoVO(String empresa, String nome, String idade, String matricula, String dataAdmissao, String dataDemissao, String cpf, String rg) {
		super();
		this.empresa = empresa;
		this.nome = nome;
		this.idade = idade;
		this.matricula = matricula;
		this.dataAdmissao = dataAdmissao;
		this.dataDemissao = dataDemissao;
		this.cpf = cpf;
		this.rg = rg;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getIdade() {
		return idade;
	}

	public void setIdade(String idade) {
		this.idade = idade;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getSetor() {
		return setor;
	}

	public void setSetor(String setor) {
		this.setor = setor;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getEmpresa() {
		return empresa;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	public String getDataAdmissao() {
		return dataAdmissao;
	}

	public void setDataAdmissao(String dataAdmissao) {
		this.dataAdmissao = dataAdmissao;
	}

	public String getDataDemissao() {
		return dataDemissao;
	}

	public void setDataDemissao(String dataDemissao) {
		this.dataDemissao = dataDemissao;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
}