package br.com.dh.dexSystem.model.vo;

public class EstatisticasEntregaVO {

	private int aguardandoEntrega;
	private int epiDia;
	private int epiVencido;
	private int entregasVencidas;
	
	public EstatisticasEntregaVO(int aguardandoEntrega, int epiDia, int epiVencido, int entregasVencidas) {
		super();
		this.aguardandoEntrega = aguardandoEntrega;
		this.epiDia = epiDia;
		this.epiVencido = epiVencido;
		this.entregasVencidas = entregasVencidas;
	}

	public int getAguardandoEntrega() {
		return aguardandoEntrega;
	}
	
	public void setAguardandoEntrega(int aguardandoEntrega) {
		this.aguardandoEntrega = aguardandoEntrega;
	}
	
	public int getEpiDia() {
		return epiDia;
	}
	
	public void setEpiDia(int epiDia) {
		this.epiDia = epiDia;
	}
	
	public int getEpiVencido() {
		return epiVencido;
	}

	public void setEpiVencido(int epiVencido) {
		this.epiVencido = epiVencido;
	}

	public int getEntregasVencidas() {
		return entregasVencidas;
	}

	public void setEntregasVencidas(int entregasVencidas) {
		this.entregasVencidas = entregasVencidas;
	}
}