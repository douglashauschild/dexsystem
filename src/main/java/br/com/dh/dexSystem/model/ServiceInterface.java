package br.com.dh.dexSystem.model;

import javax.servlet.http.HttpServletRequest;

import org.springframework.data.domain.Pageable;

import br.com.dh.dexSystem.controller.PageWrapper;

public interface ServiceInterface<T, F> {

	public PageWrapper<T> pesquisar(F filtro, Pageable pageable, HttpServletRequest httpServletRequest);
	public void salvar(T t) throws Exception;
	public void excluir(T t) throws Exception;
	public T buscarPorId(Long id);
}