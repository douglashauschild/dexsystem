package br.com.dh.dexSystem.model.constant;

public enum StatusEmpregadoEpiEnum {
	SUBSTITUIDO("EPI substituído", "uk-badge-info"),
	DEVOLVIDO("EPI devolvido", "uk-badge-warning"),
	VENCIDO("EPI vencido", "uk-badge-danger"),
	EM_DIA("EPI em dia", "uk-badge-success"),
	AGUARDANDO("Aguardando entrega", "uk-badge-primary"),
	ENTREGA_VENCIDA("Entrega atrasada", "uk-badge-danger");
	
	
	private StatusEmpregadoEpiEnum(String descricao, String cor) {
		this.descricao = descricao;
		this.cor = cor;
	}

	private String descricao;
	private String cor;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

	public static StatusEmpregadoEpiEnum getByDescricao(String descricao) throws EnumConstantNotPresentException {
		for (StatusEmpregadoEpiEnum status : StatusEmpregadoEpiEnum.values()) {
			if (status.getDescricao().equals(descricao)) {
				return status;
			}
		}
		throw new EnumConstantNotPresentException(StatusEmpregadoEpiEnum.class, "Chave " + descricao + " não encontrada");
	}
	
	public static StatusEmpregadoEpiEnum getByCor(String cor) throws EnumConstantNotPresentException {
		for (StatusEmpregadoEpiEnum status : StatusEmpregadoEpiEnum.values()) {
			if (status.getCor().equals(cor)) {
				return status;
			}
		}
		throw new EnumConstantNotPresentException(StatusEmpregadoEpiEnum.class, "Permissão " + cor + " não encontrada");
	}
}