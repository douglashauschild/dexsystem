package br.com.dh.dexSystem.model.vo;

public class SelectVO {

	private Long key;
	private String label;
	
	
	public SelectVO(Long key, String label) {
		super();
		this.key = key;
		this.label = label;
	}

	public Long getKey() {
		return key;
	}

	public void setKey(Long key) {
		this.key = key;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
}
