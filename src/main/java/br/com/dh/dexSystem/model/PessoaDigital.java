package br.com.dh.dexSystem.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "pessoa_digital")	
public class PessoaDigital extends SincronizacaoAbstract implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Sincronizar
	@Column(name = "id")
	private Long id;
	
	@Sincronizar
	@Column(name = "pessoa_id")
	private Long idPessoa;
	
	@Sincronizar
	@Column(name = "posicao")
	private String posicao;
	
	@Sincronizar
	@Column(name = "digital")
	private byte[] digital;
	
	@Sincronizar
	@Column(name = "anulado")
	private Character anulado;
	
	@Sincronizar
	@Column(name = "motivo_anulacao")
	private String motivoAnulacao;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "pessoa_id", insertable = false, updatable = false)
    private PessoaFisica pessoaFisica;
	
	
	public PessoaDigital() {
		super();
	}
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdPessoa() {
		return idPessoa;
	}

	public void setIdPessoa(Long idPessoa) {
		this.idPessoa = idPessoa;
	}

	public String getPosicao() {
		return posicao;
	}

	public void setPosicao(String posicao) {
		this.posicao = posicao;
	}

	public byte[] getDigital() {
		return digital;
	}

	public void setDigital(byte[] digital) {
		this.digital = digital;
	}

	public Character getAnulado() {
		return anulado;
	}

	public void setAnulado(Character anulado) {
		this.anulado = anulado;
	}

	public String getMotivoAnulacao() {
		return motivoAnulacao;
	}

	public void setMotivoAnulacao(String motivoAnulacao) {
		this.motivoAnulacao = motivoAnulacao;
	}

	public PessoaFisica getPessoaFisica() {
		return pessoaFisica;
	}

	public void setPessoaFisica(PessoaFisica pessoaFisica) {
		this.pessoaFisica = pessoaFisica;
	}
	
	@Override
	public String getHash(boolean inclusao) throws Exception {
		return gerarHash(inclusao, getChecksum(), getPosicao(), getDigital(), getAnulado(), getMotivoAnulacao());
	}
}