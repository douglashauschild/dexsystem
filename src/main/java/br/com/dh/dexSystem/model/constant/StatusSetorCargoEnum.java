package br.com.dh.dexSystem.model.constant;


public enum StatusSetorCargoEnum {
	INATIVO(0L, "Inativo", "uk-badge-danger"),
	ATIVO(1L, "Ativo", "uk-badge-success");

	
	private Long key;
	private String descricao;
	private String cor;

	private StatusSetorCargoEnum (Long key, String descricao, String cor) {
		this.key = key;
		this.descricao = descricao;
		this.cor = cor;
	}
	
	public Long getKey() {
		return key;
	}

	public void setKey(Long key) {
		this.key = key;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

	public static StatusSetorCargoEnum getByKey(Long key) throws EnumConstantNotPresentException {
		for (StatusSetorCargoEnum permissao : StatusSetorCargoEnum.values()) {
			if (permissao.getKey().equals(key)) {
				return permissao;
			}
		}
		throw new EnumConstantNotPresentException(StatusSetorCargoEnum.class, "Chave " + key + " não encontrada");
	}
	
	public static StatusSetorCargoEnum getByDescricao(String descricao) throws EnumConstantNotPresentException {
		for (StatusSetorCargoEnum permissao : StatusSetorCargoEnum.values()) {
			if (permissao.getDescricao().equals(descricao)) {
				return permissao;
			}
		}
		throw new EnumConstantNotPresentException(StatusSetorCargoEnum.class, "Permissão " + descricao + " não encontrada");
	}
}