package br.com.dh.dexSystem.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import br.com.dh.dexSystem.util.FormatacaoUtils;

@Entity
@Table(name = "atividade")	
public class Atividade extends SincronizacaoAbstract implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Sincronizar
	@Column(name = "id")
	private Long id;
	
	@Sincronizar
	@Column(name = "empregado_id")
	private Long idEmpregado;
	
	@Sincronizar
	@Column(name = "cargo_id")
	private Long idCargo;
	
	@Sincronizar
	@Temporal(TemporalType.DATE)
	@Column(name = "data_inicio")
	private Date dataInicio;
	
	@Sincronizar
	@Temporal(TemporalType.DATE)
	@Column(name = "data_fim")
	private Date dataFim;
	
	@Sincronizar
	@Column(name = "funcao")
	private String funcao;
	
	@Sincronizar
	@Column(name = "excluido")
	private Character excluido;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "empregado_id", insertable = false, updatable = false)
    private Empregado empregado;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "cargo_id", insertable = false, updatable = false)
    private Cargo cargo;
	
	@Transient
	private Long idEmpresa;
	
	@Transient
	private Long idSetor;
	
	@Transient
	private String dataInicioString;
	
	@Transient
	private String dataFimString;
	
	@Transient
	private boolean finalizada;
	
	public Atividade() {
		super();
		this.excluido = '0';
		this.dataInicioString = FormatacaoUtils.getDataString(new Date());
	}
	
	public String getDataInicioFormatada() {
		return dataInicio != null ? FormatacaoUtils.getDataString(dataInicio) : "";
	}
	
	public String getDataFimFormatada() {
		return dataFim != null ? FormatacaoUtils.getDataString(dataFim) : "";
	}
	
	public boolean isAtiva() {
		Date dataHoje = new Date();
		return (dataInicio.before(dataHoje) || dataInicio.equals(dataHoje)) && (dataFim == null || dataFim.after(dataHoje));
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdEmpregado() {
		return idEmpregado;
	}

	public void setIdEmpregado(Long idEmpregado) {
		this.idEmpregado = idEmpregado;
	}

	public Long getIdCargo() {
		return idCargo;
	}

	public void setIdCargo(Long idCargo) {
		this.idCargo = idCargo;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	public String getFuncao() {
		return funcao;
	}

	public void setFuncao(String funcao) {
		this.funcao = funcao;
	}

	public Empregado getEmpregado() {
		return empregado;
	}

	public void setEmpregado(Empregado empregado) {
		this.empregado = empregado;
	}

	public Cargo getCargo() {
		return cargo;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}

	public String getDataInicioString() {
		return dataInicioString;
	}

	public void setDataInicioString(String dataInicioString) {
		this.dataInicioString = dataInicioString;
	}

	public String getDataFimString() {
		return dataFimString;
	}

	public void setDataFimString(String dataFimString) {
		this.dataFimString = dataFimString;
	}

	public Long getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public Long getIdSetor() {
		return idSetor;
	}

	public void setIdSetor(Long idSetor) {
		this.idSetor = idSetor;
	}

	public boolean isFinalizada() {
		return finalizada;
	}

	public void setFinalizada(boolean finalizada) {
		this.finalizada = finalizada;
	}
	
	public Character getExcluido() {
		return excluido;
	}

	public void setExcluido(Character excluido) {
		this.excluido = excluido;
	}

	@Override
	public String getHash(boolean inclusao) throws Exception {
		return gerarHash(inclusao, getChecksum(), getDataInicio(), getDataFim(), getFuncao(), getExcluido());
	}
}