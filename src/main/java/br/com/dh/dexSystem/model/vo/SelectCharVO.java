package br.com.dh.dexSystem.model.vo;

public class SelectCharVO {

	private Character key;
	private String label;
	
	
	public SelectCharVO(Character key, String label) {
		super();
		this.key = key;
		this.label = label;
	}

	public Character getKey() {
		return key;
	}

	public void setKey(Character key) {
		this.key = key;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
}
