package br.com.dh.dexSystem.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.com.dh.dexSystem.model.constant.StatusCaEnum;
import br.com.dh.dexSystem.util.FormatacaoUtils;

@Entity
@Table(name = "epi")	
public class Epi extends SincronizacaoAbstract implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Sincronizar
	@Column(name = "id")
	private Long id;
	
	@Sincronizar
	@Column(name = "grade_id")
	private Long idGrade;
	
	@Sincronizar
	@Column(name = "unidade_id")
	private Long idUnidade;
	
	@Sincronizar
	@Column(name = "nome")
	private String nome;
	
	@Sincronizar
	@Column(name = "ca")
	private String ca;
	
	@Sincronizar
	@JsonFormat(pattern="yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	@Column(name = "data_validade_ca")
	private Date dataValidadeCa;

	@Sincronizar
	@Column(name = "vida_util")
	private Integer vidaUtil;
	
	@Sincronizar
	@Column(name = "vida_util_unidade")
	private Character vidaUtilUnidade;
	
	@Sincronizar
	@Column(name = "descartavel")
	private Character descartavel;
	
	@Sincronizar
	@Column(name = "excluido")
	private Character excluido;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "grade_id", insertable = false, updatable = false)
    private Grade grade;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "unidade_id", insertable = false, updatable = false)
    private UnidadeMedida unidadeMedida;
	
	@Transient
	private String validadeCa;
	
	@Transient
	private boolean descartavelB;
	
	public Epi() {
		super();
		this.excluido = '0';
	}
	
	public String getNomeGrade() {
		return isPossuiGrade() ? grade.getNome() : "";
	}
	
	public boolean isPossuiGrade() {
		return idGrade != null;
	}
	
	public boolean isDescartavel() {
		return descartavel != null && descartavel.equals('1');
	}
	
	public boolean isCaVencido() {
		Date dataAtual = new Date();
		return dataValidadeCa != null && (dataValidadeCa.before(dataAtual) || dataValidadeCa.equals(dataAtual));
	}
	
	public String getDataValidadeCaFormatada() {
		return dataValidadeCa != null ? FormatacaoUtils.getDataString(dataValidadeCa) : "";
	}
	
	public String getStatusCaDescricao() {
		if (dataValidadeCa == null) {
			return StatusCaEnum.SEM_CA.getDescricao();
		} else if (isCaVencido()) {
			return StatusCaEnum.CA_VENCIDO.getDescricao();
		} else {
			return StatusCaEnum.CA_EM_DIA.getDescricao();
		}
	}
	
	public String getStatusCaCor() {
		if (dataValidadeCa == null) {
			return "list-status uk-badge " + StatusCaEnum.SEM_CA.getCor();
		} else if (isCaVencido()) {
			return "list-status uk-badge " + StatusCaEnum.CA_VENCIDO.getCor();
		} else {
			return "list-status uk-badge " + StatusCaEnum.CA_EM_DIA.getCor();
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdGrade() {
		return idGrade;
	}

	public void setIdGrade(Long idGrade) {
		this.idGrade = idGrade;
	}

	public Long getIdUnidade() {
		return idUnidade;
	}

	public void setIdUnidade(Long idUnidade) {
		this.idUnidade = idUnidade;
	}

	public String getCa() {
		return ca;
	}

	public void setCa(String ca) {
		this.ca = ca;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getVidaUtil() {
		return vidaUtil;
	}

	public void setVidaUtil(Integer vidaUtil) {
		this.vidaUtil = vidaUtil;
	}

	public Character getVidaUtilUnidade() {
		return vidaUtilUnidade;
	}

	public void setVidaUtilUnidade(Character vidaUtilUnidade) {
		this.vidaUtilUnidade = vidaUtilUnidade;
	}

	public Character getDescartavel() {
		return descartavel;
	}

	public void setDescartavel(Character descartavel) {
		this.descartavel = descartavel;
	}

	public Date getDataValidadeCa() {
		return dataValidadeCa;
	}

	public void setDataValidadeCa(Date dataValidadeCa) {
		this.dataValidadeCa = dataValidadeCa;
	}

	public Grade getGrade() {
		return grade;
	}

	public void setGrade(Grade grade) {
		this.grade = grade;
	}

	public UnidadeMedida getUnidadeMedida() {
		return unidadeMedida;
	}

	public void setUnidadeMedida(UnidadeMedida unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}

	public String getValidadeCa() {
		return validadeCa;
	}

	public void setValidadeCa(String validadeCa) {
		this.validadeCa = validadeCa;
	}

	public boolean isDescartavelB() {
		return descartavelB;
	}

	public void setDescartavelB(boolean descartavelB) {
		this.descartavelB = descartavelB;
	}

	public Character getExcluido() {
		return excluido;
	}

	public void setExcluido(Character excluido) {
		this.excluido = excluido;
	}
	
	@Override
	public String getHash(boolean inclusao) throws Exception {
		return gerarHash(inclusao, getChecksum(), getIdGrade(), getIdUnidade(), getNome(), getCa(), getDataValidadeCa(), getVidaUtil(), getVidaUtilUnidade(), getDescartavel(), getExcluido());
	}
}