package br.com.dh.dexSystem.model.constant;


public enum StatusCaEnum {
	SEM_CA(0L, "Sem CA", "uk-badge-primary"),
	CA_EM_DIA(1L, "CA em dia", "uk-badge-success"),
	CA_VENCIDO(2L, "CA vencido", "uk-badge-danger");

	
	private Long key;
	private String descricao;
	private String cor;

	private StatusCaEnum (Long key, String descricao, String cor) {
		this.key = key;
		this.descricao = descricao;
		this.cor = cor;
	}
	
	public Long getKey() {
		return key;
	}

	public void setKey(Long key) {
		this.key = key;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

	public static StatusCaEnum getByKey(Long key) throws EnumConstantNotPresentException {
		for (StatusCaEnum permissao : StatusCaEnum.values()) {
			if (permissao.getKey().equals(key)) {
				return permissao;
			}
		}
		throw new EnumConstantNotPresentException(StatusCaEnum.class, "Chave " + key + " não encontrada");
	}
	
	public static StatusCaEnum getByDescricao(String descricao) throws EnumConstantNotPresentException {
		for (StatusCaEnum permissao : StatusCaEnum.values()) {
			if (permissao.getDescricao().equals(descricao)) {
				return permissao;
			}
		}
		throw new EnumConstantNotPresentException(StatusCaEnum.class, "Permissão " + descricao + " não encontrada");
	}
}