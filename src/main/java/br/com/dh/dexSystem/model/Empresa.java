package br.com.dh.dexSystem.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import br.com.dh.dexSystem.model.constant.StatusEmpresaEnum;
import br.com.dh.dexSystem.model.constant.TipoEmpresaEnum;
import br.com.dh.dexSystem.util.FormatacaoUtils;

@Entity
@Table(name = "empresa")	
public class Empresa extends SincronizacaoAbstract implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Sincronizar
	@Column(name = "pessoa_id")
	private Long idPessoa;
	
	@Sincronizar
	@Column(name = "tipo")
	private Character tipo;
	
	@Sincronizar
	@Column(name = "matriz_id")
	private Long idMatriz;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "pessoa_id", insertable = false, updatable = false)
    private PessoaJuridica pessoaJuridica;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "matriz_id", insertable = false, updatable = false)
    private Empresa empresa;
	
	@Transient
	private Endereco endereco;
	
	private String contatosToString;
	
	@Transient
	private String dataInicioString;
	
	@Transient
	private String dataFimString;
	
	@Transient
	private boolean finalizada;
	
	public Empresa() {
		super();
		this.contatosToString = "";
		this.tipo = TipoEmpresaEnum.MATRIZ.getKey();
		this.dataInicioString = FormatacaoUtils.getDataString(new Date());
		this.endereco = new Endereco();
	}
	
	public String getRazaoSocial() {
		String razaoSocial = null;
		if (pessoaJuridica != null && pessoaJuridica.getPessoa() != null) {
			razaoSocial = pessoaJuridica.getPessoa().getNome();
		}
		return razaoSocial;
	}
	
	public String getTipoString() {
		return tipo != null ? TipoEmpresaEnum.getByKey(tipo).getDescricao() : "";
	}
	
	public boolean isAtiva() {
		Date dataHoje = new Date();
		return (pessoaJuridica.getDataInicio().before(dataHoje) || pessoaJuridica.getDataInicio().equals(dataHoje)) && (pessoaJuridica.getDataFim() == null || pessoaJuridica.getDataFim().after(dataHoje));
	}
	
	public String getStatusDescricao() {
		if (isAtiva()) {
			return StatusEmpresaEnum.ATIVA.getDescricao();
		} else {
			return StatusEmpresaEnum.INATIVA.getDescricao();
		}
	}
	
	public String getStatusCor() {
		if (isAtiva()) {
			return "list-status uk-badge " + StatusEmpresaEnum.ATIVA.getCor();
		} else {
			return "list-status uk-badge " + StatusEmpresaEnum.INATIVA.getCor();
		}
	}
	
	public Long getIdPessoa() {
		return idPessoa;
	}

	public void setIdPessoa(Long idPessoa) {
		this.idPessoa = idPessoa;
	}

	public PessoaJuridica getPessoaJuridica() {
		return pessoaJuridica;
	}

	public void setPessoaJuridica(PessoaJuridica pessoaJuridica) {
		this.pessoaJuridica = pessoaJuridica;
	}

	public Character getTipo() {
		return tipo;
	}

	public void setTipo(Character tipo) {
		this.tipo = tipo;
	}

	public String getDataInicioString() {
		return dataInicioString;
	}

	public void setDataInicioString(String dataInicioString) {
		this.dataInicioString = dataInicioString;
	}

	public String getDataFimString() {
		return dataFimString;
	}

	public void setDataFimString(String dataFimString) {
		this.dataFimString = dataFimString;
	}

	public Long getIdMatriz() {
		return idMatriz;
	}

	public void setIdMatriz(Long idMatriz) {
		this.idMatriz = idMatriz;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public String getContatosToString() {
		return contatosToString;
	}

	public void setContatosToString(String contatosToString) {
		this.contatosToString = contatosToString;
	}

	public boolean isFinalizada() {
		return finalizada;
	}

	public void setFinalizada(boolean finalizada) {
		this.finalizada = finalizada;
	}
	
	@Override
	public String getHash(boolean inclusao) throws Exception {
		return gerarHash(inclusao, getChecksum(), getTipo(), getIdMatriz());
	}
}