package br.com.dh.dexSystem.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.dh.dexSystem.model.constant.StatusSetorCargoEnum;
import br.com.dh.dexSystem.util.FormatacaoUtils;

@Entity
@Table(name = "cargo")	
public class Cargo extends SincronizacaoAbstract implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Sincronizar
	@Column(name = "id")
	private Long id;
	
	@Sincronizar
	@Column(name = "setor_id")
	private Long idSetor;
	
	@Sincronizar
	@Column(name = "nome")
	private String nome;
	
	@Sincronizar
	@Temporal(TemporalType.DATE)
	@Column(name = "data_inicio")
	private Date dataInicio;
	
	@Sincronizar
	@Temporal(TemporalType.DATE)
	@Column(name = "data_fim")
	private Date dataFim;
	
	@Sincronizar
	@Column(name = "excluido")
	private Character excluido;
	
	@JsonIgnore
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "setor_id", insertable = false, updatable = false)
    private Setor setor;
	
	@JsonIgnore
	@Transient
	private Long idEmpresa;
	
	@JsonIgnore
	@Transient
	private String dataInicioString;
	
	@JsonIgnore
	@Transient
	private String dataFimString;
	
	@JsonIgnore
	@Transient
	private boolean finalizado;
	
	public Cargo() {
		super();
		this.excluido = '0';
		this.dataInicioString = FormatacaoUtils.getDataString(new Date());
	}
	
	@JsonIgnore
	public String getDataInicioFormatada() {
		return dataInicio != null ? FormatacaoUtils.getDataString(dataInicio) : "";
	}
	
	@JsonIgnore
	public String getDataFimFormatada() {
		return dataFim != null ? FormatacaoUtils.getDataString(dataFim) : "";
	}
	
	@JsonIgnore
	public boolean isAtivo() {
		Date dataHoje = new Date();
		return (dataInicio.before(dataHoje) || dataInicio.equals(dataHoje)) && (dataFim == null || dataFim.after(dataHoje));
	}
	
	@JsonIgnore
	public String getStatusDescricao() {
		if (isAtivo()) {
			return StatusSetorCargoEnum.ATIVO.getDescricao();
		} else {
			return StatusSetorCargoEnum.INATIVO.getDescricao();
		}
	}
	
	@JsonIgnore
	public String getStatusCor() {
		if (isAtivo()) {
			return "list-status uk-badge " + StatusSetorCargoEnum.ATIVO.getCor();
		} else {
			return "list-status uk-badge " + StatusSetorCargoEnum.INATIVO.getCor();
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdSetor() {
		return idSetor;
	}

	public void setIdSetor(Long idSetor) {
		this.idSetor = idSetor;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	public Setor getSetor() {
		return setor;
	}

	public void setSetor(Setor setor) {
		this.setor = setor;
	}

	public String getDataInicioString() {
		return dataInicioString;
	}

	public void setDataInicioString(String dataInicioString) {
		this.dataInicioString = dataInicioString;
	}

	public String getDataFimString() {
		return dataFimString;
	}

	public void setDataFimString(String dataFimString) {
		this.dataFimString = dataFimString;
	}

	public Long getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public boolean isFinalizado() {
		return finalizado;
	}

	public void setFinalizado(boolean finalizado) {
		this.finalizado = finalizado;
	}

	public Character getExcluido() {
		return excluido;
	}

	public void setExcluido(Character excluido) {
		this.excluido = excluido;
	}
	
	@Override
	public String getHash(boolean inclusao) throws Exception {
		return gerarHash(inclusao, getChecksum(), getNome(), getDataInicio(), getDataFim(), getExcluido());
	}
}