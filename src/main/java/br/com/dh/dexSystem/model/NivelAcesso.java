package br.com.dh.dexSystem.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "nivel_acesso")	
public class NivelAcesso implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "nome")
	private String nome;
	
	@Column(name = "nivel")
	private Long nivel;
	
	@JsonIgnore
	@Column(name = "excluido")
	private Character excluido;
	
	
	@Transient
	private List<VwMenuAcessoPermissao> menuAcessos;
	
	
	public NivelAcesso() {
		super();
		this.excluido = '0';
	}
	
	public String getNomeCompleto() {
		return nivel + " - " + nome;
	}
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Long getNivel() {
		return nivel;
	}

	public void setNivel(Long nivel) {
		this.nivel = nivel;
	}
	
	public List<VwMenuAcessoPermissao> getMenuAcessos() {
		return menuAcessos;
	}

	public void setMenuAcessos(List<VwMenuAcessoPermissao> menuAcessos) {
		this.menuAcessos = menuAcessos;
	}

	public Character getExcluido() {
		return excluido;
	}

	public void setExcluido(Character excluido) {
		this.excluido = excluido;
	}
}