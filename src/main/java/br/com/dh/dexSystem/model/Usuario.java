package br.com.dh.dexSystem.model;

import java.io.Serializable;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.security.core.context.SecurityContextHolder;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.dh.dexSystem.model.constant.SimNaoEnum;
import br.com.dh.dexSystem.security.UsuarioSistema;

@Entity
@Table(name = "usuario")	
public class Usuario extends SincronizacaoAbstract implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Sincronizar
	@Column(name = "id")
	private Long id;

	@Sincronizar
	@Column(name = "nome")
	private String nome;	

	@Sincronizar
	@Column(name = "email")
	private String email;

	@Sincronizar
	@Column(name = "senha")
	private String senha;
	
	@Column(name = "nivel_acesso_id")
	private Long idNivelAcesso;
	
	@Sincronizar
	@Column(name = "acesso_desktop")
	private Character acessoModuloDesktop;
	
	@Sincronizar
	@Column(name = "excluido")
	private Character excluido;
	
	@JsonIgnore
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "nivel_acesso_id", insertable = false, updatable = false)
    private NivelAcesso nivelAcesso;

	@Transient
	private String confirmaSenha;
	
	@JsonIgnore
	@Transient
	private Map<String, Map<String, Boolean>> permissoes;
	
	
	public Usuario() {
		super();
		this.excluido = '0';
		this.acessoModuloDesktop = SimNaoEnum.NAO.getKey();
	}

	//usado no cabecalho da pagina
	@JsonIgnore
	public String getPrimeiroNome() {
		return ((UsuarioSistema) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsuario().getNome().split(" ")[0];
	}
	
	//usado na exportacao dos usuarios
	@JsonIgnore
	public String getNomeNivelAcesso() {
		return nivelAcesso != null ? nivelAcesso.getNomeCompleto() : idNivelAcesso.toString();
	}
	
	@JsonIgnore
	public boolean isExcluido() {
		return excluido != null && excluido.equals('1');
	}
	
	@JsonIgnore
	public boolean isPossuiAcessoDesktop() {
		return acessoModuloDesktop != null && acessoModuloDesktop.equals(SimNaoEnum.SIM.getKey());
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getConfirmaSenha() {
		return confirmaSenha;
	}

	public void setConfirmaSenha(String confirmaSenha) {
		this.confirmaSenha = confirmaSenha;
	}

	public Long getIdNivelAcesso() {
		return idNivelAcesso;
	}

	public void setIdNivelAcesso(Long idNivelAcesso) {
		this.idNivelAcesso = idNivelAcesso;
	}

	public NivelAcesso getNivelAcesso() {
		return nivelAcesso;
	}

	public void setNivelAcesso(NivelAcesso nivelAcesso) {
		this.nivelAcesso = nivelAcesso;
	}

	public Map<String, Map<String, Boolean>> getPermissoes() {
		return permissoes;
	}

	public void setPermissoes(Map<String, Map<String, Boolean>> permissoes) {
		this.permissoes = permissoes;
	}

	public Character getExcluido() {
		return excluido;
	}

	public void setExcluido(Character excluido) {
		this.excluido = excluido;
	}
	
	public Character getAcessoModuloDesktop() {
		return acessoModuloDesktop;
	}

	public void setAcessoModuloDesktop(Character acessoModuloDesktop) {
		this.acessoModuloDesktop = acessoModuloDesktop;
	}

	@Override
	public String getHash(boolean inclusao) throws Exception {
		return gerarHash(inclusao, getChecksum(), getNome(), getSenha(), getEmail(), getAcessoModuloDesktop(), getExcluido());
	}
}