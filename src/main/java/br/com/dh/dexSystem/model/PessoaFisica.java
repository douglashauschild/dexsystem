package br.com.dh.dexSystem.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import br.com.dh.dexSystem.model.constant.SexoEnum;
import br.com.dh.dexSystem.util.FormatacaoUtils;

@Entity
@Table(name = "pessoa_fisica")	
public class PessoaFisica extends SincronizacaoAbstract implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Sincronizar
	@Column(name = "pessoa_id")
	private Long idPessoa;
	
	@Sincronizar
	@Temporal(TemporalType.DATE)
	@Column(name = "data_nascimento")
	private Date dataNascimento;	
	
	@Sincronizar
	@Column(name = "cpf")
	private String cpf;
	
	@Sincronizar
	@Column(name = "rg")
	private String rg;
	
	@Sincronizar
	@Column(name = "sexo")
	private Character sexo;
	
	@Sincronizar
	@Column(name = "excluido")
	private Character excluido;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "pessoa_id", insertable = false, updatable = false)
    private Pessoa pessoa;
	
	@Transient
	private String dataNascimentoString;
	
	@Transient
	private Endereco endereco;
	
	@Transient
	private String contatosToString;
	
	public PessoaFisica() {
		super();
		this.excluido = '0';
		this.contatosToString = "";
		this.endereco = new Endereco();
	}
	
	public String getIdade() {
		if (dataNascimento != null) {
			Integer anos = FormatacaoUtils.getIdade(dataNascimento, new Date());
			if (anos == null) {
				return "0 anos";
			}
			return anos + " anos";
		}
		return "";
	}

	
	public String getCpfLimpo() {
		return cpf != null ? FormatacaoUtils.removerFormatacao(cpf) : "";
	}
	
	public String getCpfFormatado() {
		String cpfFormatado = ""; 
		try {
			cpfFormatado = cpf != null ? FormatacaoUtils.getCpfFormatado(cpf) : "";
		} catch(Exception e) {
			e.printStackTrace();
		}
		return cpfFormatado;
	}
	
	public String getDataNascimentoFormatada() {
		return dataNascimento != null ? FormatacaoUtils.getDataString(dataNascimento) : "";
	}
	
	public String getSexoFormatado() {
		return sexo != null ? SexoEnum.getByKey(sexo).getLabel() : "";
	}
	

	public Long getIdPessoa() {
		return idPessoa;
	}

	public void setIdPessoa(Long idPessoa) {
		this.idPessoa = idPessoa;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public Character getSexo() {
		return sexo;
	}

	public void setSexo(Character sexo) {
		this.sexo = sexo;
	}

	public String getDataNascimentoString() {
		return dataNascimentoString;
	}

	public void setDataNascimentoString(String dataNascimentoString) {
		this.dataNascimentoString = dataNascimentoString;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public String getContatosToString() {
		return contatosToString;
	}

	public void setContatosToString(String contatosToString) {
		this.contatosToString = contatosToString;
	}

	public Character getExcluido() {
		return excluido;
	}

	public void setExcluido(Character excluido) {
		this.excluido = excluido;
	}
	
	@Override
	public String getHash(boolean inclusao) throws Exception {
		return gerarHash(inclusao, getChecksum(), getCpf(), getRg(), getDataNascimento(), getSexo(), getExcluido());
	}
}