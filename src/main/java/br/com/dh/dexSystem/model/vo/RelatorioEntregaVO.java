package br.com.dh.dexSystem.model.vo;

import java.util.List;

import br.com.dh.dexSystem.model.EmpregadoEpi;

public class RelatorioEntregaVO {

	private List<EmpregadoEpi> empregadoEpis;
	
	public RelatorioEntregaVO(List<EmpregadoEpi> empregadoEpis) {
		super();
		this.empregadoEpis = empregadoEpis;
	}

	public List<EmpregadoEpi> getEmpregadoEpis() {
		return empregadoEpis;
	}
	
	public void setEmpregadoEpis(List<EmpregadoEpi> empregadoEpis) {
		this.empregadoEpis = empregadoEpis;
	}
}