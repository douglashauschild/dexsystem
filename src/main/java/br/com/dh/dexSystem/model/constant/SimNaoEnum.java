package br.com.dh.dexSystem.model.constant;


public enum SimNaoEnum {
	SIM('S', "Sim"),
	NAO('N', "Não");
	
	private Character key;
	private String descricao;

	private SimNaoEnum (Character key, String descricao) {
		this.key = key;
		this.descricao = descricao;
	}
	
	public Character getKey() {
		return key;
	}

	public void setKey(Character key) {
		this.key = key;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}


	public static SimNaoEnum getByKey(Character key) throws EnumConstantNotPresentException {
		for (SimNaoEnum tipo : SimNaoEnum.values()) {
			if (tipo.getKey().equals(key)) {
				return tipo;
			}
		}
		throw new EnumConstantNotPresentException(SimNaoEnum.class, "Chave " + key + " não encontrada");
	}
	
	public static SimNaoEnum getByDescricao(String descricao) throws EnumConstantNotPresentException {
		for (SimNaoEnum tipo : SimNaoEnum.values()) {
			if (tipo.getDescricao().equals(descricao)) {
				return tipo;
			}
		}
		throw new EnumConstantNotPresentException(SimNaoEnum.class, "Tipo " + descricao + " não encontrado");
	}
}