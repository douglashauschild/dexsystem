package br.com.dh.dexSystem.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import br.com.dh.dexSystem.util.CriptografiaUtils;
import br.com.dh.dexSystem.util.FormatacaoUtils;

@MappedSuperclass
public abstract class SincronizacaoAbstract implements SincronizacaoInterface, Serializable {

	private static final long serialVersionUID = 1L;
	
	@Sincronizar
	@Column(name = "checksum")
	private String checksum;
	
	@Sincronizar
	@Column(name = "checksum_alteracao")
	private String checksumAlteracao;
	
	
	public SincronizacaoAbstract() {
		super();
	}
	
	
	public SincronizacaoAbstract(String checksum, String checksumAlteracao) {
		super();
		this.checksum = checksum;
		this.checksumAlteracao = checksumAlteracao;
	}


	public String gerarHash(boolean inclusao, Object ... objects) throws Exception {
		String checksum = "";
		for (Object obj : objects) {
	        if (!checksum.isEmpty()) {
	            checksum += "-";
	        }
	        if (obj == null || (obj instanceof String && ((String) obj).isEmpty())) {
	            checksum += "NULL";
	        } else if (obj instanceof Date) {
	            checksum += FormatacaoUtils.getDataChecksum((Date) obj).trim();
	        } else {
	            checksum += obj.toString().trim();
	        }
		}
		if (inclusao) {
			checksum += "-" + FormatacaoUtils.getDataChecksum(new Date());
        }
        return CriptografiaUtils.gerarHash(checksum);
    }

	public String getChecksum() {
		return checksum;
	}

	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}

	public String getChecksumAlteracao() {
		return checksumAlteracao;
	}

	public void setChecksumAlteracao(String checksumAlteracao) {
		this.checksumAlteracao = checksumAlteracao;
	}
}