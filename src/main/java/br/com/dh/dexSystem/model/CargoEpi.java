package br.com.dh.dexSystem.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import br.com.dh.dexSystem.util.FormatacaoUtils;

@Entity
@Table(name = "cargo_epi")	
public class CargoEpi extends SincronizacaoAbstract implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Sincronizar
	@Column(name = "id")
	private Long id;
	
	@Sincronizar
	@Column(name = "cargo_id")
	private Long idCargo;
	
	@Sincronizar
	@Column(name = "epi_id")
	private Long idEpi;
	
	@Sincronizar
	@Column(name = "quantidade")
	private Integer quantidade;
	
	@Sincronizar
	@Temporal(TemporalType.DATE)
	@Column(name = "data_inicio")
	private Date dataInicio;
	
	@Sincronizar
	@Temporal(TemporalType.DATE)
	@Column(name = "data_fim")
	private Date dataFim;
	
	@Sincronizar
	@Column(name = "excluido")
	private Character excluido;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "cargo_id", insertable = false, updatable = false)
    private Cargo cargo;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "epi_id", insertable = false, updatable = false)
    private Epi epi;
	
	@Transient
	private Long idEmpresa;
	
	@Transient
	private Long idSetor;
	
	@Transient
	private String dataInicioString;
	
	@Transient
	private String dataFimString;
	
	@Transient
	private boolean finalizado;
	
	public CargoEpi() {
		super();
		this.excluido = '0';
		this.dataInicioString = FormatacaoUtils.getDataString(new Date());
		this.quantidade = 1;
	}
	
	public String getDataInicioFormatada() {
		return dataInicio != null ? FormatacaoUtils.getDataString(dataInicio) : "";
	}
	
	public String getDataFimFormatada() {
		return dataFim != null ? FormatacaoUtils.getDataString(dataFim) : "";
	}
	
	public boolean isAtivo() {
		Date dataHoje = new Date();
		return (dataInicio.before(dataHoje) || dataInicio.equals(dataHoje)) && (dataFim == null || dataFim.after(dataHoje));
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdSetor() {
		return idSetor;
	}

	public void setIdSetor(Long idSetor) {
		this.idSetor = idSetor;
	}

	public Long getIdCargo() {
		return idCargo;
	}

	public void setIdCargo(Long idCargo) {
		this.idCargo = idCargo;
	}

	public Long getIdEpi() {
		return idEpi;
	}

	public void setIdEpi(Long idEpi) {
		this.idEpi = idEpi;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	public Cargo getCargo() {
		return cargo;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}

	public Epi getEpi() {
		return epi;
	}

	public void setEpi(Epi epi) {
		this.epi = epi;
	}

	public Long getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public String getDataInicioString() {
		return dataInicioString;
	}

	public void setDataInicioString(String dataInicioString) {
		this.dataInicioString = dataInicioString;
	}

	public String getDataFimString() {
		return dataFimString;
	}

	public void setDataFimString(String dataFimString) {
		this.dataFimString = dataFimString;
	}

	public boolean isFinalizado() {
		return finalizado;
	}

	public void setFinalizado(boolean finalizado) {
		this.finalizado = finalizado;
	}

	public Character getExcluido() {
		return excluido;
	}

	public void setExcluido(Character excluido) {
		this.excluido = excluido;
	}
	
	@Override
	public String getHash(boolean inclusao) throws Exception {
		return gerarHash(inclusao, getChecksum(), getQuantidade(), getDataInicio(), getDataFim(), getExcluido());
	}
}