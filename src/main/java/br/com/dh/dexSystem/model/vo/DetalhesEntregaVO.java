package br.com.dh.dexSystem.model.vo;

public class DetalhesEntregaVO {

	private String epi;
	private String ca;
	private String grade;
	private String tamanho;
	private String quantidade;
	private String previsaoEntrega;
	private String entrega;
	private String dataConfirmacao;
	private String tipoConfirmacao;
	private String previsaoTroca;
	private String dataSubstituicao;
	private String dataDevolucao;
	
	public String getEpi() {
		return epi;
	}
	
	public void setEpi(String epi) {
		this.epi = epi;
	}
	
	public String getCa() {
		return ca;
	}
	
	public void setCa(String ca) {
		this.ca = ca;
	}
	
	public String getGrade() {
		return grade;
	}
	
	public void setGrade(String grade) {
		this.grade = grade;
	}
	
	public String getTamanho() {
		return tamanho;
	}
	
	public void setTamanho(String tamanho) {
		this.tamanho = tamanho;
	}
	
	public String getQuantidade() {
		return quantidade;
	}
	
	public void setQuantidade(String quantidade) {
		this.quantidade = quantidade;
	}
	
	public String getPrevisaoEntrega() {
		return previsaoEntrega;
	}
	
	public void setPrevisaoEntrega(String previsaoEntrega) {
		this.previsaoEntrega = previsaoEntrega;
	}
	
	public String getEntrega() {
		return entrega;
	}
	
	public void setEntrega(String entrega) {
		this.entrega = entrega;
	}
	
	public String getDataConfirmacao() {
		return dataConfirmacao;
	}
	
	public void setDataConfirmacao(String dataConfirmacao) {
		this.dataConfirmacao = dataConfirmacao;
	}

	public String getTipoConfirmacao() {
		return tipoConfirmacao;
	}
	
	public void setTipoConfirmacao(String tipoConfirmacao) {
		this.tipoConfirmacao = tipoConfirmacao;
	}
	
	public String getPrevisaoTroca() {
		return previsaoTroca;
	}
	
	public void setPrevisaoTroca(String previsaoTroca) {
		this.previsaoTroca = previsaoTroca;
	}
	
	public String getDataSubstituicao() {
		return dataSubstituicao;
	}
	
	public void setDataSubstituicao(String dataSubstituicao) {
		this.dataSubstituicao = dataSubstituicao;
	}
	
	public String getDataDevolucao() {
		return dataDevolucao;
	}
	
	public void setDataDevolucao(String dataDevolucao) {
		this.dataDevolucao = dataDevolucao;
	}
}