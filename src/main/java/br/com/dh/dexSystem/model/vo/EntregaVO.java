package br.com.dh.dexSystem.model.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.com.dh.dexSystem.model.EmpregadoEpi;

public class EntregaVO implements Serializable {

	private static final long serialVersionUID = -525191689441606859L;
	
	private Long idEmpresa;
	private Long idEmpregado;
	private List<EmpregadoEpi> episEntrega;
	
	public EntregaVO() {
		super();
		this.episEntrega = new ArrayList<EmpregadoEpi>();
	}

	public EntregaVO(Long idEmpresa, Long idEmpregado, List<EmpregadoEpi> episEntrega) {
		super();
		this.idEmpresa = idEmpresa;
		this.idEmpregado = idEmpregado;
		this.episEntrega = episEntrega;
	}

	public Long getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(Long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public Long getIdEmpregado() {
		return idEmpregado;
	}

	public void setIdEmpregado(Long idEmpregado) {
		this.idEmpregado = idEmpregado;
	}

	public List<EmpregadoEpi> getEpisEntrega() {
		return episEntrega;
	}

	public void setEpisEntrega(List<EmpregadoEpi> episEntrega) {
		this.episEntrega = episEntrega;
	}
}