package br.com.dh.dexSystem.model.constant;


public enum DiaSemanaEnum {
	JANEIRO(1, "Janeiro"),
	FEVEREIRO(2, "Fevereiro"),
	MARCO(3, "Março"),
	ABRIL(4, "Abril"),
	MAIO(5, "Maio"),
	JUNHO(6, "Junho"),
	JULHO(7, "Julho"),
	AGOSTO(8, "Agosto"),
	SETEMBRO(9, "Setembro"),
	OUTUBRO(10, "Outubro"),
	NOVEMBRO(11, "Novembro"),
	DEZEMBRO(12, "Dezembro");
	
	private Integer key;
	private String descricao;

	private DiaSemanaEnum (Integer key, String descricao) {
		this.key = key;
		this.descricao = descricao;
	}

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setLabel(String descricao) {
		this.descricao = descricao;
	}
	
	public static DiaSemanaEnum getByKey(Integer key) throws EnumConstantNotPresentException {
		for (DiaSemanaEnum tipo : DiaSemanaEnum.values()) {
			if (tipo.getKey().equals(key)) {
				return tipo;
			}
		}
		throw new EnumConstantNotPresentException(DiaSemanaEnum.class, "Chave " + key + " não encontrada");
	}
	
	public static DiaSemanaEnum getByDescricao(String descricao) throws EnumConstantNotPresentException {
		for (DiaSemanaEnum tipo : DiaSemanaEnum.values()) {
			if (tipo.getDescricao().equals(descricao)) {
				return tipo;
			}
		}
		throw new EnumConstantNotPresentException(DiaSemanaEnum.class, "Dia da semana " + descricao + " não encontrada");
	}
}