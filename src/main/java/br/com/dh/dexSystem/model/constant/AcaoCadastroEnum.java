package br.com.dh.dexSystem.model.constant;


public enum AcaoCadastroEnum {
	CADASTRADO('C', "Cadastrado"),
	NOVO('N', "Novo"),
	ALTERAR('A', "Alterar"),
	EXCLUIR('E', "Excluir");
	
	private Character key;
	private String descricao;

	private AcaoCadastroEnum (Character key, String descricao) {
		this.key = key;
		this.descricao = descricao;
	}
	
	public Character getKey() {
		return key;
	}

	public void setKey(Character key) {
		this.key = key;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}


	public static AcaoCadastroEnum getByKey(Character key) throws EnumConstantNotPresentException {
		for (AcaoCadastroEnum permissao : AcaoCadastroEnum.values()) {
			if (permissao.getKey().equals(key)) {
				return permissao;
			}
		}
		throw new EnumConstantNotPresentException(AcaoCadastroEnum.class, "Chave " + key + " não encontrada");
	}
	
	public static AcaoCadastroEnum getByDescricao(String descricao) throws EnumConstantNotPresentException {
		for (AcaoCadastroEnum permissao : AcaoCadastroEnum.values()) {
			if (permissao.getDescricao().equals(descricao)) {
				return permissao;
			}
		}
		throw new EnumConstantNotPresentException(AcaoCadastroEnum.class, "Permissão " + descricao + " não encontrada");
	}
}