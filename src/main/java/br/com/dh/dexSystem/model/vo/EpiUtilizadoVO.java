package br.com.dh.dexSystem.model.vo;

public class EpiUtilizadoVO {

	private String nome;
	private String qtd;
	
	public EpiUtilizadoVO() {
		super();
	}

	public EpiUtilizadoVO(String nome, String qtd) {
		super();
		this.nome = nome;
		this.qtd = qtd;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getQtd() {
		return qtd;
	}

	public void setQtd(String qtd) {
		this.qtd = qtd;
	}
}