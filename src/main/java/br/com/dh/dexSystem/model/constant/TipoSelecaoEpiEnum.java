package br.com.dh.dexSystem.model.constant;


public enum TipoSelecaoEpiEnum {
	NOME('S', "Nome"),
	CA_NOME('C', "CA | Nome");
	
	private Character key;
	private String descricao;

	private TipoSelecaoEpiEnum (Character key, String descricao) {
		this.key = key;
		this.descricao = descricao;
	}
	
	public Character getKey() {
		return key;
	}

	public void setKey(Character key) {
		this.key = key;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}


	public static TipoSelecaoEpiEnum getByKey(Character key) throws EnumConstantNotPresentException {
		for (TipoSelecaoEpiEnum tipo : TipoSelecaoEpiEnum.values()) {
			if (tipo.getKey().equals(key)) {
				return tipo;
			}
		}
		throw new EnumConstantNotPresentException(TipoSelecaoEpiEnum.class, "Chave " + key + " não encontrada");
	}
	
	public static TipoSelecaoEpiEnum getByDescricao(String descricao) throws EnumConstantNotPresentException {
		for (TipoSelecaoEpiEnum tipo : TipoSelecaoEpiEnum.values()) {
			if (tipo.getDescricao().equals(descricao)) {
				return tipo;
			}
		}
		throw new EnumConstantNotPresentException(TipoSelecaoEpiEnum.class, "Tipo " + descricao + " não encontrado");
	}
}