package br.com.dh.dexSystem.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.com.dh.dexSystem.model.constant.StatusEmpregadoEpiEnum;
import br.com.dh.dexSystem.model.constant.TipoConfirmacaoEnum;
import br.com.dh.dexSystem.model.vo.SelectVO;
import br.com.dh.dexSystem.util.FormatacaoUtils;

@Entity
@Table(name = "empregado_epi")	
public class EmpregadoEpi extends SincronizacaoAbstract implements Serializable, Cloneable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Sincronizar
	@Column(name = "id")
	private Long id;
	
	@Sincronizar
	@Column(name = "empregado_id")
	private Long idEmpregado;
	
	@Sincronizar
	@Column(name = "epi_id")
	private Long idEpi;
	
	@Sincronizar
	@Column(name = "usuario_id")
	private Long idUsuario;
	
	@Sincronizar
	@Column(name = "grade_item_id")
	private Long idGradeItem;
	
	@Sincronizar
	@Column(name = "quantidade")
	private Integer quantidade;
	
	@Sincronizar
	@Temporal(TemporalType.DATE)
	@Column(name = "data_prevista_entrega")
	private Date dataPrevistaEntrega;
	
	@Sincronizar
	@Temporal(TemporalType.DATE)
	@Column(name = "data_entrega")
	private Date dataEntrega;
	
	@Sincronizar
	@Temporal(TemporalType.DATE)
	@Column(name = "data_prevista_troca")
	private Date dataPrevistaTroca;
	
	@Sincronizar
	@JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_confirmacao")
	private Date dataConfirmacao;
	
	@Sincronizar
	@Temporal(TemporalType.DATE)
	@Column(name = "data_substituicao")
	private Date dataSubstituicao;
	
	@Sincronizar
	@Temporal(TemporalType.DATE)
	@Column(name = "data_devolucao")
	private Date dataDevolucao;
	
	@Sincronizar
	@Column(name = "motivo_id")
	private Long idMotivo;
	
	@Sincronizar
	@Column(name = "tipo_confirmacao")
	private Character tipoConfirmacao;
	
	@Sincronizar
	@Column(name = "inclusao_manual")
	private Character inclusaoManual;
	
	@Sincronizar
	@Column(name = "excluido")
	private Character excluido;

	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "empregado_id", insertable = false, updatable = false)
    private Empregado empregado;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "epi_id", insertable = false, updatable = false)
    private Epi epi;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "grade_item_id", insertable = false, updatable = false)
    private GradeItem gradeItem;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "motivo_id", insertable = false, updatable = false)
    private Motivo motivo;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "usuario_id", insertable = false, updatable = false)
    private Usuario usuario;
	
	@Transient
	private String dataPrevistaEntregaString;
	
	@Transient
	private String dataEntregaString;
	
	@Transient
	private boolean substituir;
	
	@Transient
	private boolean devolver;
	
	@Transient
	private List<SelectVO> gradeItens;
	
	public EmpregadoEpi() {
		super();
		this.excluido = '0';
		this.dataEntregaString = FormatacaoUtils.getDataString(new Date());
	}
	
	public EmpregadoEpi clone() throws CloneNotSupportedException {
		return (EmpregadoEpi) super.clone();
    }
	
	public boolean isConfirmado() {
		return dataConfirmacao != null;
	}

	public boolean isSubstituicao() {
		return dataConfirmacao != null;
	}
	
	public boolean isFinalizado() {
		return dataSubstituicao != null || dataDevolucao != null;
	}
	
	public String getDataPrevistaEntregaFormatada() {
		return dataPrevistaEntrega != null ? FormatacaoUtils.getDataString(dataPrevistaEntrega) : "";
	}
	
	public String getDataEntregaFormatada() {
		return dataEntrega != null ? FormatacaoUtils.getDataString(dataEntrega) : "";
	}
	
	public String getDataConfirmacaoFormatada() {
		return dataConfirmacao != null ? FormatacaoUtils.getDataHoraString(dataConfirmacao) : "";
	}
	
	public String getDataPrevistaTrocaFormatada() {
		return dataPrevistaTroca != null ? FormatacaoUtils.getDataString(dataPrevistaTroca) : "";
	}
	
	public String getDataSubstituicaoFormatada() {
		return dataSubstituicao != null ? FormatacaoUtils.getDataString(dataSubstituicao) : "";
	}
	
	public String getDataDevolucaoFormatada() {
		return dataDevolucao != null ? FormatacaoUtils.getDataString(dataDevolucao) : "";
	}
	
	public String getStatusDescricao() throws Exception {
		String retorno = "";
		if (dataSubstituicao != null) {
			retorno = StatusEmpregadoEpiEnum.SUBSTITUIDO.getDescricao();
		} else if (dataDevolucao != null) {
			retorno = StatusEmpregadoEpiEnum.DEVOLVIDO.getDescricao();
		} else if (dataConfirmacao == null && dataPrevistaEntrega != null && dataPrevistaEntrega.before(FormatacaoUtils.getData(new Date()))) {
			retorno = StatusEmpregadoEpiEnum.ENTREGA_VENCIDA.getDescricao();
		} else if (dataConfirmacao != null && dataPrevistaTroca != null && dataPrevistaTroca.before(FormatacaoUtils.getData(new Date()))) {
			retorno = StatusEmpregadoEpiEnum.VENCIDO.getDescricao();		
		} else if (dataConfirmacao != null) {
			retorno = StatusEmpregadoEpiEnum.EM_DIA.getDescricao();
		} else {
			retorno = StatusEmpregadoEpiEnum.AGUARDANDO.getDescricao();
		}
		return retorno;
	}
	
	public String getStatusCor() throws Exception {
		String retorno = "";
		if (dataSubstituicao != null) {
			retorno = StatusEmpregadoEpiEnum.SUBSTITUIDO.getCor();
		} else if (dataDevolucao != null) {
			retorno = StatusEmpregadoEpiEnum.DEVOLVIDO.getCor();
		} else if (dataConfirmacao == null && dataPrevistaEntrega != null && dataPrevistaEntrega.before(FormatacaoUtils.getData(new Date()))) {
			retorno = StatusEmpregadoEpiEnum.ENTREGA_VENCIDA.getCor();
		} else if (dataConfirmacao != null && dataPrevistaTroca != null && dataPrevistaTroca.before(FormatacaoUtils.getData(new Date()))) {
			retorno = StatusEmpregadoEpiEnum.VENCIDO.getCor();
		} else if (dataConfirmacao != null) {
			retorno = StatusEmpregadoEpiEnum.EM_DIA.getCor();
		} else {
			retorno = StatusEmpregadoEpiEnum.AGUARDANDO.getCor();
		}
		return "list-status uk-badge " + retorno;
	}
	
	public String getCa() {
		return epi.getCa() != null && !epi.getCa().isEmpty() ? epi.getCa() : "Sem CA";
	}
	
	public boolean isEpiCaVencido() {
		return epi.getCa() != null && !epi.getCa().isEmpty() && epi.isCaVencido();
	}
	
	public String getGrade() {
		return epi.getIdGrade() != null && epi.getGrade() != null ? epi.getGrade().getNome() + " - Tamanho: " + getTamanho() : "Sem grade";
	}
	
	public String getTamanho() {
		return idGradeItem != null && gradeItem != null ? gradeItem.getTamanho() : "";
	}
	
	public String getAcaoEntrega() throws Exception {
		String retorno = "";
		if (dataConfirmacao != null) {
			retorno = "Substituição";
		} else {
			retorno = "Primeira Entrega";
		}
		return retorno;
	}
	
	public String getTipoConfirmacaoString() {
		return tipoConfirmacao != null ? TipoConfirmacaoEnum.getByKey(tipoConfirmacao).getDescricao() : "";
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdEmpregado() {
		return idEmpregado;
	}

	public void setIdEmpregado(Long idEmpregado) {
		this.idEmpregado = idEmpregado;
	}

	public Long getIdEpi() {
		return idEpi;
	}

	public void setIdEpi(Long idEpi) {
		this.idEpi = idEpi;
	}

	public Long getIdGradeItem() {
		return idGradeItem;
	}

	public void setIdGradeItem(Long idGradeItem) {
		this.idGradeItem = idGradeItem;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public Date getDataPrevistaEntrega() {
		return dataPrevistaEntrega;
	}

	public void setDataPrevistaEntrega(Date dataPrevistaEntrega) {
		this.dataPrevistaEntrega = dataPrevistaEntrega;
	}

	public Date getDataEntrega() {
		return dataEntrega;
	}

	public void setDataEntrega(Date dataEntrega) {
		this.dataEntrega = dataEntrega;
	}

	public Date getDataPrevistaTroca() {
		return dataPrevistaTroca;
	}

	public void setDataPrevistaTroca(Date dataPrevistaTroca) {
		this.dataPrevistaTroca = dataPrevistaTroca;
	}

	public Date getDataConfirmacao() {
		return dataConfirmacao;
	}

	public void setDataConfirmacao(Date dataConfirmacao) {
		this.dataConfirmacao = dataConfirmacao;
	}

	public Date getDataSubstituicao() {
		return dataSubstituicao;
	}

	public void setDataSubstituicao(Date dataSubstituicao) {
		this.dataSubstituicao = dataSubstituicao;
	}

	public Date getDataDevolucao() {
		return dataDevolucao;
	}

	public void setDataDevolucao(Date dataDevolucao) {
		this.dataDevolucao = dataDevolucao;
	}

	public Long getIdMotivo() {
		return idMotivo;
	}

	public void setIdMotivo(Long idMotivo) {
		this.idMotivo = idMotivo;
	}

	public Character getTipoConfirmacao() {
		return tipoConfirmacao;
	}

	public void setTipoConfirmacao(Character tipoConfirmacao) {
		this.tipoConfirmacao = tipoConfirmacao;
	}

	public Empregado getEmpregado() {
		return empregado;
	}

	public void setEmpregado(Empregado empregado) {
		this.empregado = empregado;
	}

	public Epi getEpi() {
		return epi;
	}

	public void setEpi(Epi epi) {
		this.epi = epi;
	}

	public GradeItem getGradeItem() {
		return gradeItem;
	}

	public void setGradeItem(GradeItem gradeItem) {
		this.gradeItem = gradeItem;
	}

	public Motivo getMotivo() {
		return motivo;
	}

	public void setMotivo(Motivo motivo) {
		this.motivo = motivo;
	}

	public String getDataEntregaString() {
		return dataEntregaString;
	}

	public void setDataEntregaString(String dataEntregaString) {
		this.dataEntregaString = dataEntregaString;
	}

	public Character getInclusaoManual() {
		return inclusaoManual;
	}

	public void setInclusaoManual(Character inclusaoManual) {
		this.inclusaoManual = inclusaoManual;
	}

	public boolean isDevolver() {
		return devolver;
	}

	public void setDevolver(boolean devolver) {
		this.devolver = devolver;
	}

	public List<SelectVO> getGradeItens() {
		return gradeItens;
	}

	public void setGradeItens(List<SelectVO> gradeItens) {
		this.gradeItens = gradeItens;
	}

	public Character getExcluido() {
		return excluido;
	}

	public void setExcluido(Character excluido) {
		this.excluido = excluido;
	}
	
	
	public Long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public boolean isSubstituir() {
		return substituir;
	}

	public void setSubstituir(boolean substituir) {
		this.substituir = substituir;
	}

	public String getDataPrevistaEntregaString() {
		return dataPrevistaEntregaString;
	}

	public void setDataPrevistaEntregaString(String dataPrevistaEntregaString) {
		this.dataPrevistaEntregaString = dataPrevistaEntregaString;
	}

	@Override
	public String getHash(boolean inclusao) throws Exception {
		return gerarHash(inclusao, getChecksum(), getIdGradeItem(), getQuantidade(), getDataPrevistaEntrega(), getDataEntrega(), getDataPrevistaTroca(), getDataConfirmacao(), getDataSubstituicao(), getDataDevolucao(), getIdMotivo(), getTipoConfirmacao(), getInclusaoManual(), getExcluido());
	}
}