package br.com.dh.dexSystem.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "grade")	
public class Grade extends SincronizacaoAbstract implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Sincronizar
	@Column(name = "id")
	private Long id;
	
	@Sincronizar
	@Column(name = "nome")
	private String nome;	
	
	@Sincronizar
	@Column(name = "excluido")
	private Character excluido;
	
	@Transient
	private String itensToString;
	
	
	public Grade() {
		super();
		this.excluido = '0';
		this.itensToString = "";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getItensToString() {
		return itensToString;
	}

	public void setItensToString(String itensToString) {
		this.itensToString = itensToString;
	}

	public Character getExcluido() {
		return excluido;
	}

	public void setExcluido(Character excluido) {
		this.excluido = excluido;
	}
	
	@Override
	public String getHash(boolean inclusao) throws Exception {
		return gerarHash(inclusao, getChecksum(), getNome(), getExcluido());
	}
}