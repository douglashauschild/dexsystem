package br.com.dh.dexSystem.model.constant;


public enum SexoEnum {
	MASCULINO('M', "Masculino"),
	FEMININO('F', "Feminino");

	private Character key;
	private String label;

	private SexoEnum (Character key, String label) {
		this.key = key;
		this.label = label;
	}

	public Character getKey() {
		return key;
	}

	public void setKey(Character key) {
		this.key = key;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public static SexoEnum getByKey(Character key) throws EnumConstantNotPresentException {
		for (SexoEnum tipo : SexoEnum.values()) {
			if (tipo.getKey().equals(key)) {
				return tipo;
			}
		}
		throw new EnumConstantNotPresentException(SexoEnum.class, "Chave " + key + " não encontrada");
	}
}