package br.com.dh.dexSystem.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "controle_sincronizacao")	
public class ControleSincronizacao extends SincronizacaoAbstract implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "dispositivo_id")
	private Long idDispositivo;
	
	@Column(name = "tabela")
	private String tabela;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_hora")
	private Date dataHora;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "dispositivo_id", insertable = false, updatable = false)
    private Dispositivo dispositivo;
	
	public ControleSincronizacao() {
		super();
	}
	
	public ControleSincronizacao(Long idDispositivo, String tabela, String checksum, String checksumAlteracao) {
		super(checksum, checksumAlteracao);
		this.idDispositivo = idDispositivo;
		this.tabela = tabela;
		this.dataHora = new Date();
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdDispositivo() {
		return idDispositivo;
	}

	public void setIdDispositivo(Long idDispositivo) {
		this.idDispositivo = idDispositivo;
	}

	public String getTabela() {
		return tabela;
	}

	public void setTabela(String tabela) {
		this.tabela = tabela;
	}

	public Date getDataHora() {
		return dataHora;
	}

	public void setDataHora(Date dataHora) {
		this.dataHora = dataHora;
	}

	public Dispositivo getDispositivo() {
		return dispositivo;
	}

	public void setDispositivo(Dispositivo dispositivo) {
		this.dispositivo = dispositivo;
	}
	
	@Override
	public String getHash(boolean inclusao) throws Exception {
		return null;
	}
}