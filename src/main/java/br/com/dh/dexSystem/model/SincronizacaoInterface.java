package br.com.dh.dexSystem.model;

public interface SincronizacaoInterface {

	public String getHash(boolean inclusao) throws Exception;
}