package br.com.dh.dexSystem.model.vo;

import java.util.List;


public class GraficoVO {

	private List<String> meses;
	private String json;
	private String ano;
	
	public List<String> getMeses() {
		return meses;
	}
	
	public void setMeses(List<String> meses) {
		this.meses = meses;
	}
	
	public String getJson() {
		return json;
	}

	public void setJson(String json) {
		this.json = json;
	}

	public String getAno() {
		return ano;
	}

	public void setAno(String ano) {
		this.ano = ano;
	}
}