package br.com.dh.dexSystem.model.constant;


public enum TipoEmpresaEnum {
	MATRIZ('M', "Matriz"),
	FILIAL('F', "Filial");

	
	private Character key;
	private String descricao;

	private TipoEmpresaEnum (Character key, String descricao) {
		this.key = key;
		this.descricao = descricao;
	}
	
	public Character getKey() {
		return key;
	}

	public void setKey(Character key) {
		this.key = key;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}


	public static TipoEmpresaEnum getByKey(Character key) throws EnumConstantNotPresentException {
		for (TipoEmpresaEnum permissao : TipoEmpresaEnum.values()) {
			if (permissao.getKey().equals(key)) {
				return permissao;
			}
		}
		throw new EnumConstantNotPresentException(TipoEmpresaEnum.class, "Chave " + key + " não encontrada");
	}
	
	public static TipoEmpresaEnum getByDescricao(String descricao) throws EnumConstantNotPresentException {
		for (TipoEmpresaEnum permissao : TipoEmpresaEnum.values()) {
			if (permissao.getDescricao().equals(descricao)) {
				return permissao;
			}
		}
		throw new EnumConstantNotPresentException(TipoEmpresaEnum.class, "Tipo de empresa " + descricao + " não encontrada");
	}
}