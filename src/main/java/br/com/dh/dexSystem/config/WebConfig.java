package br.com.dh.dexSystem.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import br.com.dh.dexSystem.security.RouteInterceptor;


@EnableWebMvc
@Configuration
public class WebConfig extends WebMvcConfigurerAdapter {
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {     
		registry.addResourceHandler("/**").addResourceLocations("classpath:/static/");
	}
	
	@Override
    public void addInterceptors (InterceptorRegistry registry) {
        registry.addInterceptor(new RouteInterceptor());
    }
}