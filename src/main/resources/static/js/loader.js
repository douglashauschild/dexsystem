function ajaxStart() {
	$('html, body').css({overflow:'hidden', height:'100%'});
	$('#page_loader').addClass('page_loader');
	altair_helpers.content_preloader_show();
};

function ajaxStop() {
	$('html, body').css({overflow:'auto', height:'auto'});
	$('#page_loader').removeClass('page_loader');
	altair_helpers.content_preloader_hide();
};