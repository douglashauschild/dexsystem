$(function() {
	activeMenu();
	formatarCampoCep();
	formatarCampoCnpj();
	formatarCampoCpf();
});

$(".login_eye_pass").mousedown(function() {
	$('#senha').attr("type", "text");
});
$(".login_eye_pass").mouseup(function() {
	$('#senha').attr("type", "password");
});
$(".login_eye_pass").mouseout(function() {
	$('#senha').attr("type", "password");
});

$('#pesquisar').keypress(function(e) {
	var key = e.keyCode ? e.keyCode : e.which;
	if (key == 13) {
		e.preventDefault();
		$('#btnSearch').click();
	}
});

function activeMenu() {
	var url = window.location;
	url = url.toString().split("/");
	url[4] = url[4].split("?")[0];
	url = url[0] + "/" + url[1] + "/" + url[2] + "/" + url[3] + "/" + url[4];

	$('.menu_section a').parent('li').removeClass('act_item current_section active');
	$('.menu_section a').parent('li').removeClass('act_item current_section menu_active');
	$('.menu_section a').filter(function() {
		return this.href == url;
	}).parent('li').addClass('act_item current_section menu_active').parents('ul').css("display", "block").parents('li').slideDown().addClass('act_section current_section active');
}

function abrirCadastroModal(idModal) {
	$('#'+idModal).on('show.uk.modal', function(event) {
		var form = $('#'+idModal).find('form');
		form.attr('action', 'cadastrar');
		
		limparForm(form);
	});
	UIkit.modal('#'+idModal, {bgclose: false, modal: false, keyboard: false }).show();
}

function limparForm(form) {
	form.each(function(){
		this.reset();
	});
	form.parsley().reset();
	form.find('label').parent().removeClass('md-input-filled');
	form.find('label').parent().removeClass('md-input-wrapper-success');
	$('#msgErro').find("div").remove();
}

function abrirModal(idModal) {
	UIkit.modal('#'+idModal, {bgclose: false, modal: false, keyboard: false }).show();
}

function fecharModal(idModal) {
	UIkit.modal('#'+idModal).hide();
}

function excluir(url) {
	$('#confirmaExclusaoModal').on('show.uk.modal', function(event) {
		var modal = $(this);
		var form = modal.find('form');
		form.attr('action', url);
	});
	UIkit.modal("#confirmaExclusaoModal").show();
}

function limparStorage() {
	localStorage.removeItem('DEX_MENU');
}

function formatarCampoCep() {
	var elemento = $('.input-cep');
	elemento.mask('00000-000', {clearIfNotMatch: true});
	elemento.focus(function() {
		elemento.attr('placeholder', '_____-___');
	});
	elemento.blur(function() {
		elemento.removeAttr('placeholder');
	});
}

function formatarCampoCnpj() {
	var elemento = $('.input_cnpj');
	elemento.mask('00.000.000/0000-00', {reverse: true, clearIfNotMatch: true});
	elemento.focus(function() {
		elemento.attr('placeholder', '__.___.___/____-__');
	});
	elemento.blur(function() {
		elemento.removeAttr('placeholder');
	});
}

function formatarCampoCpf() {
	var elemento = $('.input_cpf');
	elemento.mask('000.000.000-00', {reverse: true, clearIfNotMatch: true});
	elemento.focus(function() {
		elemento.attr('placeholder', '___.___.___-__');
	});
	elemento.blur(function() {
		elemento.removeAttr('placeholder');
	});
}

function formatarCamposData(elemento) {
	UIkit.datepicker(elemento, {
		format:'DD/MM/YYYY',
		i18n: getI18nDatePicker()
	});
	elemento.mask('00/00/0000', {clearIfNotMatch: true});
	elemento.focus(function() {
		elemento.attr('placeholder', '__/__/____');
	});
	elemento.blur(function() {
		elemento.removeAttr('placeholder');
	});
}

function getI18nDatePicker() {
	return { months:['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'], weekdays:['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex','Sáb'] };
}

function formatarCampoTelefone(elemento) {
	var maskTel = function (val) {
		return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
	},
	telOptions = {
		onKeyPress: function(val, e, field, options) {
			field.mask(maskTel.apply({}, arguments), options);
	    },
	    clearIfNotMatch: true
	};
	elemento.mask(maskTel, telOptions);
}

function somenteNumero(evt) {
	var theEvent = evt || window.event;
	var key = theEvent.keyCode || theEvent.which;
	key = String.fromCharCode( key );
	var regex = /^[\d-,.\/\\]+$/;
	if (!regex.test(key)) {
		theEvent.returnValue = false;
		if (theEvent.preventDefault) {
			theEvent.preventDefault();
		}
	}
}