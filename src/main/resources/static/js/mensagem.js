function mensagemSucesso(msg) {
	mensagem('success', 'Sucesso', msg);
}

function mensagemAtencao(msg) {
	mensagem('danger', 'Atenção', msg);
}

function mensagemErro(msg) {
	mensagem('danger', 'Erro', msg);
}

function mensagem(tipo, title, msg) {
	UIkit.notify({
	    message : '<html><body><div style="font-size:18px;font-weight:bold;padding-bottom:5px"><i class="material-icons" style="float:right;color:white;">close</i>'+title+'!</div>'+msg+'</body></html>',
	    status  : tipo,
	    timeout : 6000,
	    pos     : 'top-right'
	});
}