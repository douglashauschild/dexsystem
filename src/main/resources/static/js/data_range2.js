$(function() {
	iniciarValidacao2();
});

function iniciarValidacao2() {
	var $dateFrom2 = $('.date-from2'), $dateTo2 = $('.date-to2');

	UIkit.datepicker($dateFrom2, { 
		format: 'DD/MM/YYYY', 
		i18n: getI18nDatePicker()
	});
	$dateFrom2.mask("00/00/0000");
	$dateFrom2.focus(function() {
		$dateFrom2.attr('placeholder', '__/__/____');
	});
	$dateFrom2.blur(function() {
		$dateFrom2.removeAttr('placeholder');
	});

	$dateFrom2.on('change', function() {
		var datepickerTo = UIkit.datepicker($dateTo2, {
			format: 'DD/MM/YYYY',
			i18n: getI18nDatePicker(),
			minDate: $(this).val()
		});
	});
	$dateTo2.mask("00/00/0000");
	$dateTo2.focus(function() {
		$dateTo2.attr('placeholder', '__/__/____');
	});
	$dateTo2.blur(function() {
		$dateTo2.removeAttr('placeholder');
	});
}
