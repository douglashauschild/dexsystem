$(function() {
	popularEmpresas(null);
	buscarTotais(null);
	buscarGraficoEntregas(null);
	buscarGraficoSituacaoEpis();
	buscarGraficoEpisUtilizados();
	buscarStatusEntrega(null);
});

$('#empresaCombo').change(function(e) {
	var idEmpresa = $('#empresaCombo').val();
	buscarTotais(idEmpresa);
	buscarStatusEntrega(idEmpresa);
	buscarGraficoEntregas(idEmpresa);
});

function buscarTotais(idEmpresa) {
	var resposta = $.ajax({
		url: '/dexSystem/dashboard/buscarTotais',
		method: 'GET',
		data: { 'idEmpresa': idEmpresa },
		contentType: 'application/json'
	});
	resposta.done(carregarTotais.bind(this));
}
function carregarTotais(totais) {
	$("#total_empregados").html(totais.empregados);
	$("#total_epis").html(totais.epis);
	$("#total_entregas").html(totais.entregas);
	$("#total_epis_entregues").html(totais.episEntregues);
}

function buscarGraficoSituacaoEpis() {
	var resposta = $.ajax({
		url: '/dexSystem/dashboard/situacaoEpis',
		method: 'GET',
		contentType: 'application/json'
	});
	resposta.done(carregarGraficoSituacaoEpis.bind(this));
}
function carregarGraficoSituacaoEpis(situacao) {
	var json = $.parseJSON(situacao);
	var chart_valor = c3.generate({
		data: {
	        columns: [
	            ['CA em dia', json.emDia],
	            ['CA vencido', json.caVencido],
	            ['Sem CA', json.semCa],
	        ],
	        type : 'donut'
	    },
	    color: {
	        pattern: ['#629b58', '#e53935', '#2196f3']
	    },
	    donut: {
	        label: {
	            format: function (value, ratio, id) {
	                return value;
	            }
	        }
	    },
	    size: {
			height: 350,
		},
        bindto: "#chart_situacao_epis",
    });
}


function buscarGraficoEpisUtilizados() {
	var resposta = $.ajax({
		url: '/dexSystem/dashboard/episUtilizados',
		method: 'GET',
		contentType: 'application/json'
	});
	resposta.done(carregarGraficoEpisUtilizados.bind(this));
}
function carregarGraficoEpisUtilizados(epis) {
	var chart_valor = c3.generate({
		data: {
			type: 'bar',
	        json: $.parseJSON(epis),
	        keys: {
	        	x: 'Nome',
	        	value: ['Quantidade'],
	        }
		},
		size: {
			height: 350,
		},
		axis: {
			x: {
	            type: 'category',
	            height: 75
	        }
	    },
	    bar: {
	        width: {
	            ratio: 0.5
	        }
	    },
        bindto: "#chart_epis_utilizados",
    });
}



function buscarStatusEntrega(idEmpresa) {
	var resposta = $.ajax({
		url: '/dexSystem/dashboard/statusEntregas',
		method: 'GET',
		data: { 'idEmpresa': idEmpresa },
		contentType: 'application/json'
	});
	resposta.done(carregarStatusEntrega.bind(this));
}
function carregarStatusEntrega(status) {
	var json = $.parseJSON(status);
	var li = '';
	for (i in json.status) {
		li = li + '<li>' +
			'<div class="md-list-content">' +
				'<span class="md-list-heading">'+json.status[i].nome+'</span>' +
				'<div class="uk-progress uk-progress-mini uk-progress-'+json.status[i].cor+' uk-margin-remove">' +
					'<div class="uk-progress-bar" style="width: '+json.status[i].porcentagem+'%;"></div>' +
				'</div>' +
				'<span class="uk-text-small uk-text-muted dashboard-text-status-entrega">'+json.status[i].total+'/'+json.total+'</span>' +
			'</div>' +
		'</li>';
	}
	var status = '' +
		'<ul class="md-list md-list-addon dashboard-list-status-entrega">' +
			li +
		'</ul>';
	$('#status_entregas').html(status);
}

function buscarGraficoEntregas(idEmpresa) {
	var resposta = $.ajax({
		url: '/dexSystem/dashboard/buscarEntregas',
		method: 'GET',
		data: { 'idEmpresa': idEmpresa },
		contentType: 'application/json'
	});
	resposta.done(carregarGraficoEntregas.bind(this));
}
function carregarGraficoEntregas(grafico) {
	var chart_valor = c3.generate({
		data: {
	        json: $.parseJSON(grafico.json)
	    },
	    axis: {
	        x: {
	            type: 'category',
	            categories: grafico.meses
	        }
	    },
        bindto: "#chart_entregas",
    });
	$("#chart_entregas_label").html("Total de entregas realizadas x Total de EPIs entregues em "+grafico.ano);
}
