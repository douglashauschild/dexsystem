function popularEmpresas(idEmpresa) {
	var combo = $("#empresaCombo");
	$.ajax({
	     type: 'GET',
	     url: '/dexSystem/empresa/buscar',
	     data: { 
	    	 	'filtro': combo.hasClass('list'),
	    	 	'idEmpresa': idEmpresa,
	     		},
	     async: false,
	     success: function(response) {
	    	combo.find('option').remove();
	    	combo.append(new Option(combo.hasClass('list') ? '-- Todas --' : '-- Selecione --', combo.hasClass('list') ? null : 0));
			$.each(response, function() {
				combo.append(new Option(this.label, this.key));
			});
			
			if (idEmpresa != null) {
				combo.val(idEmpresa);
			}
	     }
	});
}