function popularCargos(idCargo) {
	var combo = $("#cargoCombo");
	var idSetor =  $('#setorCombo').val();
	if (idSetor === null || idSetor === 'null' || idSetor === "" || idSetor === '0') {
		combo.find('option').remove();
		combo.append(new Option('-- Selecione primeiro um setor --', 0));
	} else {
		$.ajax({
		     type: 'GET',
		     url: '/dexSystem/cargo/buscar',
		     data: { 
		    	 	'filtro': combo.hasClass('list'),
		    	 	'idSetor': idSetor != undefined ? idSetor : null,
		    	 	'idCargo': idCargo != undefined ? idCargo : null
		    	 	},
		     async: false,
		     success: function(response) {
		    	combo.find('option').remove();
		    	combo.append(new Option(combo.hasClass('list') ? '-- Todos --' : '-- Selecione --', combo.hasClass('list') ? null : 0));
				$.each(response, function() {
					combo.append(new Option(this.label, this.key));
				});
				
				if (idCargo != null) {
					combo.val(idCargo);
				}
		     }
		});
	}
}