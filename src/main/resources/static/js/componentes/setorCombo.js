function popularSetores(idSetor) {
	var combo = $("#setorCombo");
	var idEmpresa =  $('#empresaCombo').val();
	if (idEmpresa === null || idEmpresa === 'null' || idEmpresa === "" || idEmpresa === '0') {
		combo.find('option').remove();
		combo.append(new Option('-- Selecione primeiro uma empresa --', 0));
	} else {
		$.ajax({
		     type: 'GET',
		     url: '/dexSystem/setor/buscar',
		     data: { 
		    	 	'filtro': combo.hasClass('list'),
		    	 	'idEmpresa': idEmpresa,
		    	 	'idSetor': idSetor != undefined ? idSetor : null
		    	 	},
		     async: false,
		     success: function(response) {
		    	combo.find('option').remove();
		    	combo.append(new Option(combo.hasClass('list') ? '-- Todos --' : '-- Selecione --', combo.hasClass('list') ? null : 0));
				$.each(response, function() {
					combo.append(new Option(this.label, this.key));
				});
				
				if (idSetor != null) {
					combo.val(idSetor);
				}
		     }
		});
	}
}