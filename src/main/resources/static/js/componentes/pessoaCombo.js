function popularPessoas(idPessoa) {
	var combo = $("#pessoaFisicaCombo");
	$.ajax({
	     type: 'GET',
	     url: '/dexSystem/pessoaFisica/buscar',
	     data: { 
	    	 	'filtro': combo.hasClass('list')
	     		},
	     async: false,
	     success: function(response) {
	    	combo.find('option').remove();
	    	combo.append(new Option(combo.hasClass('list') ? '-- Todas --' : '-- Selecione --', combo.hasClass('list') ? null : 0));
			$.each(response, function() {
				combo.append(new Option(this.label, this.key));
			});
			
			if (idPessoa != null) {
				combo.val(idPessoa);
			}
	     }
	});
}