function validate_cnpj(val, msk) {
    val = val.replace(/[^\d]+/g, '');

    // Elimina CNPJs inválidos conhecidos
    if (val == '' || val.length != 14 || /^(.)\1+$/.test(val))
        return false;

    // Valida DVs
    tamanho = val.length - 2
    numeros = val.substring(0, tamanho);
    digitos = val.substring(tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
        soma += numeros.charAt(tamanho - i) * pos--;
        if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(0))
        return false;

    tamanho = tamanho + 1;
    numeros = val.substring(0, tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
        soma += numeros.charAt(tamanho - i) * pos--;
        if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(1))
        return false;

    return true;
}

function validate_cpf(val, msk) {
    var regex = msk != undefined && msk ? /^\d{3}\.\d{3}\.\d{3}\-\d{2}$/ : /^[0-9]{11}$/;

    if (val.match(regex) != null) {
        //check all same numbers
        if (val.match(/\b(.+).*(\1.*){10,}\b/g) != null)
            return false;

        var strCPF = val.replace(/\D/g, '');
        var sum;
        var rest;
        sum = 0;

        for (i = 1; i <= 9; i++)
            sum = sum + parseInt(strCPF.substring(i - 1, i)) * (11 - i);

        rest = (sum * 10) % 11;

        if ((rest == 10) || (rest == 11))
            rest = 0;

        if (rest != parseInt(strCPF.substring(9, 10)))
            return false;

        sum = 0;
        for (i = 1; i <= 10; i++)
            sum = sum + parseInt(strCPF.substring(i - 1, i)) * (12 - i);

        rest = (sum * 10) % 11;

        if ((rest == 10) || (rest == 11))
            rest = 0;
        if (rest != parseInt(strCPF.substring(10, 11)))
            return false;

        return true;
    }

    return false;
}
