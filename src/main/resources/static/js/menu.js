$(function() {
	carregarMenu();
});

function carregarMenu() {
	var menus = localStorage.getItem("DEX_MENU");
	if (menus != null) {
		montarMenu(JSON.parse(decriptar(menus)));
	} else {
		$.ajax({
			url: '/dexSystem/menu/buscar',
			method: 'GET',
			async: false,
			contentType: 'application/json'
		}).done(function(menus) {
			localStorage.setItem("DEX_MENU", encriptar(JSON.stringify(menus)));
			montarMenu(menus);
		});
	}
}

function montarMenu(menus) { 
	var html = "<div class='menu_section'><ul style='display: block;'>";
	if (menus != null) {
		for (index in menus) {
			var menu = menus[index];
			if (menu.uri == null && menu.subMenus.length != 0) {
				var htmlSub = "";
				for (indexSub in menu.subMenus) {
					htmlSub += montarSubMenu(menu.subMenus[indexSub])
				}
				html += montarItemSubMenu(htmlSub, menu);
			} else {
				html += montarItemMenu(menu)
			}
		}
	}
	html += "</ul></div>";
	$(".menu").html(html);
}

function montarItemMenu(menu) {
	return "<li>" +
				"<a href=/dexSystem"+menu.uri+">" +
					"<span class='menu_icon'>" +
						"<i class='material-icons'>"+menu.icone+"</i>" +
					"</span>" +
					"<span class='menu_title'>"+menu.nome+"</span>" +
				"</a>" +
			"</li>";
}

function montarSubMenu(subMenu) {
	if (subMenu.subMenus.length != 0) {
		var htmlSub = "";
		for (indexSub in subMenu.subMenus) {
			htmlSub += montarSubMenu(subMenu.subMenus[indexSub])
		}
		return montarItemSubMenu(htmlSub, subMenu);
	}
	return "<li><a href=/dexSystem"+subMenu.uri+">"+subMenu.nome+"</a></li>";
}

function montarItemSubMenu(htmlSub, menu) {
	return "<li class='submenu_trigger'>" +
			    "<a href='#'>" +
			    	(menu.icone !== null ? "<span class='menu_icon'><i class='material-icons'>"+menu.icone+"</i></span>" : "") +
			        "<span class='menu_title'>"+menu.nome+"</span>" +
			    "</a>" +
			    "<ul>" + htmlSub + "</ul>" +
			"</li>";
}

function encriptar(menu) {
	return CryptoJS.AES.encrypt(menu, '1234');
}

function decriptar(menuEncrypt) {
	var decrypted = CryptoJS.AES.decrypt(menuEncrypt, '1234');
	return decrypted.toString(CryptoJS.enc.Utf8);
}