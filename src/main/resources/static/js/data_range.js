$(function() {
	iniciarValidacao();
});

function iniciarValidacao() {
	var $dateFrom = $('.date-from'), $dateTo = $('.date-to');

	UIkit.datepicker($dateFrom, { 
		format: 'DD/MM/YYYY', 
		i18n: getI18nDatePicker()
	});
	$dateFrom.mask("00/00/0000");
	$dateFrom.focus(function() {
		$dateFrom.attr('placeholder', '__/__/____');
	});
	$dateFrom.blur(function() {
		$dateFrom.removeAttr('placeholder');
	});

	$dateFrom.on('change', function() {
		var datepickerTo = UIkit.datepicker($dateTo, {
			format: 'DD/MM/YYYY',
			i18n: getI18nDatePicker(),
			minDate: $(this).val()
		});
	});
	$dateTo.mask("00/00/0000");
	$dateTo.focus(function() {
		$dateTo.attr('placeholder', '__/__/____');
	});
	$dateTo.blur(function() {
		$dateTo.removeAttr('placeholder');
	});
}
