$(function() {
	popularEmpresas(idEmpresa);
	popularSetores(idSetor);
	popularCargos(idCargo);
});

$('#empresaCombo').change(function(e) {
	popularSetores();
	popularCargos();
});

$('#setorCombo').change(function(e) {
	popularCargos();
});