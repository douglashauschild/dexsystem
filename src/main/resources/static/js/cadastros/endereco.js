$(function() {
	popularEstados();
	popularCidades(idCidade);
	verificaCampoCep();
});

$('#cep').keypress(function(e) {
	if (e.keyCode == 13) {
		e.preventDefault();
		buscarViaCep();
	}
});

$('#cepSearch').click(function(e) {
	buscarViaCep();
});

function buscarViaCep() {
	var cep = $('#cep').val();
	if (cep != '') {
		ajaxStart();
		$.ajax({
		     type: 'GET',
		     url: '/dexSystem/endereco/consultar/cep',
		     data: { 'cep': cep },
		     success: function(response) {
				if (response != "") {
		    	    if (response.idEstado != "" && response.idEstado != null && response.idCidade != "" && response.idCidade != null) {
			    	    if (response.idEstado != "" && response.idEstado != null) {
							$('#estadoCombo').val(response.idEstado);
			    	    } else {
			    	    	$('#estadoCombo').val(0);
						}
						popularCidades(response.idCidade);
						
						if (response.bairro != "" && response.bairro != null) {
							$('#bairro').val(response.bairro);
							$('#bairro').parent().addClass('md-input-filled');
						} else {
							$('#bairro').val(null);
							$('#bairro').parent().removeClass('md-input-filled');
						}
						if (response.logradouro != "" && response.logradouro != null) {
							$('#endereco').val(response.logradouro);
							$('#endereco').parent().addClass('md-input-filled');
						} else {
							$('#endereco').val(null);
							$('#endereco').parent().removeClass('md-input-filled');	
						}
						if (response.complemento != "" && response.complemento != null) {
							$('#complemento').val(response.complemento);
							$('#complemento').parent().addClass('md-input-filled');
						} else {
							$('#complemento').val(null);
							$('#complemento').parent().removeClass('md-input-filled');
						}
		    	    } else {
						mensagemErro('Não foi possível localizar as informações automaticamente!');					
					}
				} else {
					mensagemErro('Não foi possível localizar as informações automaticamente!');					
				}
				ajaxStop();
		     }
		});
	} else {
		mensagemErro('Informe o CEP!');
	}
}

$('#estadoCombo').change(function(e) {
	popularCidades();
});

function popularEstados() {
	var combo = $("#estadoCombo");
	$.ajax({
	     type: 'GET',
	     url: '/dexSystem/endereco/estados',
	     async: false,
	     success: function(response) {
	    	combo.find('option').remove();
	    	combo.append(new Option('-- Selecione --', 0));
			$.each(response, function() {
				combo.append(new Option(this.label, this.key));
			});
			
			if (idEstado != null) {
				combo.val(idEstado);
			}
	     }
	});
}

function popularCidades(idCidade) {
	var combo = $("#cidadeCombo");
	var idEstado =  $('#estadoCombo').val();
	if (idEstado === '0') {
		combo.find('option').remove();
		combo.append(new Option('-- Selecione primeiro um estado --', 0));
	} else {
		$.ajax({
		     type: 'GET',
		     url: '/dexSystem/endereco/cidades',
		     data: { 'idEstado': $('#estadoCombo').val() },
		     success: function(response) {
		    	combo.find('option').remove();
		    	combo.append(new Option('-- Selecione --', 0));
				$.each(response, function() {
					combo.append(new Option(this.label, this.key));
				});
				
				if (idCidade != null) {
					combo.val(idCidade);
				}
		     }
		});
	}
}

function verificaCampoCep() {
	var cep = $('#cep').val();
	if (cep != null && cep != "") {
		$('#cep').parent().addClass('md-input-filled');
	}
}