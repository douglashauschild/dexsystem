$(function() {
	popularEmpresas(idEmpresa);
	popularEmpregados(idEmpregado);
	buscarInformacoesEmpregado(idEmpregado);
	buscarEstatisticas(idEmpregado);
});

$('#empresaCombo').change(function(e) {
	popularEmpregados();
});

$('#empregadoCombo').change(function(e) {
	$(this).closest('form').submit();
});

$(".confirmaEntregaModal").submit(function(e) {
	var valid = $(this).parsley().isValid();
	if (valid) {
	    e.preventDefault();
	    confirmarEntrega();
	}
});

$('#checkTodos').on('ifChecked', function (event) {
	$('input[type="checkbox"]').prop('checked', true);
	$('.check-linha').parent().addClass('checked');
});

$('#checkTodos').on('ifUnchecked', function (event){
	$('input[type="checkbox"]').prop('checked', false);
	$('.check-linha').parent().removeClass('checked');
});


function popularEmpresas(idEmpresa) {
	var combo = $("#empresaCombo");
	$.ajax({
	     type: 'GET',
	     url: '/dexSystem/empresa/buscar',
	     data: { 
	    	 	'filtro': combo.hasClass('list'),
	    	 	'idEmpresa': idEmpresa,
	     		},
	     async: false,
	     success: function(response) {
	    	combo.find('option').remove();
	    	combo.append(new Option(combo.hasClass('list') ? '-- Todos --' : '-- Selecione --', 0));
			$.each(response, function() {
				combo.append(new Option(this.label, this.key));
			});
			
			if (idEmpresa != null) {
				combo.val(idEmpresa);
			}
	     }
	});
}

function popularEmpregados(idEmpregado) {
	var combo = $("#empregadoCombo");
	var idEmpresa =  $('#empresaCombo').val();
	if (idEmpresa === "" || idEmpresa === '0') {
		combo.find('option').remove();
		combo.append(new Option('-- Selecione primeiro uma empresa --', 0));
		$('#div-content').addClass('no-display')
	} else {
		$.ajax({
		     type: 'GET',
		     url: '/dexSystem/empregado/buscar',
		     data: { 'idEmpresa': idEmpresa },
		     success: function(response) {
		    	combo.find('option').remove();
		    	combo.append(new Option('-- Selecione --', 0));
				$.each(response, function() {
					combo.append(new Option(this.label, this.key));
				});
		
				$('#div-content').addClass('no-display')
				if (idEmpregado != null && idEmpregado != 0) {
					combo.val(idEmpregado);
					$('#div-content').removeClass('no-display')
				} 
		     }
		});
	}
}

function entregar() {
	var cadastrarSenha = $('.uk-sem-senha').hasClass('cadastrar-senha');
	if (cadastrarSenha) {
		mensagemAtencao('Antes de realizar uma entrega e/ou devolução, é necessário cadastrar uma senha para este empregado!');
	} else {
		var rows = "";
		$('#tableEpi tr td input[type="checkbox"]:checked').each(function(){
			rows += $(this).closest('tr td').attr('id') + ',';
		});
		if (rows == '') {
			mensagemAtencao('Selecione os EPIs que você deseja entregar!');
		} else {
			$('#entregaModal').on('show.uk.modal', function(event) {
				$.ajax({
				     type: 'POST',
				     url: '/dexSystem/controleEntrega/entregar',
				     data: { 
				    	 'idEmpresa': $('#empresaCombo').val(),
				    	 'idEmpregado': $('#empregadoCombo').val(),
				    	 'episSelecionados': rows
				     },
				     success: function(response) {
				    	if (response != "") {
				    		$('#entregaModalContent').replaceWith($(response));
						} else {
							mensagemErro('Não foi possível carregar os EPIs para a entrega!');					
						}
				     }
				});
			});
			abrirModal('entregaModal');
		}
	}
}

function validarCheckDevolucao(checkbox) {
	var id = checkbox.id;
	var index = id.replace('checkDevolucao','');
	validarDevolucaoSubstituicao(index, false);
}

function validarCheckSubstituicao(checkbox) {
	var id = checkbox.id;
	var index = id.replace('checkSubstituicao','');
	validarDevolucaoSubstituicao(index, true);
}

function validarDevolucaoSubstituicao(index, substituicao) {
	var checkSubstituicao = $('#checkSubstituicao'+index);
	var checkDevolucao = $('#checkDevolucao'+index);
	
	var substituicaoCheck = checkSubstituicao.is(':checked');
	var devolucaoCheck = checkDevolucao.is(':checked');
	
	if (!devolucaoCheck && !substituicaoCheck) {
		mensagemAtencao('Você deve ter no mínimo uma ação selecionada para este registro!');
		
		if (substituicao) {
			checkSubstituicao.prop('checked', true);
		} else {
			checkDevolucao.prop('checked', true);
		}
	}
	
	var inputQuantidade = $('#quantidade'+index);
	var comboTamanho = $('#tamanhoCombo'+index);
	if (devolucaoCheck && !substituicaoCheck) {
		var qtdOriginal = $('#quantidadeOriginal'+index).val();
		var tamanhoOriginal = $('#tamanhoOriginal'+index).val();
		
		inputQuantidade.val(qtdOriginal);
		comboTamanho.val(tamanhoOriginal);
		
		inputQuantidade.prop('disabled', true);
		comboTamanho.prop('disabled', true);
		$('#spanAcao'+index).html('Ação: Devolução');
	} else {
		inputQuantidade.prop('disabled', false);
		comboTamanho.prop('disabled', false);
		$('#spanAcao'+index).html('Ação: Substituição');
	}
}

function validar() {
	var quantidadeValidada = true;
	var tamanhoValidado = true;
	var motivoValidado = true;
	
	$('.input-quantidade').each(function(){
		if (!$(this).val() || $(this).val() < 1) {
			quantidadeValidada = false;
		}
	});
	$('.select-tamanho').each(function(){
		if ($(this).val() == 0) {
			tamanhoValidado = false;
		}
	});
	$('.select-motivo').each(function(){
		if ($(this).val() == 0) {
			motivoValidado = false;
		}
	});
	if (!quantidadeValidada) {
		mensagemErro('Informe a quantidade, mínimo 1, de todos EPIs desta entrega!');
	} else if (!tamanhoValidado) {
		mensagemErro('Informe o tamanho de todos EPIs desta entrega!');
	} else if (!motivoValidado) {
		mensagemErro('Informe o motivo de todas entregas!');
	} else {
		abrirConfirmacaoEntrega();
	}
}

function abrirConfirmacaoEntrega() {
	$('#confirmaEntregaModal').on('show.uk.modal', function(event) {
		var form = $('#confirmaEntregaModal').find('form');
		limparForm(form);
		
		$('#empregado').parent().addClass('md-input-filled');
		$('#empregado').val(getNomeEmpregado());
		$('#senha').focus();
	});
	abrirModal('confirmaEntregaModal');
}

function getNomeEmpregado() {
	var nome = $("#empregadoCombo option:selected").html();
	if (nome.includes('|')) {
		nome = nome.split('|')[1];
	} 
	return nome.trim();
}

function confirmarEntrega() {
	var idEmpregado = $('#empregadoCombo').val();
	var senha = $('#senha').val();
	$.ajax({
	     type: 'POST',
	     url: '/dexSystem/controleEntrega/entregar/verificarSenha',
	     data: { 
	    	 'idEmpregado': idEmpregado,
	    	 'senha': senha
	     },
	     success: function(response) {
	    	if (response) {
	    		$('#formEntregaModal').submit();
	    	} else {
	    		mensagemErro('Senha incorreta!');
	    	}
	     }
	});
}	

function buscarInformacoesEmpregado(idEmpregado) {
	if (idEmpregado != null && idEmpregado != 0) {
		$.ajax({
		     type: 'POST',
		     url: '/dexSystem/empregado/buscar/informacoes',
		     data: { 
		    	 'idEmpregado': idEmpregado,
		     },
		     success: function(response) {
		    	if (response) {
		    		montarInformacoesEmpregado(response)
		    	} else {
		    		montarInformacoesEmpregado(null);
		    	}
		
		     }
		});
	} else {
		montarInformacoesEmpregado(null);
	}
}

function montarInformacoesEmpregado(empregadoVO) {
	var nome = "Não informado";
	var idade = "Não informado";
	var matricula = "Não informado";
	var setor = "Não informado";
	var cargo = "Não informado";
	
	if (empregadoVO != null) {
		nome = empregadoVO.nome;
		idade = empregadoVO.idade != null && empregadoVO.idade != "" ? empregadoVO.idade : "Não informado";
		matricula = empregadoVO.matricula != null && empregadoVO.matricula != "" ? empregadoVO.matricula : "Não informado";
		setor = empregadoVO.setor != null && empregadoVO.setor != "" ? empregadoVO.setor : "Não informado";
		cargo = empregadoVO.cargo != null && empregadoVO.cargo != "" ? empregadoVO.cargo : "Não informado";
	}
	$('#nomeEmpregado').html(nome);
	$('#idadeEmpregado').html("Idade: " + idade);
	$('#matriculaEmpregado').html("Matrícula: " + matricula);
	$('#setorEmpregado').html("Setor: " + setor);
	$('#cargoEmpregado').html("Cargo: " + cargo);
}

function buscarEstatisticas(idEmpregado) {
	if (idEmpregado != null && idEmpregado != 0) {
		$.ajax({
		     type: 'POST',
		     url: '/dexSystem/controleEntrega/buscar/estatisticas',
		     data: { 
		    	 'idEmpregado': idEmpregado,
		     },
		     success: function(response) {
		    	if (response) {
		    		$('#aguardandoEntrega').html(response.aguardandoEntrega);
		    		$('#entregasVencidas').html(response.entregasVencidas);
		    		$('#epiDia').html(response.epiDia);
		    		$('#epiVencido').html(response.epiVencido);
		    	} 
		     }
		});
	}
}

function visualizarDetalhes(idEmpregadoEpi) {
	if (idEmpregadoEpi != null && idEmpregadoEpi != 0) {
		$('#detalhesEntregaModal').on('show.uk.modal', function(event) {
			$.ajax({
			     type: 'POST',
			     url: '/dexSystem/controleEntrega/buscar/detalhes',
			     data: { 
			    	 'idEmpregadoEpi': idEmpregadoEpi,
			     },
			     success: function(response) {
			    	if (response != "") {
			    		$('.detalhe_epi').html(response.epi);
			    		$('.detalhe_qtd').html(response.quantidade);
			    		$('.detalhe_prev_entrega').html(response.previsaoEntrega);
			    		
			    		if (isVazio(response.ca)) {
			    			$('.detalhe_ca').closest("li").addClass('no-display')
			    		} else {
			    			$('.detalhe_ca').closest("li").removeClass('no-display');
			    			$('.detalhe_ca').html(response.ca);
			    		}
			    		if (isVazio(response.grade)) {
				    		$('.detalhe_grade').closest("li").addClass('no-display');
			    		} else {
			    			$('.detalhe_grade').closest("li").removeClass('no-display');
				    		$('.detalhe_grade').html(response.grade);
			    		}
			    		if (isVazio(response.tamanho)) {
				    		$('.detalhe_tamanho').closest("li").addClass('no-display');
			    		} else {
			    			$('.detalhe_tamanho').closest("li").removeClass('no-display');
				    		$('.detalhe_tamanho').html(response.tamanho);
			    		}
			    		if (isVazio(response.entrega)) {
				    		$('.detalhe_entrega').closest("li").addClass('no-display');
			    		} else {
			    			$('.detalhe_entrega').closest("li").removeClass('no-display');
				    		$('.detalhe_entrega').html(response.entrega);
			    		}
			    		if (isVazio(response.dataConfirmacao)) {
				    		$('.detalhe_confirmacao').closest("li").addClass('no-display');
			    		} else {
			    			$('.detalhe_confirmacao').closest("li").removeClass('no-display');
				    		$('.detalhe_confirmacao').html(response.dataConfirmacao);
			    		}
			    		if (isVazio(response.tipoConfirmacao)) {
				    		$('.detalhe_tipo_conf').closest("li").addClass('no-display');
			    		} else {
			    			$('.detalhe_tipo_conf').closest("li").removeClass('no-display');
				    		$('.detalhe_tipo_conf').html(response.tipoConfirmacao);
			    		}
			    		if (isVazio(response.previsaoTroca)) {
				    		$('.detalhe_prev_troca').closest("li").addClass('no-display');
			    		} else {
			    			$('.detalhe_prev_troca').closest("li").removeClass('no-display');
				    		$('.detalhe_prev_troca').html(response.previsaoTroca);
			    		}
			    		if (isVazio(response.dataSubstituicao)) {
				    		$('.detalhe_substituicao').closest("li").addClass('no-display');
			    		} else {
			    			$('.detalhe_substituicao').closest("li").removeClass('no-display');
				    		$('.detalhe_substituicao').html(response.dataSubstituicao);
			    		}
			    		if (isVazio(response.dataDevolucao)) {
				    		$('.detalhe_devolucao').closest("li").addClass('no-display');
			    		} else {
			    			$('.detalhe_devolucao').closest("li").removeClass('no-display');
				    		$('.detalhe_devolucao').html(response.dataDevolucao);
			    		}
					} else {
						mensagemErro('Não foi possível carregar os detalhes desta entrega!');					
					}
			     }
			});
		});
		abrirModal('detalhesEntregaModal');
	} else {
		mensagemErro('Não foi possível carregar os detalhes desta entrega!');		
	}
}

function abrirCadastroSenha() {
	$('#cadastroSenhaModal').on('show.uk.modal', function(event) {
		var form = $('#cadastroSenhaModal').find('form');
		limparForm(form);
		
		$('#cadastroSenha').focus();
	});	
	abrirModal('cadastroSenhaModal');
}

$('#cadastroSenhaModal').find('form').submit(function(e) {
	var valid = $(this).parsley().isValid();
	if (valid) {
	    e.preventDefault();

	    var idEmpregado = $('#empregadoCombo').val();
	    var senha = $('#cadastroSenha').val();
	    var confirmaSenha = $('#cadastroConfirmacaoSenha').val();
	    
	    $.ajax({
		     type: 'POST',
		     url: '/dexSystem/empregado/cadastro/senha',
		     data: { 
		    	 'idEmpregado': idEmpregado,
		    	 'senha': senha,
		    	 'confirmaSenha': confirmaSenha,
		     },
		     statusCode: {
	    	    200: function(response) {
	    	    	fecharModal('cadastroSenhaModal');
	    	    	mensagemSucesso(response);
	    	    	$('.uk-sem-senha').remove();
	    	    },
	    	    403: function(response) {
	    	    	var erro = '<div class="uk-alert uk-alert-danger" style="margin-top:-12px;margin-bottom: 25px;" data-uk-alert="data-uk-alert">' +
	    	    					'<span>'+response.responseText+'</span>' +
	    	    				'</div>';	
	    	    	$('#msgErro').html(erro);
	    	    },
	    	    500: function(response) {
	    	    	mensagemErro(response.responseText);
	    	    },
		     }
		});
	}
});

function isVazio(texto) {
	return texto == null || texto == "";
}