$(function() {
	popularEmpresas(idEmpresa);
	popularEmpregados(idEmpregado)
	popularSetores(idSetor);
	popularCargos(idCargo);
	verificarObservacao();
});

$('#empresaCombo').change(function(e) {
	popularEmpregados();
	popularSetores();
	popularCargos();
	verificarObservacao();
});

$('#empregadoCombo').change(function(e) {
	verificarObservacao();
});

$('#setorCombo').change(function(e) {
	popularCargos();
	verificarObservacao();
});

$('#cargoCombo').change(function(e) {
	verificarObservacao();
});

$('.btn-gerar').click(function (e) {
    e.preventDefault();

    $.ajax({
    	type: 'GET',
	    url: '/dexSystem/fichaEpi/gerar',
	    data: getData(),
	    ajax: false,
        success: function (link) {
        	window.location.href = 'http://'+window.location.host+'/dexSystem/download/zip/'+link;
        }
    });
});

function verificarObservacao() {
	$.ajax({
	     type: 'GET',
	     url: '/dexSystem/fichaEpi/observacao',
	     data: getData(),
	     success: function(response) {
	    	 $('.ficha-epi-obs').html(response);
	     }
	});
}

function getData() {
	var idEmpresa = ajustarNulo($('#empresaCombo').val());
	var idEmpregado = ajustarNulo($('#empregadoCombo').val());
	var idSetor = ajustarNulo($('#setorCombo').val());
	var idCargo = ajustarNulo($('#cargoCombo').val());
	
	var data = { 
	    	 'idEmpresa': idEmpresa,
	    	 'idEmpregado': idEmpregado,
	    	 'idSetor': idSetor,
	    	 'idCargo': idCargo
     };
	return data;
}

function ajustarNulo(valor) {
	return valor == "null" ? 0 : valor;
}
