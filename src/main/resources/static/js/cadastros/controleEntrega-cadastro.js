$(function() {
	popularGrade();
	popularTamanhos(idGradeItem);
});

$('#epiCombo').change(function(e) {
	popularGrade();
});

function popularGrade() {
	var idEpi =  $('#epiCombo').val();
	if (idEpi === "" || idEpi === '0') {
		$('.uk-epi').removeClass('uk-width-medium-4-6');
		$('.uk-epi').addClass('uk-width-medium-1-1');
		$('.uk-grade').addClass('no-display');
		$('.uk-grade-tamanho').addClass('no-display');
	} else {
		$.ajax({
		     type: 'GET',
		     url: '/dexSystem/epi/buscarPorId',
		     data: { 'idEpi': idEpi },
		     success: function(response) {
		    	 if (response != null && response.idGrade != null) {
		    		 
		    		 $('.uk-epi').addClass('uk-width-medium-4-6');
		    		 $('.uk-epi').removeClass('uk-width-medium-1-1');
		    		 $('.uk-grade').removeClass('no-display');
		    		 $('.uk-grade-tamanho').removeClass('no-display');
		    		 
		    		 $('#grade').val(response.grade.nome);
		    		 $('#grade').parent().addClass('md-input-filled md-input-wrapper-success');
		    		 $('#grade').addClass('md-input-success');
		    		 
		    		 popularTamanhos(response.idGrade);
		    	 }
		     }
		});
	}
}

function popularTamanhos(idGrade, idGradeItem) {
	if (idGrade != null) {
		var combo = $("#tamanhoCombo");
		$.ajax({
		     type: 'GET',
		     url: '/dexSystem/grade/buscarTamanhos',
		     data: { 
		    	 	'idGrade': idGrade,
		     		},
		     async: false,
		     success: function(response) {
				$.each(response, function() {
					combo.append(new Option(this.label, this.key));
				});
				
				if (idGradeItem != null) {
					combo.val(idGradeItem);
				}
		     }
		});
	}
}
