$(function() {
	popularEmpresas(idEmpresa);
	popularEmpregados(idEmpregado)
	popularSetores(idSetor);
	popularCargos(idCargo);
});

$('#empresaCombo').change(function(e) {
	popularEmpregados();
	popularSetores();
	popularCargos();
});

$('#setorCombo').change(function(e) {
	popularCargos();
});