$(function() {
	validarAba();
});

$('#nome').change(function() {
	validarAba();
});

$("#formCadastroItem").submit(function(e) {
	var valid = $(this).parsley().isValid();
	if (valid) {
	    e.preventDefault();

	    var tamanho = $('#tamanho').val();
	    var ordem = $('#ordem').val();
	    
	    var action = $(this).attr('action');
	    if (action === 'cadastrar') {
	    	adicionar(tamanho, ordem);
	    } else {
	    	alterar(tamanho, ordem);
	    }
	}
});

function validarAba() {
	var nome = $('#nome').val();
	if (nome != undefined && nome != "") {
		$('#abaItens').removeClass('uk-disabled');
	} else {
		$('#abaItens').addClass('uk-disabled');
	}
}

function atualizaItens(itens) {
    $('#gradeItens').replaceWith($(itens));
}

function adicionarItem() {
	$('#cadastroItem').on('show.uk.modal', function(event) {
		var form = $('#cadastroItem').find('form');
		form.attr('action', 'cadastrar');
		limparForm(form);
		form.find('label').parent().addClass('md-input-filled');
		
		var ordens = [];
		var itens = grade.itensToString.split(sepItem);
		for (item of itens) {
			if (item.split(sepAcao)[1] != acaoExcluir) {
				ordens.push(item.substring(item.indexOf(sepInfo)+1, item.indexOf(sepAcao)));
			}
		}
		var proxOrdem = Math.max.apply(Math, ordens) + 1;
		$('#ordem').val(proxOrdem);
		$('#tamanho').focus();
	});
	abrirModal('cadastroItem');
}

function alterarItem(item) {
	$('#cadastroItem').on('show.uk.modal', function(event) {
		var form = $('#cadastroItem').find('form');
		form.find('label').parent().addClass('md-input-filled');
		
		$('#msgErro').find("div").remove();
		$('#tamanho').val(item.split(sepInfo)[0]);
		$('#ordem').val((item.split(sepInfo)[1]).split(sepAcao)[0]);
		$('#tamanho').focus();
		
		$('#itemOriginal').val(item);
		
		form.attr('action', 'editar');
	});
	abrirModal('cadastroItem');
}

function excluirItem(item) {
	$('#confirmaExclusaoJSModal').on('show.uk.modal', function(event) {
		$('#btnSim').attr('onClick', 'remover("'+item+'");');
	});
	abrirModal('confirmaExclusaoJSModal');
}

function adicionar(tamanho, ordem) {
	var obj = {
		grade: grade,
		tamanho: tamanho,
		ordem: ordem
	}
	$.ajax({
		type : "POST",
		contentType : "application/json",
		url : window.location.origin + "/dexSystem/grade/item/incluir",
		data : JSON.stringify(obj),
		async: false,
		success : function(result) {
			var result = $(result);
			if (result[0].id === "msgErro") {
				$('#msgErro').replaceWith(result);	
			} else {
				atualizaItens(result);
				fecharModal('cadastroItem');
			    mensagemSucesso('Tamanho salvo com sucesso!');
			}
		},
		error : function() { }
	});
}

function alterar(tamanho, ordem) {
	var obj = {
		grade: grade,
		tamanho: tamanho,
		ordem: ordem,
		itemExclusao: $('#itemOriginal').val()
	}
	$.ajax({
		type : "POST",
		contentType : "application/json",
		url : window.location.origin + "/dexSystem/grade/item/alterar",
		data : JSON.stringify(obj),
		async: false,
		success : function(result) {
			var result = $(result);
			if (result[0].id === "msgErro") {
				$('#msgErro').replaceWith(result);	
			} else {
				atualizaItens(result);
				fecharModal('cadastroItem');
			    mensagemSucesso('Tamanho salvo com sucesso!');
			}
		},
		error : function() { }
	});
}

function remover(item) {
	var obj = {
		grade: grade,
		itemExclusao: item,
	}
	$.ajax({
		type : "POST",
		contentType : "application/json",
		url : window.location.origin + "/dexSystem/grade/item/remover",
		data : JSON.stringify(obj),
		async: false,
		success : function(result) {
			atualizaItens(result);
			fecharModal('confirmaExclusaoJSModal');
		    mensagemSucesso('Tamanho excluído com sucesso!');
		},
		error : function() { }
	});
}