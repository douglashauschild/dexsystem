
$('#tipoCombo').change(function(e) {
	iniciarCamposContato();
});

$("#formCadastroContato").submit(function(e) {
	var valid = $(this).parsley().isValid();
	if (valid) {
	    e.preventDefault();

	    var tipo = $('#tipoCombo').val();
	    var tipoOutro = $('#tipoOutro').val();
	    var contato = $('#contato').val();
	    
	    var action = $(this).attr('action');
	    if (action === 'cadastrar') {
	    	adicionar(tipo, tipoOutro, contato);
	    } else {
	    	alterar(tipo, tipoOutro, contato);
	    }
	}
});

function iniciarCamposContato() {
	$('#cadastroContato').find('form').parsley().reset();
	$('#contato').val(null);
	validarCamposContato();
}

function validarCamposContato() {
	var contatoComponent = $('#contato');
	contatoComponent.unmask();
	contatoComponent.attr('type', 'text');
	
	var tipoOutroComponent = $('#tipoOutro');
	var tipo = $('#tipoCombo').val();
	if (tipo == tipoOutro) {
		$('#tipoOutroDisplay').removeClass('no-display');
		tipoOutroComponent.attr('required', 'required');
		tipoOutroComponent.focus();
	} else {
		$('#tipoOutroDisplay').addClass('no-display');
		tipoOutroComponent.removeAttr('required');
		tipoOutroComponent.val(null);
		
		var contatoComponent = $('#contato');
		if (tipo == tipoTelefone) {
			formatarCampoTelefone(contatoComponent);
		} else if (tipo == tipoEmail) {
			contatoComponent.attr('type', 'email');
		}
		contatoComponent.focus();
	}
}

function carregarTipos() {
	var combo = $("#tipoCombo");
	$.ajax({
	     type: 'GET',
	     async: false,
	     url: '/dexSystem/contato/tipos',
	     success: function(response) {
	    	combo.find('option').remove();
			$.each(response, function() {
				combo.append(new Option(this.label, this.key));
			});
	     }
	});
}

function adicionarContato() {
	$('#cadastroContato').on('show.uk.modal', function(event) {
		var form = $('#cadastroContato').find('form');
		form.attr('action', 'cadastrar');
		limparForm(form);
		form.find('label').parent().addClass('md-input-filled');

		carregarTipos();
		iniciarCamposContato();
		$('#contato').focus();
	});
	abrirModal('cadastroContato');
}

function alterarContato(contato) {
	$('#cadastroContato').on('show.uk.modal', function(event) {
		var form = $('#cadastroContato').find('form');
		form.find('label').parent().addClass('md-input-filled');
		
		$('#msgErro').find("div").remove();
		carregarTipos();
		
		//busca a chave do enum
		var tipo = contato.split(sepInfo)[0];
		$.ajax({
		     type: 'GET',
		     async: false,
		     url: '/dexSystem/contato/tipo',
		     data: { tipo: tipo },
		     success: function(response) {
		    	 $('#tipoCombo').val(response);
		    	 validarCamposContato();
		     }
		});
		var tipoOutro = contato.split(sepInfo)[1];
		if (tipoOutro !== "NULL") {
			$('#tipoOutro').val(tipoOutro);
		}
		$('#contato').val((contato.split(sepInfo)[2]).split(sepAcao)[0]);
		$('#contatoOriginal').val(contato);
		$('#contato').focus();
		form.attr('action', 'editar');
	});
	abrirModal('cadastroContato');
}

function excluirContato(contato) {
	$('#confirmaExclusaoJSModal').on('show.uk.modal', function(event) {
		$('#btnSim').attr('onClick', 'remover("'+contato+'");');
	});
	abrirModal('confirmaExclusaoJSModal');
}

function adicionar(tipo, tipoOutro, contato) {
	 var obj = {
		contatosToString: $('#contatosToString').val(),
		tipo: tipo,
		tipoOutro: tipoOutro,
		contato: contato
	}
	$.ajax({
		type : "POST",
		contentType : "application/json",
		url : window.location.origin + "/dexSystem/contato/incluir",
		data : JSON.stringify(obj),
		async: false,
		success : function(result) {
			var result = $(result);
			if (result[0].id === "msgErro") {
				$('#msgErro').replaceWith(result);	
			} else {
				atualizaContatos(result);
				fecharModal('cadastroContato');
			    mensagemSucesso('Contato salvo com sucesso!');
			}
		},
		error : function() { }
	});
}

function alterar(tipo, tipoOutro, contato) {
	var obj = {
		contatosToString: $('#contatosToString').val(),
		tipo: tipo,
		tipoOutro: tipoOutro,
		contato: contato,
		contatoExclusao: $('#contatoOriginal').val()
	}
	$.ajax({
		type : "POST",
		contentType : "application/json",
		url : window.location.origin + "/dexSystem/contato/alterar",
		data : JSON.stringify(obj),
		async: false,
		success : function(result) {
			var result = $(result);
			if (result[0].id === "msgErro") {
				$('#msgErro').replaceWith(result);	
			} else {
				atualizaContatos(result);
				fecharModal('cadastroContato');
			    mensagemSucesso('Contato salvo com sucesso!');
			}
		},
		error : function() { }
	});
}

function remover(contato) {
	var obj = {
		contatosToString: $('#contatosToString').val(),
		contatoExclusao: contato,
	}
	$.ajax({
		type : "POST",
		contentType : "application/json",
		url : window.location.origin + "/dexSystem/contato/remover",
		data : JSON.stringify(obj),
		async: false,
		success : function(result) {
			atualizaContatos(result);
			fecharModal('confirmaExclusaoJSModal');
		    mensagemSucesso('Contato excluído com sucesso!');
		},
		error : function() { }
	});
}

function atualizaContatos(contatos) {
    $('#contatos').replaceWith($(contatos));
}